<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.7.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="1.254" unitdist="mm" unit="mm" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tPadExt" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bPadExt" color="1" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="t3D" color="6" fill="5" visible="no" active="no"/>
<layer number="58" name="b3D" color="5" fill="4" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="7" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="LOGO_BLAU" color="1" fill="1" visible="yes" active="yes"/>
<layer number="101" name="LOGO_SCHW" color="15" fill="1" visible="yes" active="yes"/>
<layer number="102" name="LOGO_GRAU" color="8" fill="1" visible="yes" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="114" name="Änderung" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="Component" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="117" name="housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="Layout_Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="lotbs" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="148" name="Document_mirrored" color="7" fill="1" visible="no" active="no"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="156" name="gesam-Maß" color="1" fill="7" visible="no" active="no"/>
<layer number="157" name="FaceMchng" color="3" fill="1" visible="no" active="no"/>
<layer number="158" name="FaceMMeas" color="3" fill="1" visible="no" active="no"/>
<layer number="159" name="Geh-Bear2" color="1" fill="7" visible="no" active="no"/>
<layer number="160" name="Topologie" color="9" fill="1" visible="no" active="no"/>
<layer number="161" name="tomplace2" color="7" fill="1" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="4" fill="9" visible="yes" active="yes"/>
<layer number="252" name="BR-BS" color="7" fill="1" visible="no" active="no"/>
<layer number="253" name="BR-LS" color="7" fill="1" visible="no" active="no"/>
<layer number="254" name="Cool" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="ZchnBlatt" color="7" fill="1" visible="no" active="no"/>
</layers>
<schematic>
<libraries>
<library name="HAutomat">
<packages>
<package name="SCHRIFTF">
<wire x1="-46.355" y1="0" x2="-46.355" y2="3.81" width="0.254" layer="48"/>
<wire x1="-46.355" y1="3.81" x2="-46.355" y2="4.445" width="0.254" layer="48"/>
<wire x1="-46.355" y1="4.445" x2="-46.355" y2="6.985" width="0.254" layer="48"/>
<wire x1="-46.355" y1="6.985" x2="-46.355" y2="12.065" width="0.254" layer="48"/>
<wire x1="-46.355" y1="12.065" x2="-46.355" y2="17.145" width="0.254" layer="48"/>
<wire x1="-46.355" y1="17.145" x2="-54.61" y2="17.145" width="0.254" layer="48"/>
<wire x1="-54.61" y1="17.145" x2="-66.675" y2="17.145" width="0.254" layer="48"/>
<wire x1="-66.675" y1="17.145" x2="-79.375" y2="17.145" width="0.254" layer="48"/>
<wire x1="-79.375" y1="17.145" x2="-88.9" y2="17.145" width="0.254" layer="48"/>
<wire x1="-88.9" y1="17.145" x2="-88.9" y2="14.605" width="0.254" layer="48"/>
<wire x1="-88.9" y1="14.605" x2="-88.9" y2="12.065" width="0.254" layer="48"/>
<wire x1="-88.9" y1="12.065" x2="-88.9" y2="9.525" width="0.254" layer="48"/>
<wire x1="-88.9" y1="9.525" x2="-88.9" y2="6.985" width="0.254" layer="48"/>
<wire x1="-88.9" y1="6.985" x2="-88.9" y2="4.445" width="0.254" layer="48"/>
<wire x1="-88.9" y1="17.145" x2="-88.9" y2="19.685" width="0.254" layer="48"/>
<wire x1="-88.9" y1="19.685" x2="-88.9" y2="22.225" width="0.254" layer="48"/>
<wire x1="-88.9" y1="22.225" x2="-88.9" y2="24.765" width="0.254" layer="48"/>
<wire x1="-88.9" y1="24.765" x2="-88.9" y2="27.94" width="0.254" layer="48"/>
<wire x1="-88.9" y1="27.94" x2="-79.375" y2="27.94" width="0.254" layer="48"/>
<wire x1="-79.375" y1="27.94" x2="-66.675" y2="27.94" width="0.254" layer="48"/>
<wire x1="-66.675" y1="27.94" x2="-54.61" y2="27.94" width="0.254" layer="48"/>
<wire x1="-54.61" y1="27.94" x2="-46.355" y2="27.94" width="0.254" layer="48"/>
<wire x1="-46.355" y1="27.94" x2="-46.355" y2="24.765" width="0.254" layer="48"/>
<wire x1="-46.355" y1="24.765" x2="-46.355" y2="22.225" width="0.254" layer="48"/>
<wire x1="-46.355" y1="22.225" x2="-46.355" y2="19.685" width="0.254" layer="48"/>
<wire x1="-46.355" y1="19.685" x2="-46.355" y2="17.145" width="0.254" layer="48"/>
<wire x1="-88.9" y1="19.685" x2="-46.355" y2="19.685" width="0.127" layer="48"/>
<wire x1="-88.9" y1="22.225" x2="-46.355" y2="22.225" width="0.127" layer="48"/>
<wire x1="-88.9" y1="24.765" x2="-46.355" y2="24.765" width="0.127" layer="48"/>
<wire x1="-54.61" y1="27.94" x2="-54.61" y2="17.145" width="0.127" layer="48"/>
<wire x1="-88.9" y1="27.94" x2="-97.79" y2="27.94" width="0.254" layer="48"/>
<wire x1="-97.79" y1="27.94" x2="-109.855" y2="27.94" width="0.254" layer="48"/>
<wire x1="-109.855" y1="27.94" x2="-120.015" y2="27.94" width="0.254" layer="48"/>
<wire x1="-120.015" y1="27.94" x2="-120.015" y2="24.765" width="0.254" layer="48"/>
<wire x1="-120.015" y1="24.765" x2="-120.015" y2="22.225" width="0.254" layer="48"/>
<wire x1="-120.015" y1="22.225" x2="-120.015" y2="19.685" width="0.254" layer="48"/>
<wire x1="-120.015" y1="19.685" x2="-120.015" y2="17.145" width="0.254" layer="48"/>
<wire x1="-120.015" y1="17.145" x2="-120.015" y2="14.605" width="0.254" layer="48"/>
<wire x1="-120.015" y1="14.605" x2="-120.015" y2="12.065" width="0.254" layer="48"/>
<wire x1="-120.015" y1="12.065" x2="-120.015" y2="9.525" width="0.254" layer="48"/>
<wire x1="-120.015" y1="9.525" x2="-120.015" y2="6.985" width="0.254" layer="48"/>
<wire x1="-120.015" y1="6.985" x2="-120.015" y2="4.445" width="0.254" layer="48"/>
<wire x1="-120.015" y1="4.445" x2="-120.015" y2="0" width="0.254" layer="48"/>
<wire x1="-120.015" y1="0" x2="-109.855" y2="0" width="0.254" layer="48"/>
<wire x1="-109.855" y1="0" x2="-97.79" y2="0" width="0.254" layer="48"/>
<wire x1="-97.79" y1="0" x2="-88.9" y2="0" width="0.254" layer="48"/>
<wire x1="-88.9" y1="0" x2="-78.74" y2="0" width="0.254" layer="48"/>
<wire x1="-78.74" y1="0" x2="-70.485" y2="0" width="0.254" layer="48"/>
<wire x1="-70.485" y1="0" x2="-60.96" y2="0" width="0.254" layer="48"/>
<wire x1="-60.96" y1="0" x2="-46.355" y2="0" width="0.254" layer="48"/>
<wire x1="-120.015" y1="24.765" x2="-88.9" y2="24.765" width="0.127" layer="48"/>
<wire x1="-120.015" y1="22.225" x2="-88.9" y2="22.225" width="0.127" layer="48"/>
<wire x1="-120.015" y1="19.685" x2="-88.9" y2="19.685" width="0.127" layer="48"/>
<wire x1="-120.015" y1="17.145" x2="-88.9" y2="17.145" width="0.127" layer="48"/>
<wire x1="-120.015" y1="14.605" x2="-88.9" y2="14.605" width="0.127" layer="48"/>
<wire x1="-120.015" y1="12.065" x2="-88.9" y2="12.065" width="0.127" layer="48"/>
<wire x1="-120.015" y1="9.525" x2="-88.9" y2="9.525" width="0.127" layer="48"/>
<wire x1="-88.9" y1="4.445" x2="-78.74" y2="4.445" width="0.254" layer="48"/>
<wire x1="-78.74" y1="4.445" x2="-70.485" y2="4.445" width="0.254" layer="48"/>
<wire x1="-70.485" y1="4.445" x2="-60.96" y2="4.445" width="0.254" layer="48"/>
<wire x1="-60.96" y1="4.445" x2="-46.355" y2="4.445" width="0.254" layer="48"/>
<wire x1="-97.79" y1="27.94" x2="-97.79" y2="0" width="0.127" layer="48"/>
<wire x1="-66.675" y1="27.94" x2="-66.675" y2="17.145" width="0.127" layer="48"/>
<wire x1="-79.375" y1="17.145" x2="-79.375" y2="27.94" width="0.127" layer="48"/>
<wire x1="-109.855" y1="27.94" x2="-109.855" y2="0" width="0.127" layer="48"/>
<wire x1="-88.9" y1="4.445" x2="-88.9" y2="0" width="0.127" layer="48"/>
<wire x1="-78.74" y1="4.445" x2="-78.74" y2="0" width="0.127" layer="48"/>
<wire x1="-70.485" y1="4.445" x2="-70.485" y2="0" width="0.127" layer="48"/>
<wire x1="-60.96" y1="4.445" x2="-60.96" y2="0" width="0.127" layer="48"/>
<wire x1="-46.355" y1="27.94" x2="0" y2="27.94" width="0.254" layer="48"/>
<wire x1="0" y1="27.94" x2="0" y2="22.225" width="0.254" layer="48"/>
<wire x1="0" y1="22.225" x2="0" y2="17.145" width="0.254" layer="48"/>
<wire x1="0" y1="17.145" x2="0" y2="12.065" width="0.254" layer="48"/>
<wire x1="0" y1="12.065" x2="0" y2="6.985" width="0.254" layer="48"/>
<wire x1="0" y1="6.985" x2="0" y2="0" width="0.254" layer="48"/>
<wire x1="0" y1="0" x2="-12.065" y2="0" width="0.254" layer="48"/>
<wire x1="-12.065" y1="0" x2="-46.355" y2="0" width="0.254" layer="48"/>
<wire x1="-46.355" y1="22.225" x2="0" y2="22.225" width="0.127" layer="48"/>
<wire x1="-46.355" y1="17.145" x2="0" y2="17.145" width="0.127" layer="48"/>
<wire x1="-46.355" y1="12.065" x2="0" y2="12.065" width="0.127" layer="48"/>
<wire x1="-46.355" y1="6.985" x2="-12.065" y2="6.985" width="0.127" layer="48"/>
<wire x1="-12.065" y1="6.985" x2="0" y2="6.985" width="0.127" layer="48"/>
<wire x1="-12.065" y1="6.985" x2="-12.065" y2="3.81" width="0.127" layer="48"/>
<wire x1="-12.065" y1="3.81" x2="-12.065" y2="0" width="0.127" layer="48"/>
<wire x1="-46.355" y1="3.81" x2="-12.065" y2="3.81" width="0.127" layer="48"/>
<wire x1="-120.015" y1="6.985" x2="-88.9" y2="6.985" width="0.127" layer="48"/>
<wire x1="-120.015" y1="4.445" x2="-88.9" y2="4.445" width="0.127" layer="48"/>
<text x="-45.56" y="1.1999" size="1.6764" layer="48">&gt;LAST_DATE_TIME</text>
<text x="-45.56" y="4.3749" size="1.6764" layer="48">&gt;DRAWING_NAME</text>
<text x="-88.265" y="22.86" size="1.6764" layer="48">Bearb.</text>
<text x="-88.265" y="20.32" size="1.6764" layer="48">Gepr.</text>
<text x="-88.265" y="17.78" size="1.6764" layer="48">Norm</text>
<text x="-78.74" y="25.4" size="1.6764" layer="48">Datum</text>
<text x="-66.04" y="25.4" size="1.6764" layer="48">Name</text>
<text x="-53.975" y="25.4" size="1.6764" layer="48">Sign.</text>
<text x="-118.745" y="22.86" size="1.6764" layer="48">stand</text>
<text x="-118.745" y="25.4" size="1.6764" layer="48">Änd.-</text>
<text x="-109.22" y="22.86" size="1.6764" layer="48">Datum</text>
<text x="-97.155" y="22.86" size="1.6764" layer="48">Sign.</text>
<text x="-11.43" y="4.445" size="1.6764" layer="48">Blatt</text>
<text x="-11.43" y="1.27" size="1.6764" layer="48">1/1</text>
<text x="-45.56" y="1.1999" size="1.6764" layer="48">&gt;LAST_DATE_TIME</text>
<text x="-45.56" y="4.3749" size="1.6764" layer="48">&gt;DRAWING_NAME</text>
<text x="-88.265" y="22.86" size="1.6764" layer="48">Bearb.</text>
<text x="-88.265" y="20.32" size="1.6764" layer="48">Gepr.</text>
<text x="-88.265" y="17.78" size="1.6764" layer="48">Norm</text>
<text x="-78.74" y="25.4" size="1.6764" layer="48">Datum</text>
<text x="-66.04" y="25.4" size="1.6764" layer="48">Name</text>
<text x="-53.975" y="25.4" size="1.6764" layer="48">Sign.</text>
<text x="-118.745" y="22.86" size="1.6764" layer="48">stand</text>
<text x="-118.745" y="25.4" size="1.6764" layer="48">Änd.-</text>
<text x="-109.22" y="22.86" size="1.6764" layer="48">Datum</text>
<text x="-97.155" y="22.86" size="1.6764" layer="48">Sign.</text>
<text x="-11.43" y="4.445" size="1.6764" layer="48">Blatt</text>
<text x="-11.43" y="1.27" size="1.6764" layer="48">1/1</text>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-85.09" y="15.7425"/>
<vertex x="-85.09" y="9.74"/>
<vertex x="-83.7075" y="9.74"/>
<vertex x="-83.7075" y="12.225"/>
<vertex x="-81.485" y="12.225"/>
<vertex x="-81.485" y="9.74"/>
<vertex x="-80.155" y="9.74"/>
<vertex x="-80.155" y="15.7425"/>
<vertex x="-81.485" y="15.7425"/>
<vertex x="-81.485" y="13.3275"/>
<vertex x="-83.7075" y="13.3275"/>
<vertex x="-83.7075" y="15.7425"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-78.16" y="15.7425"/>
<vertex x="-77.3725" y="15.7425"/>
<vertex x="-77.3725" y="15.0775"/>
<vertex x="-78.16" y="15.0775"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-74.765" y="15.7425"/>
<vertex x="-73.9425" y="15.7425"/>
<vertex x="-73.9425" y="15.0775"/>
<vertex x="-74.765" y="15.0775"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-79.315" y="9.74"/>
<vertex x="-77.8625" y="9.74"/>
<vertex x="-76.095" y="14.395"/>
<vertex x="-72.6825" y="5.715"/>
<vertex x="-71.335" y="5.715"/>
<vertex x="-75.325" y="15.7425"/>
<vertex x="-76.76" y="15.7425"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-76.9175" y="12.2425"/>
<vertex x="-75.2375" y="12.2425"/>
<vertex x="-74.8175" y="11.1575"/>
<vertex x="-77.3375" y="11.1575"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-67.835" y="15.7425"/>
<vertex x="-67.835" y="14.7275"/>
<vertex x="-69.865" y="14.7275"/>
<vertex x="-70.425" y="14.6225"/>
<vertex x="-70.74" y="14.4825"/>
<vertex x="-70.95" y="14.3075"/>
<vertex x="-71.055" y="14.1325"/>
<vertex x="-71.195" y="13.8525"/>
<vertex x="-71.3" y="13.4675"/>
<vertex x="-71.37" y="13.0125"/>
<vertex x="-71.37" y="12.19"/>
<vertex x="-71.3" y="11.6825"/>
<vertex x="-71.1075" y="11.2625"/>
<vertex x="-70.915" y="11.0175"/>
<vertex x="-70.5825" y="10.8425"/>
<vertex x="-70.285" y="10.79"/>
<vertex x="-69.935" y="10.7725"/>
<vertex x="-67.835" y="10.7725"/>
<vertex x="-67.835" y="9.74"/>
<vertex x="-70.6" y="9.74"/>
<vertex x="-71.09" y="9.8275"/>
<vertex x="-71.5625" y="10.0025"/>
<vertex x="-72.035" y="10.3525"/>
<vertex x="-72.4025" y="10.7725"/>
<vertex x="-72.5775" y="11.14"/>
<vertex x="-72.7525" y="11.6825"/>
<vertex x="-72.805" y="12.0675"/>
<vertex x="-72.8225" y="12.6275"/>
<vertex x="-72.805" y="13.2575"/>
<vertex x="-72.7" y="13.9225"/>
<vertex x="-72.5075" y="14.4125"/>
<vertex x="-72.28" y="14.78"/>
<vertex x="-71.93" y="15.1825"/>
<vertex x="-71.475" y="15.4625"/>
<vertex x="-71.1075" y="15.6025"/>
<vertex x="-70.705" y="15.6725"/>
<vertex x="-70.25" y="15.725"/>
<vertex x="-69.83" y="15.7425"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-66.96" y="15.7425"/>
<vertex x="-66.96" y="9.74"/>
<vertex x="-65.63" y="9.74"/>
<vertex x="-65.63" y="15.7425"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-65.63" y="12.8725"/>
<vertex x="-63.32" y="15.7425"/>
<vertex x="-61.675" y="15.7425"/>
<vertex x="-64.125" y="12.8025"/>
<vertex x="-61.6925" y="9.74"/>
<vertex x="-63.425" y="9.74"/>
<vertex x="-65.63" y="12.645"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-56.6875" y="15.7425"/>
<vertex x="-56.6875" y="14.6575"/>
<vertex x="-58.9275" y="14.6575"/>
<vertex x="-59.2775" y="14.6225"/>
<vertex x="-59.5925" y="14.4825"/>
<vertex x="-59.8025" y="14.325"/>
<vertex x="-59.9425" y="14.1325"/>
<vertex x="-60.1" y="13.835"/>
<vertex x="-60.17" y="13.45"/>
<vertex x="-60.17" y="11.8575"/>
<vertex x="-60.1525" y="11.6825"/>
<vertex x="-59.96" y="11.2625"/>
<vertex x="-59.7675" y="11.0175"/>
<vertex x="-59.435" y="10.8425"/>
<vertex x="-59.1375" y="10.79"/>
<vertex x="-58.7875" y="10.7725"/>
<vertex x="-56.6875" y="10.7725"/>
<vertex x="-56.6875" y="9.74"/>
<vertex x="-59.4525" y="9.74"/>
<vertex x="-59.9425" y="9.8275"/>
<vertex x="-60.415" y="10.0025"/>
<vertex x="-60.8875" y="10.3525"/>
<vertex x="-61.255" y="10.7725"/>
<vertex x="-61.43" y="11.14"/>
<vertex x="-61.605" y="11.6825"/>
<vertex x="-61.6575" y="12.0675"/>
<vertex x="-61.675" y="12.6275"/>
<vertex x="-61.6575" y="13.2575"/>
<vertex x="-61.5525" y="13.9225"/>
<vertex x="-61.36" y="14.4125"/>
<vertex x="-61.1325" y="14.78"/>
<vertex x="-60.7825" y="15.1825"/>
<vertex x="-60.3275" y="15.4625"/>
<vertex x="-59.96" y="15.6025"/>
<vertex x="-59.5575" y="15.6725"/>
<vertex x="-59.1025" y="15.725"/>
<vertex x="-58.6825" y="15.7425"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-60.17" y="12.225"/>
<vertex x="-56.6875" y="12.225"/>
<vertex x="-56.6875" y="13.3275"/>
<vertex x="-60.17" y="13.3275"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-55.655" y="15.7425"/>
<vertex x="-52.295" y="15.7425"/>
<vertex x="-51.665" y="15.6725"/>
<vertex x="-51.245" y="15.55"/>
<vertex x="-50.965" y="15.375"/>
<vertex x="-50.7725" y="15.1825"/>
<vertex x="-50.545" y="14.885"/>
<vertex x="-50.4225" y="14.5175"/>
<vertex x="-50.3525" y="14.15"/>
<vertex x="-50.3525" y="13.7475"/>
<vertex x="-50.4225" y="13.31"/>
<vertex x="-50.615" y="12.8725"/>
<vertex x="-50.8425" y="12.5925"/>
<vertex x="-51.0875" y="12.4175"/>
<vertex x="-51.4025" y="12.26"/>
<vertex x="-51.525" y="12.2075"/>
<vertex x="-50.09" y="9.74"/>
<vertex x="-51.595" y="9.74"/>
<vertex x="-52.89" y="12.05"/>
<vertex x="-52.89" y="13.135"/>
<vertex x="-52.505" y="13.135"/>
<vertex x="-52.12" y="13.24"/>
<vertex x="-51.8225" y="13.415"/>
<vertex x="-51.7175" y="13.6425"/>
<vertex x="-51.7175" y="14.1675"/>
<vertex x="-51.8225" y="14.3775"/>
<vertex x="-51.98" y="14.4825"/>
<vertex x="-52.1375" y="14.5525"/>
<vertex x="-52.4175" y="14.6225"/>
<vertex x="-52.6625" y="14.6575"/>
<vertex x="-54.3075" y="14.6575"/>
<vertex x="-54.3075" y="13.135"/>
<vertex x="-52.8917" y="13.135"/>
<vertex x="-52.8917" y="12.05"/>
<vertex x="-54.3075" y="12.05"/>
<vertex x="-54.3075" y="9.74"/>
<vertex x="-55.655" y="9.74"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-71.16" y="7.9725"/>
<vertex x="-70.495" y="7.9725"/>
<vertex x="-70.495" y="6.415"/>
<vertex x="-70.46" y="6.3625"/>
<vertex x="-70.39" y="6.2925"/>
<vertex x="-70.32" y="6.2575"/>
<vertex x="-69.69" y="6.2575"/>
<vertex x="-69.69" y="7.9725"/>
<vertex x="-69.025" y="7.9725"/>
<vertex x="-69.025" y="5.8025"/>
<vertex x="-69.1125" y="5.715"/>
<vertex x="-70.32" y="5.715"/>
<vertex x="-70.5475" y="5.7325"/>
<vertex x="-70.635" y="5.75"/>
<vertex x="-70.7575" y="5.8025"/>
<vertex x="-70.88" y="5.8725"/>
<vertex x="-71.0375" y="6.03"/>
<vertex x="-71.1075" y="6.1525"/>
<vertex x="-71.1425" y="6.24"/>
<vertex x="-71.16" y="6.31"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-68.7975" y="7.9725"/>
<vertex x="-68.7975" y="7.5525"/>
<vertex x="-68.3775" y="7.5525"/>
<vertex x="-68.3775" y="6.2575"/>
<vertex x="-68.3075" y="6.0825"/>
<vertex x="-68.1675" y="5.9075"/>
<vertex x="-67.9925" y="5.785"/>
<vertex x="-67.7475" y="5.715"/>
<vertex x="-67.1525" y="5.715"/>
<vertex x="-67.065" y="5.785"/>
<vertex x="-67.065" y="6.205"/>
<vertex x="-67.485" y="6.205"/>
<vertex x="-67.5725" y="6.2575"/>
<vertex x="-67.66" y="6.3625"/>
<vertex x="-67.7125" y="6.5025"/>
<vertex x="-67.7125" y="7.5525"/>
<vertex x="-67.065" y="7.5525"/>
<vertex x="-67.065" y="7.9725"/>
<vertex x="-67.7125" y="7.9725"/>
<vertex x="-67.7125" y="8.8125"/>
<vertex x="-68.3775" y="8.8125"/>
<vertex x="-68.3775" y="7.9725"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-66.96" y="6.8858"/>
<vertex x="-66.96" y="6.7125"/>
<vertex x="-66.89" y="6.3975"/>
<vertex x="-66.715" y="6.1"/>
<vertex x="-66.4525" y="5.89"/>
<vertex x="-66.1725" y="5.75"/>
<vertex x="-65.945" y="5.715"/>
<vertex x="-65.49" y="5.715"/>
<vertex x="-65.175" y="5.785"/>
<vertex x="-64.93" y="5.9075"/>
<vertex x="-64.7375" y="6.1"/>
<vertex x="-64.58" y="6.3275"/>
<vertex x="-64.5275" y="6.4973"/>
<vertex x="-64.51" y="6.6075"/>
<vertex x="-64.51" y="7.115"/>
<vertex x="-64.58" y="7.4125"/>
<vertex x="-64.72" y="7.6225"/>
<vertex x="-64.93" y="7.815"/>
<vertex x="-65.175" y="7.9375"/>
<vertex x="-65.49" y="8.025"/>
<vertex x="-65.98" y="8.025"/>
<vertex x="-66.225" y="7.9725"/>
<vertex x="-66.4875" y="7.8325"/>
<vertex x="-66.645" y="7.675"/>
<vertex x="-66.82" y="7.4475"/>
<vertex x="-66.925" y="7.2025"/>
<vertex x="-66.96" y="7.01"/>
<vertex x="-66.96" y="6.8875"/>
<vertex x="-66.295" y="6.8875"/>
<vertex x="-66.295" y="6.9925"/>
<vertex x="-66.2425" y="7.15"/>
<vertex x="-66.1725" y="7.2725"/>
<vertex x="-66.015" y="7.4475"/>
<vertex x="-65.84" y="7.535"/>
<vertex x="-65.6125" y="7.535"/>
<vertex x="-65.5075" y="7.5"/>
<vertex x="-65.35" y="7.395"/>
<vertex x="-65.2275" y="7.2025"/>
<vertex x="-65.175" y="7.0275"/>
<vertex x="-65.175" y="6.695"/>
<vertex x="-65.2275" y="6.5375"/>
<vertex x="-65.28" y="6.4325"/>
<vertex x="-65.4025" y="6.2925"/>
<vertex x="-65.525" y="6.2225"/>
<vertex x="-65.6125" y="6.205"/>
<vertex x="-65.8575" y="6.205"/>
<vertex x="-66.015" y="6.2575"/>
<vertex x="-66.155" y="6.3975"/>
<vertex x="-66.26" y="6.6075"/>
<vertex x="-66.295" y="6.765"/>
<vertex x="-66.295" y="6.8858"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-54.99" y="6.8858"/>
<vertex x="-54.99" y="6.7125"/>
<vertex x="-54.92" y="6.3975"/>
<vertex x="-54.745" y="6.1"/>
<vertex x="-54.4825" y="5.89"/>
<vertex x="-54.2025" y="5.75"/>
<vertex x="-53.975" y="5.715"/>
<vertex x="-53.52" y="5.715"/>
<vertex x="-53.205" y="5.785"/>
<vertex x="-52.96" y="5.9075"/>
<vertex x="-52.7675" y="6.1"/>
<vertex x="-52.61" y="6.3275"/>
<vertex x="-52.5575" y="6.4973"/>
<vertex x="-52.54" y="6.6075"/>
<vertex x="-52.54" y="7.115"/>
<vertex x="-52.61" y="7.4125"/>
<vertex x="-52.75" y="7.6225"/>
<vertex x="-52.96" y="7.815"/>
<vertex x="-53.205" y="7.9375"/>
<vertex x="-53.52" y="8.025"/>
<vertex x="-54.01" y="8.025"/>
<vertex x="-54.255" y="7.9725"/>
<vertex x="-54.5175" y="7.8325"/>
<vertex x="-54.675" y="7.675"/>
<vertex x="-54.85" y="7.4475"/>
<vertex x="-54.955" y="7.2025"/>
<vertex x="-54.99" y="7.01"/>
<vertex x="-54.99" y="6.8875"/>
<vertex x="-54.325" y="6.8875"/>
<vertex x="-54.325" y="6.9925"/>
<vertex x="-54.2725" y="7.15"/>
<vertex x="-54.2025" y="7.2725"/>
<vertex x="-54.045" y="7.4475"/>
<vertex x="-53.87" y="7.535"/>
<vertex x="-53.6425" y="7.535"/>
<vertex x="-53.5375" y="7.5"/>
<vertex x="-53.38" y="7.395"/>
<vertex x="-53.2575" y="7.2025"/>
<vertex x="-53.205" y="7.0275"/>
<vertex x="-53.205" y="6.695"/>
<vertex x="-53.2575" y="6.5375"/>
<vertex x="-53.31" y="6.4325"/>
<vertex x="-53.4325" y="6.2925"/>
<vertex x="-53.555" y="6.2225"/>
<vertex x="-53.6425" y="6.205"/>
<vertex x="-53.8875" y="6.205"/>
<vertex x="-54.045" y="6.2575"/>
<vertex x="-54.185" y="6.3975"/>
<vertex x="-54.29" y="6.6075"/>
<vertex x="-54.325" y="6.765"/>
<vertex x="-54.325" y="6.8858"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-55.935" y="7.9725"/>
<vertex x="-55.935" y="5.715"/>
<vertex x="-55.27" y="5.715"/>
<vertex x="-55.27" y="7.9725"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-55.935" y="8.8125"/>
<vertex x="-55.935" y="8.2525"/>
<vertex x="-55.27" y="8.2525"/>
<vertex x="-55.27" y="8.8125"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-50.1075" y="5.715"/>
<vertex x="-50.7725" y="5.715"/>
<vertex x="-50.7725" y="7.325"/>
<vertex x="-50.8075" y="7.3775"/>
<vertex x="-50.8775" y="7.4475"/>
<vertex x="-50.9475" y="7.4825"/>
<vertex x="-51.5425" y="7.4825"/>
<vertex x="-51.5425" y="5.715"/>
<vertex x="-52.19" y="5.715"/>
<vertex x="-52.19" y="7.9725"/>
<vertex x="-50.6675" y="7.9725"/>
<vertex x="-50.5975" y="7.955"/>
<vertex x="-50.51" y="7.92"/>
<vertex x="-50.37" y="7.85"/>
<vertex x="-50.2825" y="7.7625"/>
<vertex x="-50.195" y="7.64"/>
<vertex x="-50.125" y="7.5"/>
<vertex x="-50.1075" y="7.3775"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-60.975" y="5.715"/>
<vertex x="-61.57" y="5.715"/>
<vertex x="-61.57" y="7.325"/>
<vertex x="-61.605" y="7.3775"/>
<vertex x="-61.675" y="7.4475"/>
<vertex x="-61.745" y="7.4825"/>
<vertex x="-62.2" y="7.4825"/>
<vertex x="-62.2" y="5.785"/>
<vertex x="-62.27" y="5.715"/>
<vertex x="-62.83" y="5.715"/>
<vertex x="-62.9175" y="5.8025"/>
<vertex x="-62.9175" y="7.4825"/>
<vertex x="-63.5125" y="7.4825"/>
<vertex x="-63.5125" y="5.785"/>
<vertex x="-63.5825" y="5.715"/>
<vertex x="-64.1775" y="5.715"/>
<vertex x="-64.1775" y="7.9725"/>
<vertex x="-61.465" y="7.9725"/>
<vertex x="-61.395" y="7.955"/>
<vertex x="-61.3075" y="7.92"/>
<vertex x="-61.1675" y="7.85"/>
<vertex x="-61.08" y="7.7625"/>
<vertex x="-60.9925" y="7.64"/>
<vertex x="-60.9225" y="7.5"/>
<vertex x="-60.905" y="7.3775"/>
<vertex x="-60.905" y="5.8025"/>
<vertex x="-60.975" y="5.7325"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-58.175" y="5.8025"/>
<vertex x="-58.2625" y="5.715"/>
<vertex x="-59.925" y="5.715"/>
<vertex x="-60.205" y="5.785"/>
<vertex x="-60.415" y="5.96"/>
<vertex x="-60.5375" y="6.135"/>
<vertex x="-60.555" y="6.2575"/>
<vertex x="-60.555" y="6.6075"/>
<vertex x="-60.45" y="6.835"/>
<vertex x="-60.345" y="6.9575"/>
<vertex x="-60.275" y="6.9925"/>
<vertex x="-60.1175" y="7.0625"/>
<vertex x="-59.925" y="7.0975"/>
<vertex x="-58.8942" y="7.0975"/>
<vertex x="-58.8942" y="6.66"/>
<vertex x="-59.6975" y="6.66"/>
<vertex x="-59.7675" y="6.625"/>
<vertex x="-59.82" y="6.59"/>
<vertex x="-59.855" y="6.5375"/>
<vertex x="-59.8725" y="6.5025"/>
<vertex x="-59.89" y="6.4325"/>
<vertex x="-59.89" y="6.38"/>
<vertex x="-59.8725" y="6.3275"/>
<vertex x="-59.82" y="6.2575"/>
<vertex x="-59.7675" y="6.2225"/>
<vertex x="-59.715" y="6.205"/>
<vertex x="-58.8925" y="6.205"/>
<vertex x="-58.8925" y="7.325"/>
<vertex x="-58.9275" y="7.3775"/>
<vertex x="-58.9975" y="7.4475"/>
<vertex x="-59.0675" y="7.4825"/>
<vertex x="-60.38" y="7.4825"/>
<vertex x="-60.38" y="7.9725"/>
<vertex x="-58.7875" y="7.9725"/>
<vertex x="-58.7175" y="7.955"/>
<vertex x="-58.63" y="7.92"/>
<vertex x="-58.49" y="7.85"/>
<vertex x="-58.4025" y="7.7625"/>
<vertex x="-58.315" y="7.64"/>
<vertex x="-58.245" y="7.5"/>
<vertex x="-58.175" y="7.22"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-57.9475" y="7.9725"/>
<vertex x="-57.9475" y="7.5525"/>
<vertex x="-57.5275" y="7.5525"/>
<vertex x="-57.5275" y="6.2575"/>
<vertex x="-57.4575" y="6.0825"/>
<vertex x="-57.3175" y="5.9075"/>
<vertex x="-57.1425" y="5.785"/>
<vertex x="-56.8975" y="5.715"/>
<vertex x="-56.3025" y="5.715"/>
<vertex x="-56.215" y="5.785"/>
<vertex x="-56.215" y="6.205"/>
<vertex x="-56.635" y="6.205"/>
<vertex x="-56.7225" y="6.2575"/>
<vertex x="-56.81" y="6.3625"/>
<vertex x="-56.8625" y="6.5025"/>
<vertex x="-56.8625" y="7.5525"/>
<vertex x="-56.215" y="7.5525"/>
<vertex x="-56.215" y="7.9725"/>
<vertex x="-56.8625" y="7.9725"/>
<vertex x="-56.8625" y="8.8125"/>
<vertex x="-57.5275" y="8.8125"/>
<vertex x="-57.5275" y="7.9725"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-85.09" y="6.1636"/>
<vertex x="-84.6414" y="6.1636"/>
<vertex x="-84.6414" y="5.715"/>
<vertex x="-85.09" y="5.715"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-85.09" y="7.0609"/>
<vertex x="-85.09" y="6.6123"/>
<vertex x="-84.6414" y="6.6123"/>
<vertex x="-84.6414" y="7.0609"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-85.09" y="7.9582"/>
<vertex x="-85.09" y="7.5095"/>
<vertex x="-84.6414" y="7.5095"/>
<vertex x="-84.6414" y="7.9582"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-84.1927" y="7.9582"/>
<vertex x="-84.1927" y="7.5095"/>
<vertex x="-83.7441" y="7.5095"/>
<vertex x="-83.7441" y="7.9582"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-84.1927" y="7.0609"/>
<vertex x="-84.1927" y="6.6123"/>
<vertex x="-83.7441" y="6.6123"/>
<vertex x="-83.7441" y="7.0609"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-84.1927" y="6.1636"/>
<vertex x="-84.1927" y="5.715"/>
<vertex x="-83.7441" y="5.715"/>
<vertex x="-83.7441" y="6.1636"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-83.2955" y="7.9582"/>
<vertex x="-83.2955" y="7.5095"/>
<vertex x="-82.8468" y="7.5095"/>
<vertex x="-82.8468" y="7.9582"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-79.7064" y="7.9582"/>
<vertex x="-79.7064" y="7.5095"/>
<vertex x="-79.2578" y="7.5095"/>
<vertex x="-79.2578" y="7.9582"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-78.8091" y="7.9582"/>
<vertex x="-78.8091" y="7.5095"/>
<vertex x="-78.3605" y="7.5095"/>
<vertex x="-78.3605" y="7.9582"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-83.2955" y="7.0609"/>
<vertex x="-83.2955" y="6.6123"/>
<vertex x="-82.8468" y="6.6123"/>
<vertex x="-82.8468" y="7.0609"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-83.2955" y="6.1636"/>
<vertex x="-83.2955" y="5.715"/>
<vertex x="-82.8468" y="5.715"/>
<vertex x="-82.8468" y="6.1636"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-82.3982" y="6.1636"/>
<vertex x="-81.9496" y="6.1636"/>
<vertex x="-81.9496" y="5.715"/>
<vertex x="-82.3982" y="5.715"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-82.3982" y="7.0609"/>
<vertex x="-82.3982" y="6.6123"/>
<vertex x="-81.9496" y="6.6123"/>
<vertex x="-81.9496" y="7.0609"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-82.3982" y="7.9582"/>
<vertex x="-82.3982" y="7.5095"/>
<vertex x="-81.9496" y="7.5095"/>
<vertex x="-81.9496" y="7.9582"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-81.5009" y="7.9582"/>
<vertex x="-81.5009" y="7.5095"/>
<vertex x="-81.0523" y="7.5095"/>
<vertex x="-81.0523" y="7.9582"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-81.5009" y="7.0609"/>
<vertex x="-81.5009" y="6.6123"/>
<vertex x="-81.0523" y="6.6123"/>
<vertex x="-81.0523" y="7.0609"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-81.5009" y="6.1636"/>
<vertex x="-81.5009" y="5.715"/>
<vertex x="-81.0523" y="5.715"/>
<vertex x="-81.0523" y="6.1636"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-80.6037" y="7.9582"/>
<vertex x="-80.6037" y="7.5095"/>
<vertex x="-80.155" y="7.5095"/>
<vertex x="-80.155" y="7.9582"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-80.6037" y="7.0609"/>
<vertex x="-80.6037" y="6.6123"/>
<vertex x="-80.155" y="6.6123"/>
<vertex x="-80.155" y="7.0609"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-80.6037" y="6.1636"/>
<vertex x="-80.6037" y="5.715"/>
<vertex x="-80.155" y="5.715"/>
<vertex x="-80.155" y="6.1636"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="A3_LOC">
<wire x1="-384.175" y1="-3.175" x2="-381" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="-381" y1="-3.175" x2="-333.375" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-333.375" y1="-3.175" x2="-285.75" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-285.75" y1="-3.175" x2="-238.125" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-238.125" y1="-3.175" x2="-190.5" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-190.5" y1="-3.175" x2="-142.875" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-142.875" y1="-3.175" x2="-95.25" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-95.25" y1="-3.175" x2="-47.625" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-47.625" y1="-3.175" x2="0" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="0" y1="-3.175" x2="3.175" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="3.175" y1="-3.175" x2="3.175" y2="50.8" width="0.1016" layer="94"/>
<wire x1="3.175" y1="50.8" x2="3.175" y2="101.6" width="0.1016" layer="94"/>
<wire x1="3.175" y1="101.6" x2="3.175" y2="152.4" width="0.1016" layer="94"/>
<wire x1="3.175" y1="152.4" x2="3.175" y2="203.2" width="0.1016" layer="94"/>
<wire x1="3.175" y1="203.2" x2="3.175" y2="257.175" width="0.1016" layer="94"/>
<wire x1="-238.125" y1="257.175" x2="-285.75" y2="257.175" width="0.1016" layer="94"/>
<wire x1="-285.75" y1="257.175" x2="-333.375" y2="257.175" width="0.1016" layer="94"/>
<wire x1="-333.375" y1="257.175" x2="-384.175" y2="257.175" width="0.1016" layer="94"/>
<wire x1="-384.175" y1="257.175" x2="-384.175" y2="203.2" width="0.1016" layer="94"/>
<wire x1="-384.175" y1="203.2" x2="-384.175" y2="152.4" width="0.1016" layer="94"/>
<wire x1="-384.175" y1="152.4" x2="-384.175" y2="101.6" width="0.1016" layer="94"/>
<wire x1="-384.175" y1="101.6" x2="-384.175" y2="50.8" width="0.1016" layer="94"/>
<wire x1="-384.175" y1="50.8" x2="-384.175" y2="0" width="0.1016" layer="94"/>
<wire x1="-384.175" y1="0" x2="-384.175" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="-381" y1="0" x2="-333.375" y2="0" width="0.1016" layer="94"/>
<wire x1="-333.375" y1="0" x2="-285.75" y2="0" width="0.1016" layer="94"/>
<wire x1="-285.75" y1="0" x2="-238.125" y2="0" width="0.1016" layer="94"/>
<wire x1="-238.125" y1="0" x2="-190.5" y2="0" width="0.1016" layer="94"/>
<wire x1="-190.5" y1="0" x2="-142.875" y2="0" width="0.1016" layer="94"/>
<wire x1="-142.875" y1="0" x2="-95.25" y2="0" width="0.1016" layer="94"/>
<wire x1="-95.25" y1="0" x2="-47.625" y2="0" width="0.1016" layer="94"/>
<wire x1="-47.625" y1="0" x2="0" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="50.8" width="0.1016" layer="94"/>
<wire x1="0" y1="50.8" x2="0" y2="101.6" width="0.1016" layer="94"/>
<wire x1="0" y1="101.6" x2="0" y2="152.4" width="0.1016" layer="94"/>
<wire x1="0" y1="152.4" x2="0" y2="203.2" width="0.1016" layer="94"/>
<wire x1="0" y1="203.2" x2="0" y2="254" width="0.1016" layer="94"/>
<wire x1="0" y1="254" x2="-47.625" y2="254" width="0.1016" layer="94"/>
<wire x1="-47.625" y1="254" x2="-95.25" y2="254" width="0.1016" layer="94"/>
<wire x1="-95.25" y1="254" x2="-142.875" y2="254" width="0.1016" layer="94"/>
<wire x1="-142.875" y1="254" x2="-190.5" y2="254" width="0.1016" layer="94"/>
<wire x1="-190.5" y1="254" x2="-238.125" y2="254" width="0.1016" layer="94"/>
<wire x1="-238.125" y1="254" x2="-285.75" y2="254" width="0.1016" layer="94"/>
<wire x1="-285.75" y1="254" x2="-333.375" y2="254" width="0.1016" layer="94"/>
<wire x1="-333.375" y1="254" x2="-381" y2="254" width="0.1016" layer="94"/>
<wire x1="-381" y1="254" x2="-381" y2="203.2" width="0.1016" layer="94"/>
<wire x1="-381" y1="203.2" x2="-381" y2="152.4" width="0.1016" layer="94"/>
<wire x1="-381" y1="152.4" x2="-381" y2="101.6" width="0.1016" layer="94"/>
<wire x1="-381" y1="101.6" x2="-381" y2="50.8" width="0.1016" layer="94"/>
<wire x1="-381" y1="50.8" x2="-381" y2="0" width="0.1016" layer="94"/>
<wire x1="3.175" y1="257.175" x2="-47.625" y2="257.175" width="0.1016" layer="94"/>
<wire x1="-47.625" y1="257.175" x2="-95.25" y2="257.175" width="0.1016" layer="94"/>
<wire x1="-95.25" y1="257.175" x2="-142.875" y2="257.175" width="0.1016" layer="94"/>
<wire x1="-142.875" y1="257.175" x2="-190.5" y2="257.175" width="0.1016" layer="94"/>
<wire x1="-190.5" y1="257.175" x2="-238.125" y2="257.175" width="0.1016" layer="94"/>
<wire x1="-190.5" y1="257.175" x2="-190.5" y2="254" width="0.1016" layer="94"/>
<wire x1="-190.5" y1="0" x2="-190.5" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-384.175" y1="101.6" x2="-381" y2="101.6" width="0.1016" layer="94"/>
<wire x1="0" y1="152.4" x2="3.175" y2="152.4" width="0.1016" layer="94"/>
<wire x1="-285.75" y1="254" x2="-285.75" y2="257.175" width="0.1016" layer="94"/>
<wire x1="-285.75" y1="0" x2="-285.75" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-95.25" y1="257.175" x2="-95.25" y2="254" width="0.1016" layer="94"/>
<wire x1="-95.25" y1="0" x2="-95.25" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-384.175" y1="50.8" x2="-381" y2="50.8" width="0.1016" layer="94"/>
<wire x1="0" y1="101.6" x2="3.175" y2="101.6" width="0.1016" layer="94"/>
<wire x1="-384.175" y1="152.4" x2="-381" y2="152.4" width="0.1016" layer="94"/>
<wire x1="0" y1="203.2" x2="3.175" y2="203.2" width="0.1016" layer="94"/>
<wire x1="-333.375" y1="254" x2="-333.375" y2="257.175" width="0.1016" layer="94"/>
<wire x1="-384.175" y1="203.2" x2="-381" y2="203.2" width="0.1016" layer="94"/>
<wire x1="0" y1="50.8" x2="3.175" y2="50.8" width="0.1016" layer="94"/>
<wire x1="-238.125" y1="254" x2="-238.125" y2="257.175" width="0.1016" layer="94"/>
<wire x1="-142.875" y1="257.175" x2="-142.875" y2="254" width="0.1016" layer="94"/>
<wire x1="-47.625" y1="257.175" x2="-47.625" y2="254" width="0.1016" layer="94"/>
<wire x1="-47.625" y1="0" x2="-47.625" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-142.875" y1="0" x2="-142.875" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-238.125" y1="0" x2="-238.125" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-333.375" y1="-3.175" x2="-333.375" y2="0" width="0.1016" layer="94"/>
<text x="-359.791" y="-2.921" size="2.54" layer="94">A</text>
<text x="-309.753" y="-2.921" size="2.54" layer="94">B</text>
<text x="-263.017" y="-2.921" size="2.54" layer="94">C</text>
<text x="-214.757" y="-2.921" size="2.54" layer="94">D</text>
<text x="-167.259" y="-2.921" size="2.54" layer="94">E</text>
<text x="-120.523" y="-2.921" size="2.54" layer="94">F</text>
<text x="-73.533" y="-2.921" size="2.54" layer="94">G</text>
<text x="-23.241" y="-2.921" size="2.54" layer="94">H</text>
<text x="0.889" y="25.527" size="2.54" layer="94">1</text>
<text x="0.635" y="76.327" size="2.54" layer="94">2</text>
<text x="0.635" y="127.127" size="2.54" layer="94">3</text>
<text x="0.635" y="178.689" size="2.54" layer="94">4</text>
<text x="0.635" y="227.965" size="2.54" layer="94">5</text>
<text x="-22.987" y="254.381" size="2.54" layer="94">H</text>
<text x="-72.771" y="254.381" size="2.54" layer="94">G</text>
<text x="-121.285" y="254.381" size="2.54" layer="94">F</text>
<text x="-168.275" y="254.381" size="2.54" layer="94">E</text>
<text x="-216.027" y="254.381" size="2.54" layer="94">D</text>
<text x="-263.271" y="254.381" size="2.54" layer="94">C</text>
<text x="-311.277" y="254.381" size="2.54" layer="94">B</text>
<text x="-359.791" y="254.381" size="2.54" layer="94">A</text>
<text x="-383.413" y="227.965" size="2.54" layer="94">5</text>
<text x="-383.413" y="178.435" size="2.54" layer="94">4</text>
<text x="-383.413" y="127.127" size="2.54" layer="94">3</text>
<text x="-383.413" y="76.073" size="2.54" layer="94">2</text>
<text x="-383.159" y="23.495" size="2.54" layer="94">1</text>
</symbol>
<symbol name="A1_LOC">
<wire x1="3.175" y1="-3.175" x2="-771.525" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-771.525" y1="-3.175" x2="-771.525" y2="528.955" width="0.1016" layer="94"/>
<wire x1="-771.525" y1="528.955" x2="3.175" y2="528.955" width="0.1016" layer="94"/>
<wire x1="3.175" y1="528.955" x2="3.175" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="525.78" width="0.1016" layer="94"/>
<wire x1="0" y1="525.78" x2="-768.35" y2="525.78" width="0.1016" layer="94"/>
<wire x1="-768.35" y1="525.78" x2="-768.35" y2="0" width="0.1016" layer="94"/>
<wire x1="-768.35" y1="0" x2="0" y2="0" width="0.1016" layer="94"/>
<wire x1="-771.525" y1="101.6" x2="-768.35" y2="101.6" width="0.1016" layer="94"/>
<wire x1="-771.525" y1="50.8" x2="-768.35" y2="50.8" width="0.1016" layer="94"/>
<wire x1="-771.525" y1="152.4" x2="-768.35" y2="152.4" width="0.1016" layer="94"/>
<wire x1="-771.525" y1="203.2" x2="-768.35" y2="203.2" width="0.1016" layer="94"/>
<wire x1="-771.525" y1="304.8" x2="-768.35" y2="304.8" width="0.1016" layer="94"/>
<wire x1="-771.525" y1="254" x2="-768.35" y2="254" width="0.1016" layer="94"/>
<wire x1="-771.525" y1="355.6" x2="-768.35" y2="355.6" width="0.1016" layer="94"/>
<wire x1="-771.525" y1="406.4" x2="-768.35" y2="406.4" width="0.1016" layer="94"/>
<wire x1="-771.525" y1="508" x2="-768.35" y2="508" width="0.1016" layer="94"/>
<wire x1="-771.525" y1="457.2" x2="-768.35" y2="457.2" width="0.1016" layer="94"/>
<wire x1="-572.135" y1="528.955" x2="-572.135" y2="525.78" width="0.1016" layer="94"/>
<wire x1="-667.385" y1="525.78" x2="-667.385" y2="528.955" width="0.1016" layer="94"/>
<wire x1="-476.885" y1="528.955" x2="-476.885" y2="525.78" width="0.1016" layer="94"/>
<wire x1="-715.01" y1="525.78" x2="-715.01" y2="528.955" width="0.1016" layer="94"/>
<wire x1="-619.76" y1="525.78" x2="-619.76" y2="528.955" width="0.1016" layer="94"/>
<wire x1="-524.51" y1="528.955" x2="-524.51" y2="525.78" width="0.1016" layer="94"/>
<wire x1="-429.26" y1="528.955" x2="-429.26" y2="525.78" width="0.1016" layer="94"/>
<wire x1="-238.76" y1="528.955" x2="-238.76" y2="525.78" width="0.1016" layer="94"/>
<wire x1="-334.01" y1="525.78" x2="-334.01" y2="528.955" width="0.1016" layer="94"/>
<wire x1="-143.51" y1="528.955" x2="-143.51" y2="525.78" width="0.1016" layer="94"/>
<wire x1="-381.635" y1="525.78" x2="-381.635" y2="528.955" width="0.1016" layer="94"/>
<wire x1="-286.385" y1="525.78" x2="-286.385" y2="528.955" width="0.1016" layer="94"/>
<wire x1="-191.135" y1="528.955" x2="-191.135" y2="525.78" width="0.1016" layer="94"/>
<wire x1="-95.885" y1="528.955" x2="-95.885" y2="525.78" width="0.1016" layer="94"/>
<wire x1="-48.26" y1="525.78" x2="-48.26" y2="528.955" width="0.1016" layer="94"/>
<wire x1="-572.135" y1="0" x2="-572.135" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-667.385" y1="-3.175" x2="-667.385" y2="0" width="0.1016" layer="94"/>
<wire x1="-476.885" y1="0" x2="-476.885" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-715.01" y1="-3.175" x2="-715.01" y2="0" width="0.1016" layer="94"/>
<wire x1="-619.76" y1="-3.175" x2="-619.76" y2="0" width="0.1016" layer="94"/>
<wire x1="-524.51" y1="0" x2="-524.51" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-429.26" y1="0" x2="-429.26" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-238.76" y1="0" x2="-238.76" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-334.01" y1="-3.175" x2="-334.01" y2="0" width="0.1016" layer="94"/>
<wire x1="-143.51" y1="0" x2="-143.51" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-381.635" y1="-3.175" x2="-381.635" y2="0" width="0.1016" layer="94"/>
<wire x1="-286.385" y1="-3.175" x2="-286.385" y2="0" width="0.1016" layer="94"/>
<wire x1="-191.135" y1="0" x2="-191.135" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-95.885" y1="0" x2="-95.885" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-48.26" y1="-3.175" x2="-48.26" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="101.6" x2="3.175" y2="101.6" width="0.1016" layer="94"/>
<wire x1="0" y1="50.8" x2="3.175" y2="50.8" width="0.1016" layer="94"/>
<wire x1="0" y1="152.4" x2="3.175" y2="152.4" width="0.1016" layer="94"/>
<wire x1="0" y1="203.2" x2="3.175" y2="203.2" width="0.1016" layer="94"/>
<wire x1="0" y1="304.8" x2="3.175" y2="304.8" width="0.1016" layer="94"/>
<wire x1="0" y1="254" x2="3.175" y2="254" width="0.1016" layer="94"/>
<wire x1="0" y1="355.6" x2="3.175" y2="355.6" width="0.1016" layer="94"/>
<wire x1="0" y1="406.4" x2="3.175" y2="406.4" width="0.1016" layer="94"/>
<wire x1="0" y1="508" x2="3.175" y2="508" width="0.1016" layer="94"/>
<wire x1="0" y1="457.2" x2="3.175" y2="457.2" width="0.1016" layer="94"/>
<text x="-770.763" y="178.435" size="2.54" layer="94">4</text>
<text x="-770.763" y="127.127" size="2.54" layer="94">3</text>
<text x="-770.763" y="76.073" size="2.54" layer="94">2</text>
<text x="-770.509" y="23.495" size="2.54" layer="94">1</text>
<text x="-454.406" y="526.161" size="2.54" layer="94">G</text>
<text x="-502.92" y="526.161" size="2.54" layer="94">F</text>
<text x="-549.91" y="526.161" size="2.54" layer="94">E</text>
<text x="-597.662" y="526.161" size="2.54" layer="94">D</text>
<text x="-644.906" y="526.161" size="2.54" layer="94">C</text>
<text x="-692.912" y="526.161" size="2.54" layer="94">B</text>
<text x="-741.426" y="526.161" size="2.54" layer="94">A</text>
<text x="-408.051" y="526.161" size="2.54" layer="94">H</text>
<text x="-359.537" y="526.161" size="2.54" layer="94">I</text>
<text x="-311.531" y="526.161" size="2.54" layer="94">J</text>
<text x="-264.287" y="526.161" size="2.54" layer="94">K</text>
<text x="-216.535" y="526.161" size="2.54" layer="94">L</text>
<text x="-169.545" y="526.161" size="2.54" layer="94">M</text>
<text x="-121.031" y="526.161" size="2.54" layer="94">N</text>
<text x="-74.676" y="526.161" size="2.54" layer="94">O</text>
<text x="-26.162" y="526.161" size="2.54" layer="94">P</text>
<text x="-454.406" y="-2.794" size="2.54" layer="94">G</text>
<text x="-502.92" y="-2.794" size="2.54" layer="94">F</text>
<text x="-549.91" y="-2.794" size="2.54" layer="94">E</text>
<text x="-597.662" y="-2.794" size="2.54" layer="94">D</text>
<text x="-644.906" y="-2.794" size="2.54" layer="94">C</text>
<text x="-692.912" y="-2.794" size="2.54" layer="94">B</text>
<text x="-741.426" y="-2.794" size="2.54" layer="94">A</text>
<text x="-408.051" y="-2.794" size="2.54" layer="94">H</text>
<text x="-359.537" y="-2.794" size="2.54" layer="94">I</text>
<text x="-311.531" y="-2.794" size="2.54" layer="94">J</text>
<text x="-264.287" y="-2.794" size="2.54" layer="94">K</text>
<text x="-216.535" y="-2.794" size="2.54" layer="94">L</text>
<text x="-169.545" y="-2.794" size="2.54" layer="94">M</text>
<text x="-121.031" y="-2.794" size="2.54" layer="94">N</text>
<text x="-74.676" y="-2.794" size="2.54" layer="94">O</text>
<text x="-26.162" y="-2.794" size="2.54" layer="94">P</text>
<text x="-770.509" y="226.695" size="2.54" layer="94">5</text>
<text x="-770.763" y="279.273" size="2.54" layer="94">6</text>
<text x="-770.763" y="330.327" size="2.54" layer="94">7</text>
<text x="-770.763" y="381.635" size="2.54" layer="94">8</text>
<text x="-770.509" y="429.895" size="2.54" layer="94">9</text>
<text x="-771.271" y="482.473" size="1.778" layer="94" ratio="10">10</text>
<text x="0.762" y="178.435" size="2.54" layer="94">4</text>
<text x="0.762" y="127.127" size="2.54" layer="94">3</text>
<text x="0.762" y="76.073" size="2.54" layer="94">2</text>
<text x="1.016" y="23.495" size="2.54" layer="94">1</text>
<text x="1.016" y="226.695" size="2.54" layer="94">5</text>
<text x="0.762" y="279.273" size="2.54" layer="94">6</text>
<text x="0.762" y="330.327" size="2.54" layer="94">7</text>
<text x="0.762" y="381.635" size="2.54" layer="94">8</text>
<text x="1.016" y="429.895" size="2.54" layer="94">9</text>
<text x="0.254" y="482.473" size="1.778" layer="94" ratio="10">10</text>
</symbol>
<symbol name="A0_LOC">
<wire x1="-914.4" y1="758.825" x2="-914.4" y2="755.65" width="0.1016" layer="94"/>
<wire x1="-1009.65" y1="755.65" x2="-1009.65" y2="758.825" width="0.1016" layer="94"/>
<wire x1="-819.15" y1="758.825" x2="-819.15" y2="755.65" width="0.1016" layer="94"/>
<wire x1="-1057.275" y1="755.65" x2="-1057.275" y2="758.825" width="0.1016" layer="94"/>
<wire x1="-962.025" y1="755.65" x2="-962.025" y2="758.825" width="0.1016" layer="94"/>
<wire x1="-866.775" y1="758.825" x2="-866.775" y2="755.65" width="0.1016" layer="94"/>
<wire x1="-771.525" y1="758.825" x2="-771.525" y2="755.65" width="0.1016" layer="94"/>
<wire x1="-581.025" y1="758.825" x2="-581.025" y2="755.65" width="0.1016" layer="94"/>
<wire x1="-676.275" y1="755.65" x2="-676.275" y2="758.825" width="0.1016" layer="94"/>
<wire x1="-485.775" y1="758.825" x2="-485.775" y2="755.65" width="0.1016" layer="94"/>
<wire x1="-723.9" y1="755.65" x2="-723.9" y2="758.825" width="0.1016" layer="94"/>
<wire x1="-628.65" y1="755.65" x2="-628.65" y2="758.825" width="0.1016" layer="94"/>
<wire x1="-533.4" y1="758.825" x2="-533.4" y2="755.65" width="0.1016" layer="94"/>
<wire x1="-438.15" y1="758.825" x2="-438.15" y2="755.65" width="0.1016" layer="94"/>
<wire x1="-390.525" y1="755.65" x2="-390.525" y2="758.825" width="0.1016" layer="94"/>
<wire x1="-1108.075" y1="-3.175" x2="-555.625" y2="-3.175" width="0.254" layer="94"/>
<wire x1="-1108.075" y1="-3.175" x2="-1108.075" y2="758.825" width="0.1016" layer="94"/>
<wire x1="-1108.075" y1="758.825" x2="3.175" y2="758.825" width="0.1016" layer="94"/>
<wire x1="3.175" y1="758.825" x2="3.175" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-552.45" y1="-3.175" x2="3.175" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-1104.9" y1="0" x2="0" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="755.65" width="0.1016" layer="94"/>
<wire x1="0" y1="755.65" x2="-1104.9" y2="755.65" width="0.1016" layer="94"/>
<wire x1="-1104.9" y1="755.65" x2="-1104.9" y2="0" width="0.1016" layer="94"/>
<wire x1="-1108.075" y1="101.6" x2="-1104.9" y2="101.6" width="0.1016" layer="94"/>
<wire x1="-1108.075" y1="50.8" x2="-1104.9" y2="50.8" width="0.1016" layer="94"/>
<wire x1="-1108.075" y1="152.4" x2="-1104.9" y2="152.4" width="0.1016" layer="94"/>
<wire x1="-1108.075" y1="203.2" x2="-1104.9" y2="203.2" width="0.1016" layer="94"/>
<wire x1="-1108.075" y1="304.8" x2="-1104.9" y2="304.8" width="0.1016" layer="94"/>
<wire x1="-1108.075" y1="254" x2="-1104.9" y2="254" width="0.1016" layer="94"/>
<wire x1="-1108.075" y1="355.6" x2="-1104.9" y2="355.6" width="0.1016" layer="94"/>
<wire x1="-1108.075" y1="406.4" x2="-1104.9" y2="406.4" width="0.1016" layer="94"/>
<wire x1="-1108.075" y1="508" x2="-1104.9" y2="508" width="0.1016" layer="94"/>
<wire x1="-1108.075" y1="457.2" x2="-1104.9" y2="457.2" width="0.1016" layer="94"/>
<wire x1="-1108.075" y1="558.8" x2="-1104.9" y2="558.8" width="0.1016" layer="94"/>
<wire x1="-1108.075" y1="609.6" x2="-1104.9" y2="609.6" width="0.1016" layer="94"/>
<wire x1="-1108.075" y1="660.4" x2="-1104.9" y2="660.4" width="0.1016" layer="94"/>
<wire x1="-1108.075" y1="711.2" x2="-1104.9" y2="711.2" width="0.1016" layer="94"/>
<wire x1="0" y1="101.6" x2="3.175" y2="101.6" width="0.1016" layer="94"/>
<wire x1="0" y1="50.8" x2="3.175" y2="50.8" width="0.1016" layer="94"/>
<wire x1="0" y1="152.4" x2="3.175" y2="152.4" width="0.1016" layer="94"/>
<wire x1="0" y1="203.2" x2="3.175" y2="203.2" width="0.1016" layer="94"/>
<wire x1="0" y1="304.8" x2="3.175" y2="304.8" width="0.1016" layer="94"/>
<wire x1="0" y1="254" x2="3.175" y2="254" width="0.1016" layer="94"/>
<wire x1="0" y1="355.6" x2="3.175" y2="355.6" width="0.1016" layer="94"/>
<wire x1="0" y1="406.4" x2="3.175" y2="406.4" width="0.1016" layer="94"/>
<wire x1="0" y1="508" x2="3.175" y2="508" width="0.1016" layer="94"/>
<wire x1="0" y1="457.2" x2="3.175" y2="457.2" width="0.1016" layer="94"/>
<wire x1="0" y1="558.8" x2="3.175" y2="558.8" width="0.1016" layer="94"/>
<wire x1="0" y1="609.6" x2="3.175" y2="609.6" width="0.1016" layer="94"/>
<wire x1="0" y1="660.4" x2="3.175" y2="660.4" width="0.1016" layer="94"/>
<wire x1="0" y1="711.2" x2="3.175" y2="711.2" width="0.1016" layer="94"/>
<wire x1="-200.025" y1="758.825" x2="-200.025" y2="755.65" width="0.1016" layer="94"/>
<wire x1="-295.275" y1="755.65" x2="-295.275" y2="758.825" width="0.1016" layer="94"/>
<wire x1="-342.9" y1="755.65" x2="-342.9" y2="758.825" width="0.1016" layer="94"/>
<wire x1="-247.65" y1="755.65" x2="-247.65" y2="758.825" width="0.1016" layer="94"/>
<wire x1="-152.4" y1="758.825" x2="-152.4" y2="755.65" width="0.1016" layer="94"/>
<wire x1="-57.15" y1="755.65" x2="-57.15" y2="758.825" width="0.1016" layer="94"/>
<wire x1="-104.775" y1="755.65" x2="-104.775" y2="758.825" width="0.1016" layer="94"/>
<wire x1="-914.4" y1="0" x2="-914.4" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-1009.65" y1="-3.175" x2="-1009.65" y2="0" width="0.1016" layer="94"/>
<wire x1="-819.15" y1="0" x2="-819.15" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-1057.275" y1="-3.175" x2="-1057.275" y2="0" width="0.1016" layer="94"/>
<wire x1="-962.025" y1="-3.175" x2="-962.025" y2="0" width="0.1016" layer="94"/>
<wire x1="-866.775" y1="0" x2="-866.775" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-771.525" y1="0" x2="-771.525" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-581.025" y1="0" x2="-581.025" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-676.275" y1="-3.175" x2="-676.275" y2="0" width="0.1016" layer="94"/>
<wire x1="-485.775" y1="0" x2="-485.775" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-723.9" y1="-3.175" x2="-723.9" y2="0" width="0.1016" layer="94"/>
<wire x1="-628.65" y1="-3.175" x2="-628.65" y2="0" width="0.1016" layer="94"/>
<wire x1="-533.4" y1="0" x2="-533.4" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-438.15" y1="0" x2="-438.15" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-390.525" y1="-3.175" x2="-390.525" y2="0" width="0.1016" layer="94"/>
<wire x1="-200.025" y1="0" x2="-200.025" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-295.275" y1="-3.175" x2="-295.275" y2="0" width="0.1016" layer="94"/>
<wire x1="-342.9" y1="-3.175" x2="-342.9" y2="0" width="0.1016" layer="94"/>
<wire x1="-247.65" y1="-3.175" x2="-247.65" y2="0" width="0.1016" layer="94"/>
<wire x1="-152.4" y1="0" x2="-152.4" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-57.15" y1="-3.175" x2="-57.15" y2="0" width="0.1016" layer="94"/>
<wire x1="-104.775" y1="-3.175" x2="-104.775" y2="0" width="0.1016" layer="94"/>
<text x="-796.671" y="756.031" size="2.54" layer="94">G</text>
<text x="-845.185" y="756.031" size="2.54" layer="94">F</text>
<text x="-892.175" y="756.031" size="2.54" layer="94">E</text>
<text x="-939.927" y="756.031" size="2.54" layer="94">D</text>
<text x="-987.171" y="756.031" size="2.54" layer="94">C</text>
<text x="-1035.177" y="756.031" size="2.54" layer="94">B</text>
<text x="-1083.691" y="756.031" size="2.54" layer="94">A</text>
<text x="-750.316" y="756.031" size="2.54" layer="94">H</text>
<text x="-701.802" y="756.031" size="2.54" layer="94">I</text>
<text x="-653.796" y="756.031" size="2.54" layer="94">J</text>
<text x="-606.552" y="756.031" size="2.54" layer="94">K</text>
<text x="-558.8" y="756.031" size="2.54" layer="94">L</text>
<text x="-511.81" y="756.031" size="2.54" layer="94">M</text>
<text x="-463.296" y="756.031" size="2.54" layer="94">N</text>
<text x="-416.941" y="756.031" size="2.54" layer="94">O</text>
<text x="-368.427" y="756.031" size="2.54" layer="94">P</text>
<text x="-1107.313" y="178.435" size="2.54" layer="94">4</text>
<text x="-1107.313" y="127.127" size="2.54" layer="94">3</text>
<text x="-1107.313" y="76.073" size="2.54" layer="94">2</text>
<text x="-1107.059" y="23.495" size="2.54" layer="94">1</text>
<text x="-1107.059" y="226.695" size="2.54" layer="94">5</text>
<text x="-1107.313" y="279.273" size="2.54" layer="94">6</text>
<text x="-1107.313" y="330.327" size="2.54" layer="94">7</text>
<text x="-1107.313" y="381.635" size="2.54" layer="94">8</text>
<text x="-1107.059" y="429.895" size="2.54" layer="94">9</text>
<text x="-1107.821" y="482.473" size="1.778" layer="94" ratio="10">10</text>
<text x="-1107.821" y="533.273" size="1.778" layer="94" ratio="10">11</text>
<text x="-1107.821" y="584.073" size="1.778" layer="94" ratio="10">12</text>
<text x="-1107.821" y="634.873" size="1.778" layer="94" ratio="10">13</text>
<text x="-1107.821" y="685.673" size="1.778" layer="94" ratio="10">14</text>
<text x="-1107.821" y="736.473" size="1.778" layer="94" ratio="10">15</text>
<text x="0.762" y="178.435" size="2.54" layer="94">4</text>
<text x="0.762" y="127.127" size="2.54" layer="94">3</text>
<text x="0.762" y="76.073" size="2.54" layer="94">2</text>
<text x="1.016" y="23.495" size="2.54" layer="94">1</text>
<text x="1.016" y="226.695" size="2.54" layer="94">5</text>
<text x="0.762" y="279.273" size="2.54" layer="94">6</text>
<text x="0.762" y="330.327" size="2.54" layer="94">7</text>
<text x="0.762" y="381.635" size="2.54" layer="94">8</text>
<text x="1.016" y="429.895" size="2.54" layer="94">9</text>
<text x="0.254" y="482.473" size="1.778" layer="94" ratio="10">10</text>
<text x="0.254" y="533.273" size="1.778" layer="94" ratio="10">11</text>
<text x="0.254" y="584.073" size="1.778" layer="94" ratio="10">12</text>
<text x="0.254" y="634.873" size="1.778" layer="94" ratio="10">13</text>
<text x="0.254" y="685.673" size="1.778" layer="94" ratio="10">14</text>
<text x="0.254" y="736.473" size="1.778" layer="94" ratio="10">15</text>
<text x="-320.802" y="756.031" size="2.54" layer="94">Q</text>
<text x="-272.796" y="756.031" size="2.54" layer="94">R</text>
<text x="-225.552" y="756.031" size="2.54" layer="94">S</text>
<text x="-177.8" y="756.031" size="2.54" layer="94">T</text>
<text x="-130.81" y="756.031" size="2.54" layer="94">U</text>
<text x="-82.677" y="756.031" size="2.54" layer="94">V</text>
<text x="-34.671" y="756.031" size="2.54" layer="94">W</text>
<text x="-796.671" y="-2.794" size="2.54" layer="94">G</text>
<text x="-845.185" y="-2.794" size="2.54" layer="94">F</text>
<text x="-892.175" y="-2.794" size="2.54" layer="94">E</text>
<text x="-939.927" y="-2.794" size="2.54" layer="94">D</text>
<text x="-987.171" y="-2.794" size="2.54" layer="94">C</text>
<text x="-1035.177" y="-2.794" size="2.54" layer="94">B</text>
<text x="-1083.691" y="-2.794" size="2.54" layer="94">A</text>
<text x="-750.316" y="-2.794" size="2.54" layer="94">H</text>
<text x="-701.802" y="-2.794" size="2.54" layer="94">I</text>
<text x="-653.796" y="-2.794" size="2.54" layer="94">J</text>
<text x="-606.552" y="-2.794" size="2.54" layer="94">K</text>
<text x="-558.8" y="-2.794" size="2.54" layer="94">L</text>
<text x="-511.81" y="-2.794" size="2.54" layer="94">M</text>
<text x="-463.296" y="-2.794" size="2.54" layer="94">N</text>
<text x="-416.941" y="-2.794" size="2.54" layer="94">O</text>
<text x="-368.427" y="-2.794" size="2.54" layer="94">P</text>
<text x="-320.802" y="-2.794" size="2.54" layer="94">Q</text>
<text x="-272.796" y="-2.794" size="2.54" layer="94">R</text>
<text x="-225.552" y="-2.794" size="2.54" layer="94">S</text>
<text x="-177.8" y="-2.794" size="2.54" layer="94">T</text>
<text x="-130.81" y="-2.794" size="2.54" layer="94">U</text>
<text x="-82.677" y="-2.794" size="2.54" layer="94">V</text>
<text x="-34.671" y="-2.794" size="2.54" layer="94">W</text>
</symbol>
<symbol name="A2_LOC">
<wire x1="3.175" y1="-3.175" x2="-555.625" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-555.625" y1="-3.175" x2="-555.625" y2="381.635" width="0.1016" layer="94"/>
<wire x1="-555.625" y1="381.635" x2="3.175" y2="381.635" width="0.1016" layer="94"/>
<wire x1="3.175" y1="381.635" x2="3.175" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="378.46" width="0.1016" layer="94"/>
<wire x1="0" y1="378.46" x2="-552.45" y2="378.46" width="0.1016" layer="94"/>
<wire x1="-552.45" y1="378.46" x2="-552.45" y2="0" width="0.1016" layer="94"/>
<wire x1="-552.45" y1="0" x2="0" y2="0" width="0.1016" layer="94"/>
<wire x1="-555.625" y1="5.08" x2="-552.45" y2="5.08" width="0.1016" layer="94"/>
<wire x1="-555.625" y1="55.88" x2="-552.45" y2="55.88" width="0.1016" layer="94"/>
<wire x1="-555.625" y1="157.48" x2="-552.45" y2="157.48" width="0.1016" layer="94"/>
<wire x1="-555.625" y1="106.68" x2="-552.45" y2="106.68" width="0.1016" layer="94"/>
<wire x1="-555.625" y1="208.28" x2="-552.45" y2="208.28" width="0.1016" layer="94"/>
<wire x1="-555.625" y1="259.08" x2="-552.45" y2="259.08" width="0.1016" layer="94"/>
<wire x1="-555.625" y1="360.68" x2="-552.45" y2="360.68" width="0.1016" layer="94"/>
<wire x1="-555.625" y1="309.88" x2="-552.45" y2="309.88" width="0.1016" layer="94"/>
<wire x1="-356.235" y1="381.635" x2="-356.235" y2="378.46" width="0.1016" layer="94"/>
<wire x1="-451.485" y1="378.46" x2="-451.485" y2="381.635" width="0.1016" layer="94"/>
<wire x1="-260.985" y1="381.635" x2="-260.985" y2="378.46" width="0.1016" layer="94"/>
<wire x1="-499.11" y1="378.46" x2="-499.11" y2="381.635" width="0.1016" layer="94"/>
<wire x1="-403.86" y1="378.46" x2="-403.86" y2="381.635" width="0.1016" layer="94"/>
<wire x1="-308.61" y1="381.635" x2="-308.61" y2="378.46" width="0.1016" layer="94"/>
<wire x1="-213.36" y1="381.635" x2="-213.36" y2="378.46" width="0.1016" layer="94"/>
<wire x1="-22.86" y1="381.635" x2="-22.86" y2="378.46" width="0.1016" layer="94"/>
<wire x1="-118.11" y1="378.46" x2="-118.11" y2="381.635" width="0.1016" layer="94"/>
<wire x1="-165.735" y1="378.46" x2="-165.735" y2="381.635" width="0.1016" layer="94"/>
<wire x1="-70.485" y1="378.46" x2="-70.485" y2="381.635" width="0.1016" layer="94"/>
<wire x1="-356.235" y1="0" x2="-356.235" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-451.485" y1="-3.175" x2="-451.485" y2="0" width="0.1016" layer="94"/>
<wire x1="-260.985" y1="0" x2="-260.985" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-499.11" y1="-3.175" x2="-499.11" y2="0" width="0.1016" layer="94"/>
<wire x1="-403.86" y1="-3.175" x2="-403.86" y2="0" width="0.1016" layer="94"/>
<wire x1="-308.61" y1="0" x2="-308.61" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-213.36" y1="0" x2="-213.36" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-22.86" y1="0" x2="-22.86" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-118.11" y1="-3.175" x2="-118.11" y2="0" width="0.1016" layer="94"/>
<wire x1="-165.735" y1="-3.175" x2="-165.735" y2="0" width="0.1016" layer="94"/>
<wire x1="-70.485" y1="-3.175" x2="-70.485" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="3.175" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="55.88" x2="3.175" y2="55.88" width="0.1016" layer="94"/>
<wire x1="0" y1="157.48" x2="3.175" y2="157.48" width="0.1016" layer="94"/>
<wire x1="0" y1="106.68" x2="3.175" y2="106.68" width="0.1016" layer="94"/>
<wire x1="0" y1="208.28" x2="3.175" y2="208.28" width="0.1016" layer="94"/>
<wire x1="0" y1="259.08" x2="3.175" y2="259.08" width="0.1016" layer="94"/>
<wire x1="0" y1="360.68" x2="3.175" y2="360.68" width="0.1016" layer="94"/>
<wire x1="0" y1="309.88" x2="3.175" y2="309.88" width="0.1016" layer="94"/>
<text x="-238.506" y="378.841" size="2.54" layer="94">G</text>
<text x="-287.02" y="378.841" size="2.54" layer="94">F</text>
<text x="-334.01" y="378.841" size="2.54" layer="94">E</text>
<text x="-381.762" y="378.841" size="2.54" layer="94">D</text>
<text x="-429.006" y="378.841" size="2.54" layer="94">C</text>
<text x="-477.012" y="378.841" size="2.54" layer="94">B</text>
<text x="-525.526" y="378.841" size="2.54" layer="94">A</text>
<text x="-192.151" y="378.841" size="2.54" layer="94">H</text>
<text x="-143.637" y="378.841" size="2.54" layer="94">I</text>
<text x="-95.631" y="378.841" size="2.54" layer="94">J</text>
<text x="-48.387" y="378.841" size="2.54" layer="94">K</text>
<text x="-0.635" y="378.841" size="2.54" layer="94">L</text>
<text x="-238.506" y="-2.794" size="2.54" layer="94">G</text>
<text x="-287.02" y="-2.794" size="2.54" layer="94">F</text>
<text x="-334.01" y="-2.794" size="2.54" layer="94">E</text>
<text x="-381.762" y="-2.794" size="2.54" layer="94">D</text>
<text x="-429.006" y="-2.794" size="2.54" layer="94">C</text>
<text x="-477.012" y="-2.794" size="2.54" layer="94">B</text>
<text x="-525.526" y="-2.794" size="2.54" layer="94">A</text>
<text x="-192.151" y="-2.794" size="2.54" layer="94">H</text>
<text x="-143.637" y="-2.794" size="2.54" layer="94">I</text>
<text x="-95.631" y="-2.794" size="2.54" layer="94">J</text>
<text x="-48.387" y="-2.794" size="2.54" layer="94">K</text>
<text x="-0.635" y="-2.794" size="2.54" layer="94">L</text>
<text x="-554.863" y="31.115" size="2.54" layer="94">1</text>
<text x="-554.609" y="79.375" size="2.54" layer="94">2</text>
<text x="-554.863" y="131.953" size="2.54" layer="94">3</text>
<text x="-554.863" y="183.007" size="2.54" layer="94">4</text>
<text x="-554.863" y="234.315" size="2.54" layer="94">5</text>
<text x="-554.609" y="282.575" size="2.54" layer="94">6</text>
<text x="-555.371" y="335.153" size="1.778" layer="94" ratio="10">7</text>
<text x="0.254" y="335.153" size="1.778" layer="94" ratio="10">7</text>
<text x="1.016" y="282.575" size="2.54" layer="94">6</text>
<text x="0.762" y="234.315" size="2.54" layer="94">5</text>
<text x="0.762" y="183.007" size="2.54" layer="94">4</text>
<text x="0.762" y="131.953" size="2.54" layer="94">3</text>
<text x="1.016" y="79.375" size="2.54" layer="94">2</text>
<text x="0.762" y="31.115" size="2.54" layer="94">1</text>
</symbol>
<symbol name="A4_LOC2">
<wire x1="-257.175" y1="-3.175" x2="-254" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="-254" y1="-3.175" x2="-15.875" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="3.175" y1="-3.175" x2="3.175" y2="50.8" width="0.1524" layer="94"/>
<wire x1="3.175" y1="50.8" x2="3.175" y2="101.6" width="0.1524" layer="94"/>
<wire x1="3.175" y1="101.6" x2="3.175" y2="152.4" width="0.1524" layer="94"/>
<wire x1="3.175" y1="152.4" x2="3.175" y2="175.895" width="0.1524" layer="94"/>
<wire x1="-111.125" y1="175.895" x2="-158.75" y2="175.895" width="0.1524" layer="94"/>
<wire x1="-158.75" y1="175.895" x2="-257.175" y2="175.895" width="0.1524" layer="94"/>
<wire x1="-257.175" y1="175.895" x2="-257.175" y2="152.4" width="0.1524" layer="94"/>
<wire x1="-257.175" y1="152.4" x2="-257.175" y2="101.6" width="0.1524" layer="94"/>
<wire x1="-257.175" y1="101.6" x2="-257.175" y2="50.8" width="0.1524" layer="94"/>
<wire x1="-257.175" y1="50.8" x2="-257.175" y2="0" width="0.1524" layer="94"/>
<wire x1="-257.175" y1="0" x2="-257.175" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="50.8" width="0.1524" layer="94"/>
<wire x1="0" y1="50.8" x2="0" y2="101.6" width="0.1524" layer="94"/>
<wire x1="0" y1="101.6" x2="0" y2="152.4" width="0.1524" layer="94"/>
<wire x1="0" y1="152.4" x2="0" y2="172.72" width="0.1524" layer="94"/>
<wire x1="-111.125" y1="172.72" x2="-158.75" y2="172.72" width="0.1524" layer="94"/>
<wire x1="-158.75" y1="172.72" x2="-254" y2="172.72" width="0.1524" layer="94"/>
<wire x1="-254" y1="172.72" x2="-254" y2="152.4" width="0.1524" layer="94"/>
<wire x1="-254" y1="152.4" x2="-254" y2="101.6" width="0.1524" layer="94"/>
<wire x1="-254" y1="101.6" x2="-254" y2="50.8" width="0.1524" layer="94"/>
<wire x1="-254" y1="50.8" x2="-254" y2="0" width="0.1524" layer="94"/>
<wire x1="-257.175" y1="101.6" x2="-254" y2="101.6" width="0.1016" layer="94"/>
<wire x1="0" y1="152.4" x2="3.175" y2="152.4" width="0.1016" layer="94"/>
<wire x1="-257.175" y1="50.8" x2="-254" y2="50.8" width="0.1016" layer="94"/>
<wire x1="0" y1="101.6" x2="3.175" y2="101.6" width="0.1016" layer="94"/>
<wire x1="-257.175" y1="152.4" x2="-254" y2="152.4" width="0.1016" layer="94"/>
<wire x1="0" y1="50.8" x2="3.175" y2="50.8" width="0.1016" layer="94"/>
<wire x1="0" y1="172.72" x2="-25.4" y2="172.72" width="0.1524" layer="94"/>
<wire x1="-25.4" y1="172.72" x2="-50.8" y2="172.72" width="0.1524" layer="94"/>
<wire x1="-50.8" y1="172.72" x2="-111.125" y2="172.72" width="0.1524" layer="94"/>
<wire x1="3.175" y1="175.895" x2="-25.4" y2="175.895" width="0.1524" layer="94"/>
<wire x1="-25.4" y1="175.895" x2="-111.125" y2="175.895" width="0.1524" layer="94"/>
<wire x1="-254" y1="0" x2="-63.627" y2="0" width="0.1524" layer="94"/>
<wire x1="-63.246" y1="0" x2="-15.875" y2="0" width="0.1524" layer="94"/>
<wire x1="-15.748" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-15.748" y1="-3.175" x2="3.175" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="-25.4" y1="172.72" x2="-25.4" y2="175.895" width="0.1016" layer="94"/>
<wire x1="-50.8" y1="172.72" x2="-50.8" y2="175.895" width="0.1016" layer="94"/>
<wire x1="-76.2" y1="172.72" x2="-76.2" y2="175.895" width="0.1016" layer="94"/>
<wire x1="-101.6" y1="172.72" x2="-101.6" y2="175.895" width="0.1016" layer="94"/>
<wire x1="-127" y1="172.72" x2="-127" y2="175.895" width="0.1016" layer="94"/>
<wire x1="-152.4" y1="172.72" x2="-152.4" y2="175.895" width="0.1016" layer="94"/>
<wire x1="-177.8" y1="172.72" x2="-177.8" y2="175.895" width="0.1016" layer="94"/>
<wire x1="-203.2" y1="172.72" x2="-203.2" y2="175.895" width="0.1016" layer="94"/>
<wire x1="-228.6" y1="172.72" x2="-228.6" y2="175.895" width="0.1016" layer="94"/>
<wire x1="-25.4" y1="-3.175" x2="-25.4" y2="0" width="0.1016" layer="94"/>
<wire x1="-50.8" y1="-3.175" x2="-50.8" y2="0" width="0.1016" layer="94"/>
<wire x1="-76.2" y1="-3.175" x2="-76.2" y2="0" width="0.1016" layer="94"/>
<wire x1="-101.6" y1="-3.175" x2="-101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="-127" y1="-3.175" x2="-127" y2="0" width="0.1016" layer="94"/>
<wire x1="-152.4" y1="-3.175" x2="-152.4" y2="0" width="0.1016" layer="94"/>
<wire x1="-177.8" y1="-3.175" x2="-177.8" y2="0" width="0.1016" layer="94"/>
<wire x1="-203.2" y1="-3.175" x2="-203.2" y2="0" width="0.1016" layer="94"/>
<wire x1="-228.6" y1="-3.175" x2="-228.6" y2="0" width="0.1016" layer="94"/>
<text x="0.889" y="25.527" size="2.54" layer="94">1</text>
<text x="0.635" y="76.327" size="2.54" layer="94">2</text>
<text x="0.635" y="127.127" size="2.54" layer="94">3</text>
<text x="-140.335" y="173.101" size="2.54" layer="94">E</text>
<text x="-165.227" y="173.101" size="2.54" layer="94">D</text>
<text x="-192.151" y="173.101" size="2.54" layer="94">C</text>
<text x="-217.297" y="173.101" size="2.54" layer="94">B</text>
<text x="-242.951" y="173.101" size="2.54" layer="94">A</text>
<text x="-256.413" y="127.127" size="2.54" layer="94">3</text>
<text x="-256.413" y="76.073" size="2.54" layer="94">2</text>
<text x="-256.159" y="23.495" size="2.54" layer="94">1</text>
<text x="-114.173" y="173.101" size="2.54" layer="94">F</text>
<text x="0.381" y="164.973" size="2.54" layer="94">4</text>
<text x="-256.667" y="164.973" size="2.54" layer="94">4</text>
<text x="-88.773" y="173.101" size="2.54" layer="94">G</text>
<text x="-63.373" y="173.101" size="2.54" layer="94">H</text>
<text x="-37.973" y="173.101" size="2.54" layer="94">I</text>
<text x="-12.573" y="173.101" size="2.54" layer="94">J</text>
<text x="-140.335" y="-2.794" size="2.54" layer="94">E</text>
<text x="-165.227" y="-2.794" size="2.54" layer="94">D</text>
<text x="-192.151" y="-2.794" size="2.54" layer="94">C</text>
<text x="-217.297" y="-2.794" size="2.54" layer="94">B</text>
<text x="-242.951" y="-2.794" size="2.54" layer="94">A</text>
<text x="-114.173" y="-2.794" size="2.54" layer="94">F</text>
<text x="-88.773" y="-2.794" size="2.54" layer="94">G</text>
<text x="-63.373" y="-2.794" size="2.54" layer="94">H</text>
<text x="-37.973" y="-2.794" size="2.54" layer="94">I</text>
<text x="-12.573" y="-2.794" size="2.54" layer="94">J</text>
</symbol>
<symbol name="SCHRIFTF">
<wire x1="-22.86" y1="-10.16" x2="-12.7" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-10.16" x2="-4.445" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-10.16" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="-10.16" x2="19.685" y2="-10.16" width="0.254" layer="94"/>
<wire x1="19.685" y1="-10.16" x2="19.685" y2="-6.35" width="0.254" layer="94"/>
<wire x1="19.685" y1="-6.35" x2="19.685" y2="-5.715" width="0.254" layer="94"/>
<wire x1="19.685" y1="-5.715" x2="19.685" y2="-3.175" width="0.254" layer="94"/>
<wire x1="19.685" y1="-3.175" x2="19.685" y2="1.905" width="0.254" layer="94"/>
<wire x1="19.685" y1="1.905" x2="19.685" y2="6.985" width="0.254" layer="94"/>
<wire x1="19.685" y1="6.985" x2="11.43" y2="6.985" width="0.254" layer="94"/>
<wire x1="11.43" y1="6.985" x2="-0.635" y2="6.985" width="0.254" layer="94"/>
<wire x1="-0.635" y1="6.985" x2="-13.335" y2="6.985" width="0.254" layer="94"/>
<wire x1="-13.335" y1="6.985" x2="-22.86" y2="6.985" width="0.254" layer="94"/>
<wire x1="-22.86" y1="6.985" x2="-22.86" y2="4.445" width="0.254" layer="94"/>
<wire x1="-22.86" y1="4.445" x2="-22.86" y2="1.905" width="0.254" layer="94"/>
<wire x1="-22.86" y1="1.905" x2="-22.86" y2="-0.635" width="0.254" layer="94"/>
<wire x1="-22.86" y1="-0.635" x2="-22.86" y2="-3.175" width="0.254" layer="94"/>
<wire x1="-22.86" y1="-3.175" x2="-22.86" y2="-5.715" width="0.254" layer="94"/>
<wire x1="-22.86" y1="6.985" x2="-22.86" y2="9.525" width="0.254" layer="94"/>
<wire x1="-22.86" y1="9.525" x2="-22.86" y2="12.065" width="0.254" layer="94"/>
<wire x1="-22.86" y1="12.065" x2="-22.86" y2="14.605" width="0.254" layer="94"/>
<wire x1="-22.86" y1="14.605" x2="-22.86" y2="17.78" width="0.254" layer="94"/>
<wire x1="-22.86" y1="17.78" x2="-13.335" y2="17.78" width="0.254" layer="94"/>
<wire x1="-13.335" y1="17.78" x2="-0.635" y2="17.78" width="0.254" layer="94"/>
<wire x1="-0.635" y1="17.78" x2="11.43" y2="17.78" width="0.254" layer="94"/>
<wire x1="11.43" y1="17.78" x2="19.685" y2="17.78" width="0.254" layer="94"/>
<wire x1="19.685" y1="17.78" x2="19.685" y2="14.605" width="0.254" layer="94"/>
<wire x1="19.685" y1="14.605" x2="19.685" y2="12.065" width="0.254" layer="94"/>
<wire x1="19.685" y1="12.065" x2="19.685" y2="9.525" width="0.254" layer="94"/>
<wire x1="19.685" y1="9.525" x2="19.685" y2="6.985" width="0.254" layer="94"/>
<wire x1="-22.86" y1="9.525" x2="19.685" y2="9.525" width="0.127" layer="94"/>
<wire x1="-22.86" y1="12.065" x2="19.685" y2="12.065" width="0.127" layer="94"/>
<wire x1="-22.86" y1="14.605" x2="19.685" y2="14.605" width="0.127" layer="94"/>
<wire x1="11.43" y1="17.78" x2="11.43" y2="6.985" width="0.127" layer="94"/>
<wire x1="-22.86" y1="17.78" x2="-31.75" y2="17.78" width="0.254" layer="94"/>
<wire x1="-31.75" y1="17.78" x2="-43.815" y2="17.78" width="0.254" layer="94"/>
<wire x1="-43.815" y1="17.78" x2="-53.975" y2="17.78" width="0.254" layer="94"/>
<wire x1="-53.975" y1="17.78" x2="-53.975" y2="14.605" width="0.254" layer="94"/>
<wire x1="-53.975" y1="14.605" x2="-53.975" y2="12.065" width="0.254" layer="94"/>
<wire x1="-53.975" y1="12.065" x2="-53.975" y2="9.525" width="0.254" layer="94"/>
<wire x1="-53.975" y1="9.525" x2="-53.975" y2="6.985" width="0.254" layer="94"/>
<wire x1="-53.975" y1="6.985" x2="-53.975" y2="4.445" width="0.254" layer="94"/>
<wire x1="-53.975" y1="4.445" x2="-53.975" y2="1.905" width="0.254" layer="94"/>
<wire x1="-53.975" y1="1.905" x2="-53.975" y2="-0.635" width="0.254" layer="94"/>
<wire x1="-53.975" y1="-0.635" x2="-53.975" y2="-3.175" width="0.254" layer="94"/>
<wire x1="-53.975" y1="-3.175" x2="-53.975" y2="-5.715" width="0.254" layer="94"/>
<wire x1="-53.975" y1="-5.715" x2="-53.975" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-53.975" y1="-10.16" x2="-43.815" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-43.815" y1="-10.16" x2="-31.75" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-31.75" y1="-10.16" x2="-22.86" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-53.975" y1="14.605" x2="-22.86" y2="14.605" width="0.127" layer="94"/>
<wire x1="-53.975" y1="12.065" x2="-22.86" y2="12.065" width="0.127" layer="94"/>
<wire x1="-53.975" y1="9.525" x2="-22.86" y2="9.525" width="0.127" layer="94"/>
<wire x1="-53.975" y1="6.985" x2="-22.86" y2="6.985" width="0.127" layer="94"/>
<wire x1="-53.975" y1="4.445" x2="-22.86" y2="4.445" width="0.127" layer="94"/>
<wire x1="-53.975" y1="1.905" x2="-22.86" y2="1.905" width="0.127" layer="94"/>
<wire x1="-53.975" y1="-0.635" x2="-22.86" y2="-0.635" width="0.127" layer="94"/>
<wire x1="-22.86" y1="-5.715" x2="-12.7" y2="-5.715" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-5.715" x2="-4.445" y2="-5.715" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-5.715" x2="5.08" y2="-5.715" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.715" x2="19.685" y2="-5.715" width="0.254" layer="94"/>
<wire x1="-31.75" y1="17.78" x2="-31.75" y2="-10.16" width="0.127" layer="94"/>
<wire x1="-0.635" y1="17.78" x2="-0.635" y2="6.985" width="0.127" layer="94"/>
<wire x1="-13.335" y1="6.985" x2="-13.335" y2="17.78" width="0.127" layer="94"/>
<wire x1="-43.815" y1="17.78" x2="-43.815" y2="-10.16" width="0.127" layer="94"/>
<wire x1="-22.86" y1="-5.715" x2="-22.86" y2="-10.16" width="0.127" layer="94"/>
<wire x1="-12.7" y1="-5.715" x2="-12.7" y2="-10.16" width="0.127" layer="94"/>
<wire x1="-4.445" y1="-5.715" x2="-4.445" y2="-10.16" width="0.127" layer="94"/>
<wire x1="5.08" y1="-5.715" x2="5.08" y2="-10.16" width="0.127" layer="94"/>
<wire x1="19.685" y1="17.78" x2="66.04" y2="17.78" width="0.254" layer="94"/>
<wire x1="66.04" y1="17.78" x2="66.04" y2="12.065" width="0.254" layer="94"/>
<wire x1="66.04" y1="12.065" x2="66.04" y2="6.985" width="0.254" layer="94"/>
<wire x1="66.04" y1="6.985" x2="66.04" y2="1.905" width="0.254" layer="94"/>
<wire x1="66.04" y1="1.905" x2="66.04" y2="-3.175" width="0.254" layer="94"/>
<wire x1="66.04" y1="-3.175" x2="66.04" y2="-10.16" width="0.254" layer="94"/>
<wire x1="66.04" y1="-10.16" x2="53.975" y2="-10.16" width="0.254" layer="94"/>
<wire x1="53.975" y1="-10.16" x2="19.685" y2="-10.16" width="0.254" layer="94"/>
<wire x1="19.685" y1="12.065" x2="66.04" y2="12.065" width="0.127" layer="94"/>
<wire x1="19.685" y1="6.985" x2="66.04" y2="6.985" width="0.127" layer="94"/>
<wire x1="19.685" y1="1.905" x2="66.04" y2="1.905" width="0.127" layer="94"/>
<wire x1="19.685" y1="-3.175" x2="53.975" y2="-3.175" width="0.127" layer="94"/>
<wire x1="53.975" y1="-3.175" x2="66.04" y2="-3.175" width="0.127" layer="94"/>
<wire x1="53.975" y1="-3.175" x2="53.975" y2="-6.35" width="0.127" layer="94"/>
<wire x1="53.975" y1="-6.35" x2="53.975" y2="-10.16" width="0.127" layer="94"/>
<wire x1="19.685" y1="-6.35" x2="53.975" y2="-6.35" width="0.127" layer="94"/>
<wire x1="-53.975" y1="-3.175" x2="-22.86" y2="-3.175" width="0.127" layer="94"/>
<wire x1="-53.975" y1="-5.715" x2="-22.86" y2="-5.715" width="0.127" layer="94"/>
<text x="20.48" y="-8.9601" size="1.6764" layer="94">&gt;LAST_DATE_TIME</text>
<text x="20.48" y="-5.7851" size="1.6764" layer="94">&gt;DRAWING_NAME</text>
<text x="-22.225" y="12.7" size="1.6764" layer="94">Bearb.</text>
<text x="-22.225" y="10.16" size="1.6764" layer="94">Gepr.</text>
<text x="-22.225" y="7.62" size="1.6764" layer="94">Norm</text>
<text x="-12.7" y="15.24" size="1.6764" layer="94">Datum</text>
<text x="0" y="15.24" size="1.6764" layer="94">Name</text>
<text x="12.065" y="15.24" size="1.6764" layer="94">Sign.</text>
<text x="-52.705" y="12.7" size="1.6764" layer="94">stand</text>
<text x="-52.705" y="15.24" size="1.6764" layer="94">Änd.-</text>
<text x="-43.18" y="12.7" size="1.6764" layer="94">Datum</text>
<text x="-31.115" y="12.7" size="1.6764" layer="94">Sign.</text>
<text x="54.61" y="-5.715" size="1.6764" layer="94">Blatt</text>
<text x="54.61" y="-8.89" size="1.6764" layer="94">&gt;SHEET</text>
<text x="20.48" y="-8.9601" size="1.6764" layer="94">&gt;LAST_DATE_TIME</text>
<text x="20.48" y="-5.7851" size="1.6764" layer="94">&gt;DRAWING_NAME</text>
<text x="-22.225" y="12.7" size="1.6764" layer="94">Bearb.</text>
<text x="-22.225" y="10.16" size="1.6764" layer="94">Gepr.</text>
<text x="-22.225" y="7.62" size="1.6764" layer="94">Norm</text>
<text x="-12.7" y="15.24" size="1.6764" layer="94">Datum</text>
<text x="0" y="15.24" size="1.6764" layer="94">Name</text>
<text x="12.065" y="15.24" size="1.6764" layer="94">Sign.</text>
<text x="-52.705" y="12.7" size="1.6764" layer="94">stand</text>
<text x="-52.705" y="15.24" size="1.6764" layer="94">Änd.-</text>
<text x="-43.18" y="12.7" size="1.6764" layer="94">Datum</text>
<text x="-31.115" y="12.7" size="1.6764" layer="94">Sign.</text>
<text x="54.61" y="-5.715" size="1.6764" layer="94">Blatt</text>
<text x="54.61" y="-8.89" size="1.6764" layer="94">&gt;SHEET</text>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-19.05" y="5.5825"/>
<vertex x="-19.05" y="-0.42"/>
<vertex x="-17.6675" y="-0.42"/>
<vertex x="-17.6675" y="2.065"/>
<vertex x="-15.445" y="2.065"/>
<vertex x="-15.445" y="-0.42"/>
<vertex x="-14.115" y="-0.42"/>
<vertex x="-14.115" y="5.5825"/>
<vertex x="-15.445" y="5.5825"/>
<vertex x="-15.445" y="3.1675"/>
<vertex x="-17.6675" y="3.1675"/>
<vertex x="-17.6675" y="5.5825"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-12.12" y="5.5825"/>
<vertex x="-11.3325" y="5.5825"/>
<vertex x="-11.3325" y="4.9175"/>
<vertex x="-12.12" y="4.9175"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-8.725" y="5.5825"/>
<vertex x="-7.9025" y="5.5825"/>
<vertex x="-7.9025" y="4.9175"/>
<vertex x="-8.725" y="4.9175"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-13.275" y="-0.42"/>
<vertex x="-11.8225" y="-0.42"/>
<vertex x="-10.055" y="4.235"/>
<vertex x="-6.6425" y="-4.445"/>
<vertex x="-5.295" y="-4.445"/>
<vertex x="-9.285" y="5.5825"/>
<vertex x="-10.72" y="5.5825"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-10.8775" y="2.0825"/>
<vertex x="-9.1975" y="2.0825"/>
<vertex x="-8.7775" y="0.9975"/>
<vertex x="-11.2975" y="0.9975"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-1.795" y="5.5825"/>
<vertex x="-1.795" y="4.5675"/>
<vertex x="-3.825" y="4.5675"/>
<vertex x="-4.385" y="4.4625"/>
<vertex x="-4.7" y="4.3225"/>
<vertex x="-4.91" y="4.1475"/>
<vertex x="-5.015" y="3.9725"/>
<vertex x="-5.155" y="3.6925"/>
<vertex x="-5.26" y="3.3075"/>
<vertex x="-5.33" y="2.8525"/>
<vertex x="-5.33" y="2.03"/>
<vertex x="-5.26" y="1.5225"/>
<vertex x="-5.0675" y="1.1025"/>
<vertex x="-4.875" y="0.8575"/>
<vertex x="-4.5425" y="0.6825"/>
<vertex x="-4.245" y="0.63"/>
<vertex x="-3.895" y="0.6125"/>
<vertex x="-1.795" y="0.6125"/>
<vertex x="-1.795" y="-0.42"/>
<vertex x="-4.56" y="-0.42"/>
<vertex x="-5.05" y="-0.3325"/>
<vertex x="-5.5225" y="-0.1575"/>
<vertex x="-5.995" y="0.1925"/>
<vertex x="-6.3625" y="0.6125"/>
<vertex x="-6.5375" y="0.98"/>
<vertex x="-6.7125" y="1.5225"/>
<vertex x="-6.765" y="1.9075"/>
<vertex x="-6.7825" y="2.4675"/>
<vertex x="-6.765" y="3.0975"/>
<vertex x="-6.66" y="3.7625"/>
<vertex x="-6.4675" y="4.2525"/>
<vertex x="-6.24" y="4.62"/>
<vertex x="-5.89" y="5.0225"/>
<vertex x="-5.435" y="5.3025"/>
<vertex x="-5.0675" y="5.4425"/>
<vertex x="-4.665" y="5.5125"/>
<vertex x="-4.21" y="5.565"/>
<vertex x="-3.79" y="5.5825"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-0.92" y="5.5825"/>
<vertex x="-0.92" y="-0.42"/>
<vertex x="0.41" y="-0.42"/>
<vertex x="0.41" y="5.5825"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="0.41" y="2.7125"/>
<vertex x="2.72" y="5.5825"/>
<vertex x="4.365" y="5.5825"/>
<vertex x="1.915" y="2.6425"/>
<vertex x="4.3475" y="-0.42"/>
<vertex x="2.615" y="-0.42"/>
<vertex x="0.41" y="2.485"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="9.3525" y="5.5825"/>
<vertex x="9.3525" y="4.4975"/>
<vertex x="7.1125" y="4.4975"/>
<vertex x="6.7625" y="4.4625"/>
<vertex x="6.4475" y="4.3225"/>
<vertex x="6.2375" y="4.165"/>
<vertex x="6.0975" y="3.9725"/>
<vertex x="5.94" y="3.675"/>
<vertex x="5.87" y="3.29"/>
<vertex x="5.87" y="1.6975"/>
<vertex x="5.8875" y="1.5225"/>
<vertex x="6.08" y="1.1025"/>
<vertex x="6.2725" y="0.8575"/>
<vertex x="6.605" y="0.6825"/>
<vertex x="6.9025" y="0.63"/>
<vertex x="7.2525" y="0.6125"/>
<vertex x="9.3525" y="0.6125"/>
<vertex x="9.3525" y="-0.42"/>
<vertex x="6.5875" y="-0.42"/>
<vertex x="6.0975" y="-0.3325"/>
<vertex x="5.625" y="-0.1575"/>
<vertex x="5.1525" y="0.1925"/>
<vertex x="4.785" y="0.6125"/>
<vertex x="4.61" y="0.98"/>
<vertex x="4.435" y="1.5225"/>
<vertex x="4.3825" y="1.9075"/>
<vertex x="4.365" y="2.4675"/>
<vertex x="4.3825" y="3.0975"/>
<vertex x="4.4875" y="3.7625"/>
<vertex x="4.68" y="4.2525"/>
<vertex x="4.9075" y="4.62"/>
<vertex x="5.2575" y="5.0225"/>
<vertex x="5.7125" y="5.3025"/>
<vertex x="6.08" y="5.4425"/>
<vertex x="6.4825" y="5.5125"/>
<vertex x="6.9375" y="5.565"/>
<vertex x="7.3575" y="5.5825"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="5.87" y="2.065"/>
<vertex x="9.3525" y="2.065"/>
<vertex x="9.3525" y="3.1675"/>
<vertex x="5.87" y="3.1675"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="10.385" y="5.5825"/>
<vertex x="13.745" y="5.5825"/>
<vertex x="14.375" y="5.5125"/>
<vertex x="14.795" y="5.39"/>
<vertex x="15.075" y="5.215"/>
<vertex x="15.2675" y="5.0225"/>
<vertex x="15.495" y="4.725"/>
<vertex x="15.6175" y="4.3575"/>
<vertex x="15.6875" y="3.99"/>
<vertex x="15.6875" y="3.5875"/>
<vertex x="15.6175" y="3.15"/>
<vertex x="15.425" y="2.7125"/>
<vertex x="15.1975" y="2.4325"/>
<vertex x="14.9525" y="2.2575"/>
<vertex x="14.6375" y="2.1"/>
<vertex x="14.515" y="2.0475"/>
<vertex x="15.95" y="-0.42"/>
<vertex x="14.445" y="-0.42"/>
<vertex x="13.15" y="1.89"/>
<vertex x="13.15" y="2.975"/>
<vertex x="13.535" y="2.975"/>
<vertex x="13.92" y="3.08"/>
<vertex x="14.2175" y="3.255"/>
<vertex x="14.3225" y="3.4825"/>
<vertex x="14.3225" y="4.0075"/>
<vertex x="14.2175" y="4.2175"/>
<vertex x="14.06" y="4.3225"/>
<vertex x="13.9025" y="4.3925"/>
<vertex x="13.6225" y="4.4625"/>
<vertex x="13.3775" y="4.4975"/>
<vertex x="11.7325" y="4.4975"/>
<vertex x="11.7325" y="2.975"/>
<vertex x="13.1483" y="2.975"/>
<vertex x="13.1483" y="1.89"/>
<vertex x="11.7325" y="1.89"/>
<vertex x="11.7325" y="-0.42"/>
<vertex x="10.385" y="-0.42"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-5.12" y="-2.1875"/>
<vertex x="-4.455" y="-2.1875"/>
<vertex x="-4.455" y="-3.745"/>
<vertex x="-4.42" y="-3.7975"/>
<vertex x="-4.35" y="-3.8675"/>
<vertex x="-4.28" y="-3.9025"/>
<vertex x="-3.65" y="-3.9025"/>
<vertex x="-3.65" y="-2.1875"/>
<vertex x="-2.985" y="-2.1875"/>
<vertex x="-2.985" y="-4.3575"/>
<vertex x="-3.0725" y="-4.445"/>
<vertex x="-4.28" y="-4.445"/>
<vertex x="-4.5075" y="-4.4275"/>
<vertex x="-4.595" y="-4.41"/>
<vertex x="-4.7175" y="-4.3575"/>
<vertex x="-4.84" y="-4.2875"/>
<vertex x="-4.9975" y="-4.13"/>
<vertex x="-5.0675" y="-4.0075"/>
<vertex x="-5.1025" y="-3.92"/>
<vertex x="-5.12" y="-3.85"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-2.7575" y="-2.1875"/>
<vertex x="-2.7575" y="-2.6075"/>
<vertex x="-2.3375" y="-2.6075"/>
<vertex x="-2.3375" y="-3.9025"/>
<vertex x="-2.2675" y="-4.0775"/>
<vertex x="-2.1275" y="-4.2525"/>
<vertex x="-1.9525" y="-4.375"/>
<vertex x="-1.7075" y="-4.445"/>
<vertex x="-1.1125" y="-4.445"/>
<vertex x="-1.025" y="-4.375"/>
<vertex x="-1.025" y="-3.955"/>
<vertex x="-1.445" y="-3.955"/>
<vertex x="-1.5325" y="-3.9025"/>
<vertex x="-1.62" y="-3.7975"/>
<vertex x="-1.6725" y="-3.6575"/>
<vertex x="-1.6725" y="-2.6075"/>
<vertex x="-1.025" y="-2.6075"/>
<vertex x="-1.025" y="-2.1875"/>
<vertex x="-1.6725" y="-2.1875"/>
<vertex x="-1.6725" y="-1.3475"/>
<vertex x="-2.3375" y="-1.3475"/>
<vertex x="-2.3375" y="-2.1875"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-0.92" y="-3.2742"/>
<vertex x="-0.92" y="-3.4475"/>
<vertex x="-0.85" y="-3.7625"/>
<vertex x="-0.675" y="-4.06"/>
<vertex x="-0.4125" y="-4.27"/>
<vertex x="-0.1325" y="-4.41"/>
<vertex x="0.095" y="-4.445"/>
<vertex x="0.55" y="-4.445"/>
<vertex x="0.865" y="-4.375"/>
<vertex x="1.11" y="-4.2525"/>
<vertex x="1.3025" y="-4.06"/>
<vertex x="1.46" y="-3.8325"/>
<vertex x="1.5125" y="-3.6627"/>
<vertex x="1.53" y="-3.5525"/>
<vertex x="1.53" y="-3.045"/>
<vertex x="1.46" y="-2.7475"/>
<vertex x="1.32" y="-2.5375"/>
<vertex x="1.11" y="-2.345"/>
<vertex x="0.865" y="-2.2225"/>
<vertex x="0.55" y="-2.135"/>
<vertex x="0.06" y="-2.135"/>
<vertex x="-0.185" y="-2.1875"/>
<vertex x="-0.4475" y="-2.3275"/>
<vertex x="-0.605" y="-2.485"/>
<vertex x="-0.78" y="-2.7125"/>
<vertex x="-0.885" y="-2.9575"/>
<vertex x="-0.92" y="-3.15"/>
<vertex x="-0.92" y="-3.2725"/>
<vertex x="-0.255" y="-3.2725"/>
<vertex x="-0.255" y="-3.1675"/>
<vertex x="-0.2025" y="-3.01"/>
<vertex x="-0.1325" y="-2.8875"/>
<vertex x="0.025" y="-2.7125"/>
<vertex x="0.2" y="-2.625"/>
<vertex x="0.4275" y="-2.625"/>
<vertex x="0.5325" y="-2.66"/>
<vertex x="0.69" y="-2.765"/>
<vertex x="0.8125" y="-2.9575"/>
<vertex x="0.865" y="-3.1325"/>
<vertex x="0.865" y="-3.465"/>
<vertex x="0.8125" y="-3.6225"/>
<vertex x="0.76" y="-3.7275"/>
<vertex x="0.6375" y="-3.8675"/>
<vertex x="0.515" y="-3.9375"/>
<vertex x="0.4275" y="-3.955"/>
<vertex x="0.1825" y="-3.955"/>
<vertex x="0.025" y="-3.9025"/>
<vertex x="-0.115" y="-3.7625"/>
<vertex x="-0.22" y="-3.5525"/>
<vertex x="-0.255" y="-3.395"/>
<vertex x="-0.255" y="-3.2742"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="11.05" y="-3.2742"/>
<vertex x="11.05" y="-3.4475"/>
<vertex x="11.12" y="-3.7625"/>
<vertex x="11.295" y="-4.06"/>
<vertex x="11.5575" y="-4.27"/>
<vertex x="11.8375" y="-4.41"/>
<vertex x="12.065" y="-4.445"/>
<vertex x="12.52" y="-4.445"/>
<vertex x="12.835" y="-4.375"/>
<vertex x="13.08" y="-4.2525"/>
<vertex x="13.2725" y="-4.06"/>
<vertex x="13.43" y="-3.8325"/>
<vertex x="13.4825" y="-3.6627"/>
<vertex x="13.5" y="-3.5525"/>
<vertex x="13.5" y="-3.045"/>
<vertex x="13.43" y="-2.7475"/>
<vertex x="13.29" y="-2.5375"/>
<vertex x="13.08" y="-2.345"/>
<vertex x="12.835" y="-2.2225"/>
<vertex x="12.52" y="-2.135"/>
<vertex x="12.03" y="-2.135"/>
<vertex x="11.785" y="-2.1875"/>
<vertex x="11.5225" y="-2.3275"/>
<vertex x="11.365" y="-2.485"/>
<vertex x="11.19" y="-2.7125"/>
<vertex x="11.085" y="-2.9575"/>
<vertex x="11.05" y="-3.15"/>
<vertex x="11.05" y="-3.2725"/>
<vertex x="11.715" y="-3.2725"/>
<vertex x="11.715" y="-3.1675"/>
<vertex x="11.7675" y="-3.01"/>
<vertex x="11.8375" y="-2.8875"/>
<vertex x="11.995" y="-2.7125"/>
<vertex x="12.17" y="-2.625"/>
<vertex x="12.3975" y="-2.625"/>
<vertex x="12.5025" y="-2.66"/>
<vertex x="12.66" y="-2.765"/>
<vertex x="12.7825" y="-2.9575"/>
<vertex x="12.835" y="-3.1325"/>
<vertex x="12.835" y="-3.465"/>
<vertex x="12.7825" y="-3.6225"/>
<vertex x="12.73" y="-3.7275"/>
<vertex x="12.6075" y="-3.8675"/>
<vertex x="12.485" y="-3.9375"/>
<vertex x="12.3975" y="-3.955"/>
<vertex x="12.1525" y="-3.955"/>
<vertex x="11.995" y="-3.9025"/>
<vertex x="11.855" y="-3.7625"/>
<vertex x="11.75" y="-3.5525"/>
<vertex x="11.715" y="-3.395"/>
<vertex x="11.715" y="-3.2742"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="10.105" y="-2.1875"/>
<vertex x="10.105" y="-4.445"/>
<vertex x="10.77" y="-4.445"/>
<vertex x="10.77" y="-2.1875"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="10.105" y="-1.3475"/>
<vertex x="10.105" y="-1.9075"/>
<vertex x="10.77" y="-1.9075"/>
<vertex x="10.77" y="-1.3475"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="15.9325" y="-4.445"/>
<vertex x="15.2675" y="-4.445"/>
<vertex x="15.2675" y="-2.835"/>
<vertex x="15.2325" y="-2.7825"/>
<vertex x="15.1625" y="-2.7125"/>
<vertex x="15.0925" y="-2.6775"/>
<vertex x="14.4975" y="-2.6775"/>
<vertex x="14.4975" y="-4.445"/>
<vertex x="13.85" y="-4.445"/>
<vertex x="13.85" y="-2.1875"/>
<vertex x="15.3725" y="-2.1875"/>
<vertex x="15.4425" y="-2.205"/>
<vertex x="15.53" y="-2.24"/>
<vertex x="15.67" y="-2.31"/>
<vertex x="15.7575" y="-2.3975"/>
<vertex x="15.845" y="-2.52"/>
<vertex x="15.915" y="-2.66"/>
<vertex x="15.9325" y="-2.7825"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="5.065" y="-4.445"/>
<vertex x="4.47" y="-4.445"/>
<vertex x="4.47" y="-2.835"/>
<vertex x="4.435" y="-2.7825"/>
<vertex x="4.365" y="-2.7125"/>
<vertex x="4.295" y="-2.6775"/>
<vertex x="3.84" y="-2.6775"/>
<vertex x="3.84" y="-4.375"/>
<vertex x="3.77" y="-4.445"/>
<vertex x="3.21" y="-4.445"/>
<vertex x="3.1225" y="-4.3575"/>
<vertex x="3.1225" y="-2.6775"/>
<vertex x="2.5275" y="-2.6775"/>
<vertex x="2.5275" y="-4.375"/>
<vertex x="2.4575" y="-4.445"/>
<vertex x="1.8625" y="-4.445"/>
<vertex x="1.8625" y="-2.1875"/>
<vertex x="4.575" y="-2.1875"/>
<vertex x="4.645" y="-2.205"/>
<vertex x="4.7325" y="-2.24"/>
<vertex x="4.8725" y="-2.31"/>
<vertex x="4.96" y="-2.3975"/>
<vertex x="5.0475" y="-2.52"/>
<vertex x="5.1175" y="-2.66"/>
<vertex x="5.135" y="-2.7825"/>
<vertex x="5.135" y="-4.3575"/>
<vertex x="5.065" y="-4.4275"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="7.865" y="-4.3575"/>
<vertex x="7.7775" y="-4.445"/>
<vertex x="6.115" y="-4.445"/>
<vertex x="5.835" y="-4.375"/>
<vertex x="5.625" y="-4.2"/>
<vertex x="5.5025" y="-4.025"/>
<vertex x="5.485" y="-3.9025"/>
<vertex x="5.485" y="-3.5525"/>
<vertex x="5.59" y="-3.325"/>
<vertex x="5.695" y="-3.2025"/>
<vertex x="5.765" y="-3.1675"/>
<vertex x="5.9225" y="-3.0975"/>
<vertex x="6.115" y="-3.0625"/>
<vertex x="7.1458" y="-3.0625"/>
<vertex x="7.1458" y="-3.5"/>
<vertex x="6.3425" y="-3.5"/>
<vertex x="6.2725" y="-3.535"/>
<vertex x="6.22" y="-3.57"/>
<vertex x="6.185" y="-3.6225"/>
<vertex x="6.1675" y="-3.6575"/>
<vertex x="6.15" y="-3.7275"/>
<vertex x="6.15" y="-3.78"/>
<vertex x="6.1675" y="-3.8325"/>
<vertex x="6.22" y="-3.9025"/>
<vertex x="6.2725" y="-3.9375"/>
<vertex x="6.325" y="-3.955"/>
<vertex x="7.1475" y="-3.955"/>
<vertex x="7.1475" y="-2.835"/>
<vertex x="7.1125" y="-2.7825"/>
<vertex x="7.0425" y="-2.7125"/>
<vertex x="6.9725" y="-2.6775"/>
<vertex x="5.66" y="-2.6775"/>
<vertex x="5.66" y="-2.1875"/>
<vertex x="7.2525" y="-2.1875"/>
<vertex x="7.3225" y="-2.205"/>
<vertex x="7.41" y="-2.24"/>
<vertex x="7.55" y="-2.31"/>
<vertex x="7.6375" y="-2.3975"/>
<vertex x="7.725" y="-2.52"/>
<vertex x="7.795" y="-2.66"/>
<vertex x="7.865" y="-2.94"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="8.0925" y="-2.1875"/>
<vertex x="8.0925" y="-2.6075"/>
<vertex x="8.5125" y="-2.6075"/>
<vertex x="8.5125" y="-3.9025"/>
<vertex x="8.5825" y="-4.0775"/>
<vertex x="8.7225" y="-4.2525"/>
<vertex x="8.8975" y="-4.375"/>
<vertex x="9.1425" y="-4.445"/>
<vertex x="9.7375" y="-4.445"/>
<vertex x="9.825" y="-4.375"/>
<vertex x="9.825" y="-3.955"/>
<vertex x="9.405" y="-3.955"/>
<vertex x="9.3175" y="-3.9025"/>
<vertex x="9.23" y="-3.7975"/>
<vertex x="9.1775" y="-3.6575"/>
<vertex x="9.1775" y="-2.6075"/>
<vertex x="9.825" y="-2.6075"/>
<vertex x="9.825" y="-2.1875"/>
<vertex x="9.1775" y="-2.1875"/>
<vertex x="9.1775" y="-1.3475"/>
<vertex x="8.5125" y="-1.3475"/>
<vertex x="8.5125" y="-2.1875"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-19.05" y="-3.9964"/>
<vertex x="-18.6014" y="-3.9964"/>
<vertex x="-18.6014" y="-4.445"/>
<vertex x="-19.05" y="-4.445"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-19.05" y="-3.0991"/>
<vertex x="-18.6014" y="-3.0991"/>
<vertex x="-18.6014" y="-3.5477"/>
<vertex x="-19.05" y="-3.5477"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-19.05" y="-2.2018"/>
<vertex x="-18.6014" y="-2.2018"/>
<vertex x="-18.6014" y="-2.6505"/>
<vertex x="-19.05" y="-2.6505"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-18.1527" y="-3.9964"/>
<vertex x="-17.7041" y="-3.9964"/>
<vertex x="-17.7041" y="-4.445"/>
<vertex x="-18.1527" y="-4.445"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-18.1527" y="-3.0991"/>
<vertex x="-17.7041" y="-3.0991"/>
<vertex x="-17.7041" y="-3.5477"/>
<vertex x="-18.1527" y="-3.5477"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-18.1527" y="-2.2018"/>
<vertex x="-17.7041" y="-2.2018"/>
<vertex x="-17.7041" y="-2.6505"/>
<vertex x="-18.1527" y="-2.6505"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-17.2555" y="-3.9964"/>
<vertex x="-16.8069" y="-3.9964"/>
<vertex x="-16.8069" y="-4.445"/>
<vertex x="-17.2555" y="-4.445"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-17.2555" y="-3.0991"/>
<vertex x="-16.8069" y="-3.0991"/>
<vertex x="-16.8069" y="-3.5477"/>
<vertex x="-17.2555" y="-3.5477"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-17.2555" y="-2.2018"/>
<vertex x="-16.8069" y="-2.2018"/>
<vertex x="-16.8069" y="-2.6505"/>
<vertex x="-17.2555" y="-2.6505"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-16.3582" y="-3.9964"/>
<vertex x="-15.9096" y="-3.9964"/>
<vertex x="-15.9096" y="-4.445"/>
<vertex x="-16.3582" y="-4.445"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-16.3582" y="-3.0991"/>
<vertex x="-15.9096" y="-3.0991"/>
<vertex x="-15.9096" y="-3.5477"/>
<vertex x="-16.3582" y="-3.5477"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-16.3582" y="-2.2018"/>
<vertex x="-15.9096" y="-2.2018"/>
<vertex x="-15.9096" y="-2.6505"/>
<vertex x="-16.3582" y="-2.6505"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-15.4609" y="-3.9964"/>
<vertex x="-15.0123" y="-3.9964"/>
<vertex x="-15.0123" y="-4.445"/>
<vertex x="-15.4609" y="-4.445"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-15.4609" y="-3.0991"/>
<vertex x="-15.0123" y="-3.0991"/>
<vertex x="-15.0123" y="-3.5477"/>
<vertex x="-15.4609" y="-3.5477"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-15.4609" y="-2.2018"/>
<vertex x="-15.0123" y="-2.2018"/>
<vertex x="-15.0123" y="-2.6505"/>
<vertex x="-15.4609" y="-2.6505"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-14.5637" y="-3.9964"/>
<vertex x="-14.1151" y="-3.9964"/>
<vertex x="-14.1151" y="-4.445"/>
<vertex x="-14.5637" y="-4.445"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-14.5637" y="-3.0991"/>
<vertex x="-14.1151" y="-3.0991"/>
<vertex x="-14.1151" y="-3.5477"/>
<vertex x="-14.5637" y="-3.5477"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-14.5637" y="-2.2018"/>
<vertex x="-14.1151" y="-2.2018"/>
<vertex x="-14.1151" y="-2.6505"/>
<vertex x="-14.5637" y="-2.6505"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-12.7691" y="-2.6505"/>
<vertex x="-12.7691" y="-2.2019"/>
<vertex x="-12.3205" y="-2.2019"/>
<vertex x="-12.3205" y="-2.6505"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-13.6664" y="-2.6505"/>
<vertex x="-13.6664" y="-2.2019"/>
<vertex x="-13.2177" y="-2.2019"/>
<vertex x="-13.2177" y="-2.6505"/>
</polygon>
</symbol>
<symbol name="A4_LOC3">
<wire x1="-257.175" y1="-3.175" x2="-254" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="-254" y1="-3.175" x2="-15.875" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="3.175" y1="-3.175" x2="3.175" y2="22.86" width="0.1016" layer="94"/>
<wire x1="3.175" y1="22.86" x2="3.175" y2="48.26" width="0.1016" layer="94"/>
<wire x1="3.175" y1="48.26" x2="3.175" y2="73.66" width="0.1016" layer="94"/>
<wire x1="3.175" y1="73.66" x2="3.175" y2="99.06" width="0.1016" layer="94"/>
<wire x1="3.175" y1="99.06" x2="3.175" y2="124.46" width="0.1016" layer="94"/>
<wire x1="3.175" y1="124.46" x2="3.175" y2="175.895" width="0.1016" layer="94"/>
<wire x1="-111.125" y1="175.895" x2="-158.75" y2="175.895" width="0.1524" layer="94"/>
<wire x1="-158.75" y1="175.895" x2="-257.175" y2="175.895" width="0.1016" layer="94"/>
<wire x1="-257.175" y1="175.895" x2="-257.175" y2="149.86" width="0.1016" layer="94"/>
<wire x1="-257.175" y1="149.86" x2="-257.175" y2="124.46" width="0.1016" layer="94"/>
<wire x1="-257.175" y1="124.46" x2="-257.175" y2="99.06" width="0.1016" layer="94"/>
<wire x1="-257.175" y1="99.06" x2="-257.175" y2="73.66" width="0.1016" layer="94"/>
<wire x1="-257.175" y1="73.66" x2="-257.175" y2="48.26" width="0.1016" layer="94"/>
<wire x1="-257.175" y1="48.26" x2="-257.175" y2="22.86" width="0.1016" layer="94"/>
<wire x1="-257.175" y1="22.86" x2="-257.175" y2="0" width="0.1016" layer="94"/>
<wire x1="-257.175" y1="0" x2="-257.175" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="48.26" width="0.1016" layer="94"/>
<wire x1="0" y1="48.26" x2="0" y2="73.66" width="0.1016" layer="94"/>
<wire x1="0" y1="73.66" x2="0" y2="99.06" width="0.1016" layer="94"/>
<wire x1="0" y1="99.06" x2="0" y2="124.46" width="0.1016" layer="94"/>
<wire x1="0" y1="124.46" x2="0" y2="149.86" width="0.1016" layer="94"/>
<wire x1="0" y1="149.86" x2="0" y2="172.72" width="0.1016" layer="94"/>
<wire x1="-111.125" y1="172.72" x2="-158.75" y2="172.72" width="0.1524" layer="94"/>
<wire x1="-158.75" y1="172.72" x2="-254" y2="172.72" width="0.1016" layer="94"/>
<wire x1="-254" y1="172.72" x2="-254" y2="124.46" width="0.1016" layer="94"/>
<wire x1="-254" y1="124.46" x2="-254" y2="99.06" width="0.1016" layer="94"/>
<wire x1="-254" y1="99.06" x2="-254" y2="73.66" width="0.1016" layer="94"/>
<wire x1="-254" y1="73.66" x2="-254" y2="48.26" width="0.1016" layer="94"/>
<wire x1="-254" y1="48.26" x2="-254" y2="22.86" width="0.1016" layer="94"/>
<wire x1="-254" y1="22.86" x2="-254" y2="0" width="0.1016" layer="94"/>
<wire x1="-257.175" y1="48.26" x2="-254" y2="48.26" width="0.1016" layer="94"/>
<wire x1="0" y1="73.66" x2="3.175" y2="73.66" width="0.1016" layer="94"/>
<wire x1="-257.175" y1="22.86" x2="-254" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="48.26" x2="3.175" y2="48.26" width="0.1016" layer="94"/>
<wire x1="-257.175" y1="73.66" x2="-254" y2="73.66" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="3.175" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="172.72" x2="-25.4" y2="172.72" width="0.1016" layer="94"/>
<wire x1="-25.4" y1="172.72" x2="-50.8" y2="172.72" width="0.1016" layer="94"/>
<wire x1="-50.8" y1="172.72" x2="-111.125" y2="172.72" width="0.1016" layer="94"/>
<wire x1="3.175" y1="175.895" x2="-25.4" y2="175.895" width="0.1016" layer="94"/>
<wire x1="-25.4" y1="175.895" x2="-111.125" y2="175.895" width="0.1016" layer="94"/>
<wire x1="-254" y1="0" x2="-63.627" y2="0" width="0.1016" layer="94"/>
<wire x1="-63.246" y1="0" x2="-15.875" y2="0" width="0.1016" layer="94"/>
<wire x1="-15.748" y1="0" x2="0" y2="0" width="0.1016" layer="94"/>
<wire x1="-15.748" y1="-3.175" x2="3.175" y2="-3.175" width="0.1016" layer="94"/>
<wire x1="-25.4" y1="172.72" x2="-25.4" y2="175.895" width="0.1016" layer="94"/>
<wire x1="-50.8" y1="172.72" x2="-50.8" y2="175.895" width="0.1016" layer="94"/>
<wire x1="-76.2" y1="172.72" x2="-76.2" y2="175.895" width="0.1016" layer="94"/>
<wire x1="-101.6" y1="172.72" x2="-101.6" y2="175.895" width="0.1016" layer="94"/>
<wire x1="-127" y1="172.72" x2="-127" y2="175.895" width="0.1016" layer="94"/>
<wire x1="-152.4" y1="172.72" x2="-152.4" y2="175.895" width="0.1016" layer="94"/>
<wire x1="-177.8" y1="172.72" x2="-177.8" y2="175.895" width="0.1016" layer="94"/>
<wire x1="-203.2" y1="172.72" x2="-203.2" y2="175.895" width="0.1016" layer="94"/>
<wire x1="-228.6" y1="172.72" x2="-228.6" y2="175.895" width="0.1016" layer="94"/>
<wire x1="-25.4" y1="-3.175" x2="-25.4" y2="0" width="0.1016" layer="94"/>
<wire x1="-50.8" y1="-3.175" x2="-50.8" y2="0" width="0.1016" layer="94"/>
<wire x1="-76.2" y1="-3.175" x2="-76.2" y2="0" width="0.1016" layer="94"/>
<wire x1="-101.6" y1="-3.175" x2="-101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="-127" y1="-3.175" x2="-127" y2="0" width="0.1016" layer="94"/>
<wire x1="-152.4" y1="-3.175" x2="-152.4" y2="0" width="0.1016" layer="94"/>
<wire x1="-177.8" y1="-3.175" x2="-177.8" y2="0" width="0.1016" layer="94"/>
<wire x1="-203.2" y1="-3.175" x2="-203.2" y2="0" width="0.1016" layer="94"/>
<wire x1="-228.6" y1="-3.175" x2="-228.6" y2="0" width="0.1016" layer="94"/>
<wire x1="-257.175" y1="99.06" x2="-254" y2="99.06" width="0.1016" layer="94"/>
<wire x1="-257.175" y1="124.46" x2="-254" y2="124.46" width="0.1016" layer="94"/>
<wire x1="-257.175" y1="149.86" x2="-254" y2="149.86" width="0.1016" layer="94"/>
<wire x1="0" y1="99.06" x2="3.175" y2="99.06" width="0.1016" layer="94"/>
<wire x1="0" y1="124.46" x2="3.175" y2="124.46" width="0.1016" layer="94"/>
<wire x1="0" y1="149.86" x2="3.175" y2="149.86" width="0.1016" layer="94"/>
<text x="-242.951" y="173.101" size="2.54" layer="94">1</text>
<text x="-217.297" y="173.101" size="2.54" layer="94">2</text>
<text x="-192.151" y="173.101" size="2.54" layer="94">3</text>
<text x="-165.227" y="173.101" size="2.54" layer="94">4</text>
<text x="-140.335" y="173.101" size="2.54" layer="94">5</text>
<text x="-114.173" y="173.101" size="2.54" layer="94">6</text>
<text x="-88.773" y="173.101" size="2.54" layer="94">7</text>
<text x="-63.373" y="173.101" size="2.54" layer="94">8</text>
<text x="-37.973" y="173.101" size="2.54" layer="94">9</text>
<text x="-12.573" y="173.101" size="2.54" layer="94">10</text>
<text x="-256.159" y="10.795" size="2.54" layer="94">A</text>
<text x="-256.413" y="35.433" size="2.54" layer="94">B</text>
<text x="-256.413" y="61.087" size="2.54" layer="94">C</text>
<text x="-256.667" y="86.233" size="2.54" layer="94">D</text>
<text x="-256.667" y="111.633" size="2.54" layer="94">E</text>
<text x="-256.667" y="137.033" size="2.54" layer="94">F</text>
<text x="-256.667" y="159.893" size="2.54" layer="94">G</text>
<text x="-242.951" y="-2.794" size="2.54" layer="94">1</text>
<text x="-217.297" y="-2.794" size="2.54" layer="94">2</text>
<text x="-192.151" y="-2.794" size="2.54" layer="94">3</text>
<text x="-165.227" y="-2.794" size="2.54" layer="94">4</text>
<text x="-140.335" y="-2.794" size="2.54" layer="94">5</text>
<text x="-114.173" y="-2.794" size="2.54" layer="94">6</text>
<text x="-88.773" y="-2.794" size="2.54" layer="94">7</text>
<text x="-63.373" y="-2.794" size="2.54" layer="94">8</text>
<text x="-37.973" y="-2.794" size="2.54" layer="94">9</text>
<text x="-12.573" y="-2.794" size="2.54" layer="94">10</text>
<text x="0.8255" y="159.893" size="2.54" layer="94">G</text>
<text x="0.8255" y="10.033" size="2.54" layer="94">A</text>
<text x="0.8255" y="35.433" size="2.54" layer="94">B</text>
<text x="0.8255" y="60.833" size="2.54" layer="94">C</text>
<text x="0.8255" y="86.233" size="2.54" layer="94">D</text>
<text x="0.8255" y="111.633" size="2.54" layer="94">E</text>
<text x="0.8255" y="137.033" size="2.54" layer="94">F</text>
</symbol>
<symbol name="B4_LOC">
<wire x1="-317.175" y1="-3.175" x2="-17.875" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="3.175" y1="-3.175" x2="3.175" y2="58.8" width="0.1524" layer="94"/>
<wire x1="3.175" y1="58.8" x2="3.175" y2="117.6" width="0.1524" layer="94"/>
<wire x1="3.175" y1="117.6" x2="3.175" y2="176.4" width="0.1524" layer="94"/>
<wire x1="3.175" y1="176.4" x2="3.175" y2="207.895" width="0.1524" layer="94"/>
<wire x1="-317.175" y1="207.895" x2="-317.175" y2="176.4" width="0.1524" layer="94"/>
<wire x1="-317.175" y1="176.4" x2="-317.175" y2="117.6" width="0.1524" layer="94"/>
<wire x1="-317.175" y1="117.6" x2="-317.175" y2="58.8" width="0.1524" layer="94"/>
<wire x1="-317.175" y1="58.8" x2="-317.175" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="58.8" width="0.1524" layer="94"/>
<wire x1="0" y1="58.8" x2="0" y2="117.6" width="0.1524" layer="94"/>
<wire x1="0" y1="117.6" x2="0" y2="176.4" width="0.1524" layer="94"/>
<wire x1="0" y1="176.4" x2="0" y2="204.72" width="0.1524" layer="94"/>
<wire x1="-314" y1="204.72" x2="-314" y2="176.4" width="0.1524" layer="94"/>
<wire x1="-314" y1="176.4" x2="-314" y2="117.6" width="0.1524" layer="94"/>
<wire x1="-314" y1="117.6" x2="-314" y2="58.8" width="0.1524" layer="94"/>
<wire x1="-314" y1="58.8" x2="-314" y2="0" width="0.1524" layer="94"/>
<wire x1="-317.175" y1="117.6" x2="-314" y2="117.6" width="0.1016" layer="94"/>
<wire x1="0" y1="176.4" x2="3.175" y2="176.4" width="0.1016" layer="94"/>
<wire x1="-317.175" y1="58.8" x2="-314" y2="58.8" width="0.1016" layer="94"/>
<wire x1="0" y1="117.6" x2="3.175" y2="117.6" width="0.1016" layer="94"/>
<wire x1="-317.175" y1="176.4" x2="-314" y2="176.4" width="0.1016" layer="94"/>
<wire x1="0" y1="58.8" x2="3.175" y2="58.8" width="0.1016" layer="94"/>
<wire x1="0" y1="204.72" x2="-31.4" y2="204.72" width="0.1524" layer="94"/>
<wire x1="-31.4" y1="204.72" x2="-62.8" y2="204.72" width="0.1524" layer="94"/>
<wire x1="-62.8" y1="204.72" x2="-314" y2="204.72" width="0.1524" layer="94"/>
<wire x1="3.175" y1="207.895" x2="-31.4" y2="207.895" width="0.1524" layer="94"/>
<wire x1="-31.4" y1="207.895" x2="-317.175" y2="207.895" width="0.1524" layer="94"/>
<wire x1="-314" y1="0" x2="-79.627" y2="0" width="0.1524" layer="94"/>
<wire x1="-79.246" y1="0" x2="-17.875" y2="0" width="0.1524" layer="94"/>
<wire x1="-17.748" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-17.748" y1="-3.175" x2="3.175" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="-31.4" y1="204.72" x2="-31.4" y2="207.895" width="0.1016" layer="94"/>
<wire x1="-62.8" y1="204.72" x2="-62.8" y2="207.895" width="0.1016" layer="94"/>
<wire x1="-94.2" y1="204.72" x2="-94.2" y2="207.895" width="0.1016" layer="94"/>
<wire x1="-125.6" y1="204.72" x2="-125.6" y2="207.895" width="0.1016" layer="94"/>
<wire x1="-157" y1="204.72" x2="-157" y2="207.895" width="0.1016" layer="94"/>
<wire x1="-188.4" y1="204.72" x2="-188.4" y2="207.895" width="0.1016" layer="94"/>
<wire x1="-219.8" y1="204.72" x2="-219.8" y2="207.895" width="0.1016" layer="94"/>
<wire x1="-251.2" y1="204.72" x2="-251.2" y2="207.895" width="0.1016" layer="94"/>
<wire x1="-282.6" y1="204.72" x2="-282.6" y2="207.895" width="0.1016" layer="94"/>
<wire x1="-31.4" y1="-3.175" x2="-31.4" y2="0" width="0.1016" layer="94"/>
<wire x1="-62.8" y1="-3.175" x2="-62.8" y2="0" width="0.1016" layer="94"/>
<wire x1="-94.2" y1="-3.175" x2="-94.2" y2="0" width="0.1016" layer="94"/>
<wire x1="-125.6" y1="-3.175" x2="-125.6" y2="0" width="0.1016" layer="94"/>
<wire x1="-157" y1="-3.175" x2="-157" y2="0" width="0.1016" layer="94"/>
<wire x1="-188.4" y1="-3.175" x2="-188.4" y2="0" width="0.1016" layer="94"/>
<wire x1="-219.8" y1="-3.175" x2="-219.8" y2="0" width="0.1016" layer="94"/>
<wire x1="-251.2" y1="-3.175" x2="-251.2" y2="0" width="0.1016" layer="94"/>
<wire x1="-282.6" y1="-3.175" x2="-282.6" y2="0" width="0.1016" layer="94"/>
<text x="0.889" y="29.527" size="2.54" layer="94">1</text>
<text x="0.635" y="88.327" size="2.54" layer="94">2</text>
<text x="0.635" y="147.127" size="2.54" layer="94">3</text>
<text x="-172.335" y="205.101" size="2.54" layer="94">E</text>
<text x="-205.227" y="205.101" size="2.54" layer="94">D</text>
<text x="-236.151" y="205.101" size="2.54" layer="94">C</text>
<text x="-267.297" y="205.101" size="2.54" layer="94">B</text>
<text x="-298.951" y="205.101" size="2.54" layer="94">A</text>
<text x="-316.413" y="147.127" size="2.54" layer="94">3</text>
<text x="-316.413" y="88.073" size="2.54" layer="94">2</text>
<text x="-316.159" y="27.495" size="2.54" layer="94">1</text>
<text x="-140.173" y="205.101" size="2.54" layer="94">F</text>
<text x="0.381" y="192.973" size="2.54" layer="94">4</text>
<text x="-316.667" y="192.973" size="2.54" layer="94">4</text>
<text x="-110.773" y="205.101" size="2.54" layer="94">G</text>
<text x="-79.373" y="205.101" size="2.54" layer="94">H</text>
<text x="-45.973" y="205.101" size="2.54" layer="94">I</text>
<text x="-14.573" y="205.101" size="2.54" layer="94">J</text>
<text x="-172.335" y="-2.794" size="2.54" layer="94">E</text>
<text x="-205.227" y="-2.794" size="2.54" layer="94">D</text>
<text x="-236.151" y="-2.794" size="2.54" layer="94">C</text>
<text x="-267.297" y="-2.794" size="2.54" layer="94">B</text>
<text x="-298.951" y="-2.794" size="2.54" layer="94">A</text>
<text x="-140.173" y="-2.794" size="2.54" layer="94">F</text>
<text x="-110.773" y="-2.794" size="2.54" layer="94">G</text>
<text x="-79.373" y="-2.794" size="2.54" layer="94">H</text>
<text x="-45.973" y="-2.794" size="2.54" layer="94">I</text>
<text x="-14.573" y="-2.794" size="2.54" layer="94">J</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="SCHRIFTF" prefix="SF" uservalue="yes">
<gates>
<gate name="FRAME_A3" symbol="A3_LOC" x="0" y="0" addlevel="request"/>
<gate name="FRAME_A1" symbol="A1_LOC" x="0" y="0" addlevel="request"/>
<gate name="FRAME_A0" symbol="A0_LOC" x="0" y="0" addlevel="request"/>
<gate name="FRAME_A2" symbol="A2_LOC" x="0" y="0" addlevel="request"/>
<gate name="G$1" symbol="A4_LOC2" x="0" y="0" addlevel="request"/>
<gate name="G$2" symbol="SCHRIFTF" x="0" y="0"/>
<gate name="FRAME_A4" symbol="A4_LOC3" x="0" y="0"/>
<gate name="FRAME_B4" symbol="B4_LOC" x="0" y="0" addlevel="request"/>
</gates>
<devices>
<device name="" package="SCHRIFTF">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="HAUTOMAT@3">
<packages>
<package name="WE-LQ121">
<wire x1="1.6" y1="1.25" x2="1.2" y2="1.25" width="0.127" layer="20"/>
<wire x1="1.2" y1="1.25" x2="-1.2" y2="1.25" width="0.127" layer="20"/>
<wire x1="-1.2" y1="1.25" x2="-1.6" y2="1.25" width="0.127" layer="20"/>
<wire x1="-1.6" y1="1.25" x2="-1.6" y2="-1.25" width="0.127" layer="20"/>
<wire x1="-1.6" y1="-1.25" x2="-1.2" y2="-1.25" width="0.127" layer="20"/>
<wire x1="-1.2" y1="-1.25" x2="1.6" y2="-1.25" width="0.127" layer="20"/>
<wire x1="1.6" y1="-1.25" x2="1.6" y2="1.25" width="0.127" layer="20"/>
<wire x1="-1.2" y1="1.25" x2="-1.2" y2="-1.25" width="0.127" layer="20"/>
<wire x1="1.2" y1="-1.25" x2="1.2" y2="1.25" width="0.127" layer="20"/>
<smd name="2" x="1.2" y="0" dx="1.4" dy="2.8" layer="1"/>
<smd name="1" x="-1.2" y="0" dx="1.4" dy="2.8" layer="1"/>
<text x="-1.27" y="-2.2225" size="0.8128" layer="25">&gt;Name</text>
<text x="-1.27" y="-3.175" size="0.8128" layer="27">&gt;Value</text>
</package>
<package name="WE-PD3">
<wire x1="-9.2075" y1="1.27" x2="-9.2075" y2="2.2225" width="0.127" layer="21"/>
<wire x1="-9.2075" y1="2.2225" x2="-6.6675" y2="3.4925" width="0.127" layer="21"/>
<wire x1="-9.2075" y1="-1.27" x2="-9.2075" y2="-2.2225" width="0.127" layer="21"/>
<wire x1="-6.6675" y1="-3.4925" x2="-9.2075" y2="-2.2225" width="0.127" layer="22"/>
<wire x1="9.2075" y1="-2.2225" x2="6.6675" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="6.6675" y1="3.4925" x2="9.2075" y2="2.2225" width="0.127" layer="22"/>
<wire x1="9.2075" y1="1.27" x2="9.2075" y2="2.2225" width="0.127" layer="21"/>
<wire x1="9.2075" y1="-1.27" x2="9.2075" y2="-2.2225" width="0.127" layer="21"/>
<wire x1="-6.6675" y1="3.4925" x2="6.6675" y2="3.4925" width="0.127" layer="21" curve="-124.708049"/>
<wire x1="-6.6675" y1="-3.4925" x2="6.6675" y2="-3.4925" width="0.127" layer="21" curve="124.708049"/>
<circle x="0" y="0" radius="7.525" width="0.127" layer="1"/>
<circle x="0" y="0" radius="6.2378" width="0.127" layer="21"/>
<circle x="0" y="0" radius="5.7938" width="0.127" layer="21"/>
<smd name="1" x="-7.725" y="0" dx="3" dy="3" layer="1"/>
<smd name="2" x="7.725" y="0" dx="3" dy="3" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="0" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="6.35" y1="-1.27" x2="8.89" y2="1.27" layer="51"/>
<rectangle x1="-8.89" y1="-1.27" x2="-6.35" y2="1.27" layer="51"/>
</package>
<package name="WE-PD3@2">
<wire x1="-9.2075" y1="1.27" x2="-9.2075" y2="2.2225" width="0.127" layer="21"/>
<wire x1="-9.2075" y1="2.2225" x2="-6.6675" y2="3.4925" width="0.127" layer="21"/>
<wire x1="-9.2075" y1="-1.27" x2="-9.2075" y2="-2.2225" width="0.127" layer="21"/>
<wire x1="-6.6675" y1="-3.4925" x2="-9.2075" y2="-2.2225" width="0.127" layer="22"/>
<wire x1="9.2075" y1="-2.2225" x2="6.6675" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="6.6675" y1="3.4925" x2="9.2075" y2="2.2225" width="0.127" layer="22"/>
<wire x1="9.2075" y1="1.27" x2="9.2075" y2="2.2225" width="0.127" layer="21"/>
<wire x1="9.2075" y1="-1.27" x2="9.2075" y2="-2.2225" width="0.127" layer="21"/>
<wire x1="-6.6675" y1="3.4925" x2="6.6675" y2="3.4925" width="0.127" layer="21" curve="-124.708049"/>
<wire x1="-6.6675" y1="-3.4925" x2="6.6675" y2="-3.4925" width="0.127" layer="21" curve="124.708049"/>
<circle x="0" y="0" radius="6.2378" width="0.127" layer="21"/>
<circle x="0" y="0" radius="5.7938" width="0.127" layer="21"/>
<smd name="1" x="-7.725" y="0" dx="3" dy="3" layer="1"/>
<smd name="2" x="7.725" y="0" dx="3" dy="3" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="0" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="6.35" y1="-1.27" x2="8.89" y2="1.27" layer="51"/>
<rectangle x1="-8.89" y1="-1.27" x2="-6.35" y2="1.27" layer="51"/>
</package>
<package name="WE-PD3@3">
<wire x1="-9.2075" y1="1.27" x2="-9.2075" y2="2.2225" width="0.127" layer="21"/>
<wire x1="-9.2075" y1="2.2225" x2="-6.6675" y2="3.4925" width="0.127" layer="21"/>
<wire x1="-9.2075" y1="-1.27" x2="-9.2075" y2="-2.2225" width="0.127" layer="21"/>
<wire x1="-6.6675" y1="-3.4925" x2="-9.2075" y2="-2.2225" width="0.127" layer="21"/>
<wire x1="9.2075" y1="-2.2225" x2="6.6675" y2="-3.4925" width="0.127" layer="21"/>
<wire x1="6.6675" y1="3.4925" x2="9.2075" y2="2.2225" width="0.127" layer="21"/>
<wire x1="9.2075" y1="1.27" x2="9.2075" y2="2.2225" width="0.127" layer="21"/>
<wire x1="9.2075" y1="-1.27" x2="9.2075" y2="-2.2225" width="0.127" layer="21"/>
<wire x1="-6.6675" y1="3.4925" x2="6.6675" y2="3.4925" width="0.127" layer="21" curve="-124.708049"/>
<wire x1="-6.6675" y1="-3.4925" x2="6.6675" y2="-3.4925" width="0.127" layer="21" curve="124.708049"/>
<circle x="0" y="0" radius="6.2378" width="0.127" layer="21"/>
<circle x="0" y="0" radius="5.7938" width="0.127" layer="21"/>
<smd name="1" x="-7.725" y="0" dx="3" dy="3" layer="1"/>
<smd name="2" x="7.725" y="0" dx="3" dy="3" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="0" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="6.35" y1="-1.27" x2="8.89" y2="1.27" layer="51"/>
<rectangle x1="-8.89" y1="-1.27" x2="-6.35" y2="1.27" layer="51"/>
</package>
<package name="WE-PD12">
<wire x1="-5" y1="6" x2="5" y2="6" width="0.127" layer="21"/>
<wire x1="-6" y1="-5" x2="-6" y2="5" width="0.127" layer="21"/>
<wire x1="5" y1="-6" x2="-5" y2="-6" width="0.127" layer="21"/>
<wire x1="6" y1="-5" x2="6" y2="5" width="0.127" layer="21"/>
<wire x1="-6" y1="5" x2="-5" y2="6" width="0.127" layer="21" curve="-90"/>
<wire x1="5" y1="6" x2="6" y2="5" width="0.127" layer="21" curve="-90"/>
<wire x1="5" y1="-6" x2="6" y2="-5" width="0.127" layer="21" curve="90"/>
<wire x1="-6" y1="-5" x2="-5" y2="-6" width="0.127" layer="21" curve="90"/>
<wire x1="-4.5" y1="2.5" x2="-2.5" y2="4.5" width="0.127" layer="21" curve="-180"/>
<wire x1="2.5" y1="-4.5" x2="4.5" y2="-2.5" width="0.127" layer="21" curve="180"/>
<wire x1="2.5" y1="4.5" x2="4.5" y2="2.5" width="0.127" layer="21" curve="-180"/>
<wire x1="-4.5" y1="-2.5" x2="-2.5" y2="-4.5" width="0.127" layer="21" curve="180"/>
<circle x="0" y="0" radius="5" width="0.127" layer="21"/>
<smd name="1" x="0" y="5.5" dx="5.5" dy="3.5" layer="1"/>
<smd name="2" x="0" y="-5.5" dx="5.5" dy="3.5" layer="1"/>
<text x="-2.405" y="0.135" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.405" y="-1.135" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.5" y1="4" x2="2.5" y2="6" layer="51"/>
<rectangle x1="-2.5" y1="-6" x2="2.5" y2="-4" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="L_KERN">
<wire x1="-3.81" y1="1.651" x2="3.81" y2="1.651" width="0.254" layer="94"/>
<text x="-3.81" y="2.286" size="1.524" layer="95">&gt;NAME</text>
<text x="-3.937" y="-3.048" size="1.524" layer="96">&gt;VALUE</text>
<rectangle x1="-3.81" y1="-0.889" x2="3.81" y2="0.889" layer="94"/>
<pin name="2" x="7.62" y="0" visible="off" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-7.62" y="0" visible="off" length="middle" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="WE-LQ121" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="L_KERN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="WE-LQ121">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="WE-LQ121@2" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="L_KERN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="WE-PD3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="WE-LQ121@3" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="L_KERN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="WE-PD3@2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="WE-LQ121@4" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="L_KERN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="WE-PD3@3">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="WE-LQ121@5" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="L_KERN" x="0" y="0"/>
</gates>
<devices>
<device name="" package="WE-PD12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND" uservalue="yes">
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-lsta">
<packages>
<package name="FE05-1">
<description>&lt;b&gt;FEMALE HEADER&lt;/b&gt;</description>
<wire x1="-6.35" y1="1.27" x2="-6.35" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-1.27" x2="-4.064" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.27" x2="-3.81" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.016" x2="-3.556" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="-1.27" x2="-1.524" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-1.27" x2="-1.27" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.016" x2="-1.016" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="-1.27" x2="1.016" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.016" y1="-1.27" x2="1.27" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.016" x2="1.524" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-1.27" x2="3.556" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.556" y1="-1.27" x2="3.81" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.016" x2="4.064" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.27" x2="6.35" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-1.27" x2="6.35" y2="1.27" width="0.1524" layer="21"/>
<wire x1="6.35" y1="1.27" x2="4.064" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.27" x2="3.81" y2="1.016" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.016" x2="3.556" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.556" y1="1.27" x2="1.524" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.524" y1="1.27" x2="1.27" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.016" x2="1.016" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.016" y1="1.27" x2="-1.016" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="1.27" x2="-1.27" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.016" x2="-1.524" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="1.27" x2="-3.556" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="1.27" x2="-3.81" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.016" x2="-4.064" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="1.27" x2="-6.35" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-5.334" y1="0.762" x2="-5.334" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-5.334" y1="0.508" x2="-5.588" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-5.588" y1="0.508" x2="-5.588" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-5.588" y1="-0.508" x2="-5.334" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-5.334" y1="-0.508" x2="-5.334" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-5.334" y1="-0.762" x2="-4.826" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-4.826" y1="-0.762" x2="-4.826" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-4.826" y1="-0.508" x2="-4.572" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-4.572" y1="-0.508" x2="-4.572" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-4.572" y1="0.508" x2="-4.826" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-4.826" y1="0.508" x2="-4.826" y2="0.762" width="0.1524" layer="51"/>
<wire x1="-4.826" y1="0.762" x2="-5.334" y2="0.762" width="0.1524" layer="51"/>
<wire x1="-2.794" y1="0.762" x2="-2.794" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-2.794" y1="0.508" x2="-3.048" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-3.048" y1="0.508" x2="-3.048" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-3.048" y1="-0.508" x2="-2.794" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-2.794" y1="-0.508" x2="-2.794" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-2.794" y1="-0.762" x2="-2.286" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-2.286" y1="-0.762" x2="-2.286" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-2.286" y1="-0.508" x2="-2.032" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-2.032" y1="-0.508" x2="-2.032" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-2.032" y1="0.508" x2="-2.286" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-2.286" y1="0.508" x2="-2.286" y2="0.762" width="0.1524" layer="51"/>
<wire x1="-2.286" y1="0.762" x2="-2.794" y2="0.762" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="0.508" x2="-0.508" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0.508" x2="-0.508" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="-0.508" x2="-0.254" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-0.508" x2="-0.254" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-0.762" x2="0.254" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="0.254" y1="-0.762" x2="0.254" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="0.254" y1="-0.508" x2="0.508" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="0.508" y1="-0.508" x2="0.508" y2="0.508" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0.508" x2="0.254" y2="0.508" width="0.1524" layer="51"/>
<wire x1="0.254" y1="0.508" x2="0.254" y2="0.762" width="0.1524" layer="51"/>
<wire x1="0.254" y1="0.762" x2="-0.254" y2="0.762" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0.762" x2="2.286" y2="0.508" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0.508" x2="2.032" y2="0.508" width="0.1524" layer="51"/>
<wire x1="2.032" y1="0.508" x2="2.032" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="2.032" y1="-0.508" x2="2.286" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="2.286" y1="-0.508" x2="2.286" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="2.286" y1="-0.762" x2="2.794" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="2.794" y1="-0.762" x2="2.794" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="2.794" y1="-0.508" x2="3.048" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="3.048" y1="-0.508" x2="3.048" y2="0.508" width="0.1524" layer="51"/>
<wire x1="3.048" y1="0.508" x2="2.794" y2="0.508" width="0.1524" layer="51"/>
<wire x1="2.794" y1="0.508" x2="2.794" y2="0.762" width="0.1524" layer="51"/>
<wire x1="2.794" y1="0.762" x2="2.286" y2="0.762" width="0.1524" layer="51"/>
<wire x1="4.826" y1="0.762" x2="4.826" y2="0.508" width="0.1524" layer="51"/>
<wire x1="4.826" y1="0.508" x2="4.572" y2="0.508" width="0.1524" layer="51"/>
<wire x1="4.572" y1="0.508" x2="4.572" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="4.572" y1="-0.508" x2="4.826" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="4.826" y1="-0.508" x2="4.826" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="4.826" y1="-0.762" x2="5.334" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="5.334" y1="-0.762" x2="5.334" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="5.334" y1="-0.508" x2="5.588" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="5.588" y1="-0.508" x2="5.588" y2="0.508" width="0.1524" layer="51"/>
<wire x1="5.588" y1="0.508" x2="5.334" y2="0.508" width="0.1524" layer="51"/>
<wire x1="5.334" y1="0.508" x2="5.334" y2="0.762" width="0.1524" layer="51"/>
<wire x1="5.334" y1="0.762" x2="4.826" y2="0.762" width="0.1524" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-2.54" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="0" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="2.54" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="5.08" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-6.35" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-7.493" y="-0.635" size="1.27" layer="21" ratio="10">1</text>
<text x="6.604" y="-0.635" size="1.27" layer="21" ratio="10">5</text>
<text x="0" y="1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.207" y1="0.254" x2="-4.953" y2="0.762" layer="51"/>
<rectangle x1="-5.207" y1="-0.762" x2="-4.953" y2="-0.254" layer="51"/>
<rectangle x1="-2.667" y1="0.254" x2="-2.413" y2="0.762" layer="51"/>
<rectangle x1="-2.667" y1="-0.762" x2="-2.413" y2="-0.254" layer="51"/>
<rectangle x1="-0.127" y1="0.254" x2="0.127" y2="0.762" layer="51"/>
<rectangle x1="-0.127" y1="-0.762" x2="0.127" y2="-0.254" layer="51"/>
<rectangle x1="2.413" y1="0.254" x2="2.667" y2="0.762" layer="51"/>
<rectangle x1="2.413" y1="-0.762" x2="2.667" y2="-0.254" layer="51"/>
<rectangle x1="4.953" y1="0.254" x2="5.207" y2="0.762" layer="51"/>
<rectangle x1="4.953" y1="-0.762" x2="5.207" y2="-0.254" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="FE05-1">
<wire x1="3.81" y1="-7.62" x2="-1.27" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.905" y1="0.635" x2="1.905" y2="-0.635" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="1.905" y1="-1.905" x2="1.905" y2="-3.175" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="1.905" y1="-4.445" x2="1.905" y2="-5.715" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="-1.27" y1="7.62" x2="-1.27" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.905" y1="5.715" x2="1.905" y2="4.445" width="0.254" layer="94" curve="-180" cap="flat"/>
<wire x1="1.905" y1="3.175" x2="1.905" y2="1.905" width="0.254" layer="94" curve="-180" cap="flat"/>
<text x="-1.27" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.27" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FE05-1" prefix="SV" uservalue="yes">
<description>&lt;b&gt;FEMALE HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="FE05-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FE05-1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="HAuto_CO">
<packages>
<package name="LPV4">
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-2.54" x2="-3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-2.54" x2="3.81" y2="-2.54" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="2.54" width="0.127" layer="21"/>
<wire x1="5.08" y1="2.54" x2="-5.08" y2="2.54" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-2.54" x2="-3.81" y2="-1.905" width="0.127" layer="21"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.127" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.127" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.127" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="-3.81" y2="-1.905" width="0.127" layer="21"/>
<wire x1="5.29" y1="3.4" x2="-5.29" y2="3.4" width="0.127" layer="51"/>
<wire x1="-5.29" y1="3.4" x2="-5.29" y2="-3.75" width="0.127" layer="51"/>
<wire x1="-5.29" y1="-3.75" x2="-3.94" y2="-3.75" width="0.127" layer="51"/>
<wire x1="-3.94" y1="-3.75" x2="-3.94" y2="-2.2" width="0.127" layer="51"/>
<wire x1="-3.94" y1="-2.2" x2="3.94" y2="-2.2" width="0.127" layer="51"/>
<wire x1="3.94" y1="-2.2" x2="3.94" y2="-3.75" width="0.127" layer="51"/>
<wire x1="3.94" y1="-3.75" x2="5.29" y2="-3.75" width="0.127" layer="51"/>
<wire x1="5.29" y1="-3.75" x2="5.29" y2="3.4" width="0.127" layer="51"/>
<wire x1="-4.04" y1="3" x2="-4.04" y2="2.6" width="0.127" layer="51"/>
<wire x1="-4.04" y1="2.6" x2="-4.74" y2="2.6" width="0.127" layer="51"/>
<wire x1="-4.74" y1="2.6" x2="-4.74" y2="1.2" width="0.127" layer="51"/>
<wire x1="-4.74" y1="1.2" x2="-4.44" y2="-0.9" width="0.127" layer="51"/>
<wire x1="-4.44" y1="-0.9" x2="-3.14" y2="-0.9" width="0.127" layer="51"/>
<wire x1="-3.44" y1="3" x2="-3.44" y2="2.6" width="0.127" layer="51"/>
<wire x1="-3.44" y1="2.6" x2="-2.84" y2="2.6" width="0.127" layer="51"/>
<wire x1="-2.84" y1="2.6" x2="-2.84" y2="1.2" width="0.127" layer="51"/>
<wire x1="-2.84" y1="1.2" x2="-3.14" y2="-0.9" width="0.127" layer="51"/>
<wire x1="-4.04" y1="3" x2="-3.44" y2="3" width="0.127" layer="51"/>
<wire x1="3.54" y1="3" x2="3.54" y2="2.6" width="0.127" layer="51"/>
<wire x1="3.54" y1="2.6" x2="2.84" y2="2.6" width="0.127" layer="51"/>
<wire x1="2.84" y1="2.6" x2="2.84" y2="1.2" width="0.127" layer="51"/>
<wire x1="2.84" y1="1.2" x2="3.14" y2="-0.9" width="0.127" layer="51"/>
<wire x1="3.14" y1="-0.9" x2="4.44" y2="-0.9" width="0.127" layer="51"/>
<wire x1="4.14" y1="3" x2="4.14" y2="2.6" width="0.127" layer="51"/>
<wire x1="4.14" y1="2.6" x2="4.74" y2="2.6" width="0.127" layer="51"/>
<wire x1="4.74" y1="2.6" x2="4.74" y2="1.2" width="0.127" layer="51"/>
<wire x1="4.74" y1="1.2" x2="4.44" y2="-0.9" width="0.127" layer="51"/>
<wire x1="3.54" y1="3" x2="4.14" y2="3" width="0.127" layer="51"/>
<wire x1="1.04" y1="3" x2="1.04" y2="2.6" width="0.127" layer="51"/>
<wire x1="1.04" y1="2.6" x2="0.34" y2="2.6" width="0.127" layer="51"/>
<wire x1="0.34" y1="2.6" x2="0.34" y2="1.2" width="0.127" layer="51"/>
<wire x1="0.34" y1="1.2" x2="0.64" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0.64" y1="-0.9" x2="1.94" y2="-0.9" width="0.127" layer="51"/>
<wire x1="1.64" y1="3" x2="1.64" y2="2.6" width="0.127" layer="51"/>
<wire x1="1.64" y1="2.6" x2="2.24" y2="2.6" width="0.127" layer="51"/>
<wire x1="2.24" y1="2.6" x2="2.24" y2="1.2" width="0.127" layer="51"/>
<wire x1="2.24" y1="1.2" x2="1.94" y2="-0.9" width="0.127" layer="51"/>
<wire x1="1.04" y1="3" x2="1.64" y2="3" width="0.127" layer="51"/>
<wire x1="-1.5" y1="3" x2="-1.5" y2="2.6" width="0.127" layer="51"/>
<wire x1="-1.5" y1="2.6" x2="-2.2" y2="2.6" width="0.127" layer="51"/>
<wire x1="-2.2" y1="2.6" x2="-2.2" y2="1.2" width="0.127" layer="51"/>
<wire x1="-2.2" y1="1.2" x2="-1.9" y2="-0.9" width="0.127" layer="51"/>
<wire x1="-1.9" y1="-0.9" x2="-0.6" y2="-0.9" width="0.127" layer="51"/>
<wire x1="-0.9" y1="3" x2="-0.9" y2="2.6" width="0.127" layer="51"/>
<wire x1="-0.9" y1="2.6" x2="-0.3" y2="2.6" width="0.127" layer="51"/>
<wire x1="-0.3" y1="2.6" x2="-0.3" y2="1.2" width="0.127" layer="51"/>
<wire x1="-0.3" y1="1.2" x2="-0.6" y2="-0.9" width="0.127" layer="51"/>
<wire x1="-1.5" y1="3" x2="-0.9" y2="3" width="0.127" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="1.016" diameter="2.1844" shape="square"/>
<pad name="2" x="-1.27" y="0" drill="1.016" diameter="2.1844"/>
<pad name="3" x="1.27" y="0" drill="1.016" diameter="2.1844"/>
<pad name="4" x="3.81" y="0" drill="1.016" diameter="2.1844"/>
<text x="-5.08" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<text x="-5.08" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.445" y="1.27" size="1.27" layer="21">1</text>
<rectangle x1="-4.7625" y1="-0.9525" x2="-2.8575" y2="0.9525" layer="41"/>
<rectangle x1="-2.2225" y1="-0.9525" x2="-0.3175" y2="0.9525" layer="41"/>
<rectangle x1="0.3175" y1="-0.9525" x2="2.2225" y2="0.9525" layer="41"/>
<rectangle x1="2.8575" y1="-0.9525" x2="4.7625" y2="0.9525" layer="41"/>
</package>
</packages>
<symbols>
<symbol name="LPV4">
<wire x1="1.27" y1="5.08" x2="-3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="-7.62" x2="-3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="1.27" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="-7.62" x2="1.27" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="0" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="-5.08" x2="0" y2="-5.08" width="0.6096" layer="94"/>
<text x="-3.81" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="5.08" y="2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="2" x="5.08" y="0" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="3" x="5.08" y="-2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="4" x="5.08" y="-5.08" visible="pad" length="middle" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LPV4" prefix="ST" uservalue="yes">
<gates>
<gate name="LPV" symbol="LPV4" x="0" y="2.54"/>
</gates>
<devices>
<device name="" package="LPV4">
<connects>
<connect gate="LPV" pin="1" pad="1"/>
<connect gate="LPV" pin="2" pad="2"/>
<connect gate="LPV" pin="3" pad="3"/>
<connect gate="LPV" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="HAuto_CON_6">
<packages>
<package name="RJ45-PLUG">
<hole x="-4.89" y="1.27" drill="2.4"/>
<hole x="4.89" y="-0.635" drill="2.4"/>
<pad name="PIN2" x="-2.54" y="1.27" drill="0.9"/>
<pad name="PIN4" x="-0.508" y="1.27" drill="0.9"/>
<pad name="PIN6" x="1.524" y="1.27" drill="0.9"/>
<pad name="PIN8" x="3.556" y="1.27" drill="0.9"/>
<pad name="PIN1" x="-3.556" y="-1.27" drill="0.9"/>
<pad name="PIN3" x="-1.524" y="-1.27" drill="0.9"/>
<pad name="PIN5" x="0.508" y="-1.27" drill="0.9"/>
<pad name="PIN7" x="2.54" y="-1.27" drill="0.9"/>
<wire x1="-6.07" y1="3.525" x2="-6.07" y2="-3.525" width="0.4064" layer="21"/>
<wire x1="-6.07" y1="-3.525" x2="6.07" y2="-3.525" width="0.4064" layer="21"/>
<wire x1="6.07" y1="-3.525" x2="6.07" y2="3.525" width="0.4064" layer="21"/>
<wire x1="6.07" y1="3.525" x2="3.106" y2="3.525" width="0.4064" layer="21"/>
<wire x1="3.106" y1="3.525" x2="3.106" y2="4.795" width="0.4064" layer="21"/>
<wire x1="3.106" y1="4.795" x2="-3.106" y2="4.795" width="0.4064" layer="21"/>
<wire x1="-3.106" y1="4.795" x2="-3.106" y2="3.525" width="0.4064" layer="21"/>
<wire x1="-3.106" y1="3.525" x2="-6.07" y2="3.525" width="0.4064" layer="21"/>
<text x="-4" y="5" size="1.778" layer="25">&gt;NAME</text>
<text x="-4" y="-6" size="1.778" layer="27">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="RJ45">
<wire x1="5.08" y1="2.54" x2="5.08" y2="-20.32" width="0.254" layer="94"/>
<wire x1="5.08" y1="-20.32" x2="20.32" y2="-20.32" width="0.254" layer="94"/>
<wire x1="20.32" y1="-20.32" x2="20.32" y2="2.54" width="0.254" layer="94"/>
<wire x1="20.32" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<text x="5.588" y="3.302" size="1.778" layer="95">&gt;NAME</text>
<text x="4.826" y="-22.86" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="-17.78" length="middle" direction="pas"/>
<pin name="2" x="0" y="-15.24" length="middle" direction="pas"/>
<pin name="3" x="0" y="-12.7" length="middle" direction="pas"/>
<pin name="4" x="0" y="-10.16" length="middle" direction="pas"/>
<pin name="5" x="0" y="-7.62" length="middle" direction="pas"/>
<pin name="6" x="0" y="-5.08" length="middle" direction="pas"/>
<pin name="7" x="0" y="-2.54" length="middle" direction="pas"/>
<pin name="8" x="0" y="0" length="middle" direction="pas"/>
<wire x1="10.68" y1="-4.44" x2="10.68" y2="-5.44" width="0.254" layer="94"/>
<wire x1="10.68" y1="-5.44" x2="10.68" y2="-6.44" width="0.254" layer="94"/>
<wire x1="10.68" y1="-6.44" x2="10.68" y2="-7.44" width="0.254" layer="94"/>
<wire x1="10.68" y1="-7.44" x2="10.68" y2="-8.44" width="0.254" layer="94"/>
<wire x1="10.68" y1="-8.44" x2="10.68" y2="-9.44" width="0.254" layer="94"/>
<wire x1="10.68" y1="-9.44" x2="10.68" y2="-10.44" width="0.254" layer="94"/>
<wire x1="10.68" y1="-10.44" x2="10.68" y2="-11.44" width="0.254" layer="94"/>
<wire x1="10.68" y1="-11.44" x2="10.68" y2="-12.44" width="0.254" layer="94"/>
<wire x1="10.68" y1="-12.44" x2="10.68" y2="-13.44" width="0.254" layer="94"/>
<wire x1="10.68" y1="-13.44" x2="16.18" y2="-13.44" width="0.254" layer="94"/>
<wire x1="10.68" y1="-4.44" x2="16.18" y2="-4.44" width="0.254" layer="94"/>
<wire x1="16.18" y1="-4.44" x2="16.18" y2="-5.94" width="0.254" layer="94"/>
<wire x1="16.18" y1="-13.44" x2="16.18" y2="-11.94" width="0.254" layer="94"/>
<wire x1="16.18" y1="-5.94" x2="17.18" y2="-5.94" width="0.254" layer="94"/>
<wire x1="17.18" y1="-5.94" x2="17.18" y2="-7.44" width="0.254" layer="94"/>
<wire x1="17.18" y1="-7.44" x2="18.18" y2="-7.44" width="0.254" layer="94"/>
<wire x1="16.18" y1="-11.94" x2="17.18" y2="-11.94" width="0.254" layer="94"/>
<wire x1="17.18" y1="-11.94" x2="17.18" y2="-10.44" width="0.254" layer="94"/>
<wire x1="17.18" y1="-10.44" x2="18.18" y2="-10.44" width="0.254" layer="94"/>
<wire x1="18.18" y1="-10.44" x2="18.18" y2="-7.44" width="0.254" layer="94"/>
<wire x1="10.68" y1="-5.44" x2="11.18" y2="-5.44" width="0.254" layer="94"/>
<wire x1="10.68" y1="-6.44" x2="11.18" y2="-6.44" width="0.254" layer="94"/>
<wire x1="10.68" y1="-7.44" x2="11.18" y2="-7.44" width="0.254" layer="94"/>
<wire x1="10.68" y1="-8.44" x2="11.18" y2="-8.44" width="0.254" layer="94"/>
<wire x1="10.68" y1="-9.44" x2="11.18" y2="-9.44" width="0.254" layer="94"/>
<wire x1="10.68" y1="-10.44" x2="11.18" y2="-10.44" width="0.254" layer="94"/>
<wire x1="10.68" y1="-11.44" x2="11.18" y2="-11.44" width="0.254" layer="94"/>
<wire x1="10.68" y1="-12.44" x2="11.18" y2="-12.44" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RJ45-PLUG" prefix="X" uservalue="yes">
<gates>
<gate name="G$1" symbol="RJ45" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RJ45-PLUG">
<connects>
<connect gate="G$1" pin="1" pad="PIN1"/>
<connect gate="G$1" pin="2" pad="PIN2"/>
<connect gate="G$1" pin="3" pad="PIN3"/>
<connect gate="G$1" pin="4" pad="PIN4"/>
<connect gate="G$1" pin="5" pad="PIN5"/>
<connect gate="G$1" pin="6" pad="PIN6"/>
<connect gate="G$1" pin="7" pad="PIN7"/>
<connect gate="G$1" pin="8" pad="PIN8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="HAutomation_6">
<packages>
<package name="SCHRIFTF">
<wire x1="-46.355" y1="0" x2="-46.355" y2="3.81" width="0.254" layer="48"/>
<wire x1="-46.355" y1="3.81" x2="-46.355" y2="4.445" width="0.254" layer="48"/>
<wire x1="-46.355" y1="4.445" x2="-46.355" y2="6.985" width="0.254" layer="48"/>
<wire x1="-46.355" y1="6.985" x2="-46.355" y2="12.065" width="0.254" layer="48"/>
<wire x1="-46.355" y1="12.065" x2="-46.355" y2="17.145" width="0.254" layer="48"/>
<wire x1="-46.355" y1="17.145" x2="-54.61" y2="17.145" width="0.254" layer="48"/>
<wire x1="-54.61" y1="17.145" x2="-66.675" y2="17.145" width="0.254" layer="48"/>
<wire x1="-66.675" y1="17.145" x2="-79.375" y2="17.145" width="0.254" layer="48"/>
<wire x1="-79.375" y1="17.145" x2="-88.9" y2="17.145" width="0.254" layer="48"/>
<wire x1="-88.9" y1="17.145" x2="-88.9" y2="14.605" width="0.254" layer="48"/>
<wire x1="-88.9" y1="14.605" x2="-88.9" y2="12.065" width="0.254" layer="48"/>
<wire x1="-88.9" y1="12.065" x2="-88.9" y2="9.525" width="0.254" layer="48"/>
<wire x1="-88.9" y1="9.525" x2="-88.9" y2="6.985" width="0.254" layer="48"/>
<wire x1="-88.9" y1="6.985" x2="-88.9" y2="4.445" width="0.254" layer="48"/>
<wire x1="-88.9" y1="17.145" x2="-88.9" y2="19.685" width="0.254" layer="48"/>
<wire x1="-88.9" y1="19.685" x2="-88.9" y2="22.225" width="0.254" layer="48"/>
<wire x1="-88.9" y1="22.225" x2="-88.9" y2="24.765" width="0.254" layer="48"/>
<wire x1="-88.9" y1="24.765" x2="-88.9" y2="27.94" width="0.254" layer="48"/>
<wire x1="-88.9" y1="27.94" x2="-79.375" y2="27.94" width="0.254" layer="48"/>
<wire x1="-79.375" y1="27.94" x2="-66.675" y2="27.94" width="0.254" layer="48"/>
<wire x1="-66.675" y1="27.94" x2="-54.61" y2="27.94" width="0.254" layer="48"/>
<wire x1="-54.61" y1="27.94" x2="-46.355" y2="27.94" width="0.254" layer="48"/>
<wire x1="-46.355" y1="27.94" x2="-46.355" y2="24.765" width="0.254" layer="48"/>
<wire x1="-46.355" y1="24.765" x2="-46.355" y2="22.225" width="0.254" layer="48"/>
<wire x1="-46.355" y1="22.225" x2="-46.355" y2="19.685" width="0.254" layer="48"/>
<wire x1="-46.355" y1="19.685" x2="-46.355" y2="17.145" width="0.254" layer="48"/>
<wire x1="-88.9" y1="19.685" x2="-46.355" y2="19.685" width="0.127" layer="48"/>
<wire x1="-88.9" y1="22.225" x2="-46.355" y2="22.225" width="0.127" layer="48"/>
<wire x1="-88.9" y1="24.765" x2="-46.355" y2="24.765" width="0.127" layer="48"/>
<wire x1="-54.61" y1="27.94" x2="-54.61" y2="17.145" width="0.127" layer="48"/>
<wire x1="-88.9" y1="27.94" x2="-97.79" y2="27.94" width="0.254" layer="48"/>
<wire x1="-97.79" y1="27.94" x2="-109.855" y2="27.94" width="0.254" layer="48"/>
<wire x1="-109.855" y1="27.94" x2="-120.015" y2="27.94" width="0.254" layer="48"/>
<wire x1="-120.015" y1="27.94" x2="-120.015" y2="24.765" width="0.254" layer="48"/>
<wire x1="-120.015" y1="24.765" x2="-120.015" y2="22.225" width="0.254" layer="48"/>
<wire x1="-120.015" y1="22.225" x2="-120.015" y2="19.685" width="0.254" layer="48"/>
<wire x1="-120.015" y1="19.685" x2="-120.015" y2="17.145" width="0.254" layer="48"/>
<wire x1="-120.015" y1="17.145" x2="-120.015" y2="14.605" width="0.254" layer="48"/>
<wire x1="-120.015" y1="14.605" x2="-120.015" y2="12.065" width="0.254" layer="48"/>
<wire x1="-120.015" y1="12.065" x2="-120.015" y2="9.525" width="0.254" layer="48"/>
<wire x1="-120.015" y1="9.525" x2="-120.015" y2="6.985" width="0.254" layer="48"/>
<wire x1="-120.015" y1="6.985" x2="-120.015" y2="4.445" width="0.254" layer="48"/>
<wire x1="-120.015" y1="4.445" x2="-120.015" y2="0" width="0.254" layer="48"/>
<wire x1="-120.015" y1="0" x2="-109.855" y2="0" width="0.254" layer="48"/>
<wire x1="-109.855" y1="0" x2="-97.79" y2="0" width="0.254" layer="48"/>
<wire x1="-97.79" y1="0" x2="-88.9" y2="0" width="0.254" layer="48"/>
<wire x1="-88.9" y1="0" x2="-78.74" y2="0" width="0.254" layer="48"/>
<wire x1="-78.74" y1="0" x2="-70.485" y2="0" width="0.254" layer="48"/>
<wire x1="-70.485" y1="0" x2="-60.96" y2="0" width="0.254" layer="48"/>
<wire x1="-60.96" y1="0" x2="-46.355" y2="0" width="0.254" layer="48"/>
<wire x1="-120.015" y1="24.765" x2="-88.9" y2="24.765" width="0.127" layer="48"/>
<wire x1="-120.015" y1="22.225" x2="-88.9" y2="22.225" width="0.127" layer="48"/>
<wire x1="-120.015" y1="19.685" x2="-88.9" y2="19.685" width="0.127" layer="48"/>
<wire x1="-120.015" y1="17.145" x2="-88.9" y2="17.145" width="0.127" layer="48"/>
<wire x1="-120.015" y1="14.605" x2="-88.9" y2="14.605" width="0.127" layer="48"/>
<wire x1="-120.015" y1="12.065" x2="-88.9" y2="12.065" width="0.127" layer="48"/>
<wire x1="-120.015" y1="9.525" x2="-88.9" y2="9.525" width="0.127" layer="48"/>
<wire x1="-88.9" y1="4.445" x2="-78.74" y2="4.445" width="0.254" layer="48"/>
<wire x1="-78.74" y1="4.445" x2="-70.485" y2="4.445" width="0.254" layer="48"/>
<wire x1="-70.485" y1="4.445" x2="-60.96" y2="4.445" width="0.254" layer="48"/>
<wire x1="-60.96" y1="4.445" x2="-46.355" y2="4.445" width="0.254" layer="48"/>
<wire x1="-97.79" y1="27.94" x2="-97.79" y2="0" width="0.127" layer="48"/>
<wire x1="-66.675" y1="27.94" x2="-66.675" y2="17.145" width="0.127" layer="48"/>
<wire x1="-79.375" y1="17.145" x2="-79.375" y2="27.94" width="0.127" layer="48"/>
<wire x1="-109.855" y1="27.94" x2="-109.855" y2="0" width="0.127" layer="48"/>
<wire x1="-88.9" y1="4.445" x2="-88.9" y2="0" width="0.127" layer="48"/>
<wire x1="-78.74" y1="4.445" x2="-78.74" y2="0" width="0.127" layer="48"/>
<wire x1="-70.485" y1="4.445" x2="-70.485" y2="0" width="0.127" layer="48"/>
<wire x1="-60.96" y1="4.445" x2="-60.96" y2="0" width="0.127" layer="48"/>
<wire x1="-46.355" y1="27.94" x2="0" y2="27.94" width="0.254" layer="48"/>
<wire x1="0" y1="27.94" x2="0" y2="22.225" width="0.254" layer="48"/>
<wire x1="0" y1="22.225" x2="0" y2="17.145" width="0.254" layer="48"/>
<wire x1="0" y1="17.145" x2="0" y2="12.065" width="0.254" layer="48"/>
<wire x1="0" y1="12.065" x2="0" y2="6.985" width="0.254" layer="48"/>
<wire x1="0" y1="6.985" x2="0" y2="0" width="0.254" layer="48"/>
<wire x1="0" y1="0" x2="-12.065" y2="0" width="0.254" layer="48"/>
<wire x1="-12.065" y1="0" x2="-46.355" y2="0" width="0.254" layer="48"/>
<wire x1="-46.355" y1="22.225" x2="0" y2="22.225" width="0.127" layer="48"/>
<wire x1="-46.355" y1="17.145" x2="0" y2="17.145" width="0.127" layer="48"/>
<wire x1="-46.355" y1="12.065" x2="0" y2="12.065" width="0.127" layer="48"/>
<wire x1="-46.355" y1="6.985" x2="-12.065" y2="6.985" width="0.127" layer="48"/>
<wire x1="-12.065" y1="6.985" x2="0" y2="6.985" width="0.127" layer="48"/>
<wire x1="-12.065" y1="6.985" x2="-12.065" y2="3.81" width="0.127" layer="48"/>
<wire x1="-12.065" y1="3.81" x2="-12.065" y2="0" width="0.127" layer="48"/>
<wire x1="-46.355" y1="3.81" x2="-12.065" y2="3.81" width="0.127" layer="48"/>
<wire x1="-120.015" y1="6.985" x2="-88.9" y2="6.985" width="0.127" layer="48"/>
<wire x1="-120.015" y1="4.445" x2="-88.9" y2="4.445" width="0.127" layer="48"/>
<text x="-45.56" y="1.1999" size="1.6764" layer="48">&gt;LAST_DATE_TIME</text>
<text x="-45.56" y="4.3749" size="1.6764" layer="48">&gt;DRAWING_NAME</text>
<text x="-88.265" y="22.86" size="1.6764" layer="48">Bearb.</text>
<text x="-88.265" y="20.32" size="1.6764" layer="48">Gepr.</text>
<text x="-88.265" y="17.78" size="1.6764" layer="48">Norm</text>
<text x="-78.74" y="25.4" size="1.6764" layer="48">Datum</text>
<text x="-66.04" y="25.4" size="1.6764" layer="48">Name</text>
<text x="-53.975" y="25.4" size="1.6764" layer="48">Sign.</text>
<text x="-118.745" y="22.86" size="1.6764" layer="48">stand</text>
<text x="-118.745" y="25.4" size="1.6764" layer="48">Änd.-</text>
<text x="-109.22" y="22.86" size="1.6764" layer="48">Datum</text>
<text x="-97.155" y="22.86" size="1.6764" layer="48">Sign.</text>
<text x="-11.43" y="4.445" size="1.6764" layer="48">Blatt</text>
<text x="-11.43" y="1.27" size="1.6764" layer="48">1/1</text>
<text x="-45.56" y="1.1999" size="1.6764" layer="48">&gt;LAST_DATE_TIME</text>
<text x="-45.56" y="4.3749" size="1.6764" layer="48">&gt;DRAWING_NAME</text>
<text x="-88.265" y="22.86" size="1.6764" layer="48">Bearb.</text>
<text x="-88.265" y="20.32" size="1.6764" layer="48">Gepr.</text>
<text x="-88.265" y="17.78" size="1.6764" layer="48">Norm</text>
<text x="-78.74" y="25.4" size="1.6764" layer="48">Datum</text>
<text x="-66.04" y="25.4" size="1.6764" layer="48">Name</text>
<text x="-53.975" y="25.4" size="1.6764" layer="48">Sign.</text>
<text x="-118.745" y="22.86" size="1.6764" layer="48">stand</text>
<text x="-118.745" y="25.4" size="1.6764" layer="48">Änd.-</text>
<text x="-109.22" y="22.86" size="1.6764" layer="48">Datum</text>
<text x="-97.155" y="22.86" size="1.6764" layer="48">Sign.</text>
<text x="-11.43" y="4.445" size="1.6764" layer="48">Blatt</text>
<text x="-11.43" y="1.27" size="1.6764" layer="48">1/1</text>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-85.09" y="15.7425"/>
<vertex x="-85.09" y="9.74"/>
<vertex x="-83.7075" y="9.74"/>
<vertex x="-83.7075" y="12.225"/>
<vertex x="-81.485" y="12.225"/>
<vertex x="-81.485" y="9.74"/>
<vertex x="-80.155" y="9.74"/>
<vertex x="-80.155" y="15.7425"/>
<vertex x="-81.485" y="15.7425"/>
<vertex x="-81.485" y="13.3275"/>
<vertex x="-83.7075" y="13.3275"/>
<vertex x="-83.7075" y="15.7425"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-78.16" y="15.7425"/>
<vertex x="-77.3725" y="15.7425"/>
<vertex x="-77.3725" y="15.0775"/>
<vertex x="-78.16" y="15.0775"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-74.765" y="15.7425"/>
<vertex x="-73.9425" y="15.7425"/>
<vertex x="-73.9425" y="15.0775"/>
<vertex x="-74.765" y="15.0775"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-79.315" y="9.74"/>
<vertex x="-77.8625" y="9.74"/>
<vertex x="-76.095" y="14.395"/>
<vertex x="-72.6825" y="5.715"/>
<vertex x="-71.335" y="5.715"/>
<vertex x="-75.325" y="15.7425"/>
<vertex x="-76.76" y="15.7425"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-76.9175" y="12.2425"/>
<vertex x="-75.2375" y="12.2425"/>
<vertex x="-74.8175" y="11.1575"/>
<vertex x="-77.3375" y="11.1575"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-67.835" y="15.7425"/>
<vertex x="-67.835" y="14.7275"/>
<vertex x="-69.865" y="14.7275"/>
<vertex x="-70.425" y="14.6225"/>
<vertex x="-70.74" y="14.4825"/>
<vertex x="-70.95" y="14.3075"/>
<vertex x="-71.055" y="14.1325"/>
<vertex x="-71.195" y="13.8525"/>
<vertex x="-71.3" y="13.4675"/>
<vertex x="-71.37" y="13.0125"/>
<vertex x="-71.37" y="12.19"/>
<vertex x="-71.3" y="11.6825"/>
<vertex x="-71.1075" y="11.2625"/>
<vertex x="-70.915" y="11.0175"/>
<vertex x="-70.5825" y="10.8425"/>
<vertex x="-70.285" y="10.79"/>
<vertex x="-69.935" y="10.7725"/>
<vertex x="-67.835" y="10.7725"/>
<vertex x="-67.835" y="9.74"/>
<vertex x="-70.6" y="9.74"/>
<vertex x="-71.09" y="9.8275"/>
<vertex x="-71.5625" y="10.0025"/>
<vertex x="-72.035" y="10.3525"/>
<vertex x="-72.4025" y="10.7725"/>
<vertex x="-72.5775" y="11.14"/>
<vertex x="-72.7525" y="11.6825"/>
<vertex x="-72.805" y="12.0675"/>
<vertex x="-72.8225" y="12.6275"/>
<vertex x="-72.805" y="13.2575"/>
<vertex x="-72.7" y="13.9225"/>
<vertex x="-72.5075" y="14.4125"/>
<vertex x="-72.28" y="14.78"/>
<vertex x="-71.93" y="15.1825"/>
<vertex x="-71.475" y="15.4625"/>
<vertex x="-71.1075" y="15.6025"/>
<vertex x="-70.705" y="15.6725"/>
<vertex x="-70.25" y="15.725"/>
<vertex x="-69.83" y="15.7425"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-66.96" y="15.7425"/>
<vertex x="-66.96" y="9.74"/>
<vertex x="-65.63" y="9.74"/>
<vertex x="-65.63" y="15.7425"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-65.63" y="12.8725"/>
<vertex x="-63.32" y="15.7425"/>
<vertex x="-61.675" y="15.7425"/>
<vertex x="-64.125" y="12.8025"/>
<vertex x="-61.6925" y="9.74"/>
<vertex x="-63.425" y="9.74"/>
<vertex x="-65.63" y="12.645"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-56.6875" y="15.7425"/>
<vertex x="-56.6875" y="14.6575"/>
<vertex x="-58.9275" y="14.6575"/>
<vertex x="-59.2775" y="14.6225"/>
<vertex x="-59.5925" y="14.4825"/>
<vertex x="-59.8025" y="14.325"/>
<vertex x="-59.9425" y="14.1325"/>
<vertex x="-60.1" y="13.835"/>
<vertex x="-60.17" y="13.45"/>
<vertex x="-60.17" y="11.8575"/>
<vertex x="-60.1525" y="11.6825"/>
<vertex x="-59.96" y="11.2625"/>
<vertex x="-59.7675" y="11.0175"/>
<vertex x="-59.435" y="10.8425"/>
<vertex x="-59.1375" y="10.79"/>
<vertex x="-58.7875" y="10.7725"/>
<vertex x="-56.6875" y="10.7725"/>
<vertex x="-56.6875" y="9.74"/>
<vertex x="-59.4525" y="9.74"/>
<vertex x="-59.9425" y="9.8275"/>
<vertex x="-60.415" y="10.0025"/>
<vertex x="-60.8875" y="10.3525"/>
<vertex x="-61.255" y="10.7725"/>
<vertex x="-61.43" y="11.14"/>
<vertex x="-61.605" y="11.6825"/>
<vertex x="-61.6575" y="12.0675"/>
<vertex x="-61.675" y="12.6275"/>
<vertex x="-61.6575" y="13.2575"/>
<vertex x="-61.5525" y="13.9225"/>
<vertex x="-61.36" y="14.4125"/>
<vertex x="-61.1325" y="14.78"/>
<vertex x="-60.7825" y="15.1825"/>
<vertex x="-60.3275" y="15.4625"/>
<vertex x="-59.96" y="15.6025"/>
<vertex x="-59.5575" y="15.6725"/>
<vertex x="-59.1025" y="15.725"/>
<vertex x="-58.6825" y="15.7425"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-60.17" y="12.225"/>
<vertex x="-56.6875" y="12.225"/>
<vertex x="-56.6875" y="13.3275"/>
<vertex x="-60.17" y="13.3275"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-55.655" y="15.7425"/>
<vertex x="-52.295" y="15.7425"/>
<vertex x="-51.665" y="15.6725"/>
<vertex x="-51.245" y="15.55"/>
<vertex x="-50.965" y="15.375"/>
<vertex x="-50.7725" y="15.1825"/>
<vertex x="-50.545" y="14.885"/>
<vertex x="-50.4225" y="14.5175"/>
<vertex x="-50.3525" y="14.15"/>
<vertex x="-50.3525" y="13.7475"/>
<vertex x="-50.4225" y="13.31"/>
<vertex x="-50.615" y="12.8725"/>
<vertex x="-50.8425" y="12.5925"/>
<vertex x="-51.0875" y="12.4175"/>
<vertex x="-51.4025" y="12.26"/>
<vertex x="-51.525" y="12.2075"/>
<vertex x="-50.09" y="9.74"/>
<vertex x="-51.595" y="9.74"/>
<vertex x="-52.89" y="12.05"/>
<vertex x="-52.89" y="13.135"/>
<vertex x="-52.505" y="13.135"/>
<vertex x="-52.12" y="13.24"/>
<vertex x="-51.8225" y="13.415"/>
<vertex x="-51.7175" y="13.6425"/>
<vertex x="-51.7175" y="14.1675"/>
<vertex x="-51.8225" y="14.3775"/>
<vertex x="-51.98" y="14.4825"/>
<vertex x="-52.1375" y="14.5525"/>
<vertex x="-52.4175" y="14.6225"/>
<vertex x="-52.6625" y="14.6575"/>
<vertex x="-54.3075" y="14.6575"/>
<vertex x="-54.3075" y="13.135"/>
<vertex x="-52.8917" y="13.135"/>
<vertex x="-52.8917" y="12.05"/>
<vertex x="-54.3075" y="12.05"/>
<vertex x="-54.3075" y="9.74"/>
<vertex x="-55.655" y="9.74"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-71.16" y="7.9725"/>
<vertex x="-70.495" y="7.9725"/>
<vertex x="-70.495" y="6.415"/>
<vertex x="-70.46" y="6.3625"/>
<vertex x="-70.39" y="6.2925"/>
<vertex x="-70.32" y="6.2575"/>
<vertex x="-69.69" y="6.2575"/>
<vertex x="-69.69" y="7.9725"/>
<vertex x="-69.025" y="7.9725"/>
<vertex x="-69.025" y="5.8025"/>
<vertex x="-69.1125" y="5.715"/>
<vertex x="-70.32" y="5.715"/>
<vertex x="-70.5475" y="5.7325"/>
<vertex x="-70.635" y="5.75"/>
<vertex x="-70.7575" y="5.8025"/>
<vertex x="-70.88" y="5.8725"/>
<vertex x="-71.0375" y="6.03"/>
<vertex x="-71.1075" y="6.1525"/>
<vertex x="-71.1425" y="6.24"/>
<vertex x="-71.16" y="6.31"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-68.7975" y="7.9725"/>
<vertex x="-68.7975" y="7.5525"/>
<vertex x="-68.3775" y="7.5525"/>
<vertex x="-68.3775" y="6.2575"/>
<vertex x="-68.3075" y="6.0825"/>
<vertex x="-68.1675" y="5.9075"/>
<vertex x="-67.9925" y="5.785"/>
<vertex x="-67.7475" y="5.715"/>
<vertex x="-67.1525" y="5.715"/>
<vertex x="-67.065" y="5.785"/>
<vertex x="-67.065" y="6.205"/>
<vertex x="-67.485" y="6.205"/>
<vertex x="-67.5725" y="6.2575"/>
<vertex x="-67.66" y="6.3625"/>
<vertex x="-67.7125" y="6.5025"/>
<vertex x="-67.7125" y="7.5525"/>
<vertex x="-67.065" y="7.5525"/>
<vertex x="-67.065" y="7.9725"/>
<vertex x="-67.7125" y="7.9725"/>
<vertex x="-67.7125" y="8.8125"/>
<vertex x="-68.3775" y="8.8125"/>
<vertex x="-68.3775" y="7.9725"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-66.96" y="6.8858"/>
<vertex x="-66.96" y="6.7125"/>
<vertex x="-66.89" y="6.3975"/>
<vertex x="-66.715" y="6.1"/>
<vertex x="-66.4525" y="5.89"/>
<vertex x="-66.1725" y="5.75"/>
<vertex x="-65.945" y="5.715"/>
<vertex x="-65.49" y="5.715"/>
<vertex x="-65.175" y="5.785"/>
<vertex x="-64.93" y="5.9075"/>
<vertex x="-64.7375" y="6.1"/>
<vertex x="-64.58" y="6.3275"/>
<vertex x="-64.5275" y="6.4973"/>
<vertex x="-64.51" y="6.6075"/>
<vertex x="-64.51" y="7.115"/>
<vertex x="-64.58" y="7.4125"/>
<vertex x="-64.72" y="7.6225"/>
<vertex x="-64.93" y="7.815"/>
<vertex x="-65.175" y="7.9375"/>
<vertex x="-65.49" y="8.025"/>
<vertex x="-65.98" y="8.025"/>
<vertex x="-66.225" y="7.9725"/>
<vertex x="-66.4875" y="7.8325"/>
<vertex x="-66.645" y="7.675"/>
<vertex x="-66.82" y="7.4475"/>
<vertex x="-66.925" y="7.2025"/>
<vertex x="-66.96" y="7.01"/>
<vertex x="-66.96" y="6.8875"/>
<vertex x="-66.295" y="6.8875"/>
<vertex x="-66.295" y="6.9925"/>
<vertex x="-66.2425" y="7.15"/>
<vertex x="-66.1725" y="7.2725"/>
<vertex x="-66.015" y="7.4475"/>
<vertex x="-65.84" y="7.535"/>
<vertex x="-65.6125" y="7.535"/>
<vertex x="-65.5075" y="7.5"/>
<vertex x="-65.35" y="7.395"/>
<vertex x="-65.2275" y="7.2025"/>
<vertex x="-65.175" y="7.0275"/>
<vertex x="-65.175" y="6.695"/>
<vertex x="-65.2275" y="6.5375"/>
<vertex x="-65.28" y="6.4325"/>
<vertex x="-65.4025" y="6.2925"/>
<vertex x="-65.525" y="6.2225"/>
<vertex x="-65.6125" y="6.205"/>
<vertex x="-65.8575" y="6.205"/>
<vertex x="-66.015" y="6.2575"/>
<vertex x="-66.155" y="6.3975"/>
<vertex x="-66.26" y="6.6075"/>
<vertex x="-66.295" y="6.765"/>
<vertex x="-66.295" y="6.8858"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-54.99" y="6.8858"/>
<vertex x="-54.99" y="6.7125"/>
<vertex x="-54.92" y="6.3975"/>
<vertex x="-54.745" y="6.1"/>
<vertex x="-54.4825" y="5.89"/>
<vertex x="-54.2025" y="5.75"/>
<vertex x="-53.975" y="5.715"/>
<vertex x="-53.52" y="5.715"/>
<vertex x="-53.205" y="5.785"/>
<vertex x="-52.96" y="5.9075"/>
<vertex x="-52.7675" y="6.1"/>
<vertex x="-52.61" y="6.3275"/>
<vertex x="-52.5575" y="6.4973"/>
<vertex x="-52.54" y="6.6075"/>
<vertex x="-52.54" y="7.115"/>
<vertex x="-52.61" y="7.4125"/>
<vertex x="-52.75" y="7.6225"/>
<vertex x="-52.96" y="7.815"/>
<vertex x="-53.205" y="7.9375"/>
<vertex x="-53.52" y="8.025"/>
<vertex x="-54.01" y="8.025"/>
<vertex x="-54.255" y="7.9725"/>
<vertex x="-54.5175" y="7.8325"/>
<vertex x="-54.675" y="7.675"/>
<vertex x="-54.85" y="7.4475"/>
<vertex x="-54.955" y="7.2025"/>
<vertex x="-54.99" y="7.01"/>
<vertex x="-54.99" y="6.8875"/>
<vertex x="-54.325" y="6.8875"/>
<vertex x="-54.325" y="6.9925"/>
<vertex x="-54.2725" y="7.15"/>
<vertex x="-54.2025" y="7.2725"/>
<vertex x="-54.045" y="7.4475"/>
<vertex x="-53.87" y="7.535"/>
<vertex x="-53.6425" y="7.535"/>
<vertex x="-53.5375" y="7.5"/>
<vertex x="-53.38" y="7.395"/>
<vertex x="-53.2575" y="7.2025"/>
<vertex x="-53.205" y="7.0275"/>
<vertex x="-53.205" y="6.695"/>
<vertex x="-53.2575" y="6.5375"/>
<vertex x="-53.31" y="6.4325"/>
<vertex x="-53.4325" y="6.2925"/>
<vertex x="-53.555" y="6.2225"/>
<vertex x="-53.6425" y="6.205"/>
<vertex x="-53.8875" y="6.205"/>
<vertex x="-54.045" y="6.2575"/>
<vertex x="-54.185" y="6.3975"/>
<vertex x="-54.29" y="6.6075"/>
<vertex x="-54.325" y="6.765"/>
<vertex x="-54.325" y="6.8858"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-55.935" y="7.9725"/>
<vertex x="-55.935" y="5.715"/>
<vertex x="-55.27" y="5.715"/>
<vertex x="-55.27" y="7.9725"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-55.935" y="8.8125"/>
<vertex x="-55.935" y="8.2525"/>
<vertex x="-55.27" y="8.2525"/>
<vertex x="-55.27" y="8.8125"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-50.1075" y="5.715"/>
<vertex x="-50.7725" y="5.715"/>
<vertex x="-50.7725" y="7.325"/>
<vertex x="-50.8075" y="7.3775"/>
<vertex x="-50.8775" y="7.4475"/>
<vertex x="-50.9475" y="7.4825"/>
<vertex x="-51.5425" y="7.4825"/>
<vertex x="-51.5425" y="5.715"/>
<vertex x="-52.19" y="5.715"/>
<vertex x="-52.19" y="7.9725"/>
<vertex x="-50.6675" y="7.9725"/>
<vertex x="-50.5975" y="7.955"/>
<vertex x="-50.51" y="7.92"/>
<vertex x="-50.37" y="7.85"/>
<vertex x="-50.2825" y="7.7625"/>
<vertex x="-50.195" y="7.64"/>
<vertex x="-50.125" y="7.5"/>
<vertex x="-50.1075" y="7.3775"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-60.975" y="5.715"/>
<vertex x="-61.57" y="5.715"/>
<vertex x="-61.57" y="7.325"/>
<vertex x="-61.605" y="7.3775"/>
<vertex x="-61.675" y="7.4475"/>
<vertex x="-61.745" y="7.4825"/>
<vertex x="-62.2" y="7.4825"/>
<vertex x="-62.2" y="5.785"/>
<vertex x="-62.27" y="5.715"/>
<vertex x="-62.83" y="5.715"/>
<vertex x="-62.9175" y="5.8025"/>
<vertex x="-62.9175" y="7.4825"/>
<vertex x="-63.5125" y="7.4825"/>
<vertex x="-63.5125" y="5.785"/>
<vertex x="-63.5825" y="5.715"/>
<vertex x="-64.1775" y="5.715"/>
<vertex x="-64.1775" y="7.9725"/>
<vertex x="-61.465" y="7.9725"/>
<vertex x="-61.395" y="7.955"/>
<vertex x="-61.3075" y="7.92"/>
<vertex x="-61.1675" y="7.85"/>
<vertex x="-61.08" y="7.7625"/>
<vertex x="-60.9925" y="7.64"/>
<vertex x="-60.9225" y="7.5"/>
<vertex x="-60.905" y="7.3775"/>
<vertex x="-60.905" y="5.8025"/>
<vertex x="-60.975" y="5.7325"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-58.175" y="5.8025"/>
<vertex x="-58.2625" y="5.715"/>
<vertex x="-59.925" y="5.715"/>
<vertex x="-60.205" y="5.785"/>
<vertex x="-60.415" y="5.96"/>
<vertex x="-60.5375" y="6.135"/>
<vertex x="-60.555" y="6.2575"/>
<vertex x="-60.555" y="6.6075"/>
<vertex x="-60.45" y="6.835"/>
<vertex x="-60.345" y="6.9575"/>
<vertex x="-60.275" y="6.9925"/>
<vertex x="-60.1175" y="7.0625"/>
<vertex x="-59.925" y="7.0975"/>
<vertex x="-58.8942" y="7.0975"/>
<vertex x="-58.8942" y="6.66"/>
<vertex x="-59.6975" y="6.66"/>
<vertex x="-59.7675" y="6.625"/>
<vertex x="-59.82" y="6.59"/>
<vertex x="-59.855" y="6.5375"/>
<vertex x="-59.8725" y="6.5025"/>
<vertex x="-59.89" y="6.4325"/>
<vertex x="-59.89" y="6.38"/>
<vertex x="-59.8725" y="6.3275"/>
<vertex x="-59.82" y="6.2575"/>
<vertex x="-59.7675" y="6.2225"/>
<vertex x="-59.715" y="6.205"/>
<vertex x="-58.8925" y="6.205"/>
<vertex x="-58.8925" y="7.325"/>
<vertex x="-58.9275" y="7.3775"/>
<vertex x="-58.9975" y="7.4475"/>
<vertex x="-59.0675" y="7.4825"/>
<vertex x="-60.38" y="7.4825"/>
<vertex x="-60.38" y="7.9725"/>
<vertex x="-58.7875" y="7.9725"/>
<vertex x="-58.7175" y="7.955"/>
<vertex x="-58.63" y="7.92"/>
<vertex x="-58.49" y="7.85"/>
<vertex x="-58.4025" y="7.7625"/>
<vertex x="-58.315" y="7.64"/>
<vertex x="-58.245" y="7.5"/>
<vertex x="-58.175" y="7.22"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-57.9475" y="7.9725"/>
<vertex x="-57.9475" y="7.5525"/>
<vertex x="-57.5275" y="7.5525"/>
<vertex x="-57.5275" y="6.2575"/>
<vertex x="-57.4575" y="6.0825"/>
<vertex x="-57.3175" y="5.9075"/>
<vertex x="-57.1425" y="5.785"/>
<vertex x="-56.8975" y="5.715"/>
<vertex x="-56.3025" y="5.715"/>
<vertex x="-56.215" y="5.785"/>
<vertex x="-56.215" y="6.205"/>
<vertex x="-56.635" y="6.205"/>
<vertex x="-56.7225" y="6.2575"/>
<vertex x="-56.81" y="6.3625"/>
<vertex x="-56.8625" y="6.5025"/>
<vertex x="-56.8625" y="7.5525"/>
<vertex x="-56.215" y="7.5525"/>
<vertex x="-56.215" y="7.9725"/>
<vertex x="-56.8625" y="7.9725"/>
<vertex x="-56.8625" y="8.8125"/>
<vertex x="-57.5275" y="8.8125"/>
<vertex x="-57.5275" y="7.9725"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-85.09" y="6.1636"/>
<vertex x="-84.6414" y="6.1636"/>
<vertex x="-84.6414" y="5.715"/>
<vertex x="-85.09" y="5.715"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-85.09" y="7.0609"/>
<vertex x="-85.09" y="6.6123"/>
<vertex x="-84.6414" y="6.6123"/>
<vertex x="-84.6414" y="7.0609"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-85.09" y="7.9582"/>
<vertex x="-85.09" y="7.5095"/>
<vertex x="-84.6414" y="7.5095"/>
<vertex x="-84.6414" y="7.9582"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-84.1927" y="7.9582"/>
<vertex x="-84.1927" y="7.5095"/>
<vertex x="-83.7441" y="7.5095"/>
<vertex x="-83.7441" y="7.9582"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-84.1927" y="7.0609"/>
<vertex x="-84.1927" y="6.6123"/>
<vertex x="-83.7441" y="6.6123"/>
<vertex x="-83.7441" y="7.0609"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-84.1927" y="6.1636"/>
<vertex x="-84.1927" y="5.715"/>
<vertex x="-83.7441" y="5.715"/>
<vertex x="-83.7441" y="6.1636"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-83.2955" y="7.9582"/>
<vertex x="-83.2955" y="7.5095"/>
<vertex x="-82.8468" y="7.5095"/>
<vertex x="-82.8468" y="7.9582"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-79.7064" y="7.9582"/>
<vertex x="-79.7064" y="7.5095"/>
<vertex x="-79.2578" y="7.5095"/>
<vertex x="-79.2578" y="7.9582"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-78.8091" y="7.9582"/>
<vertex x="-78.8091" y="7.5095"/>
<vertex x="-78.3605" y="7.5095"/>
<vertex x="-78.3605" y="7.9582"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-83.2955" y="7.0609"/>
<vertex x="-83.2955" y="6.6123"/>
<vertex x="-82.8468" y="6.6123"/>
<vertex x="-82.8468" y="7.0609"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-83.2955" y="6.1636"/>
<vertex x="-83.2955" y="5.715"/>
<vertex x="-82.8468" y="5.715"/>
<vertex x="-82.8468" y="6.1636"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-82.3982" y="6.1636"/>
<vertex x="-81.9496" y="6.1636"/>
<vertex x="-81.9496" y="5.715"/>
<vertex x="-82.3982" y="5.715"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-82.3982" y="7.0609"/>
<vertex x="-82.3982" y="6.6123"/>
<vertex x="-81.9496" y="6.6123"/>
<vertex x="-81.9496" y="7.0609"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-82.3982" y="7.9582"/>
<vertex x="-82.3982" y="7.5095"/>
<vertex x="-81.9496" y="7.5095"/>
<vertex x="-81.9496" y="7.9582"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-81.5009" y="7.9582"/>
<vertex x="-81.5009" y="7.5095"/>
<vertex x="-81.0523" y="7.5095"/>
<vertex x="-81.0523" y="7.9582"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-81.5009" y="7.0609"/>
<vertex x="-81.5009" y="6.6123"/>
<vertex x="-81.0523" y="6.6123"/>
<vertex x="-81.0523" y="7.0609"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-81.5009" y="6.1636"/>
<vertex x="-81.5009" y="5.715"/>
<vertex x="-81.0523" y="5.715"/>
<vertex x="-81.0523" y="6.1636"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-80.6037" y="7.9582"/>
<vertex x="-80.6037" y="7.5095"/>
<vertex x="-80.155" y="7.5095"/>
<vertex x="-80.155" y="7.9582"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-80.6037" y="7.0609"/>
<vertex x="-80.6037" y="6.6123"/>
<vertex x="-80.155" y="6.6123"/>
<vertex x="-80.155" y="7.0609"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-80.6037" y="6.1636"/>
<vertex x="-80.6037" y="5.715"/>
<vertex x="-80.155" y="5.715"/>
<vertex x="-80.155" y="6.1636"/>
</polygon>
</package>
<package name="SCHRI_A5">
<wire x1="-32.7779" y1="0" x2="-32.7779" y2="2.6942" width="0.1796" layer="48"/>
<wire x1="-32.7779" y1="2.6942" x2="-32.7779" y2="3.143" width="0.1796" layer="48"/>
<wire x1="-32.7779" y1="3.143" x2="-32.7779" y2="4.939" width="0.1796" layer="48"/>
<wire x1="-32.7779" y1="4.939" x2="-32.7779" y2="8.5314" width="0.1796" layer="48"/>
<wire x1="-32.7779" y1="8.5314" x2="-32.7779" y2="12.1234" width="0.1796" layer="48"/>
<wire x1="-32.7779" y1="12.1234" x2="-38.6151" y2="12.1234" width="0.1796" layer="48"/>
<wire x1="-38.6151" y1="12.1234" x2="-47.1465" y2="12.1234" width="0.1796" layer="48"/>
<wire x1="-47.1465" y1="12.1234" x2="-56.1266" y2="12.1234" width="0.1796" layer="48"/>
<wire x1="-56.1266" y1="12.1234" x2="-62.8617" y2="12.1234" width="0.1796" layer="48"/>
<wire x1="-62.8617" y1="12.1234" x2="-62.8617" y2="10.3274" width="0.1796" layer="48"/>
<wire x1="-62.8617" y1="10.3274" x2="-62.8617" y2="8.5314" width="0.1796" layer="48"/>
<wire x1="-62.8617" y1="8.5314" x2="-62.8617" y2="6.7353" width="0.1796" layer="48"/>
<wire x1="-62.8617" y1="6.7353" x2="-62.8617" y2="4.939" width="0.1796" layer="48"/>
<wire x1="-62.8617" y1="4.939" x2="-62.8617" y2="3.143" width="0.1796" layer="48"/>
<wire x1="-62.8617" y1="12.1234" x2="-62.8617" y2="13.9195" width="0.1796" layer="48"/>
<wire x1="-62.8617" y1="13.9195" x2="-62.8617" y2="15.7155" width="0.1796" layer="48"/>
<wire x1="-62.8617" y1="15.7155" x2="-62.8617" y2="17.5115" width="0.1796" layer="48"/>
<wire x1="-62.8617" y1="17.5115" x2="-62.8617" y2="19.7566" width="0.1796" layer="48"/>
<wire x1="-62.8617" y1="19.7566" x2="-56.1266" y2="19.7566" width="0.1796" layer="48"/>
<wire x1="-56.1266" y1="19.7566" x2="-47.1465" y2="19.7566" width="0.1796" layer="48"/>
<wire x1="-47.1465" y1="19.7566" x2="-38.6151" y2="19.7566" width="0.1796" layer="48"/>
<wire x1="-38.6151" y1="19.7566" x2="-32.7779" y2="19.7566" width="0.1796" layer="48"/>
<wire x1="-32.7779" y1="19.7566" x2="-32.7779" y2="17.5115" width="0.1796" layer="48"/>
<wire x1="-32.7779" y1="17.5115" x2="-32.7779" y2="15.7155" width="0.1796" layer="48"/>
<wire x1="-32.7779" y1="15.7155" x2="-32.7779" y2="13.9195" width="0.1796" layer="48"/>
<wire x1="-32.7779" y1="13.9195" x2="-32.7779" y2="12.1234" width="0.1796" layer="48"/>
<wire x1="-62.8617" y1="13.9195" x2="-32.7779" y2="13.9195" width="0.0898" layer="48"/>
<wire x1="-62.8617" y1="15.7155" x2="-32.7779" y2="15.7155" width="0.0898" layer="48"/>
<wire x1="-62.8617" y1="17.5115" x2="-32.7779" y2="17.5115" width="0.0898" layer="48"/>
<wire x1="-38.6151" y1="19.7566" x2="-38.6151" y2="12.1234" width="0.0898" layer="48"/>
<wire x1="-62.8617" y1="19.7566" x2="-69.1479" y2="19.7566" width="0.1796" layer="48"/>
<wire x1="-69.1479" y1="19.7566" x2="-77.6793" y2="19.7566" width="0.1796" layer="48"/>
<wire x1="-77.6793" y1="19.7566" x2="-84.8634" y2="19.7566" width="0.1796" layer="48"/>
<wire x1="-84.8634" y1="19.7566" x2="-84.8634" y2="15.7155" width="0.1796" layer="48"/>
<wire x1="-84.8634" y1="15.7155" x2="-84.8634" y2="13.9195" width="0.1796" layer="48"/>
<wire x1="-84.8634" y1="13.9195" x2="-84.8634" y2="12.1234" width="0.1796" layer="48"/>
<wire x1="-84.8634" y1="12.1234" x2="-84.8634" y2="10.3274" width="0.1796" layer="48"/>
<wire x1="-84.8634" y1="10.3274" x2="-84.8634" y2="8.5314" width="0.1796" layer="48"/>
<wire x1="-84.8634" y1="8.5314" x2="-84.8634" y2="6.7353" width="0.1796" layer="48"/>
<wire x1="-84.8634" y1="6.7353" x2="-84.8634" y2="4.939" width="0.1796" layer="48"/>
<wire x1="-84.8634" y1="4.939" x2="-84.8634" y2="3.143" width="0.1796" layer="48"/>
<wire x1="-84.8634" y1="3.143" x2="-84.8634" y2="0" width="0.1796" layer="48"/>
<wire x1="-84.8634" y1="0" x2="-77.6793" y2="0" width="0.1796" layer="48"/>
<wire x1="-77.6793" y1="0" x2="-69.1479" y2="0" width="0.1796" layer="48"/>
<wire x1="-69.1479" y1="0" x2="-62.8617" y2="0" width="0.1796" layer="48"/>
<wire x1="-62.8617" y1="0" x2="-55.6776" y2="0" width="0.1796" layer="48"/>
<wire x1="-55.6776" y1="0" x2="-49.8404" y2="0" width="0.1796" layer="48"/>
<wire x1="-49.8404" y1="0" x2="-43.1053" y2="0" width="0.1796" layer="48"/>
<wire x1="-43.1053" y1="0" x2="-32.7779" y2="0" width="0.1796" layer="48"/>
<wire x1="-84.8634" y1="15.7155" x2="-62.8617" y2="15.7155" width="0.0898" layer="48"/>
<wire x1="-84.8634" y1="13.9195" x2="-62.8617" y2="13.9195" width="0.0898" layer="48"/>
<wire x1="-84.8634" y1="12.1234" x2="-62.8617" y2="12.1234" width="0.0898" layer="48"/>
<wire x1="-84.8634" y1="10.3274" x2="-62.8617" y2="10.3274" width="0.0898" layer="48"/>
<wire x1="-84.8634" y1="8.5314" x2="-62.8617" y2="8.5314" width="0.0898" layer="48"/>
<wire x1="-84.8634" y1="6.7353" x2="-62.8617" y2="6.7353" width="0.0898" layer="48"/>
<wire x1="-62.8617" y1="3.143" x2="-55.6776" y2="3.143" width="0.1796" layer="48"/>
<wire x1="-55.6776" y1="3.143" x2="-49.8404" y2="3.143" width="0.1796" layer="48"/>
<wire x1="-49.8404" y1="3.143" x2="-43.1053" y2="3.143" width="0.1796" layer="48"/>
<wire x1="-43.1053" y1="3.143" x2="-32.7779" y2="3.143" width="0.1796" layer="48"/>
<wire x1="-69.1479" y1="19.7566" x2="-69.1479" y2="0" width="0.0898" layer="48"/>
<wire x1="-47.1465" y1="19.7566" x2="-47.1465" y2="12.1234" width="0.0898" layer="48"/>
<wire x1="-56.1266" y1="12.1234" x2="-56.1266" y2="19.7566" width="0.0898" layer="48"/>
<wire x1="-77.6793" y1="19.7566" x2="-77.6793" y2="0" width="0.0898" layer="48"/>
<wire x1="-62.8617" y1="3.143" x2="-62.8617" y2="0" width="0.0898" layer="48"/>
<wire x1="-55.6776" y1="3.143" x2="-55.6776" y2="0" width="0.0898" layer="48"/>
<wire x1="-49.8404" y1="3.143" x2="-49.8404" y2="0" width="0.0898" layer="48"/>
<wire x1="-43.1053" y1="3.143" x2="-43.1053" y2="0" width="0.0898" layer="48"/>
<wire x1="-32.7779" y1="19.7566" x2="0" y2="19.7566" width="0.1796" layer="48"/>
<wire x1="0" y1="19.7566" x2="0" y2="15.7155" width="0.1796" layer="48"/>
<wire x1="0" y1="15.7155" x2="0" y2="12.1234" width="0.1796" layer="48"/>
<wire x1="0" y1="12.1234" x2="0" y2="8.5314" width="0.1796" layer="48"/>
<wire x1="0" y1="8.5314" x2="0" y2="4.939" width="0.1796" layer="48"/>
<wire x1="0" y1="4.939" x2="0" y2="0" width="0.1796" layer="48"/>
<wire x1="0" y1="0" x2="-8.5314" y2="0" width="0.1796" layer="48"/>
<wire x1="-8.5314" y1="0" x2="-32.7779" y2="0" width="0.1796" layer="48"/>
<wire x1="-32.7779" y1="15.7155" x2="0" y2="15.7155" width="0.0898" layer="48"/>
<wire x1="-32.7779" y1="12.1234" x2="0" y2="12.1234" width="0.0898" layer="48"/>
<wire x1="-32.7779" y1="8.5314" x2="0" y2="8.5314" width="0.0898" layer="48"/>
<wire x1="-32.7779" y1="4.939" x2="-8.5314" y2="4.939" width="0.0898" layer="48"/>
<wire x1="-8.5314" y1="4.939" x2="0" y2="4.939" width="0.0898" layer="48"/>
<wire x1="-8.5314" y1="4.939" x2="-8.5314" y2="2.6942" width="0.0898" layer="48"/>
<wire x1="-8.5314" y1="2.6942" x2="-8.5314" y2="0" width="0.0898" layer="48"/>
<wire x1="-32.7779" y1="2.6942" x2="-8.5314" y2="2.6942" width="0.0898" layer="48"/>
<wire x1="-84.8634" y1="4.939" x2="-62.8617" y2="4.939" width="0.0898" layer="48"/>
<wire x1="-84.8634" y1="3.143" x2="-62.8617" y2="3.143" width="0.0898" layer="48"/>
<text x="-32.225" y="0.8824" size="1.1836" layer="48">&gt;LAST_DATE_TIME</text>
<text x="-32.225" y="3.1049" size="1.1836" layer="48">&gt;DRAWING_NAME</text>
<text x="-62.23" y="16.1925" size="1.1836" layer="48">Bearb.</text>
<text x="-62.23" y="14.2875" size="1.1836" layer="48">Gepr.</text>
<text x="-62.23" y="12.3825" size="1.1836" layer="48">Norm</text>
<text x="-55.5625" y="17.78" size="1.1836" layer="48">Datum</text>
<text x="-46.6725" y="17.78" size="1.1836" layer="48">Name</text>
<text x="-38.1" y="17.78" size="1.1836" layer="48">Sign.</text>
<text x="-84.455" y="16.1925" size="1.1836" layer="48">stand</text>
<text x="-84.455" y="17.78" size="1.1836" layer="48">Änd.-</text>
<text x="-76.835" y="16.1925" size="1.1836" layer="48">Datum</text>
<text x="-68.58" y="16.1925" size="1.1836" layer="48">Sign.</text>
<text x="-6.6675" y="3.175" size="1.1836" layer="48">Blatt</text>
<text x="-6.6675" y="1.27" size="1.1836" layer="48">1/1</text>
<polygon width="0.003" layer="100" spacing="0.889">
<vertex x="-60.1678" y="11.1316"/>
<vertex x="-60.1678" y="6.8872"/>
<vertex x="-59.1901" y="6.8872"/>
<vertex x="-59.1901" y="8.6444"/>
<vertex x="-57.6186" y="8.6444"/>
<vertex x="-57.6186" y="6.8872"/>
<vertex x="-56.6781" y="6.8872"/>
<vertex x="-56.6781" y="11.1316"/>
<vertex x="-57.6186" y="11.1316"/>
<vertex x="-57.6186" y="9.4239"/>
<vertex x="-59.1901" y="9.4239"/>
<vertex x="-59.1901" y="11.1316"/>
</polygon>
<polygon width="0.003" layer="100" spacing="0.889">
<vertex x="-55.2676" y="11.1316"/>
<vertex x="-54.7106" y="11.1316"/>
<vertex x="-54.7106" y="10.6614"/>
<vertex x="-55.2676" y="10.6614"/>
</polygon>
<polygon width="0.003" layer="100" spacing="0.889">
<vertex x="-52.8668" y="11.1316"/>
<vertex x="-52.2851" y="11.1316"/>
<vertex x="-52.2851" y="10.6614"/>
<vertex x="-52.8668" y="10.6614"/>
</polygon>
<polygon width="0.003" layer="101" spacing="0.889">
<vertex x="-56.0842" y="6.8872"/>
<vertex x="-55.057" y="6.8872"/>
<vertex x="-53.8074" y="10.1788"/>
<vertex x="-51.3944" y="4.0411"/>
<vertex x="-50.4414" y="4.0411"/>
<vertex x="-53.2628" y="11.1316"/>
<vertex x="-54.2775" y="11.1316"/>
</polygon>
<polygon width="0.003" layer="101" spacing="0.889">
<vertex x="-54.389" y="8.6568"/>
<vertex x="-53.2011" y="8.6568"/>
<vertex x="-52.9039" y="7.8895"/>
<vertex x="-54.6857" y="7.8895"/>
</polygon>
<polygon width="0.003" layer="100" spacing="0.889">
<vertex x="-47.9666" y="11.1316"/>
<vertex x="-47.9666" y="10.4137"/>
<vertex x="-49.402" y="10.4137"/>
<vertex x="-49.798" y="10.3396"/>
<vertex x="-50.0207" y="10.2408"/>
<vertex x="-50.1693" y="10.1171"/>
<vertex x="-50.2435" y="9.9931"/>
<vertex x="-50.3423" y="9.795"/>
<vertex x="-50.4167" y="9.523"/>
<vertex x="-50.4662" y="9.2012"/>
<vertex x="-50.4662" y="8.6195"/>
<vertex x="-50.4167" y="8.2608"/>
<vertex x="-50.2806" y="7.9639"/>
<vertex x="-50.1444" y="7.7904"/>
<vertex x="-49.9095" y="7.6667"/>
<vertex x="-49.6992" y="7.6297"/>
<vertex x="-49.4515" y="7.6172"/>
<vertex x="-47.9666" y="7.6172"/>
<vertex x="-47.9666" y="6.8872"/>
<vertex x="-49.9217" y="6.8872"/>
<vertex x="-50.2681" y="6.9492"/>
<vertex x="-50.6024" y="7.0729"/>
<vertex x="-50.9364" y="7.3203"/>
<vertex x="-51.1962" y="7.6172"/>
<vertex x="-51.3199" y="7.877"/>
<vertex x="-51.4436" y="8.2608"/>
<vertex x="-51.481" y="8.5331"/>
<vertex x="-51.4934" y="8.9291"/>
<vertex x="-51.481" y="9.3744"/>
<vertex x="-51.4066" y="9.8448"/>
<vertex x="-51.2707" y="10.1912"/>
<vertex x="-51.1096" y="10.4511"/>
<vertex x="-50.8622" y="10.7358"/>
<vertex x="-50.5404" y="10.9337"/>
<vertex x="-50.2806" y="11.0325"/>
<vertex x="-49.9958" y="11.0823"/>
<vertex x="-49.6743" y="11.1191"/>
<vertex x="-49.3773" y="11.1316"/>
</polygon>
<polygon width="0.003" layer="100" spacing="0.889">
<vertex x="-47.3479" y="11.1316"/>
<vertex x="-47.3479" y="6.8872"/>
<vertex x="-46.4073" y="6.8872"/>
<vertex x="-46.4073" y="11.1316"/>
</polygon>
<polygon width="0.003" layer="100" spacing="0.889">
<vertex x="-46.4073" y="9.1021"/>
<vertex x="-44.7739" y="11.1316"/>
<vertex x="-43.6108" y="11.1316"/>
<vertex x="-45.3433" y="9.0528"/>
<vertex x="-43.6232" y="6.8872"/>
<vertex x="-44.8483" y="6.8872"/>
<vertex x="-46.4073" y="8.9413"/>
</polygon>
<polygon width="0.003" layer="100" spacing="0.889">
<vertex x="-40.084" y="11.1316"/>
<vertex x="-40.084" y="10.3645"/>
<vertex x="-41.6679" y="10.3645"/>
<vertex x="-41.9156" y="10.3396"/>
<vertex x="-42.1383" y="10.2408"/>
<vertex x="-42.2867" y="10.1293"/>
<vertex x="-42.3857" y="9.9931"/>
<vertex x="-42.497" y="9.7828"/>
<vertex x="-42.5468" y="9.5105"/>
<vertex x="-42.5468" y="8.3845"/>
<vertex x="-42.5343" y="8.2608"/>
<vertex x="-42.3982" y="7.9639"/>
<vertex x="-42.262" y="7.7904"/>
<vertex x="-42.0268" y="7.6667"/>
<vertex x="-41.8165" y="7.6297"/>
<vertex x="-41.5691" y="7.6172"/>
<vertex x="-40.084" y="7.6172"/>
<vertex x="-40.084" y="6.8872"/>
<vertex x="-42.0393" y="6.8872"/>
<vertex x="-42.3857" y="6.9492"/>
<vertex x="-42.7198" y="7.0729"/>
<vertex x="-43.054" y="7.3203"/>
<vertex x="-43.3139" y="7.6172"/>
<vertex x="-43.4376" y="7.877"/>
<vertex x="-43.5613" y="8.2608"/>
<vertex x="-43.5983" y="8.5331"/>
<vertex x="-43.6108" y="8.9291"/>
<vertex x="-43.5983" y="9.3744"/>
<vertex x="-43.5242" y="9.8448"/>
<vertex x="-43.388" y="10.1912"/>
<vertex x="-43.2272" y="10.4511"/>
<vertex x="-42.9796" y="10.7358"/>
<vertex x="-42.658" y="10.9337"/>
<vertex x="-42.3982" y="11.0325"/>
<vertex x="-42.1135" y="11.0823"/>
<vertex x="-41.7919" y="11.1191"/>
<vertex x="-41.4947" y="11.1316"/>
</polygon>
<polygon width="0.003" layer="100" spacing="0.889">
<vertex x="-42.5468" y="8.6444"/>
<vertex x="-40.084" y="8.6444"/>
<vertex x="-40.084" y="9.4239"/>
<vertex x="-42.5468" y="9.4239"/>
</polygon>
<polygon width="0.003" layer="100" spacing="0.889">
<vertex x="-39.354" y="11.1316"/>
<vertex x="-36.9781" y="11.1316"/>
<vertex x="-36.5328" y="11.0823"/>
<vertex x="-36.2356" y="10.9954"/>
<vertex x="-36.0378" y="10.8717"/>
<vertex x="-35.9016" y="10.7358"/>
<vertex x="-35.7406" y="10.5253"/>
<vertex x="-35.6542" y="10.2654"/>
<vertex x="-35.6045" y="10.0056"/>
<vertex x="-35.6045" y="9.7208"/>
<vertex x="-35.6542" y="9.4117"/>
<vertex x="-35.7904" y="9.1021"/>
<vertex x="-35.9509" y="8.9042"/>
<vertex x="-36.1244" y="8.7805"/>
<vertex x="-36.3471" y="8.6693"/>
<vertex x="-36.4335" y="8.6319"/>
<vertex x="-35.419" y="6.8872"/>
<vertex x="-36.4833" y="6.8872"/>
<vertex x="-37.3987" y="8.5207"/>
<vertex x="-37.3987" y="9.288"/>
<vertex x="-37.1267" y="9.288"/>
<vertex x="-36.8544" y="9.3622"/>
<vertex x="-36.6441" y="9.4859"/>
<vertex x="-36.5697" y="9.6467"/>
<vertex x="-36.5697" y="10.018"/>
<vertex x="-36.6441" y="10.1664"/>
<vertex x="-36.7556" y="10.2408"/>
<vertex x="-36.8668" y="10.29"/>
<vertex x="-37.0647" y="10.3396"/>
<vertex x="-37.2382" y="10.3645"/>
<vertex x="-38.4012" y="10.3645"/>
<vertex x="-38.4012" y="9.288"/>
<vertex x="-37.4" y="9.288"/>
<vertex x="-37.4" y="8.5207"/>
<vertex x="-38.4012" y="8.5207"/>
<vertex x="-38.4012" y="6.8872"/>
<vertex x="-39.354" y="6.8872"/>
</polygon>
<polygon width="0.003" layer="101" spacing="0.889">
<vertex x="-50.3177" y="5.6375"/>
<vertex x="-49.8475" y="5.6375"/>
<vertex x="-49.8475" y="4.5362"/>
<vertex x="-49.8229" y="4.4988"/>
<vertex x="-49.7733" y="4.4496"/>
<vertex x="-49.7238" y="4.4247"/>
<vertex x="-49.2783" y="4.4247"/>
<vertex x="-49.2783" y="5.6375"/>
<vertex x="-48.8081" y="5.6375"/>
<vertex x="-48.8081" y="4.1029"/>
<vertex x="-48.8699" y="4.0411"/>
<vertex x="-49.7238" y="4.0411"/>
<vertex x="-49.8846" y="4.0536"/>
<vertex x="-49.9466" y="4.0658"/>
<vertex x="-50.0332" y="4.1029"/>
<vertex x="-50.1198" y="4.1524"/>
<vertex x="-50.231" y="4.2639"/>
<vertex x="-50.2806" y="4.3505"/>
<vertex x="-50.3055" y="4.4122"/>
<vertex x="-50.3177" y="4.462"/>
</polygon>
<polygon width="0.003" layer="101" spacing="0.889">
<vertex x="-48.6471" y="5.6375"/>
<vertex x="-48.6471" y="5.3404"/>
<vertex x="-48.3502" y="5.3404"/>
<vertex x="-48.3502" y="4.4247"/>
<vertex x="-48.3006" y="4.301"/>
<vertex x="-48.2016" y="4.1773"/>
<vertex x="-48.0779" y="4.0907"/>
<vertex x="-47.9047" y="4.0411"/>
<vertex x="-47.484" y="4.0411"/>
<vertex x="-47.4221" y="4.0907"/>
<vertex x="-47.4221" y="4.3876"/>
<vertex x="-47.719" y="4.3876"/>
<vertex x="-47.781" y="4.4247"/>
<vertex x="-47.8429" y="4.4988"/>
<vertex x="-47.88" y="4.5979"/>
<vertex x="-47.88" y="5.3404"/>
<vertex x="-47.4221" y="5.3404"/>
<vertex x="-47.4221" y="5.6375"/>
<vertex x="-47.88" y="5.6375"/>
<vertex x="-47.88" y="6.2314"/>
<vertex x="-48.3502" y="6.2314"/>
<vertex x="-48.3502" y="5.6375"/>
</polygon>
<polygon width="0.003" layer="101" spacing="0.889">
<vertex x="-47.3479" y="4.8689"/>
<vertex x="-47.3479" y="4.7465"/>
<vertex x="-47.2984" y="4.5237"/>
<vertex x="-47.1747" y="4.3134"/>
<vertex x="-46.989" y="4.1648"/>
<vertex x="-46.7911" y="4.0658"/>
<vertex x="-46.6301" y="4.0411"/>
<vertex x="-46.3085" y="4.0411"/>
<vertex x="-46.0855" y="4.0907"/>
<vertex x="-45.9125" y="4.1773"/>
<vertex x="-45.7764" y="4.3134"/>
<vertex x="-45.6649" y="4.4742"/>
<vertex x="-45.6278" y="4.5944"/>
<vertex x="-45.6154" y="4.6723"/>
<vertex x="-45.6154" y="5.031"/>
<vertex x="-45.6649" y="5.2413"/>
<vertex x="-45.7639" y="5.3899"/>
<vertex x="-45.9125" y="5.526"/>
<vertex x="-46.0855" y="5.6126"/>
<vertex x="-46.3085" y="5.6744"/>
<vertex x="-46.655" y="5.6744"/>
<vertex x="-46.8282" y="5.6375"/>
<vertex x="-47.0139" y="5.5385"/>
<vertex x="-47.1251" y="5.4272"/>
<vertex x="-47.2488" y="5.2662"/>
<vertex x="-47.323" y="5.093"/>
<vertex x="-47.3479" y="4.9568"/>
<vertex x="-47.3479" y="4.8702"/>
<vertex x="-46.8777" y="4.8702"/>
<vertex x="-46.8777" y="4.9446"/>
<vertex x="-46.8404" y="5.0559"/>
<vertex x="-46.7911" y="5.1425"/>
<vertex x="-46.6796" y="5.2662"/>
<vertex x="-46.5559" y="5.3279"/>
<vertex x="-46.3951" y="5.3279"/>
<vertex x="-46.321" y="5.3033"/>
<vertex x="-46.2095" y="5.2291"/>
<vertex x="-46.1228" y="5.093"/>
<vertex x="-46.0855" y="4.9693"/>
<vertex x="-46.0855" y="4.7341"/>
<vertex x="-46.1228" y="4.6228"/>
<vertex x="-46.1599" y="4.5484"/>
<vertex x="-46.2465" y="4.4496"/>
<vertex x="-46.3332" y="4.4"/>
<vertex x="-46.3951" y="4.3876"/>
<vertex x="-46.5681" y="4.3876"/>
<vertex x="-46.6796" y="4.4247"/>
<vertex x="-46.7787" y="4.5237"/>
<vertex x="-46.8528" y="4.6723"/>
<vertex x="-46.8777" y="4.7836"/>
<vertex x="-46.8777" y="4.8689"/>
</polygon>
<polygon width="0.003" layer="101" spacing="0.889">
<vertex x="-38.8838" y="4.8689"/>
<vertex x="-38.8838" y="4.7465"/>
<vertex x="-38.8343" y="4.5237"/>
<vertex x="-38.7104" y="4.3134"/>
<vertex x="-38.5249" y="4.1648"/>
<vertex x="-38.3271" y="4.0658"/>
<vertex x="-38.166" y="4.0411"/>
<vertex x="-37.8445" y="4.0411"/>
<vertex x="-37.6217" y="4.0907"/>
<vertex x="-37.4485" y="4.1773"/>
<vertex x="-37.3123" y="4.3134"/>
<vertex x="-37.2008" y="4.4742"/>
<vertex x="-37.1638" y="4.5944"/>
<vertex x="-37.1513" y="4.6723"/>
<vertex x="-37.1513" y="5.031"/>
<vertex x="-37.2008" y="5.2413"/>
<vertex x="-37.2999" y="5.3899"/>
<vertex x="-37.4485" y="5.526"/>
<vertex x="-37.6217" y="5.6126"/>
<vertex x="-37.8445" y="5.6744"/>
<vertex x="-38.1909" y="5.6744"/>
<vertex x="-38.3639" y="5.6375"/>
<vertex x="-38.5498" y="5.5385"/>
<vertex x="-38.6611" y="5.4272"/>
<vertex x="-38.7848" y="5.2662"/>
<vertex x="-38.859" y="5.093"/>
<vertex x="-38.8838" y="4.9568"/>
<vertex x="-38.8838" y="4.8702"/>
<vertex x="-38.4137" y="4.8702"/>
<vertex x="-38.4137" y="4.9446"/>
<vertex x="-38.3764" y="5.0559"/>
<vertex x="-38.3271" y="5.1425"/>
<vertex x="-38.2156" y="5.2662"/>
<vertex x="-38.0919" y="5.3279"/>
<vertex x="-37.9311" y="5.3279"/>
<vertex x="-37.8569" y="5.3033"/>
<vertex x="-37.7452" y="5.2291"/>
<vertex x="-37.6588" y="5.093"/>
<vertex x="-37.6217" y="4.9693"/>
<vertex x="-37.6217" y="4.7341"/>
<vertex x="-37.6588" y="4.6228"/>
<vertex x="-37.6959" y="4.5484"/>
<vertex x="-37.7825" y="4.4496"/>
<vertex x="-37.8691" y="4.4"/>
<vertex x="-37.9311" y="4.3876"/>
<vertex x="-38.1043" y="4.3876"/>
<vertex x="-38.2156" y="4.4247"/>
<vertex x="-38.3146" y="4.5237"/>
<vertex x="-38.3888" y="4.6723"/>
<vertex x="-38.4137" y="4.7836"/>
<vertex x="-38.4137" y="4.8689"/>
</polygon>
<polygon width="0.003" layer="101" spacing="0.889">
<vertex x="-39.5521" y="5.6375"/>
<vertex x="-39.5521" y="4.0411"/>
<vertex x="-39.0817" y="4.0411"/>
<vertex x="-39.0817" y="5.6375"/>
</polygon>
<polygon width="0.003" layer="101" spacing="0.889">
<vertex x="-39.5521" y="6.2314"/>
<vertex x="-39.5521" y="5.8354"/>
<vertex x="-39.0817" y="5.8354"/>
<vertex x="-39.0817" y="6.2314"/>
</polygon>
<polygon width="0.003" layer="101" spacing="0.889">
<vertex x="-35.4315" y="4.0411"/>
<vertex x="-35.9016" y="4.0411"/>
<vertex x="-35.9016" y="5.1796"/>
<vertex x="-35.9265" y="5.2167"/>
<vertex x="-35.9758" y="5.2662"/>
<vertex x="-36.0253" y="5.2911"/>
<vertex x="-36.446" y="5.2911"/>
<vertex x="-36.446" y="4.0411"/>
<vertex x="-36.9039" y="4.0411"/>
<vertex x="-36.9039" y="5.6375"/>
<vertex x="-35.8272" y="5.6375"/>
<vertex x="-35.7779" y="5.6251"/>
<vertex x="-35.716" y="5.6002"/>
<vertex x="-35.6169" y="5.5509"/>
<vertex x="-35.5552" y="5.4889"/>
<vertex x="-35.4932" y="5.4023"/>
<vertex x="-35.4439" y="5.3033"/>
<vertex x="-35.4315" y="5.2167"/>
</polygon>
<polygon width="0.003" layer="101" spacing="0.889">
<vertex x="-43.1157" y="4.0411"/>
<vertex x="-43.5366" y="4.0411"/>
<vertex x="-43.5366" y="5.1796"/>
<vertex x="-43.5613" y="5.2167"/>
<vertex x="-43.6108" y="5.2662"/>
<vertex x="-43.6603" y="5.2911"/>
<vertex x="-43.9821" y="5.2911"/>
<vertex x="-43.9821" y="4.0907"/>
<vertex x="-44.0314" y="4.0411"/>
<vertex x="-44.4274" y="4.0411"/>
<vertex x="-44.4894" y="4.1029"/>
<vertex x="-44.4894" y="5.2911"/>
<vertex x="-44.91" y="5.2911"/>
<vertex x="-44.91" y="4.0907"/>
<vertex x="-44.9598" y="4.0411"/>
<vertex x="-45.3804" y="4.0411"/>
<vertex x="-45.3804" y="5.6375"/>
<vertex x="-43.4622" y="5.6375"/>
<vertex x="-43.4129" y="5.6251"/>
<vertex x="-43.3509" y="5.6002"/>
<vertex x="-43.2519" y="5.5509"/>
<vertex x="-43.1899" y="5.4889"/>
<vertex x="-43.1282" y="5.4023"/>
<vertex x="-43.0787" y="5.3033"/>
<vertex x="-43.0662" y="5.2167"/>
<vertex x="-43.0662" y="4.1029"/>
<vertex x="-43.1157" y="4.0536"/>
</polygon>
<polygon width="0.003" layer="101" spacing="0.889">
<vertex x="-41.1358" y="4.1029"/>
<vertex x="-41.1978" y="4.0411"/>
<vertex x="-42.3733" y="4.0411"/>
<vertex x="-42.5714" y="4.0907"/>
<vertex x="-42.7198" y="4.2144"/>
<vertex x="-42.8066" y="4.3381"/>
<vertex x="-42.8188" y="4.4247"/>
<vertex x="-42.8188" y="4.6723"/>
<vertex x="-42.7446" y="4.8331"/>
<vertex x="-42.6705" y="4.9197"/>
<vertex x="-42.6209" y="4.9446"/>
<vertex x="-42.5094" y="4.9939"/>
<vertex x="-42.3733" y="5.0188"/>
<vertex x="-41.6446" y="5.0188"/>
<vertex x="-41.6446" y="4.7092"/>
<vertex x="-42.2125" y="4.7092"/>
<vertex x="-42.262" y="4.6845"/>
<vertex x="-42.2991" y="4.6599"/>
<vertex x="-42.324" y="4.6228"/>
<vertex x="-42.3365" y="4.5979"/>
<vertex x="-42.3487" y="4.5484"/>
<vertex x="-42.3487" y="4.5113"/>
<vertex x="-42.3365" y="4.4742"/>
<vertex x="-42.2991" y="4.4247"/>
<vertex x="-42.262" y="4.4"/>
<vertex x="-42.2247" y="4.3876"/>
<vertex x="-41.6433" y="4.3876"/>
<vertex x="-41.6433" y="5.1796"/>
<vertex x="-41.6679" y="5.2167"/>
<vertex x="-41.7177" y="5.2662"/>
<vertex x="-41.767" y="5.2911"/>
<vertex x="-42.6951" y="5.2911"/>
<vertex x="-42.6951" y="5.6375"/>
<vertex x="-41.5691" y="5.6375"/>
<vertex x="-41.5196" y="5.6251"/>
<vertex x="-41.4576" y="5.6002"/>
<vertex x="-41.3588" y="5.5509"/>
<vertex x="-41.2968" y="5.4889"/>
<vertex x="-41.2351" y="5.4023"/>
<vertex x="-41.1853" y="5.3033"/>
<vertex x="-41.1358" y="5.1054"/>
</polygon>
<polygon width="0.003" layer="101" spacing="0.889">
<vertex x="-40.975" y="5.6375"/>
<vertex x="-40.975" y="5.3404"/>
<vertex x="-40.6781" y="5.3404"/>
<vertex x="-40.6781" y="4.4247"/>
<vertex x="-40.6286" y="4.301"/>
<vertex x="-40.5295" y="4.1773"/>
<vertex x="-40.4058" y="4.0907"/>
<vertex x="-40.2326" y="4.0411"/>
<vertex x="-39.8117" y="4.0411"/>
<vertex x="-39.75" y="4.0907"/>
<vertex x="-39.75" y="4.3876"/>
<vertex x="-40.0469" y="4.3876"/>
<vertex x="-40.1089" y="4.4247"/>
<vertex x="-40.1706" y="4.4988"/>
<vertex x="-40.2079" y="4.5979"/>
<vertex x="-40.2079" y="5.3404"/>
<vertex x="-39.75" y="5.3404"/>
<vertex x="-39.75" y="5.6375"/>
<vertex x="-40.2079" y="5.6375"/>
<vertex x="-40.2079" y="6.2314"/>
<vertex x="-40.6781" y="6.2314"/>
<vertex x="-40.6781" y="5.6375"/>
</polygon>
<polygon width="0.003" layer="100">
<vertex x="-60.1678" y="4.3584"/>
<vertex x="-59.8505" y="4.3584"/>
<vertex x="-59.8505" y="4.0411"/>
<vertex x="-60.1678" y="4.0411"/>
</polygon>
<polygon width="0.003" layer="100">
<vertex x="-60.1678" y="4.9929"/>
<vertex x="-60.1678" y="4.6756"/>
<vertex x="-59.8505" y="4.6756"/>
<vertex x="-59.8505" y="4.9929"/>
</polygon>
<polygon width="0.003" layer="100">
<vertex x="-60.1678" y="5.6271"/>
<vertex x="-60.1678" y="5.3101"/>
<vertex x="-59.8505" y="5.3101"/>
<vertex x="-59.8505" y="5.6271"/>
</polygon>
<polygon width="0.003" layer="100">
<vertex x="-59.5333" y="5.6271"/>
<vertex x="-59.5333" y="5.3101"/>
<vertex x="-59.216" y="5.3101"/>
<vertex x="-59.216" y="5.6271"/>
</polygon>
<polygon width="0.003" layer="100">
<vertex x="-59.5333" y="4.9929"/>
<vertex x="-59.5333" y="4.6756"/>
<vertex x="-59.216" y="4.6756"/>
<vertex x="-59.216" y="4.9929"/>
</polygon>
<polygon width="0.003" layer="100">
<vertex x="-59.5333" y="4.3584"/>
<vertex x="-59.5333" y="4.0411"/>
<vertex x="-59.216" y="4.0411"/>
<vertex x="-59.216" y="4.3584"/>
</polygon>
<polygon width="0.003" layer="100">
<vertex x="-58.8988" y="5.6271"/>
<vertex x="-58.8988" y="5.3101"/>
<vertex x="-58.5815" y="5.3101"/>
<vertex x="-58.5815" y="5.6271"/>
</polygon>
<polygon width="0.003" layer="100">
<vertex x="-56.3611" y="5.6271"/>
<vertex x="-56.3611" y="5.3101"/>
<vertex x="-56.0438" y="5.3101"/>
<vertex x="-56.0438" y="5.6271"/>
</polygon>
<polygon width="0.003" layer="100">
<vertex x="-55.7263" y="5.6271"/>
<vertex x="-55.7263" y="5.3101"/>
<vertex x="-55.4093" y="5.3101"/>
<vertex x="-55.4093" y="5.6271"/>
</polygon>
<polygon width="0.003" layer="100">
<vertex x="-58.8988" y="4.9929"/>
<vertex x="-58.8988" y="4.6756"/>
<vertex x="-58.5815" y="4.6756"/>
<vertex x="-58.5815" y="4.9929"/>
</polygon>
<polygon width="0.003" layer="100">
<vertex x="-58.8988" y="4.3584"/>
<vertex x="-58.8988" y="4.0411"/>
<vertex x="-58.5815" y="4.0411"/>
<vertex x="-58.5815" y="4.3584"/>
</polygon>
<polygon width="0.003" layer="100">
<vertex x="-58.2643" y="4.3584"/>
<vertex x="-57.9471" y="4.3584"/>
<vertex x="-57.9471" y="4.0411"/>
<vertex x="-58.2643" y="4.0411"/>
</polygon>
<polygon width="0.003" layer="100">
<vertex x="-58.2643" y="4.9929"/>
<vertex x="-58.2643" y="4.6756"/>
<vertex x="-57.9471" y="4.6756"/>
<vertex x="-57.9471" y="4.9929"/>
</polygon>
<polygon width="0.003" layer="100">
<vertex x="-58.2643" y="5.6271"/>
<vertex x="-58.2643" y="5.3101"/>
<vertex x="-57.9471" y="5.3101"/>
<vertex x="-57.9471" y="5.6271"/>
</polygon>
<polygon width="0.003" layer="100">
<vertex x="-57.6298" y="5.6271"/>
<vertex x="-57.6298" y="5.3101"/>
<vertex x="-57.3128" y="5.3101"/>
<vertex x="-57.3128" y="5.6271"/>
</polygon>
<polygon width="0.003" layer="100">
<vertex x="-57.6298" y="4.9929"/>
<vertex x="-57.6298" y="4.6756"/>
<vertex x="-57.3128" y="4.6756"/>
<vertex x="-57.3128" y="4.9929"/>
</polygon>
<polygon width="0.003" layer="100">
<vertex x="-57.6298" y="4.3584"/>
<vertex x="-57.6298" y="4.0411"/>
<vertex x="-57.3128" y="4.0411"/>
<vertex x="-57.3128" y="4.3584"/>
</polygon>
<polygon width="0.003" layer="100">
<vertex x="-56.9953" y="5.6271"/>
<vertex x="-56.9953" y="5.3101"/>
<vertex x="-56.6781" y="5.3101"/>
<vertex x="-56.6781" y="5.6271"/>
</polygon>
<polygon width="0.003" layer="100">
<vertex x="-56.9953" y="4.9929"/>
<vertex x="-56.9953" y="4.6756"/>
<vertex x="-56.6781" y="4.6756"/>
<vertex x="-56.6781" y="4.9929"/>
</polygon>
<polygon width="0.003" layer="100">
<vertex x="-56.9953" y="4.3584"/>
<vertex x="-56.9953" y="4.0411"/>
<vertex x="-56.6781" y="4.0411"/>
<vertex x="-56.6781" y="4.3584"/>
</polygon>
</package>
<package name="SCHRI_A6">
<wire x1="-23.1775" y1="0" x2="-23.1775" y2="1.905" width="0.127" layer="48"/>
<wire x1="-23.1775" y1="1.905" x2="-23.1775" y2="2.2225" width="0.127" layer="48"/>
<wire x1="-23.1775" y1="2.2225" x2="-23.1775" y2="3.4925" width="0.127" layer="48"/>
<wire x1="-23.1775" y1="3.4925" x2="-23.1775" y2="6.0325" width="0.127" layer="48"/>
<wire x1="-23.1775" y1="6.0325" x2="-23.1775" y2="8.5725" width="0.127" layer="48"/>
<wire x1="-23.1775" y1="8.5725" x2="-27.305" y2="8.5725" width="0.127" layer="48"/>
<wire x1="-27.305" y1="8.5725" x2="-33.3375" y2="8.5725" width="0.127" layer="48"/>
<wire x1="-33.3375" y1="8.5725" x2="-39.6875" y2="8.5725" width="0.127" layer="48"/>
<wire x1="-39.6875" y1="8.5725" x2="-44.45" y2="8.5725" width="0.127" layer="48"/>
<wire x1="-44.45" y1="8.5725" x2="-44.45" y2="7.3025" width="0.127" layer="48"/>
<wire x1="-44.45" y1="7.3025" x2="-44.45" y2="6.0325" width="0.127" layer="48"/>
<wire x1="-44.45" y1="6.0325" x2="-44.45" y2="3.4925" width="0.127" layer="48"/>
<wire x1="-44.45" y1="3.4925" x2="-44.45" y2="2.2225" width="0.127" layer="48"/>
<wire x1="-44.45" y1="8.5725" x2="-44.45" y2="9.8425" width="0.127" layer="48"/>
<wire x1="-44.45" y1="9.8425" x2="-44.45" y2="11.1125" width="0.127" layer="48"/>
<wire x1="-44.45" y1="11.1125" x2="-44.45" y2="12.3825" width="0.127" layer="48"/>
<wire x1="-44.45" y1="12.3825" x2="-44.45" y2="13.97" width="0.127" layer="48"/>
<wire x1="-44.45" y1="13.97" x2="-39.6875" y2="13.97" width="0.127" layer="48"/>
<wire x1="-39.6875" y1="13.97" x2="-33.3375" y2="13.97" width="0.127" layer="48"/>
<wire x1="-33.3375" y1="13.97" x2="-27.305" y2="13.97" width="0.127" layer="48"/>
<wire x1="-27.305" y1="13.97" x2="-23.1775" y2="13.97" width="0.127" layer="48"/>
<wire x1="-23.1775" y1="13.97" x2="-23.1775" y2="12.3825" width="0.127" layer="48"/>
<wire x1="-23.1775" y1="12.3825" x2="-23.1775" y2="11.1125" width="0.127" layer="48"/>
<wire x1="-23.1775" y1="11.1125" x2="-23.1775" y2="9.8425" width="0.127" layer="48"/>
<wire x1="-23.1775" y1="9.8425" x2="-23.1775" y2="8.5725" width="0.127" layer="48"/>
<wire x1="-44.45" y1="9.8425" x2="-23.1775" y2="9.8425" width="0.0634" layer="48"/>
<wire x1="-44.45" y1="11.1125" x2="-23.1775" y2="11.1125" width="0.0634" layer="48"/>
<wire x1="-44.45" y1="12.3825" x2="-23.1775" y2="12.3825" width="0.0634" layer="48"/>
<wire x1="-27.305" y1="13.97" x2="-27.305" y2="8.5725" width="0.0634" layer="48"/>
<wire x1="-44.45" y1="13.97" x2="-48.895" y2="13.97" width="0.127" layer="48"/>
<wire x1="-48.895" y1="13.97" x2="-54.9275" y2="13.97" width="0.127" layer="48"/>
<wire x1="-54.9275" y1="13.97" x2="-60.0075" y2="13.97" width="0.127" layer="48"/>
<wire x1="-60.0075" y1="13.97" x2="-60.0075" y2="11.1125" width="0.127" layer="48"/>
<wire x1="-60.0075" y1="11.1125" x2="-60.0075" y2="9.8425" width="0.127" layer="48"/>
<wire x1="-60.0075" y1="9.8425" x2="-60.0075" y2="8.5725" width="0.127" layer="48"/>
<wire x1="-60.0075" y1="8.5725" x2="-60.0075" y2="7.3025" width="0.127" layer="48"/>
<wire x1="-60.0075" y1="7.3025" x2="-60.0075" y2="6.0325" width="0.127" layer="48"/>
<wire x1="-60.0075" y1="6.0325" x2="-60.0075" y2="3.4925" width="0.127" layer="48"/>
<wire x1="-60.0075" y1="3.4925" x2="-60.0075" y2="2.2225" width="0.127" layer="48"/>
<wire x1="-60.0075" y1="2.2225" x2="-60.0075" y2="0" width="0.127" layer="48"/>
<wire x1="-60.0075" y1="0" x2="-54.9275" y2="0" width="0.127" layer="48"/>
<wire x1="-54.9275" y1="0" x2="-48.895" y2="0" width="0.127" layer="48"/>
<wire x1="-48.895" y1="0" x2="-44.45" y2="0" width="0.127" layer="48"/>
<wire x1="-44.45" y1="0" x2="-39.37" y2="0" width="0.127" layer="48"/>
<wire x1="-39.37" y1="0" x2="-35.2425" y2="0" width="0.127" layer="48"/>
<wire x1="-35.2425" y1="0" x2="-30.48" y2="0" width="0.127" layer="48"/>
<wire x1="-30.48" y1="0" x2="-23.1775" y2="0" width="0.127" layer="48"/>
<wire x1="-60.0075" y1="11.1125" x2="-44.45" y2="11.1125" width="0.0634" layer="48"/>
<wire x1="-60.0075" y1="9.8425" x2="-44.45" y2="9.8425" width="0.0634" layer="48"/>
<wire x1="-60.0075" y1="8.5725" x2="-44.45" y2="8.5725" width="0.0634" layer="48"/>
<wire x1="-60.0075" y1="7.3025" x2="-44.45" y2="7.3025" width="0.0634" layer="48"/>
<wire x1="-60.0075" y1="6.0325" x2="-44.45" y2="6.0325" width="0.0634" layer="48"/>
<wire x1="-44.45" y1="2.2225" x2="-39.37" y2="2.2225" width="0.127" layer="48"/>
<wire x1="-39.37" y1="2.2225" x2="-35.2425" y2="2.2225" width="0.127" layer="48"/>
<wire x1="-35.2425" y1="2.2225" x2="-30.48" y2="2.2225" width="0.127" layer="48"/>
<wire x1="-30.48" y1="2.2225" x2="-23.1775" y2="2.2225" width="0.127" layer="48"/>
<wire x1="-48.895" y1="13.97" x2="-48.895" y2="0" width="0.0634" layer="48"/>
<wire x1="-33.3375" y1="13.97" x2="-33.3375" y2="8.5725" width="0.0634" layer="48"/>
<wire x1="-39.6875" y1="8.5725" x2="-39.6875" y2="13.97" width="0.0634" layer="48"/>
<wire x1="-54.9275" y1="13.97" x2="-54.9275" y2="0" width="0.0634" layer="48"/>
<wire x1="-44.45" y1="2.2225" x2="-44.45" y2="0" width="0.0634" layer="48"/>
<wire x1="-39.37" y1="2.2225" x2="-39.37" y2="0" width="0.0634" layer="48"/>
<wire x1="-35.2425" y1="2.2225" x2="-35.2425" y2="0" width="0.0634" layer="48"/>
<wire x1="-30.48" y1="2.2225" x2="-30.48" y2="0" width="0.0634" layer="48"/>
<wire x1="-23.1775" y1="13.97" x2="0" y2="13.97" width="0.127" layer="48"/>
<wire x1="0" y1="13.97" x2="0" y2="11.1125" width="0.127" layer="48"/>
<wire x1="0" y1="11.1125" x2="0" y2="8.5725" width="0.127" layer="48"/>
<wire x1="0" y1="8.5725" x2="0" y2="6.0325" width="0.127" layer="48"/>
<wire x1="0" y1="6.0325" x2="0" y2="3.4925" width="0.127" layer="48"/>
<wire x1="0" y1="3.4925" x2="0" y2="0" width="0.127" layer="48"/>
<wire x1="0" y1="0" x2="-6.0325" y2="0" width="0.127" layer="48"/>
<wire x1="-6.0325" y1="0" x2="-23.1775" y2="0" width="0.127" layer="48"/>
<wire x1="-23.1775" y1="11.1125" x2="0" y2="11.1125" width="0.0634" layer="48"/>
<wire x1="-23.1775" y1="8.5725" x2="0" y2="8.5725" width="0.0634" layer="48"/>
<wire x1="-23.1775" y1="6.0325" x2="0" y2="6.0325" width="0.0634" layer="48"/>
<wire x1="-23.1775" y1="3.4925" x2="-6.0325" y2="3.4925" width="0.0634" layer="48"/>
<wire x1="-6.0325" y1="3.4925" x2="0" y2="3.4925" width="0.0634" layer="48"/>
<wire x1="-6.0325" y1="3.4925" x2="-6.0325" y2="1.905" width="0.0634" layer="48"/>
<wire x1="-6.0325" y1="1.905" x2="-6.0325" y2="0" width="0.0634" layer="48"/>
<wire x1="-23.1775" y1="1.905" x2="-6.0325" y2="1.905" width="0.0634" layer="48"/>
<wire x1="-60.0075" y1="3.4925" x2="-44.45" y2="3.4925" width="0.0634" layer="48"/>
<wire x1="-60.0075" y1="2.2225" x2="-44.45" y2="2.2225" width="0.0634" layer="48"/>
<wire x1="-60.1075" y1="4.7425" x2="-44.55" y2="4.7425" width="0.0634" layer="48"/>
<text x="-22.695" y="0.5349" size="0.8128" layer="48">&gt;LAST_DATE_TIME</text>
<text x="-22.795" y="2.2099" size="0.8128" layer="48">&gt;DRAWING_NAME</text>
<text x="-44.2" y="11.295" size="0.8128" layer="48">Bearb.</text>
<text x="-44.1" y="10.055" size="0.8128" layer="48">Gepr.</text>
<text x="-44.1" y="8.815" size="0.8128" layer="48">Norm</text>
<text x="-39.375" y="12.735" size="0.8128" layer="48">Datum</text>
<text x="-32.875" y="12.735" size="0.8128" layer="48">Name</text>
<text x="-26.91" y="12.735" size="0.8128" layer="48">Sign.</text>
<text x="-59.48" y="11.395" size="0.8128" layer="48">stand</text>
<text x="-59.48" y="12.635" size="0.8128" layer="48">Änd.-</text>
<text x="-54.555" y="11.395" size="0.8128" layer="48">Datum</text>
<text x="-48.39" y="11.395" size="0.8128" layer="48">Sign.</text>
<text x="-5.08" y="1.905" size="0.8128" layer="48">Blatt</text>
<text x="-4.965" y="0.74" size="0.8128" layer="48">1/1</text>
<polygon width="0.0022" layer="100" spacing="0.889">
<vertex x="-42.545" y="7.8712"/>
<vertex x="-42.545" y="4.8699"/>
<vertex x="-41.8536" y="4.8699"/>
<vertex x="-41.8536" y="6.1125"/>
<vertex x="-40.7426" y="6.1125"/>
<vertex x="-40.7426" y="4.8699"/>
<vertex x="-40.0774" y="4.8699"/>
<vertex x="-40.0774" y="7.8712"/>
<vertex x="-40.7426" y="7.8712"/>
<vertex x="-40.7426" y="6.6637"/>
<vertex x="-41.8536" y="6.6637"/>
<vertex x="-41.8536" y="7.8712"/>
</polygon>
<polygon width="0.0022" layer="100" spacing="0.889">
<vertex x="-39.0802" y="7.8712"/>
<vertex x="-38.6862" y="7.8712"/>
<vertex x="-38.6862" y="7.5387"/>
<vertex x="-39.0802" y="7.5387"/>
</polygon>
<polygon width="0.0022" layer="100" spacing="0.889">
<vertex x="-37.3825" y="7.8712"/>
<vertex x="-36.9712" y="7.8712"/>
<vertex x="-36.9712" y="7.5387"/>
<vertex x="-37.3825" y="7.5387"/>
</polygon>
<polygon width="0.0022" layer="101" spacing="0.889">
<vertex x="-39.6575" y="4.8699"/>
<vertex x="-38.9313" y="4.8699"/>
<vertex x="-38.0474" y="7.1973"/>
<vertex x="-36.3413" y="2.8575"/>
<vertex x="-35.6674" y="2.8575"/>
<vertex x="-37.6626" y="7.8712"/>
<vertex x="-38.3799" y="7.8712"/>
</polygon>
<polygon width="0.0022" layer="101" spacing="0.889">
<vertex x="-38.4589" y="6.1214"/>
<vertex x="-37.6187" y="6.1211"/>
<vertex x="-37.4086" y="5.5786"/>
<vertex x="-38.6687" y="5.5786"/>
</polygon>
<polygon width="0.0022" layer="100" spacing="0.889">
<vertex x="-33.9176" y="7.8712"/>
<vertex x="-33.9176" y="7.3637"/>
<vertex x="-34.9326" y="7.3637"/>
<vertex x="-35.2125" y="7.3111"/>
<vertex x="-35.37" y="7.2413"/>
<vertex x="-35.4749" y="7.1539"/>
<vertex x="-35.5275" y="7.0663"/>
<vertex x="-35.5973" y="6.9261"/>
<vertex x="-35.6499" y="6.7338"/>
<vertex x="-35.685" y="6.5062"/>
<vertex x="-35.685" y="6.095"/>
<vertex x="-35.6499" y="5.8412"/>
<vertex x="-35.5537" y="5.6312"/>
<vertex x="-35.4576" y="5.5088"/>
<vertex x="-35.2913" y="5.4214"/>
<vertex x="-35.1427" y="5.395"/>
<vertex x="-34.9677" y="5.3863"/>
<vertex x="-33.9176" y="5.3863"/>
<vertex x="-33.9176" y="4.8699"/>
<vertex x="-35.3002" y="4.8699"/>
<vertex x="-35.545" y="4.9136"/>
<vertex x="-35.7812" y="5.0013"/>
<vertex x="-36.0175" y="5.1763"/>
<vertex x="-36.2014" y="5.3861"/>
<vertex x="-36.2887" y="5.57"/>
<vertex x="-36.3764" y="5.8412"/>
<vertex x="-36.4025" y="6.0338"/>
<vertex x="-36.4114" y="6.3137"/>
<vertex x="-36.4025" y="6.6289"/>
<vertex x="-36.3499" y="6.9614"/>
<vertex x="-36.2539" y="7.2062"/>
<vertex x="-36.1399" y="7.3899"/>
<vertex x="-35.9649" y="7.5913"/>
<vertex x="-35.7375" y="7.7313"/>
<vertex x="-35.5537" y="7.8014"/>
<vertex x="-35.3525" y="7.8364"/>
<vertex x="-35.1249" y="7.8626"/>
<vertex x="-34.9148" y="7.8712"/>
</polygon>
<polygon width="0.0022" layer="100" spacing="0.889">
<vertex x="-33.48" y="7.8712"/>
<vertex x="-33.48" y="4.8699"/>
<vertex x="-32.815" y="4.8699"/>
<vertex x="-32.815" y="7.8712"/>
</polygon>
<polygon width="0.0022" layer="100" spacing="0.889">
<vertex x="-32.815" y="6.4364"/>
<vertex x="-31.6601" y="7.8712"/>
<vertex x="-30.8374" y="7.8712"/>
<vertex x="-32.0624" y="6.4013"/>
<vertex x="-30.8463" y="4.8699"/>
<vertex x="-31.7127" y="4.8699"/>
<vertex x="-32.815" y="6.3223"/>
</polygon>
<polygon width="0.0022" layer="100" spacing="0.889">
<vertex x="-28.3436" y="7.8712"/>
<vertex x="-28.3436" y="7.3287"/>
<vertex x="-29.4637" y="7.3287"/>
<vertex x="-29.6388" y="7.3111"/>
<vertex x="-29.7962" y="7.2413"/>
<vertex x="-29.9014" y="7.1625"/>
<vertex x="-29.9712" y="7.0663"/>
<vertex x="-30.05" y="6.9177"/>
<vertex x="-30.085" y="6.7252"/>
<vertex x="-30.085" y="5.9286"/>
<vertex x="-30.0764" y="5.8412"/>
<vertex x="-29.9799" y="5.6314"/>
<vertex x="-29.8836" y="5.5088"/>
<vertex x="-29.7175" y="5.4211"/>
<vertex x="-29.5686" y="5.395"/>
<vertex x="-29.3936" y="5.3863"/>
<vertex x="-28.3436" y="5.3861"/>
<vertex x="-28.3436" y="4.8699"/>
<vertex x="-29.7261" y="4.8699"/>
<vertex x="-29.9712" y="4.9139"/>
<vertex x="-30.2075" y="5.0013"/>
<vertex x="-30.4439" y="5.1763"/>
<vertex x="-30.6273" y="5.3861"/>
<vertex x="-30.715" y="5.57"/>
<vertex x="-30.8023" y="5.8412"/>
<vertex x="-30.8287" y="6.0338"/>
<vertex x="-30.8374" y="6.3139"/>
<vertex x="-30.8287" y="6.6289"/>
<vertex x="-30.7762" y="6.9614"/>
<vertex x="-30.6799" y="7.2062"/>
<vertex x="-30.5661" y="7.3899"/>
<vertex x="-30.3914" y="7.5913"/>
<vertex x="-30.1638" y="7.7313"/>
<vertex x="-29.9799" y="7.8014"/>
<vertex x="-29.7787" y="7.8362"/>
<vertex x="-29.5511" y="7.8626"/>
<vertex x="-29.3411" y="7.8712"/>
</polygon>
<polygon width="0.0022" layer="100" spacing="0.889">
<vertex x="-30.085" y="6.1125"/>
<vertex x="-28.3436" y="6.1125"/>
<vertex x="-28.3436" y="6.6637"/>
<vertex x="-30.085" y="6.6637"/>
</polygon>
<polygon width="0.0022" layer="100" spacing="0.889">
<vertex x="-27.8275" y="7.8712"/>
<vertex x="-26.1475" y="7.8712"/>
<vertex x="-25.8326" y="7.8364"/>
<vertex x="-25.6225" y="7.7749"/>
<vertex x="-25.4826" y="7.6876"/>
<vertex x="-25.3863" y="7.5913"/>
<vertex x="-25.2725" y="7.4425"/>
<vertex x="-25.2113" y="7.2588"/>
<vertex x="-25.1762" y="7.0749"/>
<vertex x="-25.1762" y="6.8737"/>
<vertex x="-25.2113" y="6.6551"/>
<vertex x="-25.3075" y="6.4361"/>
<vertex x="-25.4213" y="6.2964"/>
<vertex x="-25.5438" y="6.2088"/>
<vertex x="-25.7012" y="6.13"/>
<vertex x="-25.7625" y="6.1036"/>
<vertex x="-25.0452" y="4.8699"/>
<vertex x="-25.7975" y="4.8699"/>
<vertex x="-26.445" y="6.0249"/>
<vertex x="-26.445" y="6.5677"/>
<vertex x="-26.2524" y="6.5674"/>
<vertex x="-26.0601" y="6.62"/>
<vertex x="-25.9113" y="6.7074"/>
<vertex x="-25.8587" y="6.8212"/>
<vertex x="-25.8587" y="7.0838"/>
<vertex x="-25.9113" y="7.1887"/>
<vertex x="-25.99" y="7.2413"/>
<vertex x="-26.0688" y="7.2761"/>
<vertex x="-26.2087" y="7.3111"/>
<vertex x="-26.3314" y="7.3287"/>
<vertex x="-27.1536" y="7.3289"/>
<vertex x="-27.1536" y="6.5677"/>
<vertex x="-26.446" y="6.5677"/>
<vertex x="-26.446" y="6.0249"/>
<vertex x="-27.1536" y="6.0251"/>
<vertex x="-27.1536" y="4.8699"/>
<vertex x="-27.8275" y="4.8699"/>
</polygon>
<polygon width="0.0022" layer="101" spacing="0.889">
<vertex x="-35.5801" y="3.9863"/>
<vertex x="-35.2476" y="3.9863"/>
<vertex x="-35.2476" y="3.2075"/>
<vertex x="-35.2301" y="3.1814"/>
<vertex x="-35.195" y="3.1463"/>
<vertex x="-35.16" y="3.1288"/>
<vertex x="-34.845" y="3.1288"/>
<vertex x="-34.845" y="3.9863"/>
<vertex x="-34.5125" y="3.9863"/>
<vertex x="-34.5125" y="2.9012"/>
<vertex x="-34.5562" y="2.8575"/>
<vertex x="-35.16" y="2.8575"/>
<vertex x="-35.2737" y="2.8661"/>
<vertex x="-35.3174" y="2.875"/>
<vertex x="-35.3786" y="2.9012"/>
<vertex x="-35.4399" y="2.9362"/>
<vertex x="-35.5189" y="3.015"/>
<vertex x="-35.5537" y="3.0762"/>
<vertex x="-35.5714" y="3.1201"/>
<vertex x="-35.5801" y="3.1552"/>
</polygon>
<polygon width="0.0022" layer="101" spacing="0.889">
<vertex x="-34.3987" y="3.9863"/>
<vertex x="-34.3987" y="3.7762"/>
<vertex x="-34.1889" y="3.7762"/>
<vertex x="-34.1889" y="3.1288"/>
<vertex x="-34.1536" y="3.0414"/>
<vertex x="-34.0838" y="2.9538"/>
<vertex x="-33.9961" y="2.8926"/>
<vertex x="-33.8737" y="2.8575"/>
<vertex x="-33.5763" y="2.8575"/>
<vertex x="-33.5326" y="2.8926"/>
<vertex x="-33.5326" y="3.1026"/>
<vertex x="-33.7426" y="3.1024"/>
<vertex x="-33.7861" y="3.1288"/>
<vertex x="-33.83" y="3.1811"/>
<vertex x="-33.8564" y="3.2512"/>
<vertex x="-33.8564" y="3.7762"/>
<vertex x="-33.5326" y="3.7762"/>
<vertex x="-33.5326" y="3.9863"/>
<vertex x="-33.8564" y="3.9863"/>
<vertex x="-33.8564" y="4.4064"/>
<vertex x="-34.1889" y="4.4061"/>
<vertex x="-34.1889" y="3.9863"/>
</polygon>
<polygon width="0.0022" layer="101" spacing="0.889">
<vertex x="-33.48" y="3.4427"/>
<vertex x="-33.48" y="3.3561"/>
<vertex x="-33.4449" y="3.1986"/>
<vertex x="-33.3573" y="3.05"/>
<vertex x="-33.2262" y="2.9449"/>
<vertex x="-33.0863" y="2.875"/>
<vertex x="-32.9725" y="2.8575"/>
<vertex x="-32.7452" y="2.8575"/>
<vertex x="-32.5874" y="2.8926"/>
<vertex x="-32.465" y="2.9538"/>
<vertex x="-32.3687" y="3.05"/>
<vertex x="-32.29" y="3.1638"/>
<vertex x="-32.2638" y="3.2487"/>
<vertex x="-32.255" y="3.3038"/>
<vertex x="-32.255" y="3.5575"/>
<vertex x="-32.29" y="3.7061"/>
<vertex x="-32.3601" y="3.8113"/>
<vertex x="-32.465" y="3.9075"/>
<vertex x="-32.5874" y="3.9688"/>
<vertex x="-32.7452" y="4.0124"/>
<vertex x="-32.99" y="4.0124"/>
<vertex x="-33.1125" y="3.9863"/>
<vertex x="-33.2438" y="3.9162"/>
<vertex x="-33.3225" y="3.8374"/>
<vertex x="-33.4099" y="3.7236"/>
<vertex x="-33.4625" y="3.6012"/>
<vertex x="-33.48" y="3.5049"/>
<vertex x="-33.48" y="3.4437"/>
<vertex x="-33.1475" y="3.4437"/>
<vertex x="-33.1475" y="3.4963"/>
<vertex x="-33.1213" y="3.5751"/>
<vertex x="-33.0863" y="3.6363"/>
<vertex x="-33.0076" y="3.7236"/>
<vertex x="-32.9199" y="3.7676"/>
<vertex x="-32.8061" y="3.7676"/>
<vertex x="-32.7538" y="3.7501"/>
<vertex x="-32.6751" y="3.6975"/>
<vertex x="-32.6136" y="3.6012"/>
<vertex x="-32.5874" y="3.5138"/>
<vertex x="-32.5874" y="3.3475"/>
<vertex x="-32.6136" y="3.2687"/>
<vertex x="-32.64" y="3.2161"/>
<vertex x="-32.7012" y="3.1463"/>
<vertex x="-32.7624" y="3.1112"/>
<vertex x="-32.8061" y="3.1024"/>
<vertex x="-32.9286" y="3.1024"/>
<vertex x="-33.0076" y="3.1288"/>
<vertex x="-33.0774" y="3.1986"/>
<vertex x="-33.13" y="3.3038"/>
<vertex x="-33.1475" y="3.3825"/>
<vertex x="-33.1475" y="3.4427"/>
</polygon>
<polygon width="0.0022" layer="101" spacing="0.889">
<vertex x="-27.495" y="3.4427"/>
<vertex x="-27.495" y="3.3564"/>
<vertex x="-27.4599" y="3.1989"/>
<vertex x="-27.3723" y="3.05"/>
<vertex x="-27.2412" y="2.9449"/>
<vertex x="-27.1013" y="2.875"/>
<vertex x="-26.9875" y="2.8575"/>
<vertex x="-26.7602" y="2.8575"/>
<vertex x="-26.6027" y="2.8926"/>
<vertex x="-26.48" y="2.9538"/>
<vertex x="-26.3837" y="3.05"/>
<vertex x="-26.305" y="3.1636"/>
<vertex x="-26.2788" y="3.2487"/>
<vertex x="-26.27" y="3.3038"/>
<vertex x="-26.27" y="3.5575"/>
<vertex x="-26.305" y="3.7064"/>
<vertex x="-26.3751" y="3.8113"/>
<vertex x="-26.48" y="3.9075"/>
<vertex x="-26.6027" y="3.9688"/>
<vertex x="-26.7602" y="4.0124"/>
<vertex x="-27.005" y="4.0124"/>
<vertex x="-27.1275" y="3.9863"/>
<vertex x="-27.2588" y="3.9164"/>
<vertex x="-27.3375" y="3.8377"/>
<vertex x="-27.4249" y="3.7236"/>
<vertex x="-27.4775" y="3.6012"/>
<vertex x="-27.495" y="3.5049"/>
<vertex x="-27.495" y="3.4437"/>
<vertex x="-27.1625" y="3.4437"/>
<vertex x="-27.1625" y="3.4963"/>
<vertex x="-27.1363" y="3.5751"/>
<vertex x="-27.1013" y="3.6363"/>
<vertex x="-27.0226" y="3.7236"/>
<vertex x="-26.9349" y="3.7676"/>
<vertex x="-26.8214" y="3.7676"/>
<vertex x="-26.7688" y="3.7501"/>
<vertex x="-26.6898" y="3.6975"/>
<vertex x="-26.6286" y="3.6012"/>
<vertex x="-26.6027" y="3.5138"/>
<vertex x="-26.6027" y="3.3475"/>
<vertex x="-26.6286" y="3.2687"/>
<vertex x="-26.655" y="3.2161"/>
<vertex x="-26.7162" y="3.1463"/>
<vertex x="-26.7774" y="3.1112"/>
<vertex x="-26.8214" y="3.1024"/>
<vertex x="-26.9438" y="3.1026"/>
<vertex x="-27.0226" y="3.1288"/>
<vertex x="-27.0924" y="3.1989"/>
<vertex x="-27.145" y="3.3038"/>
<vertex x="-27.1625" y="3.3825"/>
<vertex x="-27.1625" y="3.4427"/>
</polygon>
<polygon width="0.0022" layer="101" spacing="0.889">
<vertex x="-27.9677" y="3.9863"/>
<vertex x="-27.9677" y="2.8575"/>
<vertex x="-27.6349" y="2.8575"/>
<vertex x="-27.6349" y="3.9863"/>
</polygon>
<polygon width="0.0022" layer="101" spacing="0.889">
<vertex x="-27.9677" y="4.4061"/>
<vertex x="-27.9677" y="4.1262"/>
<vertex x="-27.6349" y="4.1262"/>
<vertex x="-27.6349" y="4.4064"/>
</polygon>
<polygon width="0.0022" layer="101" spacing="0.889">
<vertex x="-25.0538" y="2.8575"/>
<vertex x="-25.3863" y="2.8575"/>
<vertex x="-25.3863" y="3.6627"/>
<vertex x="-25.4038" y="3.6888"/>
<vertex x="-25.4386" y="3.7236"/>
<vertex x="-25.4739" y="3.7412"/>
<vertex x="-25.7711" y="3.7412"/>
<vertex x="-25.7711" y="2.8575"/>
<vertex x="-26.0949" y="2.8575"/>
<vertex x="-26.0949" y="3.9863"/>
<vertex x="-25.3337" y="3.9863"/>
<vertex x="-25.2989" y="3.9774"/>
<vertex x="-25.255" y="3.9601"/>
<vertex x="-25.1849" y="3.9251"/>
<vertex x="-25.1414" y="3.8814"/>
<vertex x="-25.0975" y="3.8199"/>
<vertex x="-25.0624" y="3.7501"/>
<vertex x="-25.0538" y="3.6886"/>
</polygon>
<polygon width="0.0022" layer="101" spacing="0.889">
<vertex x="-30.4876" y="2.8575"/>
<vertex x="-30.7851" y="2.8575"/>
<vertex x="-30.7851" y="3.6624"/>
<vertex x="-30.8023" y="3.6888"/>
<vertex x="-30.8374" y="3.7239"/>
<vertex x="-30.8727" y="3.7412"/>
<vertex x="-31.1" y="3.7414"/>
<vertex x="-31.1" y="2.8926"/>
<vertex x="-31.1351" y="2.8575"/>
<vertex x="-31.415" y="2.8575"/>
<vertex x="-31.4589" y="2.9012"/>
<vertex x="-31.4589" y="3.7412"/>
<vertex x="-31.7561" y="3.7414"/>
<vertex x="-31.7561" y="2.8926"/>
<vertex x="-31.7911" y="2.8575"/>
<vertex x="-32.0888" y="2.8575"/>
<vertex x="-32.0888" y="3.9863"/>
<vertex x="-30.7325" y="3.9863"/>
<vertex x="-30.6977" y="3.9776"/>
<vertex x="-30.6537" y="3.9599"/>
<vertex x="-30.5836" y="3.9251"/>
<vertex x="-30.5399" y="3.8814"/>
<vertex x="-30.4963" y="3.8202"/>
<vertex x="-30.4612" y="3.7501"/>
<vertex x="-30.4526" y="3.6888"/>
<vertex x="-30.4526" y="2.9012"/>
<vertex x="-30.4876" y="2.8664"/>
</polygon>
<polygon width="0.0022" layer="101" spacing="0.889">
<vertex x="-29.0873" y="2.9012"/>
<vertex x="-29.1313" y="2.8575"/>
<vertex x="-29.9626" y="2.8575"/>
<vertex x="-30.1026" y="2.8926"/>
<vertex x="-30.2075" y="2.9799"/>
<vertex x="-30.2689" y="3.0676"/>
<vertex x="-30.2776" y="3.1288"/>
<vertex x="-30.2776" y="3.3038"/>
<vertex x="-30.225" y="3.4176"/>
<vertex x="-30.1727" y="3.4788"/>
<vertex x="-30.1374" y="3.4963"/>
<vertex x="-30.0589" y="3.5311"/>
<vertex x="-29.9626" y="3.5489"/>
<vertex x="-29.447" y="3.5486"/>
<vertex x="-29.447" y="3.3299"/>
<vertex x="-29.8488" y="3.3299"/>
<vertex x="-29.8836" y="3.3127"/>
<vertex x="-29.91" y="3.2949"/>
<vertex x="-29.9276" y="3.2687"/>
<vertex x="-29.9362" y="3.2512"/>
<vertex x="-29.9448" y="3.2161"/>
<vertex x="-29.9448" y="3.19"/>
<vertex x="-29.9362" y="3.1636"/>
<vertex x="-29.91" y="3.1288"/>
<vertex x="-29.8836" y="3.1112"/>
<vertex x="-29.8574" y="3.1026"/>
<vertex x="-29.4462" y="3.1026"/>
<vertex x="-29.4462" y="3.6624"/>
<vertex x="-29.4637" y="3.6886"/>
<vertex x="-29.4988" y="3.7236"/>
<vertex x="-29.5339" y="3.7414"/>
<vertex x="-30.1899" y="3.7412"/>
<vertex x="-30.1899" y="3.9863"/>
<vertex x="-29.3936" y="3.9863"/>
<vertex x="-29.3586" y="3.9776"/>
<vertex x="-29.3152" y="3.9601"/>
<vertex x="-29.2451" y="3.9251"/>
<vertex x="-29.2014" y="3.8814"/>
<vertex x="-29.1577" y="3.8202"/>
<vertex x="-29.1224" y="3.7501"/>
<vertex x="-29.0873" y="3.6098"/>
</polygon>
<polygon width="0.0022" layer="101" spacing="0.889">
<vertex x="-28.9738" y="3.9863"/>
<vertex x="-28.9738" y="3.7762"/>
<vertex x="-28.7637" y="3.7762"/>
<vertex x="-28.7637" y="3.1288"/>
<vertex x="-28.7289" y="3.0414"/>
<vertex x="-28.6586" y="2.9538"/>
<vertex x="-28.5712" y="2.8926"/>
<vertex x="-28.4488" y="2.8575"/>
<vertex x="-28.1513" y="2.8575"/>
<vertex x="-28.1074" y="2.8926"/>
<vertex x="-28.1074" y="3.1024"/>
<vertex x="-28.3174" y="3.1026"/>
<vertex x="-28.3614" y="3.1288"/>
<vertex x="-28.4051" y="3.1811"/>
<vertex x="-28.4312" y="3.2512"/>
<vertex x="-28.4312" y="3.7762"/>
<vertex x="-28.1074" y="3.7762"/>
<vertex x="-28.1074" y="3.9863"/>
<vertex x="-28.4312" y="3.9863"/>
<vertex x="-28.4312" y="4.4064"/>
<vertex x="-28.7637" y="4.4064"/>
<vertex x="-28.7637" y="3.9863"/>
</polygon>
<polygon width="0.0022" layer="100">
<vertex x="-42.545" y="3.0818"/>
<vertex x="-42.3207" y="3.0818"/>
<vertex x="-42.3207" y="2.8575"/>
<vertex x="-42.545" y="2.8575"/>
</polygon>
<polygon width="0.0022" layer="100">
<vertex x="-42.545" y="3.5306"/>
<vertex x="-42.545" y="3.3061"/>
<vertex x="-42.3207" y="3.3063"/>
<vertex x="-42.3207" y="3.5306"/>
</polygon>
<polygon width="0.0022" layer="100">
<vertex x="-42.545" y="3.9789"/>
<vertex x="-42.545" y="3.7546"/>
<vertex x="-42.3207" y="3.7549"/>
<vertex x="-42.3207" y="3.9792"/>
</polygon>
<polygon width="0.0022" layer="100">
<vertex x="-42.0962" y="3.9789"/>
<vertex x="-42.0962" y="3.7546"/>
<vertex x="-41.8722" y="3.7546"/>
<vertex x="-41.8722" y="3.9792"/>
</polygon>
<polygon width="0.0022" layer="100">
<vertex x="-42.0962" y="3.5303"/>
<vertex x="-42.0962" y="3.3063"/>
<vertex x="-41.8722" y="3.3063"/>
<vertex x="-41.8722" y="3.5303"/>
</polygon>
<polygon width="0.0022" layer="100">
<vertex x="-42.0962" y="3.0818"/>
<vertex x="-42.0962" y="2.8575"/>
<vertex x="-41.8722" y="2.8575"/>
<vertex x="-41.8722" y="3.0818"/>
</polygon>
<polygon width="0.0022" layer="100">
<vertex x="-41.6476" y="3.9792"/>
<vertex x="-41.6476" y="3.7546"/>
<vertex x="-41.4236" y="3.7549"/>
<vertex x="-41.4236" y="3.9789"/>
</polygon>
<polygon width="0.0022" layer="100">
<vertex x="-39.8534" y="3.9789"/>
<vertex x="-39.8534" y="3.7549"/>
<vertex x="-39.6288" y="3.7546"/>
<vertex x="-39.6288" y="3.9792"/>
</polygon>
<polygon width="0.0022" layer="100">
<vertex x="-39.4045" y="3.9789"/>
<vertex x="-39.4045" y="3.7549"/>
<vertex x="-39.1803" y="3.7546"/>
<vertex x="-39.1803" y="3.9792"/>
</polygon>
<polygon width="0.0022" layer="100">
<vertex x="-41.6476" y="3.5306"/>
<vertex x="-41.6476" y="3.3063"/>
<vertex x="-41.4236" y="3.3061"/>
<vertex x="-41.4236" y="3.5303"/>
</polygon>
<polygon width="0.0022" layer="100">
<vertex x="-41.6476" y="3.0818"/>
<vertex x="-41.6476" y="2.8575"/>
<vertex x="-41.4236" y="2.8575"/>
<vertex x="-41.4236" y="3.0818"/>
</polygon>
<polygon width="0.0022" layer="100">
<vertex x="-41.1991" y="3.0818"/>
<vertex x="-40.9748" y="3.0818"/>
<vertex x="-40.9748" y="2.8575"/>
<vertex x="-41.1991" y="2.8575"/>
</polygon>
<polygon width="0.0022" layer="100">
<vertex x="-41.1991" y="3.5303"/>
<vertex x="-41.1991" y="3.3063"/>
<vertex x="-40.9748" y="3.3061"/>
<vertex x="-40.9748" y="3.5306"/>
</polygon>
<polygon width="0.0022" layer="100">
<vertex x="-41.1991" y="3.9789"/>
<vertex x="-41.1991" y="3.7546"/>
<vertex x="-40.9748" y="3.7549"/>
<vertex x="-40.9748" y="3.9792"/>
</polygon>
<polygon width="0.0022" layer="100">
<vertex x="-40.7505" y="3.9789"/>
<vertex x="-40.7505" y="3.7549"/>
<vertex x="-40.5262" y="3.7549"/>
<vertex x="-40.5262" y="3.9789"/>
</polygon>
<polygon width="0.0022" layer="100">
<vertex x="-40.7505" y="3.5306"/>
<vertex x="-40.7505" y="3.3063"/>
<vertex x="-40.5262" y="3.3061"/>
<vertex x="-40.5262" y="3.5303"/>
</polygon>
<polygon width="0.0022" layer="100">
<vertex x="-40.7505" y="3.0818"/>
<vertex x="-40.7505" y="2.8575"/>
<vertex x="-40.5262" y="2.8575"/>
<vertex x="-40.5262" y="3.0818"/>
</polygon>
<polygon width="0.0022" layer="100">
<vertex x="-40.3019" y="3.9792"/>
<vertex x="-40.3019" y="3.7546"/>
<vertex x="-40.0774" y="3.7546"/>
<vertex x="-40.0774" y="3.9789"/>
</polygon>
<polygon width="0.0022" layer="100">
<vertex x="-40.3019" y="3.5303"/>
<vertex x="-40.3019" y="3.3063"/>
<vertex x="-40.0774" y="3.3061"/>
<vertex x="-40.0774" y="3.5303"/>
</polygon>
<polygon width="0.0022" layer="100">
<vertex x="-40.3019" y="3.0818"/>
<vertex x="-40.3019" y="2.8575"/>
<vertex x="-40.0774" y="2.8575"/>
<vertex x="-40.0774" y="3.0818"/>
</polygon>
</package>
<package name="SCHRIFTA">
<wire x1="20.955" y1="-11.43" x2="20.955" y2="-7.62" width="0.254" layer="48"/>
<wire x1="20.955" y1="-7.62" x2="20.955" y2="-6.985" width="0.254" layer="48"/>
<wire x1="20.955" y1="-6.985" x2="20.955" y2="-4.445" width="0.254" layer="48"/>
<wire x1="20.955" y1="-4.445" x2="20.955" y2="0.635" width="0.254" layer="48"/>
<wire x1="20.955" y1="0.635" x2="20.955" y2="5.715" width="0.254" layer="48"/>
<wire x1="20.955" y1="5.715" x2="12.7" y2="5.715" width="0.254" layer="48"/>
<wire x1="12.7" y1="5.715" x2="0.635" y2="5.715" width="0.254" layer="48"/>
<wire x1="0.635" y1="5.715" x2="-12.065" y2="5.715" width="0.254" layer="48"/>
<wire x1="-12.065" y1="5.715" x2="-21.59" y2="5.715" width="0.254" layer="48"/>
<wire x1="-21.59" y1="5.715" x2="-21.59" y2="3.175" width="0.254" layer="48"/>
<wire x1="-21.59" y1="3.175" x2="-21.59" y2="0.635" width="0.254" layer="48"/>
<wire x1="-21.59" y1="0.635" x2="-21.59" y2="-1.905" width="0.254" layer="48"/>
<wire x1="-21.59" y1="-1.905" x2="-21.59" y2="-4.445" width="0.254" layer="48"/>
<wire x1="-21.59" y1="-4.445" x2="-21.59" y2="-6.985" width="0.254" layer="48"/>
<wire x1="-21.59" y1="5.715" x2="-21.59" y2="8.255" width="0.254" layer="48"/>
<wire x1="-21.59" y1="8.255" x2="-21.59" y2="10.795" width="0.254" layer="48"/>
<wire x1="-21.59" y1="10.795" x2="-21.59" y2="13.335" width="0.254" layer="48"/>
<wire x1="-21.59" y1="13.335" x2="-21.59" y2="16.51" width="0.254" layer="48"/>
<wire x1="-21.59" y1="16.51" x2="-12.065" y2="16.51" width="0.254" layer="48"/>
<wire x1="-12.065" y1="16.51" x2="0.635" y2="16.51" width="0.254" layer="48"/>
<wire x1="0.635" y1="16.51" x2="12.7" y2="16.51" width="0.254" layer="48"/>
<wire x1="12.7" y1="16.51" x2="20.955" y2="16.51" width="0.254" layer="48"/>
<wire x1="20.955" y1="16.51" x2="20.955" y2="13.335" width="0.254" layer="48"/>
<wire x1="20.955" y1="13.335" x2="20.955" y2="10.795" width="0.254" layer="48"/>
<wire x1="20.955" y1="10.795" x2="20.955" y2="8.255" width="0.254" layer="48"/>
<wire x1="20.955" y1="8.255" x2="20.955" y2="5.715" width="0.254" layer="48"/>
<wire x1="-21.59" y1="8.255" x2="20.955" y2="8.255" width="0.127" layer="48"/>
<wire x1="-21.59" y1="10.795" x2="20.955" y2="10.795" width="0.127" layer="48"/>
<wire x1="-21.59" y1="13.335" x2="20.955" y2="13.335" width="0.127" layer="48"/>
<wire x1="12.7" y1="16.51" x2="12.7" y2="5.715" width="0.127" layer="48"/>
<wire x1="-21.59" y1="16.51" x2="-30.48" y2="16.51" width="0.254" layer="48"/>
<wire x1="-30.48" y1="16.51" x2="-42.545" y2="16.51" width="0.254" layer="48"/>
<wire x1="-42.545" y1="16.51" x2="-52.705" y2="16.51" width="0.254" layer="48"/>
<wire x1="-52.705" y1="16.51" x2="-52.705" y2="13.335" width="0.254" layer="48"/>
<wire x1="-52.705" y1="13.335" x2="-52.705" y2="10.795" width="0.254" layer="48"/>
<wire x1="-52.705" y1="10.795" x2="-52.705" y2="8.255" width="0.254" layer="48"/>
<wire x1="-52.705" y1="8.255" x2="-52.705" y2="5.715" width="0.254" layer="48"/>
<wire x1="-52.705" y1="5.715" x2="-52.705" y2="3.175" width="0.254" layer="48"/>
<wire x1="-52.705" y1="3.175" x2="-52.705" y2="0.635" width="0.254" layer="48"/>
<wire x1="-52.705" y1="0.635" x2="-52.705" y2="-1.905" width="0.254" layer="48"/>
<wire x1="-52.705" y1="-1.905" x2="-52.705" y2="-4.445" width="0.254" layer="48"/>
<wire x1="-52.705" y1="-4.445" x2="-52.705" y2="-6.985" width="0.254" layer="48"/>
<wire x1="-52.705" y1="-6.985" x2="-52.705" y2="-11.43" width="0.254" layer="48"/>
<wire x1="-52.705" y1="-11.43" x2="-42.545" y2="-11.43" width="0.254" layer="48"/>
<wire x1="-42.545" y1="-11.43" x2="-30.48" y2="-11.43" width="0.254" layer="48"/>
<wire x1="-30.48" y1="-11.43" x2="-21.59" y2="-11.43" width="0.254" layer="48"/>
<wire x1="-21.59" y1="-11.43" x2="-11.43" y2="-11.43" width="0.254" layer="48"/>
<wire x1="-11.43" y1="-11.43" x2="-3.175" y2="-11.43" width="0.254" layer="48"/>
<wire x1="-3.175" y1="-11.43" x2="6.35" y2="-11.43" width="0.254" layer="48"/>
<wire x1="6.35" y1="-11.43" x2="20.955" y2="-11.43" width="0.254" layer="48"/>
<wire x1="-52.705" y1="13.335" x2="-21.59" y2="13.335" width="0.127" layer="48"/>
<wire x1="-52.705" y1="10.795" x2="-21.59" y2="10.795" width="0.127" layer="48"/>
<wire x1="-52.705" y1="8.255" x2="-21.59" y2="8.255" width="0.127" layer="48"/>
<wire x1="-52.705" y1="5.715" x2="-21.59" y2="5.715" width="0.127" layer="48"/>
<wire x1="-52.705" y1="3.175" x2="-21.59" y2="3.175" width="0.127" layer="48"/>
<wire x1="-52.705" y1="0.635" x2="-21.59" y2="0.635" width="0.127" layer="48"/>
<wire x1="-52.705" y1="-1.905" x2="-21.59" y2="-1.905" width="0.127" layer="48"/>
<wire x1="-21.59" y1="-6.985" x2="-11.43" y2="-6.985" width="0.254" layer="48"/>
<wire x1="-11.43" y1="-6.985" x2="-3.175" y2="-6.985" width="0.254" layer="48"/>
<wire x1="-3.175" y1="-6.985" x2="6.35" y2="-6.985" width="0.254" layer="48"/>
<wire x1="6.35" y1="-6.985" x2="20.955" y2="-6.985" width="0.254" layer="48"/>
<wire x1="-30.48" y1="16.51" x2="-30.48" y2="-11.43" width="0.127" layer="48"/>
<wire x1="0.635" y1="16.51" x2="0.635" y2="5.715" width="0.127" layer="48"/>
<wire x1="-12.065" y1="5.715" x2="-12.065" y2="16.51" width="0.127" layer="48"/>
<wire x1="-42.545" y1="16.51" x2="-42.545" y2="-11.43" width="0.127" layer="48"/>
<wire x1="-21.59" y1="-6.985" x2="-21.59" y2="-11.43" width="0.127" layer="48"/>
<wire x1="-11.43" y1="-6.985" x2="-11.43" y2="-11.43" width="0.127" layer="48"/>
<wire x1="-3.175" y1="-6.985" x2="-3.175" y2="-11.43" width="0.127" layer="48"/>
<wire x1="6.35" y1="-6.985" x2="6.35" y2="-11.43" width="0.127" layer="48"/>
<wire x1="20.955" y1="16.51" x2="67.31" y2="16.51" width="0.254" layer="48"/>
<wire x1="67.31" y1="16.51" x2="67.31" y2="10.795" width="0.254" layer="48"/>
<wire x1="67.31" y1="10.795" x2="67.31" y2="5.715" width="0.254" layer="48"/>
<wire x1="67.31" y1="5.715" x2="67.31" y2="0.635" width="0.254" layer="48"/>
<wire x1="67.31" y1="0.635" x2="67.31" y2="-4.445" width="0.254" layer="48"/>
<wire x1="67.31" y1="-4.445" x2="67.31" y2="-11.43" width="0.254" layer="48"/>
<wire x1="67.31" y1="-11.43" x2="55.245" y2="-11.43" width="0.254" layer="48"/>
<wire x1="55.245" y1="-11.43" x2="20.955" y2="-11.43" width="0.254" layer="48"/>
<wire x1="20.955" y1="10.795" x2="67.31" y2="10.795" width="0.127" layer="48"/>
<wire x1="20.955" y1="5.715" x2="67.31" y2="5.715" width="0.127" layer="48"/>
<wire x1="20.955" y1="0.635" x2="67.31" y2="0.635" width="0.127" layer="48"/>
<wire x1="20.955" y1="-4.445" x2="55.245" y2="-4.445" width="0.127" layer="48"/>
<wire x1="55.245" y1="-4.445" x2="67.31" y2="-4.445" width="0.127" layer="48"/>
<wire x1="55.245" y1="-4.445" x2="55.245" y2="-7.62" width="0.127" layer="48"/>
<wire x1="55.245" y1="-7.62" x2="55.245" y2="-11.43" width="0.127" layer="48"/>
<wire x1="20.955" y1="-7.62" x2="55.245" y2="-7.62" width="0.127" layer="48"/>
<wire x1="-52.705" y1="-4.445" x2="-21.59" y2="-4.445" width="0.127" layer="48"/>
<wire x1="-52.705" y1="-6.985" x2="-21.59" y2="-6.985" width="0.127" layer="48"/>
<text x="21.75" y="-10.2301" size="1.6764" layer="48">&gt;LAST_DATE_TIME</text>
<text x="21.75" y="-7.0551" size="1.6764" layer="48">&gt;DRAWING_NAME</text>
<text x="-20.955" y="11.43" size="1.6764" layer="48">Bearb.</text>
<text x="-20.955" y="8.89" size="1.6764" layer="48">Gepr.</text>
<text x="-20.955" y="6.35" size="1.6764" layer="48">Norm</text>
<text x="-11.43" y="13.97" size="1.6764" layer="48">Datum</text>
<text x="1.27" y="13.97" size="1.6764" layer="48">Name</text>
<text x="13.335" y="13.97" size="1.6764" layer="48">Sign.</text>
<text x="-51.435" y="11.43" size="1.6764" layer="48">stand</text>
<text x="-51.435" y="13.97" size="1.6764" layer="48">Änd.-</text>
<text x="-41.91" y="11.43" size="1.6764" layer="48">Datum</text>
<text x="-29.845" y="11.43" size="1.6764" layer="48">Sign.</text>
<text x="55.88" y="-6.985" size="1.6764" layer="48">Blatt</text>
<text x="55.88" y="-10.16" size="1.6764" layer="48">1/1</text>
<text x="21.75" y="-10.2301" size="1.6764" layer="48">&gt;LAST_DATE_TIME</text>
<text x="21.75" y="-7.0551" size="1.6764" layer="48">&gt;DRAWING_NAME</text>
<text x="-20.955" y="11.43" size="1.6764" layer="48">Bearb.</text>
<text x="-20.955" y="8.89" size="1.6764" layer="48">Gepr.</text>
<text x="-20.955" y="6.35" size="1.6764" layer="48">Norm</text>
<text x="-11.43" y="13.97" size="1.6764" layer="48">Datum</text>
<text x="1.27" y="13.97" size="1.6764" layer="48">Name</text>
<text x="13.335" y="13.97" size="1.6764" layer="48">Sign.</text>
<text x="-51.435" y="11.43" size="1.6764" layer="48">stand</text>
<text x="-51.435" y="13.97" size="1.6764" layer="48">Änd.-</text>
<text x="-41.91" y="11.43" size="1.6764" layer="48">Datum</text>
<text x="-29.845" y="11.43" size="1.6764" layer="48">Sign.</text>
<text x="55.88" y="-6.985" size="1.6764" layer="48">Blatt</text>
<text x="55.88" y="-10.16" size="1.6764" layer="48">1/1</text>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-17.925" y="4.5675"/>
<vertex x="-17.925" y="-1.435"/>
<vertex x="-16.5425" y="-1.435"/>
<vertex x="-16.5425" y="1.05"/>
<vertex x="-14.32" y="1.05"/>
<vertex x="-14.32" y="-1.435"/>
<vertex x="-12.99" y="-1.435"/>
<vertex x="-12.99" y="4.5675"/>
<vertex x="-14.32" y="4.5675"/>
<vertex x="-14.32" y="2.1525"/>
<vertex x="-16.5425" y="2.1525"/>
<vertex x="-16.5425" y="4.5675"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-10.995" y="4.5675"/>
<vertex x="-10.2075" y="4.5675"/>
<vertex x="-10.2075" y="3.9025"/>
<vertex x="-10.995" y="3.9025"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-7.6" y="4.5675"/>
<vertex x="-6.7775" y="4.5675"/>
<vertex x="-6.7775" y="3.9025"/>
<vertex x="-7.6" y="3.9025"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-12.15" y="-1.435"/>
<vertex x="-10.6975" y="-1.435"/>
<vertex x="-8.93" y="3.22"/>
<vertex x="-5.5175" y="-5.46"/>
<vertex x="-4.17" y="-5.46"/>
<vertex x="-8.16" y="4.5675"/>
<vertex x="-9.595" y="4.5675"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-9.7525" y="1.0675"/>
<vertex x="-8.0725" y="1.0675"/>
<vertex x="-7.6525" y="-0.0175"/>
<vertex x="-10.1725" y="-0.0175"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-0.67" y="4.5675"/>
<vertex x="-0.67" y="3.5525"/>
<vertex x="-2.7" y="3.5525"/>
<vertex x="-3.26" y="3.4475"/>
<vertex x="-3.575" y="3.3075"/>
<vertex x="-3.785" y="3.1325"/>
<vertex x="-3.89" y="2.9575"/>
<vertex x="-4.03" y="2.6775"/>
<vertex x="-4.135" y="2.2925"/>
<vertex x="-4.205" y="1.8375"/>
<vertex x="-4.205" y="1.015"/>
<vertex x="-4.135" y="0.5075"/>
<vertex x="-3.9425" y="0.0875"/>
<vertex x="-3.75" y="-0.1575"/>
<vertex x="-3.4175" y="-0.3325"/>
<vertex x="-3.12" y="-0.385"/>
<vertex x="-2.77" y="-0.4025"/>
<vertex x="-0.67" y="-0.4025"/>
<vertex x="-0.67" y="-1.435"/>
<vertex x="-3.435" y="-1.435"/>
<vertex x="-3.925" y="-1.3475"/>
<vertex x="-4.3975" y="-1.1725"/>
<vertex x="-4.87" y="-0.8225"/>
<vertex x="-5.2375" y="-0.4025"/>
<vertex x="-5.4125" y="-0.035"/>
<vertex x="-5.5875" y="0.5075"/>
<vertex x="-5.64" y="0.8925"/>
<vertex x="-5.6575" y="1.4525"/>
<vertex x="-5.64" y="2.0825"/>
<vertex x="-5.535" y="2.7475"/>
<vertex x="-5.3425" y="3.2375"/>
<vertex x="-5.115" y="3.605"/>
<vertex x="-4.765" y="4.0075"/>
<vertex x="-4.31" y="4.2875"/>
<vertex x="-3.9425" y="4.4275"/>
<vertex x="-3.54" y="4.4975"/>
<vertex x="-3.085" y="4.55"/>
<vertex x="-2.665" y="4.5675"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="0.205" y="4.5675"/>
<vertex x="0.205" y="-1.435"/>
<vertex x="1.535" y="-1.435"/>
<vertex x="1.535" y="4.5675"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="1.535" y="1.6975"/>
<vertex x="3.845" y="4.5675"/>
<vertex x="5.49" y="4.5675"/>
<vertex x="3.04" y="1.6275"/>
<vertex x="5.4725" y="-1.435"/>
<vertex x="3.74" y="-1.435"/>
<vertex x="1.535" y="1.47"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="10.4775" y="4.5675"/>
<vertex x="10.4775" y="3.4825"/>
<vertex x="8.2375" y="3.4825"/>
<vertex x="7.8875" y="3.4475"/>
<vertex x="7.5725" y="3.3075"/>
<vertex x="7.3625" y="3.15"/>
<vertex x="7.2225" y="2.9575"/>
<vertex x="7.065" y="2.66"/>
<vertex x="6.995" y="2.275"/>
<vertex x="6.995" y="0.6825"/>
<vertex x="7.0125" y="0.5075"/>
<vertex x="7.205" y="0.0875"/>
<vertex x="7.3975" y="-0.1575"/>
<vertex x="7.73" y="-0.3325"/>
<vertex x="8.0275" y="-0.385"/>
<vertex x="8.3775" y="-0.4025"/>
<vertex x="10.4775" y="-0.4025"/>
<vertex x="10.4775" y="-1.435"/>
<vertex x="7.7125" y="-1.435"/>
<vertex x="7.2225" y="-1.3475"/>
<vertex x="6.75" y="-1.1725"/>
<vertex x="6.2775" y="-0.8225"/>
<vertex x="5.91" y="-0.4025"/>
<vertex x="5.735" y="-0.035"/>
<vertex x="5.56" y="0.5075"/>
<vertex x="5.5075" y="0.8925"/>
<vertex x="5.49" y="1.4525"/>
<vertex x="5.5075" y="2.0825"/>
<vertex x="5.6125" y="2.7475"/>
<vertex x="5.805" y="3.2375"/>
<vertex x="6.0325" y="3.605"/>
<vertex x="6.3825" y="4.0075"/>
<vertex x="6.8375" y="4.2875"/>
<vertex x="7.205" y="4.4275"/>
<vertex x="7.6075" y="4.4975"/>
<vertex x="8.0625" y="4.55"/>
<vertex x="8.4825" y="4.5675"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="6.995" y="1.05"/>
<vertex x="10.4775" y="1.05"/>
<vertex x="10.4775" y="2.1525"/>
<vertex x="6.995" y="2.1525"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="11.51" y="4.5675"/>
<vertex x="14.87" y="4.5675"/>
<vertex x="15.5" y="4.4975"/>
<vertex x="15.92" y="4.375"/>
<vertex x="16.2" y="4.2"/>
<vertex x="16.3925" y="4.0075"/>
<vertex x="16.62" y="3.71"/>
<vertex x="16.7425" y="3.3425"/>
<vertex x="16.8125" y="2.975"/>
<vertex x="16.8125" y="2.5725"/>
<vertex x="16.7425" y="2.135"/>
<vertex x="16.55" y="1.6975"/>
<vertex x="16.3225" y="1.4175"/>
<vertex x="16.0775" y="1.2425"/>
<vertex x="15.7625" y="1.085"/>
<vertex x="15.64" y="1.0325"/>
<vertex x="17.075" y="-1.435"/>
<vertex x="15.57" y="-1.435"/>
<vertex x="14.275" y="0.875"/>
<vertex x="14.275" y="1.96"/>
<vertex x="14.66" y="1.96"/>
<vertex x="15.045" y="2.065"/>
<vertex x="15.3425" y="2.24"/>
<vertex x="15.4475" y="2.4675"/>
<vertex x="15.4475" y="2.9925"/>
<vertex x="15.3425" y="3.2025"/>
<vertex x="15.185" y="3.3075"/>
<vertex x="15.0275" y="3.3775"/>
<vertex x="14.7475" y="3.4475"/>
<vertex x="14.5025" y="3.4825"/>
<vertex x="12.8575" y="3.4825"/>
<vertex x="12.8575" y="1.96"/>
<vertex x="14.2733" y="1.96"/>
<vertex x="14.2733" y="0.875"/>
<vertex x="12.8575" y="0.875"/>
<vertex x="12.8575" y="-1.435"/>
<vertex x="11.51" y="-1.435"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-5.4155" y="-5.7865"/>
<vertex x="-5.2405" y="-6.224"/>
<vertex x="17.0545" y="-6.224"/>
<vertex x="17.0545" y="-5.7865"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-3.995" y="-3.2025"/>
<vertex x="-3.33" y="-3.2025"/>
<vertex x="-3.33" y="-4.76"/>
<vertex x="-3.295" y="-4.8125"/>
<vertex x="-3.225" y="-4.8825"/>
<vertex x="-3.155" y="-4.9175"/>
<vertex x="-2.525" y="-4.9175"/>
<vertex x="-2.525" y="-3.2025"/>
<vertex x="-1.86" y="-3.2025"/>
<vertex x="-1.86" y="-5.3725"/>
<vertex x="-1.9475" y="-5.46"/>
<vertex x="-3.155" y="-5.46"/>
<vertex x="-3.3825" y="-5.4425"/>
<vertex x="-3.47" y="-5.425"/>
<vertex x="-3.5925" y="-5.3725"/>
<vertex x="-3.715" y="-5.3025"/>
<vertex x="-3.8725" y="-5.145"/>
<vertex x="-3.9425" y="-5.0225"/>
<vertex x="-3.9775" y="-4.935"/>
<vertex x="-3.995" y="-4.865"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-1.6325" y="-3.2025"/>
<vertex x="-1.6325" y="-3.6225"/>
<vertex x="-1.2125" y="-3.6225"/>
<vertex x="-1.2125" y="-4.9175"/>
<vertex x="-1.1425" y="-5.0925"/>
<vertex x="-1.0025" y="-5.2675"/>
<vertex x="-0.8275" y="-5.39"/>
<vertex x="-0.5825" y="-5.46"/>
<vertex x="0.0125" y="-5.46"/>
<vertex x="0.1" y="-5.39"/>
<vertex x="0.1" y="-4.97"/>
<vertex x="-0.32" y="-4.97"/>
<vertex x="-0.4075" y="-4.9175"/>
<vertex x="-0.495" y="-4.8125"/>
<vertex x="-0.5475" y="-4.6725"/>
<vertex x="-0.5475" y="-3.6225"/>
<vertex x="0.1" y="-3.6225"/>
<vertex x="0.1" y="-3.2025"/>
<vertex x="-0.5475" y="-3.2025"/>
<vertex x="-0.5475" y="-2.3625"/>
<vertex x="-1.2125" y="-2.3625"/>
<vertex x="-1.2125" y="-3.2025"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="0.205" y="-4.2892"/>
<vertex x="0.205" y="-4.4625"/>
<vertex x="0.275" y="-4.7775"/>
<vertex x="0.45" y="-5.075"/>
<vertex x="0.7125" y="-5.285"/>
<vertex x="0.9925" y="-5.425"/>
<vertex x="1.22" y="-5.46"/>
<vertex x="1.675" y="-5.46"/>
<vertex x="1.99" y="-5.39"/>
<vertex x="2.235" y="-5.2675"/>
<vertex x="2.4275" y="-5.075"/>
<vertex x="2.585" y="-4.8475"/>
<vertex x="2.6375" y="-4.6777"/>
<vertex x="2.655" y="-4.5675"/>
<vertex x="2.655" y="-4.06"/>
<vertex x="2.585" y="-3.7625"/>
<vertex x="2.445" y="-3.5525"/>
<vertex x="2.235" y="-3.36"/>
<vertex x="1.99" y="-3.2375"/>
<vertex x="1.675" y="-3.15"/>
<vertex x="1.185" y="-3.15"/>
<vertex x="0.94" y="-3.2025"/>
<vertex x="0.6775" y="-3.3425"/>
<vertex x="0.52" y="-3.5"/>
<vertex x="0.345" y="-3.7275"/>
<vertex x="0.24" y="-3.9725"/>
<vertex x="0.205" y="-4.165"/>
<vertex x="0.205" y="-4.2875"/>
<vertex x="0.87" y="-4.2875"/>
<vertex x="0.87" y="-4.1825"/>
<vertex x="0.9225" y="-4.025"/>
<vertex x="0.9925" y="-3.9025"/>
<vertex x="1.15" y="-3.7275"/>
<vertex x="1.325" y="-3.64"/>
<vertex x="1.5525" y="-3.64"/>
<vertex x="1.6575" y="-3.675"/>
<vertex x="1.815" y="-3.78"/>
<vertex x="1.9375" y="-3.9725"/>
<vertex x="1.99" y="-4.1475"/>
<vertex x="1.99" y="-4.48"/>
<vertex x="1.9375" y="-4.6375"/>
<vertex x="1.885" y="-4.7425"/>
<vertex x="1.7625" y="-4.8825"/>
<vertex x="1.64" y="-4.9525"/>
<vertex x="1.5525" y="-4.97"/>
<vertex x="1.3075" y="-4.97"/>
<vertex x="1.15" y="-4.9175"/>
<vertex x="1.01" y="-4.7775"/>
<vertex x="0.905" y="-4.5675"/>
<vertex x="0.87" y="-4.41"/>
<vertex x="0.87" y="-4.2892"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="12.175" y="-4.2892"/>
<vertex x="12.175" y="-4.4625"/>
<vertex x="12.245" y="-4.7775"/>
<vertex x="12.42" y="-5.075"/>
<vertex x="12.6825" y="-5.285"/>
<vertex x="12.9625" y="-5.425"/>
<vertex x="13.19" y="-5.46"/>
<vertex x="13.645" y="-5.46"/>
<vertex x="13.96" y="-5.39"/>
<vertex x="14.205" y="-5.2675"/>
<vertex x="14.3975" y="-5.075"/>
<vertex x="14.555" y="-4.8475"/>
<vertex x="14.6075" y="-4.6777"/>
<vertex x="14.625" y="-4.5675"/>
<vertex x="14.625" y="-4.06"/>
<vertex x="14.555" y="-3.7625"/>
<vertex x="14.415" y="-3.5525"/>
<vertex x="14.205" y="-3.36"/>
<vertex x="13.96" y="-3.2375"/>
<vertex x="13.645" y="-3.15"/>
<vertex x="13.155" y="-3.15"/>
<vertex x="12.91" y="-3.2025"/>
<vertex x="12.6475" y="-3.3425"/>
<vertex x="12.49" y="-3.5"/>
<vertex x="12.315" y="-3.7275"/>
<vertex x="12.21" y="-3.9725"/>
<vertex x="12.175" y="-4.165"/>
<vertex x="12.175" y="-4.2875"/>
<vertex x="12.84" y="-4.2875"/>
<vertex x="12.84" y="-4.1825"/>
<vertex x="12.8925" y="-4.025"/>
<vertex x="12.9625" y="-3.9025"/>
<vertex x="13.12" y="-3.7275"/>
<vertex x="13.295" y="-3.64"/>
<vertex x="13.5225" y="-3.64"/>
<vertex x="13.6275" y="-3.675"/>
<vertex x="13.785" y="-3.78"/>
<vertex x="13.9075" y="-3.9725"/>
<vertex x="13.96" y="-4.1475"/>
<vertex x="13.96" y="-4.48"/>
<vertex x="13.9075" y="-4.6375"/>
<vertex x="13.855" y="-4.7425"/>
<vertex x="13.7325" y="-4.8825"/>
<vertex x="13.61" y="-4.9525"/>
<vertex x="13.5225" y="-4.97"/>
<vertex x="13.2775" y="-4.97"/>
<vertex x="13.12" y="-4.9175"/>
<vertex x="12.98" y="-4.7775"/>
<vertex x="12.875" y="-4.5675"/>
<vertex x="12.84" y="-4.41"/>
<vertex x="12.84" y="-4.2892"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="11.23" y="-3.2025"/>
<vertex x="11.23" y="-5.46"/>
<vertex x="11.895" y="-5.46"/>
<vertex x="11.895" y="-3.2025"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="11.23" y="-2.3625"/>
<vertex x="11.23" y="-2.9225"/>
<vertex x="11.895" y="-2.9225"/>
<vertex x="11.895" y="-2.3625"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="17.0575" y="-5.46"/>
<vertex x="16.3925" y="-5.46"/>
<vertex x="16.3925" y="-3.85"/>
<vertex x="16.3575" y="-3.7975"/>
<vertex x="16.2875" y="-3.7275"/>
<vertex x="16.2175" y="-3.6925"/>
<vertex x="15.6225" y="-3.6925"/>
<vertex x="15.6225" y="-5.46"/>
<vertex x="14.975" y="-5.46"/>
<vertex x="14.975" y="-3.2025"/>
<vertex x="16.4975" y="-3.2025"/>
<vertex x="16.5675" y="-3.22"/>
<vertex x="16.655" y="-3.255"/>
<vertex x="16.795" y="-3.325"/>
<vertex x="16.8825" y="-3.4125"/>
<vertex x="16.97" y="-3.535"/>
<vertex x="17.04" y="-3.675"/>
<vertex x="17.0575" y="-3.7975"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="6.19" y="-5.46"/>
<vertex x="5.595" y="-5.46"/>
<vertex x="5.595" y="-3.85"/>
<vertex x="5.56" y="-3.7975"/>
<vertex x="5.49" y="-3.7275"/>
<vertex x="5.42" y="-3.6925"/>
<vertex x="4.965" y="-3.6925"/>
<vertex x="4.965" y="-5.39"/>
<vertex x="4.895" y="-5.46"/>
<vertex x="4.335" y="-5.46"/>
<vertex x="4.2475" y="-5.3725"/>
<vertex x="4.2475" y="-3.6925"/>
<vertex x="3.6525" y="-3.6925"/>
<vertex x="3.6525" y="-5.39"/>
<vertex x="3.5825" y="-5.46"/>
<vertex x="2.9875" y="-5.46"/>
<vertex x="2.9875" y="-3.2025"/>
<vertex x="5.7" y="-3.2025"/>
<vertex x="5.77" y="-3.22"/>
<vertex x="5.8575" y="-3.255"/>
<vertex x="5.9975" y="-3.325"/>
<vertex x="6.085" y="-3.4125"/>
<vertex x="6.1725" y="-3.535"/>
<vertex x="6.2425" y="-3.675"/>
<vertex x="6.26" y="-3.7975"/>
<vertex x="6.26" y="-5.3725"/>
<vertex x="6.19" y="-5.4425"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="8.99" y="-5.3725"/>
<vertex x="8.9025" y="-5.46"/>
<vertex x="7.24" y="-5.46"/>
<vertex x="6.96" y="-5.39"/>
<vertex x="6.75" y="-5.215"/>
<vertex x="6.6275" y="-5.04"/>
<vertex x="6.61" y="-4.9175"/>
<vertex x="6.61" y="-4.5675"/>
<vertex x="6.715" y="-4.34"/>
<vertex x="6.82" y="-4.2175"/>
<vertex x="6.89" y="-4.1825"/>
<vertex x="7.0475" y="-4.1125"/>
<vertex x="7.24" y="-4.0775"/>
<vertex x="8.2708" y="-4.0775"/>
<vertex x="8.2708" y="-4.515"/>
<vertex x="7.4675" y="-4.515"/>
<vertex x="7.3975" y="-4.55"/>
<vertex x="7.345" y="-4.585"/>
<vertex x="7.31" y="-4.6375"/>
<vertex x="7.2925" y="-4.6725"/>
<vertex x="7.275" y="-4.7425"/>
<vertex x="7.275" y="-4.795"/>
<vertex x="7.2925" y="-4.8475"/>
<vertex x="7.345" y="-4.9175"/>
<vertex x="7.3975" y="-4.9525"/>
<vertex x="7.45" y="-4.97"/>
<vertex x="8.2725" y="-4.97"/>
<vertex x="8.2725" y="-3.85"/>
<vertex x="8.2375" y="-3.7975"/>
<vertex x="8.1675" y="-3.7275"/>
<vertex x="8.0975" y="-3.6925"/>
<vertex x="6.785" y="-3.6925"/>
<vertex x="6.785" y="-3.2025"/>
<vertex x="8.3775" y="-3.2025"/>
<vertex x="8.4475" y="-3.22"/>
<vertex x="8.535" y="-3.255"/>
<vertex x="8.675" y="-3.325"/>
<vertex x="8.7625" y="-3.4125"/>
<vertex x="8.85" y="-3.535"/>
<vertex x="8.92" y="-3.675"/>
<vertex x="8.99" y="-3.955"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="9.2175" y="-3.2025"/>
<vertex x="9.2175" y="-3.6225"/>
<vertex x="9.6375" y="-3.6225"/>
<vertex x="9.6375" y="-4.9175"/>
<vertex x="9.7075" y="-5.0925"/>
<vertex x="9.8475" y="-5.2675"/>
<vertex x="10.0225" y="-5.39"/>
<vertex x="10.2675" y="-5.46"/>
<vertex x="10.8625" y="-5.46"/>
<vertex x="10.95" y="-5.39"/>
<vertex x="10.95" y="-4.97"/>
<vertex x="10.53" y="-4.97"/>
<vertex x="10.4425" y="-4.9175"/>
<vertex x="10.355" y="-4.8125"/>
<vertex x="10.3025" y="-4.6725"/>
<vertex x="10.3025" y="-3.6225"/>
<vertex x="10.95" y="-3.6225"/>
<vertex x="10.95" y="-3.2025"/>
<vertex x="10.3025" y="-3.2025"/>
<vertex x="10.3025" y="-2.3625"/>
<vertex x="9.6375" y="-2.3625"/>
<vertex x="9.6375" y="-3.2025"/>
</polygon>
</package>
<package name="0603">
<wire x1="-0.635" y1="0.3175" x2="0.635" y2="0.3175" width="0.127" layer="21"/>
<wire x1="0.635" y1="0.3175" x2="0.635" y2="-0.3175" width="0.127" layer="21"/>
<wire x1="0.635" y1="-0.3175" x2="-0.635" y2="-0.3175" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-0.3175" x2="-0.635" y2="0.3175" width="0.127" layer="21"/>
<smd name="2" x="0.762" y="0" dx="0.69" dy="0.93" layer="1" roundness="20"/>
<smd name="1" x="-0.762" y="0" dx="0.67" dy="0.93" layer="1" roundness="20"/>
<text x="-1.143" y="0.762" size="0.8128" layer="27" ratio="12">&gt;VALUE</text>
<text x="-1.143" y="1.7145" size="0.8128" layer="25" ratio="12">&gt;NAME</text>
<rectangle x1="0.37" y1="-0.42" x2="0.77" y2="0.42" layer="51"/>
<rectangle x1="-0.77" y1="-0.42" x2="-0.37" y2="0.42" layer="51"/>
</package>
<package name="0805">
<wire x1="-0.8731" y1="0.5556" x2="0.8731" y2="0.5556" width="0.127" layer="21"/>
<wire x1="0.8731" y1="0.5556" x2="0.8731" y2="-0.5556" width="0.127" layer="21"/>
<wire x1="0.8731" y1="-0.5556" x2="-0.8731" y2="-0.5556" width="0.127" layer="21"/>
<wire x1="-0.8731" y1="-0.5556" x2="-0.8731" y2="0.5556" width="0.127" layer="21"/>
<smd name="2" x="1.0193" y="0" dx="0.9652" dy="1.397" layer="1" roundness="20"/>
<smd name="1" x="-1.0193" y="0" dx="0.9652" dy="1.397" layer="1" roundness="20"/>
<text x="-0.889" y="1.016" size="0.8128" layer="27" ratio="12">&gt;VALUE</text>
<text x="-0.889" y="1.9685" size="0.8128" layer="25" ratio="12">&gt;NAME</text>
<rectangle x1="0.6" y1="-0.65" x2="1" y2="0.65" layer="51"/>
<rectangle x1="-1" y1="-0.65" x2="-0.6" y2="0.65" layer="51"/>
</package>
<package name="1206">
<wire x1="-1.4288" y1="0.635" x2="1.4288" y2="0.635" width="0.127" layer="21"/>
<wire x1="1.4288" y1="0.635" x2="1.4288" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.4288" y1="-0.635" x2="-1.4288" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-1.4288" y1="-0.635" x2="-1.4288" y2="0.635" width="0.127" layer="21"/>
<smd name="2" x="1.4732" y="0" dx="1.7272" dy="1.1176" layer="1" roundness="20" rot="R270"/>
<smd name="1" x="-1.4732" y="0" dx="1.7272" dy="1.1176" layer="1" roundness="20" rot="R90"/>
<text x="-2.0796" y="1.1747" size="0.8128" layer="27" ratio="12">&gt;VALUE</text>
<text x="-2.0796" y="2.1272" size="0.8128" layer="25" ratio="12">&gt;NAME</text>
<rectangle x1="0.7" y1="-0.77" x2="1.55" y2="0.77" layer="51"/>
<rectangle x1="-1.55" y1="-0.77" x2="-0.7" y2="0.77" layer="51"/>
</package>
<package name="1210">
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="21"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="21"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1" roundness="20"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1" roundness="20"/>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
</package>
<package name="2220">
<rectangle x1="2" y1="-2.5" x2="2.8" y2="2.5" layer="51"/>
<rectangle x1="-2.8" y1="-2.5" x2="-2" y2="2.5" layer="51" rot="R180"/>
<wire x1="2.8" y1="2.5" x2="-2.8" y2="2.5" width="0.254" layer="51"/>
<wire x1="-2.8" y1="2.5" x2="-2.8" y2="-2.5" width="0.254" layer="51"/>
<wire x1="-2.8" y1="-2.5" x2="2.8" y2="-2.5" width="0.254" layer="51"/>
<wire x1="2.8" y1="-2.5" x2="2.8" y2="2.5" width="0.254" layer="51"/>
<text x="-2.7146" y="3.0797" size="0.8128" layer="27" ratio="12">&gt;VALUE</text>
<text x="-2.7146" y="4.0322" size="0.8128" layer="25" ratio="12">&gt;NAME</text>
<smd name="P$1" x="-2.6" y="0" dx="1.3" dy="5.2" layer="1" roundness="20"/>
<smd name="P$2" x="2.6" y="0" dx="1.3" dy="5.2" layer="1" roundness="20"/>
</package>
<package name="0306">
<text x="-1.143" y="1.397" size="0.8128" layer="27" ratio="12">&gt;VALUE</text>
<text x="-1.143" y="2.3495" size="0.8128" layer="25" ratio="12">&gt;NAME</text>
<rectangle x1="0.2" y1="-0.8" x2="0.4" y2="0.8" layer="51"/>
<rectangle x1="-0.4" y1="-0.8" x2="-0.2" y2="0.8" layer="51" rot="R180"/>
<wire x1="-0.3" y1="0.7" x2="0.3" y2="0.7" width="0.127" layer="51"/>
<wire x1="0.3" y1="0.7" x2="0.3" y2="-0.7" width="0.127" layer="51"/>
<wire x1="0.3" y1="-0.7" x2="-0.3" y2="-0.7" width="0.127" layer="51"/>
<wire x1="-0.3" y1="-0.7" x2="-0.3" y2="0.7" width="0.127" layer="51"/>
<smd name="P$1" x="-0.33734375" y="0" dx="1.7" dy="0.35" layer="1" roundness="20" rot="R90"/>
<smd name="P$2" x="0.33734375" y="0" dx="1.7" dy="0.35" layer="1" roundness="20" rot="R90"/>
</package>
<package name="1812">
<wire x1="1.55" y1="-1.5" x2="-1.55" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1.55" y1="-1.5" x2="-1.55" y2="1.5" width="0.127" layer="21"/>
<wire x1="-1.55" y1="1.5" x2="1.55" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.55" y1="1.5" x2="1.55" y2="-1.5" width="0.127" layer="21"/>
<smd name="2" x="2.2352" y="0" dx="1.7018" dy="3.302" layer="1"/>
<smd name="1" x="-2.2352" y="0" dx="1.7018" dy="3.302" layer="1"/>
<text x="-2.032" y="1.905" size="1.27" layer="27" ratio="12">&gt;VALUE</text>
<text x="-2.032" y="3.429" size="1.27" layer="25" ratio="12">&gt;NAME</text>
<rectangle x1="1.65" y1="-1.6" x2="2.25" y2="1.6" layer="21"/>
<rectangle x1="-2.25" y1="-1.6" x2="-1.65" y2="1.6" layer="21"/>
</package>
<package name="1825">
<wire x1="1.55" y1="-3.1" x2="-1.55" y2="-3.1" width="0.127" layer="21"/>
<wire x1="-1.55" y1="-3.1" x2="-1.55" y2="3.1" width="0.127" layer="21"/>
<wire x1="-1.55" y1="3.1" x2="1.55" y2="3.1" width="0.127" layer="21"/>
<wire x1="1.55" y1="3.1" x2="1.55" y2="-3.1" width="0.127" layer="21"/>
<smd name="2" x="2.2352" y="0" dx="1.7" dy="6.5" layer="1"/>
<smd name="1" x="-2.2352" y="0" dx="1.7" dy="6.5" layer="1"/>
<text x="-2.032" y="3.3338" size="0.8128" layer="27" ratio="12">&gt;VALUE</text>
<text x="-2.032" y="4.3814" size="0.8128" layer="25" ratio="12">&gt;NAME</text>
<rectangle x1="1.65" y1="-3.2" x2="2.25" y2="3.2" layer="21"/>
<rectangle x1="-2.25" y1="-3.2" x2="-1.65" y2="3.2" layer="21"/>
</package>
<package name="3425">
<wire x1="-1.7" y1="1.25" x2="1.7" y2="1.25" width="0.05" layer="21"/>
<wire x1="1.7" y1="1.25" x2="1.7" y2="-1.25" width="0.05" layer="21"/>
<wire x1="1.7" y1="-1.25" x2="-1.7" y2="-1.25" width="0.05" layer="21"/>
<wire x1="-1.7" y1="-1.25" x2="-1.7" y2="1.25" width="0.05" layer="21"/>
<smd name="1" x="-1.7" y="0" dx="1.3" dy="2.4" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.3" dy="2.4" layer="1"/>
<text x="-1.4" y="0.15" size="0.8128" layer="25" ratio="12">&gt;Name</text>
<text x="-1.4" y="-0.95" size="0.8128" layer="27" ratio="12">&gt;Value</text>
<rectangle x1="-1.7" y1="-1" x2="-1.5" y2="1" layer="21"/>
<rectangle x1="1.5" y1="-1" x2="1.7" y2="1" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="A3_LOC">
<frame x1="-383.54" y1="-3.81" x2="3.81" y2="259.08" columns="10" rows="8" layer="94"/>
</symbol>
<symbol name="A1_LOC">
<frame x1="-772.16" y1="-3.81" x2="3.81" y2="528.32" columns="20" rows="16" layer="94"/>
</symbol>
<symbol name="A0_LOC">
<frame x1="-1109.98" y1="-3.81" x2="3.81" y2="756.92" columns="32" rows="20" layer="94"/>
</symbol>
<symbol name="A2_LOC">
<frame x1="-553.72" y1="-3.81" x2="3.81" y2="381" columns="16" rows="10" layer="94"/>
</symbol>
<symbol name="A4_LOC2">
<frame x1="-257.81" y1="-3.81" x2="3.81" y2="176.53" columns="8" rows="5" layer="94"/>
</symbol>
<symbol name="SCHRIFTF">
<wire x1="-22.86" y1="-10.16" x2="-12.7" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-10.16" x2="-4.445" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-10.16" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="-10.16" x2="19.685" y2="-10.16" width="0.254" layer="94"/>
<wire x1="19.685" y1="-10.16" x2="19.685" y2="-6.35" width="0.254" layer="94"/>
<wire x1="19.685" y1="-6.35" x2="19.685" y2="-5.715" width="0.254" layer="94"/>
<wire x1="19.685" y1="-5.715" x2="19.685" y2="-3.175" width="0.254" layer="94"/>
<wire x1="19.685" y1="-3.175" x2="19.685" y2="1.905" width="0.254" layer="94"/>
<wire x1="19.685" y1="1.905" x2="19.685" y2="6.985" width="0.254" layer="94"/>
<wire x1="19.685" y1="6.985" x2="11.43" y2="6.985" width="0.254" layer="94"/>
<wire x1="11.43" y1="6.985" x2="-0.635" y2="6.985" width="0.254" layer="94"/>
<wire x1="-0.635" y1="6.985" x2="-13.335" y2="6.985" width="0.254" layer="94"/>
<wire x1="-13.335" y1="6.985" x2="-22.86" y2="6.985" width="0.254" layer="94"/>
<wire x1="-22.86" y1="6.985" x2="-22.86" y2="4.445" width="0.254" layer="94"/>
<wire x1="-22.86" y1="4.445" x2="-22.86" y2="1.905" width="0.254" layer="94"/>
<wire x1="-22.86" y1="1.905" x2="-22.86" y2="-0.635" width="0.254" layer="94"/>
<wire x1="-22.86" y1="-0.635" x2="-22.86" y2="-3.175" width="0.254" layer="94"/>
<wire x1="-22.86" y1="-3.175" x2="-22.86" y2="-5.715" width="0.254" layer="94"/>
<wire x1="-22.86" y1="6.985" x2="-22.86" y2="9.525" width="0.254" layer="94"/>
<wire x1="-22.86" y1="9.525" x2="-22.86" y2="12.065" width="0.254" layer="94"/>
<wire x1="-22.86" y1="12.065" x2="-22.86" y2="14.605" width="0.254" layer="94"/>
<wire x1="-22.86" y1="14.605" x2="-22.86" y2="17.78" width="0.254" layer="94"/>
<wire x1="-22.86" y1="17.78" x2="-13.335" y2="17.78" width="0.254" layer="94"/>
<wire x1="-13.335" y1="17.78" x2="-0.635" y2="17.78" width="0.254" layer="94"/>
<wire x1="-0.635" y1="17.78" x2="11.43" y2="17.78" width="0.254" layer="94"/>
<wire x1="11.43" y1="17.78" x2="19.685" y2="17.78" width="0.254" layer="94"/>
<wire x1="19.685" y1="17.78" x2="19.685" y2="14.605" width="0.254" layer="94"/>
<wire x1="19.685" y1="14.605" x2="19.685" y2="12.065" width="0.254" layer="94"/>
<wire x1="19.685" y1="12.065" x2="19.685" y2="9.525" width="0.254" layer="94"/>
<wire x1="19.685" y1="9.525" x2="19.685" y2="6.985" width="0.254" layer="94"/>
<wire x1="-22.86" y1="9.525" x2="19.685" y2="9.525" width="0.127" layer="94"/>
<wire x1="-22.86" y1="12.065" x2="19.685" y2="12.065" width="0.127" layer="94"/>
<wire x1="-22.86" y1="14.605" x2="19.685" y2="14.605" width="0.127" layer="94"/>
<wire x1="11.43" y1="17.78" x2="11.43" y2="6.985" width="0.127" layer="94"/>
<wire x1="-22.86" y1="17.78" x2="-31.75" y2="17.78" width="0.254" layer="94"/>
<wire x1="-31.75" y1="17.78" x2="-43.815" y2="17.78" width="0.254" layer="94"/>
<wire x1="-43.815" y1="17.78" x2="-53.975" y2="17.78" width="0.254" layer="94"/>
<wire x1="-53.975" y1="17.78" x2="-53.975" y2="14.605" width="0.254" layer="94"/>
<wire x1="-53.975" y1="14.605" x2="-53.975" y2="12.065" width="0.254" layer="94"/>
<wire x1="-53.975" y1="12.065" x2="-53.975" y2="9.525" width="0.254" layer="94"/>
<wire x1="-53.975" y1="9.525" x2="-53.975" y2="6.985" width="0.254" layer="94"/>
<wire x1="-53.975" y1="6.985" x2="-53.975" y2="4.445" width="0.254" layer="94"/>
<wire x1="-53.975" y1="4.445" x2="-53.975" y2="1.905" width="0.254" layer="94"/>
<wire x1="-53.975" y1="1.905" x2="-53.975" y2="-0.635" width="0.254" layer="94"/>
<wire x1="-53.975" y1="-0.635" x2="-53.975" y2="-3.175" width="0.254" layer="94"/>
<wire x1="-53.975" y1="-3.175" x2="-53.975" y2="-5.715" width="0.254" layer="94"/>
<wire x1="-53.975" y1="-5.715" x2="-53.975" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-53.975" y1="-10.16" x2="-43.815" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-43.815" y1="-10.16" x2="-31.75" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-31.75" y1="-10.16" x2="-22.86" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-53.975" y1="14.605" x2="-22.86" y2="14.605" width="0.127" layer="94"/>
<wire x1="-53.975" y1="12.065" x2="-22.86" y2="12.065" width="0.127" layer="94"/>
<wire x1="-53.975" y1="9.525" x2="-22.86" y2="9.525" width="0.127" layer="94"/>
<wire x1="-53.975" y1="6.985" x2="-22.86" y2="6.985" width="0.127" layer="94"/>
<wire x1="-53.975" y1="4.445" x2="-22.86" y2="4.445" width="0.127" layer="94"/>
<wire x1="-53.975" y1="1.905" x2="-22.86" y2="1.905" width="0.127" layer="94"/>
<wire x1="-53.975" y1="-0.635" x2="-22.86" y2="-0.635" width="0.127" layer="94"/>
<wire x1="-22.86" y1="-5.715" x2="-12.7" y2="-5.715" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-5.715" x2="-4.445" y2="-5.715" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-5.715" x2="5.08" y2="-5.715" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.715" x2="19.685" y2="-5.715" width="0.254" layer="94"/>
<wire x1="-31.75" y1="17.78" x2="-31.75" y2="-10.16" width="0.127" layer="94"/>
<wire x1="-0.635" y1="17.78" x2="-0.635" y2="6.985" width="0.127" layer="94"/>
<wire x1="-13.335" y1="6.985" x2="-13.335" y2="17.78" width="0.127" layer="94"/>
<wire x1="-43.815" y1="17.78" x2="-43.815" y2="-10.16" width="0.127" layer="94"/>
<wire x1="-22.86" y1="-5.715" x2="-22.86" y2="-10.16" width="0.127" layer="94"/>
<wire x1="-12.7" y1="-5.715" x2="-12.7" y2="-10.16" width="0.127" layer="94"/>
<wire x1="-4.445" y1="-5.715" x2="-4.445" y2="-10.16" width="0.127" layer="94"/>
<wire x1="5.08" y1="-5.715" x2="5.08" y2="-10.16" width="0.127" layer="94"/>
<wire x1="19.685" y1="17.78" x2="66.04" y2="17.78" width="0.254" layer="94"/>
<wire x1="66.04" y1="17.78" x2="66.04" y2="12.065" width="0.254" layer="94"/>
<wire x1="66.04" y1="12.065" x2="66.04" y2="6.985" width="0.254" layer="94"/>
<wire x1="66.04" y1="6.985" x2="66.04" y2="1.905" width="0.254" layer="94"/>
<wire x1="66.04" y1="1.905" x2="66.04" y2="-3.175" width="0.254" layer="94"/>
<wire x1="66.04" y1="-3.175" x2="66.04" y2="-10.16" width="0.254" layer="94"/>
<wire x1="66.04" y1="-10.16" x2="53.975" y2="-10.16" width="0.254" layer="94"/>
<wire x1="53.975" y1="-10.16" x2="19.685" y2="-10.16" width="0.254" layer="94"/>
<wire x1="19.685" y1="12.065" x2="66.04" y2="12.065" width="0.127" layer="94"/>
<wire x1="19.685" y1="6.985" x2="66.04" y2="6.985" width="0.127" layer="94"/>
<wire x1="19.685" y1="1.905" x2="66.04" y2="1.905" width="0.127" layer="94"/>
<wire x1="19.685" y1="-3.175" x2="53.975" y2="-3.175" width="0.127" layer="94"/>
<wire x1="53.975" y1="-3.175" x2="66.04" y2="-3.175" width="0.127" layer="94"/>
<wire x1="53.975" y1="-3.175" x2="53.975" y2="-6.35" width="0.127" layer="94"/>
<wire x1="53.975" y1="-6.35" x2="53.975" y2="-10.16" width="0.127" layer="94"/>
<wire x1="19.685" y1="-6.35" x2="53.975" y2="-6.35" width="0.127" layer="94"/>
<wire x1="-53.975" y1="-3.175" x2="-22.86" y2="-3.175" width="0.127" layer="94"/>
<wire x1="-53.975" y1="-5.715" x2="-22.86" y2="-5.715" width="0.127" layer="94"/>
<text x="20.48" y="-8.9601" size="1.6764" layer="94">&gt;LAST_DATE_TIME</text>
<text x="20.48" y="-5.7851" size="1.6764" layer="94">&gt;DRAWING_NAME</text>
<text x="-22.225" y="12.7" size="1.6764" layer="94">Bearb.</text>
<text x="-22.225" y="10.16" size="1.6764" layer="94">Gepr.</text>
<text x="-22.225" y="7.62" size="1.6764" layer="94">Norm</text>
<text x="-12.7" y="15.24" size="1.6764" layer="94">Datum</text>
<text x="0" y="15.24" size="1.6764" layer="94">Name</text>
<text x="12.065" y="15.24" size="1.6764" layer="94">Sign.</text>
<text x="-52.705" y="12.7" size="1.6764" layer="94">stand</text>
<text x="-52.705" y="15.24" size="1.6764" layer="94">Änd.-</text>
<text x="-43.18" y="12.7" size="1.6764" layer="94">Datum</text>
<text x="-31.115" y="12.7" size="1.6764" layer="94">Sign.</text>
<text x="54.61" y="-5.715" size="1.6764" layer="94">Blatt</text>
<text x="54.61" y="-8.89" size="1.6764" layer="94">&gt;SHEET</text>
<text x="20.48" y="-8.9601" size="1.6764" layer="94">&gt;LAST_DATE_TIME</text>
<text x="20.48" y="-5.7851" size="1.6764" layer="94">&gt;DRAWING_NAME</text>
<text x="-22.225" y="12.7" size="1.6764" layer="94">Bearb.</text>
<text x="-22.225" y="10.16" size="1.6764" layer="94">Gepr.</text>
<text x="-22.225" y="7.62" size="1.6764" layer="94">Norm</text>
<text x="-12.7" y="15.24" size="1.6764" layer="94">Datum</text>
<text x="0" y="15.24" size="1.6764" layer="94">Name</text>
<text x="12.065" y="15.24" size="1.6764" layer="94">Sign.</text>
<text x="-52.705" y="12.7" size="1.6764" layer="94">stand</text>
<text x="-52.705" y="15.24" size="1.6764" layer="94">Änd.-</text>
<text x="-43.18" y="12.7" size="1.6764" layer="94">Datum</text>
<text x="-31.115" y="12.7" size="1.6764" layer="94">Sign.</text>
<text x="54.61" y="-5.715" size="1.6764" layer="94">Blatt</text>
<text x="54.61" y="-8.89" size="1.6764" layer="94">&gt;SHEET</text>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-19.05" y="5.5825"/>
<vertex x="-19.05" y="-0.42"/>
<vertex x="-17.6675" y="-0.42"/>
<vertex x="-17.6675" y="2.065"/>
<vertex x="-15.445" y="2.065"/>
<vertex x="-15.445" y="-0.42"/>
<vertex x="-14.115" y="-0.42"/>
<vertex x="-14.115" y="5.5825"/>
<vertex x="-15.445" y="5.5825"/>
<vertex x="-15.445" y="3.1675"/>
<vertex x="-17.6675" y="3.1675"/>
<vertex x="-17.6675" y="5.5825"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-12.12" y="5.5825"/>
<vertex x="-11.3325" y="5.5825"/>
<vertex x="-11.3325" y="4.9175"/>
<vertex x="-12.12" y="4.9175"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-8.725" y="5.5825"/>
<vertex x="-7.9025" y="5.5825"/>
<vertex x="-7.9025" y="4.9175"/>
<vertex x="-8.725" y="4.9175"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-13.275" y="-0.42"/>
<vertex x="-11.8225" y="-0.42"/>
<vertex x="-10.055" y="4.235"/>
<vertex x="-6.6425" y="-4.445"/>
<vertex x="-5.295" y="-4.445"/>
<vertex x="-9.285" y="5.5825"/>
<vertex x="-10.72" y="5.5825"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-10.8775" y="2.0825"/>
<vertex x="-9.1975" y="2.0825"/>
<vertex x="-8.7775" y="0.9975"/>
<vertex x="-11.2975" y="0.9975"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-1.795" y="5.5825"/>
<vertex x="-1.795" y="4.5675"/>
<vertex x="-3.825" y="4.5675"/>
<vertex x="-4.385" y="4.4625"/>
<vertex x="-4.7" y="4.3225"/>
<vertex x="-4.91" y="4.1475"/>
<vertex x="-5.015" y="3.9725"/>
<vertex x="-5.155" y="3.6925"/>
<vertex x="-5.26" y="3.3075"/>
<vertex x="-5.33" y="2.8525"/>
<vertex x="-5.33" y="2.03"/>
<vertex x="-5.26" y="1.5225"/>
<vertex x="-5.0675" y="1.1025"/>
<vertex x="-4.875" y="0.8575"/>
<vertex x="-4.5425" y="0.6825"/>
<vertex x="-4.245" y="0.63"/>
<vertex x="-3.895" y="0.6125"/>
<vertex x="-1.795" y="0.6125"/>
<vertex x="-1.795" y="-0.42"/>
<vertex x="-4.56" y="-0.42"/>
<vertex x="-5.05" y="-0.3325"/>
<vertex x="-5.5225" y="-0.1575"/>
<vertex x="-5.995" y="0.1925"/>
<vertex x="-6.3625" y="0.6125"/>
<vertex x="-6.5375" y="0.98"/>
<vertex x="-6.7125" y="1.5225"/>
<vertex x="-6.765" y="1.9075"/>
<vertex x="-6.7825" y="2.4675"/>
<vertex x="-6.765" y="3.0975"/>
<vertex x="-6.66" y="3.7625"/>
<vertex x="-6.4675" y="4.2525"/>
<vertex x="-6.24" y="4.62"/>
<vertex x="-5.89" y="5.0225"/>
<vertex x="-5.435" y="5.3025"/>
<vertex x="-5.0675" y="5.4425"/>
<vertex x="-4.665" y="5.5125"/>
<vertex x="-4.21" y="5.565"/>
<vertex x="-3.79" y="5.5825"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="-0.92" y="5.5825"/>
<vertex x="-0.92" y="-0.42"/>
<vertex x="0.41" y="-0.42"/>
<vertex x="0.41" y="5.5825"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="0.41" y="2.7125"/>
<vertex x="2.72" y="5.5825"/>
<vertex x="4.365" y="5.5825"/>
<vertex x="1.915" y="2.6425"/>
<vertex x="4.3475" y="-0.42"/>
<vertex x="2.615" y="-0.42"/>
<vertex x="0.41" y="2.485"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="9.3525" y="5.5825"/>
<vertex x="9.3525" y="4.4975"/>
<vertex x="7.1125" y="4.4975"/>
<vertex x="6.7625" y="4.4625"/>
<vertex x="6.4475" y="4.3225"/>
<vertex x="6.2375" y="4.165"/>
<vertex x="6.0975" y="3.9725"/>
<vertex x="5.94" y="3.675"/>
<vertex x="5.87" y="3.29"/>
<vertex x="5.87" y="1.6975"/>
<vertex x="5.8875" y="1.5225"/>
<vertex x="6.08" y="1.1025"/>
<vertex x="6.2725" y="0.8575"/>
<vertex x="6.605" y="0.6825"/>
<vertex x="6.9025" y="0.63"/>
<vertex x="7.2525" y="0.6125"/>
<vertex x="9.3525" y="0.6125"/>
<vertex x="9.3525" y="-0.42"/>
<vertex x="6.5875" y="-0.42"/>
<vertex x="6.0975" y="-0.3325"/>
<vertex x="5.625" y="-0.1575"/>
<vertex x="5.1525" y="0.1925"/>
<vertex x="4.785" y="0.6125"/>
<vertex x="4.61" y="0.98"/>
<vertex x="4.435" y="1.5225"/>
<vertex x="4.3825" y="1.9075"/>
<vertex x="4.365" y="2.4675"/>
<vertex x="4.3825" y="3.0975"/>
<vertex x="4.4875" y="3.7625"/>
<vertex x="4.68" y="4.2525"/>
<vertex x="4.9075" y="4.62"/>
<vertex x="5.2575" y="5.0225"/>
<vertex x="5.7125" y="5.3025"/>
<vertex x="6.08" y="5.4425"/>
<vertex x="6.4825" y="5.5125"/>
<vertex x="6.9375" y="5.565"/>
<vertex x="7.3575" y="5.5825"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="5.87" y="2.065"/>
<vertex x="9.3525" y="2.065"/>
<vertex x="9.3525" y="3.1675"/>
<vertex x="5.87" y="3.1675"/>
</polygon>
<polygon width="0.0042" layer="100" spacing="0.889">
<vertex x="10.385" y="5.5825"/>
<vertex x="13.745" y="5.5825"/>
<vertex x="14.375" y="5.5125"/>
<vertex x="14.795" y="5.39"/>
<vertex x="15.075" y="5.215"/>
<vertex x="15.2675" y="5.0225"/>
<vertex x="15.495" y="4.725"/>
<vertex x="15.6175" y="4.3575"/>
<vertex x="15.6875" y="3.99"/>
<vertex x="15.6875" y="3.5875"/>
<vertex x="15.6175" y="3.15"/>
<vertex x="15.425" y="2.7125"/>
<vertex x="15.1975" y="2.4325"/>
<vertex x="14.9525" y="2.2575"/>
<vertex x="14.6375" y="2.1"/>
<vertex x="14.515" y="2.0475"/>
<vertex x="15.95" y="-0.42"/>
<vertex x="14.445" y="-0.42"/>
<vertex x="13.15" y="1.89"/>
<vertex x="13.15" y="2.975"/>
<vertex x="13.535" y="2.975"/>
<vertex x="13.92" y="3.08"/>
<vertex x="14.2175" y="3.255"/>
<vertex x="14.3225" y="3.4825"/>
<vertex x="14.3225" y="4.0075"/>
<vertex x="14.2175" y="4.2175"/>
<vertex x="14.06" y="4.3225"/>
<vertex x="13.9025" y="4.3925"/>
<vertex x="13.6225" y="4.4625"/>
<vertex x="13.3775" y="4.4975"/>
<vertex x="11.7325" y="4.4975"/>
<vertex x="11.7325" y="2.975"/>
<vertex x="13.1483" y="2.975"/>
<vertex x="13.1483" y="1.89"/>
<vertex x="11.7325" y="1.89"/>
<vertex x="11.7325" y="-0.42"/>
<vertex x="10.385" y="-0.42"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-5.12" y="-2.1875"/>
<vertex x="-4.455" y="-2.1875"/>
<vertex x="-4.455" y="-3.745"/>
<vertex x="-4.42" y="-3.7975"/>
<vertex x="-4.35" y="-3.8675"/>
<vertex x="-4.28" y="-3.9025"/>
<vertex x="-3.65" y="-3.9025"/>
<vertex x="-3.65" y="-2.1875"/>
<vertex x="-2.985" y="-2.1875"/>
<vertex x="-2.985" y="-4.3575"/>
<vertex x="-3.0725" y="-4.445"/>
<vertex x="-4.28" y="-4.445"/>
<vertex x="-4.5075" y="-4.4275"/>
<vertex x="-4.595" y="-4.41"/>
<vertex x="-4.7175" y="-4.3575"/>
<vertex x="-4.84" y="-4.2875"/>
<vertex x="-4.9975" y="-4.13"/>
<vertex x="-5.0675" y="-4.0075"/>
<vertex x="-5.1025" y="-3.92"/>
<vertex x="-5.12" y="-3.85"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-2.7575" y="-2.1875"/>
<vertex x="-2.7575" y="-2.6075"/>
<vertex x="-2.3375" y="-2.6075"/>
<vertex x="-2.3375" y="-3.9025"/>
<vertex x="-2.2675" y="-4.0775"/>
<vertex x="-2.1275" y="-4.2525"/>
<vertex x="-1.9525" y="-4.375"/>
<vertex x="-1.7075" y="-4.445"/>
<vertex x="-1.1125" y="-4.445"/>
<vertex x="-1.025" y="-4.375"/>
<vertex x="-1.025" y="-3.955"/>
<vertex x="-1.445" y="-3.955"/>
<vertex x="-1.5325" y="-3.9025"/>
<vertex x="-1.62" y="-3.7975"/>
<vertex x="-1.6725" y="-3.6575"/>
<vertex x="-1.6725" y="-2.6075"/>
<vertex x="-1.025" y="-2.6075"/>
<vertex x="-1.025" y="-2.1875"/>
<vertex x="-1.6725" y="-2.1875"/>
<vertex x="-1.6725" y="-1.3475"/>
<vertex x="-2.3375" y="-1.3475"/>
<vertex x="-2.3375" y="-2.1875"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="-0.92" y="-3.2742"/>
<vertex x="-0.92" y="-3.4475"/>
<vertex x="-0.85" y="-3.7625"/>
<vertex x="-0.675" y="-4.06"/>
<vertex x="-0.4125" y="-4.27"/>
<vertex x="-0.1325" y="-4.41"/>
<vertex x="0.095" y="-4.445"/>
<vertex x="0.55" y="-4.445"/>
<vertex x="0.865" y="-4.375"/>
<vertex x="1.11" y="-4.2525"/>
<vertex x="1.3025" y="-4.06"/>
<vertex x="1.46" y="-3.8325"/>
<vertex x="1.5125" y="-3.6627"/>
<vertex x="1.53" y="-3.5525"/>
<vertex x="1.53" y="-3.045"/>
<vertex x="1.46" y="-2.7475"/>
<vertex x="1.32" y="-2.5375"/>
<vertex x="1.11" y="-2.345"/>
<vertex x="0.865" y="-2.2225"/>
<vertex x="0.55" y="-2.135"/>
<vertex x="0.06" y="-2.135"/>
<vertex x="-0.185" y="-2.1875"/>
<vertex x="-0.4475" y="-2.3275"/>
<vertex x="-0.605" y="-2.485"/>
<vertex x="-0.78" y="-2.7125"/>
<vertex x="-0.885" y="-2.9575"/>
<vertex x="-0.92" y="-3.15"/>
<vertex x="-0.92" y="-3.2725"/>
<vertex x="-0.255" y="-3.2725"/>
<vertex x="-0.255" y="-3.1675"/>
<vertex x="-0.2025" y="-3.01"/>
<vertex x="-0.1325" y="-2.8875"/>
<vertex x="0.025" y="-2.7125"/>
<vertex x="0.2" y="-2.625"/>
<vertex x="0.4275" y="-2.625"/>
<vertex x="0.5325" y="-2.66"/>
<vertex x="0.69" y="-2.765"/>
<vertex x="0.8125" y="-2.9575"/>
<vertex x="0.865" y="-3.1325"/>
<vertex x="0.865" y="-3.465"/>
<vertex x="0.8125" y="-3.6225"/>
<vertex x="0.76" y="-3.7275"/>
<vertex x="0.6375" y="-3.8675"/>
<vertex x="0.515" y="-3.9375"/>
<vertex x="0.4275" y="-3.955"/>
<vertex x="0.1825" y="-3.955"/>
<vertex x="0.025" y="-3.9025"/>
<vertex x="-0.115" y="-3.7625"/>
<vertex x="-0.22" y="-3.5525"/>
<vertex x="-0.255" y="-3.395"/>
<vertex x="-0.255" y="-3.2742"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="11.05" y="-3.2742"/>
<vertex x="11.05" y="-3.4475"/>
<vertex x="11.12" y="-3.7625"/>
<vertex x="11.295" y="-4.06"/>
<vertex x="11.5575" y="-4.27"/>
<vertex x="11.8375" y="-4.41"/>
<vertex x="12.065" y="-4.445"/>
<vertex x="12.52" y="-4.445"/>
<vertex x="12.835" y="-4.375"/>
<vertex x="13.08" y="-4.2525"/>
<vertex x="13.2725" y="-4.06"/>
<vertex x="13.43" y="-3.8325"/>
<vertex x="13.4825" y="-3.6627"/>
<vertex x="13.5" y="-3.5525"/>
<vertex x="13.5" y="-3.045"/>
<vertex x="13.43" y="-2.7475"/>
<vertex x="13.29" y="-2.5375"/>
<vertex x="13.08" y="-2.345"/>
<vertex x="12.835" y="-2.2225"/>
<vertex x="12.52" y="-2.135"/>
<vertex x="12.03" y="-2.135"/>
<vertex x="11.785" y="-2.1875"/>
<vertex x="11.5225" y="-2.3275"/>
<vertex x="11.365" y="-2.485"/>
<vertex x="11.19" y="-2.7125"/>
<vertex x="11.085" y="-2.9575"/>
<vertex x="11.05" y="-3.15"/>
<vertex x="11.05" y="-3.2725"/>
<vertex x="11.715" y="-3.2725"/>
<vertex x="11.715" y="-3.1675"/>
<vertex x="11.7675" y="-3.01"/>
<vertex x="11.8375" y="-2.8875"/>
<vertex x="11.995" y="-2.7125"/>
<vertex x="12.17" y="-2.625"/>
<vertex x="12.3975" y="-2.625"/>
<vertex x="12.5025" y="-2.66"/>
<vertex x="12.66" y="-2.765"/>
<vertex x="12.7825" y="-2.9575"/>
<vertex x="12.835" y="-3.1325"/>
<vertex x="12.835" y="-3.465"/>
<vertex x="12.7825" y="-3.6225"/>
<vertex x="12.73" y="-3.7275"/>
<vertex x="12.6075" y="-3.8675"/>
<vertex x="12.485" y="-3.9375"/>
<vertex x="12.3975" y="-3.955"/>
<vertex x="12.1525" y="-3.955"/>
<vertex x="11.995" y="-3.9025"/>
<vertex x="11.855" y="-3.7625"/>
<vertex x="11.75" y="-3.5525"/>
<vertex x="11.715" y="-3.395"/>
<vertex x="11.715" y="-3.2742"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="10.105" y="-2.1875"/>
<vertex x="10.105" y="-4.445"/>
<vertex x="10.77" y="-4.445"/>
<vertex x="10.77" y="-2.1875"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="10.105" y="-1.3475"/>
<vertex x="10.105" y="-1.9075"/>
<vertex x="10.77" y="-1.9075"/>
<vertex x="10.77" y="-1.3475"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="15.9325" y="-4.445"/>
<vertex x="15.2675" y="-4.445"/>
<vertex x="15.2675" y="-2.835"/>
<vertex x="15.2325" y="-2.7825"/>
<vertex x="15.1625" y="-2.7125"/>
<vertex x="15.0925" y="-2.6775"/>
<vertex x="14.4975" y="-2.6775"/>
<vertex x="14.4975" y="-4.445"/>
<vertex x="13.85" y="-4.445"/>
<vertex x="13.85" y="-2.1875"/>
<vertex x="15.3725" y="-2.1875"/>
<vertex x="15.4425" y="-2.205"/>
<vertex x="15.53" y="-2.24"/>
<vertex x="15.67" y="-2.31"/>
<vertex x="15.7575" y="-2.3975"/>
<vertex x="15.845" y="-2.52"/>
<vertex x="15.915" y="-2.66"/>
<vertex x="15.9325" y="-2.7825"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="5.065" y="-4.445"/>
<vertex x="4.47" y="-4.445"/>
<vertex x="4.47" y="-2.835"/>
<vertex x="4.435" y="-2.7825"/>
<vertex x="4.365" y="-2.7125"/>
<vertex x="4.295" y="-2.6775"/>
<vertex x="3.84" y="-2.6775"/>
<vertex x="3.84" y="-4.375"/>
<vertex x="3.77" y="-4.445"/>
<vertex x="3.21" y="-4.445"/>
<vertex x="3.1225" y="-4.3575"/>
<vertex x="3.1225" y="-2.6775"/>
<vertex x="2.5275" y="-2.6775"/>
<vertex x="2.5275" y="-4.375"/>
<vertex x="2.4575" y="-4.445"/>
<vertex x="1.8625" y="-4.445"/>
<vertex x="1.8625" y="-2.1875"/>
<vertex x="4.575" y="-2.1875"/>
<vertex x="4.645" y="-2.205"/>
<vertex x="4.7325" y="-2.24"/>
<vertex x="4.8725" y="-2.31"/>
<vertex x="4.96" y="-2.3975"/>
<vertex x="5.0475" y="-2.52"/>
<vertex x="5.1175" y="-2.66"/>
<vertex x="5.135" y="-2.7825"/>
<vertex x="5.135" y="-4.3575"/>
<vertex x="5.065" y="-4.4275"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="7.865" y="-4.3575"/>
<vertex x="7.7775" y="-4.445"/>
<vertex x="6.115" y="-4.445"/>
<vertex x="5.835" y="-4.375"/>
<vertex x="5.625" y="-4.2"/>
<vertex x="5.5025" y="-4.025"/>
<vertex x="5.485" y="-3.9025"/>
<vertex x="5.485" y="-3.5525"/>
<vertex x="5.59" y="-3.325"/>
<vertex x="5.695" y="-3.2025"/>
<vertex x="5.765" y="-3.1675"/>
<vertex x="5.9225" y="-3.0975"/>
<vertex x="6.115" y="-3.0625"/>
<vertex x="7.1458" y="-3.0625"/>
<vertex x="7.1458" y="-3.5"/>
<vertex x="6.3425" y="-3.5"/>
<vertex x="6.2725" y="-3.535"/>
<vertex x="6.22" y="-3.57"/>
<vertex x="6.185" y="-3.6225"/>
<vertex x="6.1675" y="-3.6575"/>
<vertex x="6.15" y="-3.7275"/>
<vertex x="6.15" y="-3.78"/>
<vertex x="6.1675" y="-3.8325"/>
<vertex x="6.22" y="-3.9025"/>
<vertex x="6.2725" y="-3.9375"/>
<vertex x="6.325" y="-3.955"/>
<vertex x="7.1475" y="-3.955"/>
<vertex x="7.1475" y="-2.835"/>
<vertex x="7.1125" y="-2.7825"/>
<vertex x="7.0425" y="-2.7125"/>
<vertex x="6.9725" y="-2.6775"/>
<vertex x="5.66" y="-2.6775"/>
<vertex x="5.66" y="-2.1875"/>
<vertex x="7.2525" y="-2.1875"/>
<vertex x="7.3225" y="-2.205"/>
<vertex x="7.41" y="-2.24"/>
<vertex x="7.55" y="-2.31"/>
<vertex x="7.6375" y="-2.3975"/>
<vertex x="7.725" y="-2.52"/>
<vertex x="7.795" y="-2.66"/>
<vertex x="7.865" y="-2.94"/>
</polygon>
<polygon width="0.0042" layer="101" spacing="0.889">
<vertex x="8.0925" y="-2.1875"/>
<vertex x="8.0925" y="-2.6075"/>
<vertex x="8.5125" y="-2.6075"/>
<vertex x="8.5125" y="-3.9025"/>
<vertex x="8.5825" y="-4.0775"/>
<vertex x="8.7225" y="-4.2525"/>
<vertex x="8.8975" y="-4.375"/>
<vertex x="9.1425" y="-4.445"/>
<vertex x="9.7375" y="-4.445"/>
<vertex x="9.825" y="-4.375"/>
<vertex x="9.825" y="-3.955"/>
<vertex x="9.405" y="-3.955"/>
<vertex x="9.3175" y="-3.9025"/>
<vertex x="9.23" y="-3.7975"/>
<vertex x="9.1775" y="-3.6575"/>
<vertex x="9.1775" y="-2.6075"/>
<vertex x="9.825" y="-2.6075"/>
<vertex x="9.825" y="-2.1875"/>
<vertex x="9.1775" y="-2.1875"/>
<vertex x="9.1775" y="-1.3475"/>
<vertex x="8.5125" y="-1.3475"/>
<vertex x="8.5125" y="-2.1875"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-19.05" y="-3.9964"/>
<vertex x="-18.6014" y="-3.9964"/>
<vertex x="-18.6014" y="-4.445"/>
<vertex x="-19.05" y="-4.445"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-19.05" y="-3.0991"/>
<vertex x="-18.6014" y="-3.0991"/>
<vertex x="-18.6014" y="-3.5477"/>
<vertex x="-19.05" y="-3.5477"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-19.05" y="-2.2018"/>
<vertex x="-18.6014" y="-2.2018"/>
<vertex x="-18.6014" y="-2.6505"/>
<vertex x="-19.05" y="-2.6505"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-18.1527" y="-3.9964"/>
<vertex x="-17.7041" y="-3.9964"/>
<vertex x="-17.7041" y="-4.445"/>
<vertex x="-18.1527" y="-4.445"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-18.1527" y="-3.0991"/>
<vertex x="-17.7041" y="-3.0991"/>
<vertex x="-17.7041" y="-3.5477"/>
<vertex x="-18.1527" y="-3.5477"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-18.1527" y="-2.2018"/>
<vertex x="-17.7041" y="-2.2018"/>
<vertex x="-17.7041" y="-2.6505"/>
<vertex x="-18.1527" y="-2.6505"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-17.2555" y="-3.9964"/>
<vertex x="-16.8069" y="-3.9964"/>
<vertex x="-16.8069" y="-4.445"/>
<vertex x="-17.2555" y="-4.445"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-17.2555" y="-3.0991"/>
<vertex x="-16.8069" y="-3.0991"/>
<vertex x="-16.8069" y="-3.5477"/>
<vertex x="-17.2555" y="-3.5477"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-17.2555" y="-2.2018"/>
<vertex x="-16.8069" y="-2.2018"/>
<vertex x="-16.8069" y="-2.6505"/>
<vertex x="-17.2555" y="-2.6505"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-16.3582" y="-3.9964"/>
<vertex x="-15.9096" y="-3.9964"/>
<vertex x="-15.9096" y="-4.445"/>
<vertex x="-16.3582" y="-4.445"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-16.3582" y="-3.0991"/>
<vertex x="-15.9096" y="-3.0991"/>
<vertex x="-15.9096" y="-3.5477"/>
<vertex x="-16.3582" y="-3.5477"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-16.3582" y="-2.2018"/>
<vertex x="-15.9096" y="-2.2018"/>
<vertex x="-15.9096" y="-2.6505"/>
<vertex x="-16.3582" y="-2.6505"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-15.4609" y="-3.9964"/>
<vertex x="-15.0123" y="-3.9964"/>
<vertex x="-15.0123" y="-4.445"/>
<vertex x="-15.4609" y="-4.445"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-15.4609" y="-3.0991"/>
<vertex x="-15.0123" y="-3.0991"/>
<vertex x="-15.0123" y="-3.5477"/>
<vertex x="-15.4609" y="-3.5477"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-15.4609" y="-2.2018"/>
<vertex x="-15.0123" y="-2.2018"/>
<vertex x="-15.0123" y="-2.6505"/>
<vertex x="-15.4609" y="-2.6505"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-14.5637" y="-3.9964"/>
<vertex x="-14.1151" y="-3.9964"/>
<vertex x="-14.1151" y="-4.445"/>
<vertex x="-14.5637" y="-4.445"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-14.5637" y="-3.0991"/>
<vertex x="-14.1151" y="-3.0991"/>
<vertex x="-14.1151" y="-3.5477"/>
<vertex x="-14.5637" y="-3.5477"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-14.5637" y="-2.2018"/>
<vertex x="-14.1151" y="-2.2018"/>
<vertex x="-14.1151" y="-2.6505"/>
<vertex x="-14.5637" y="-2.6505"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-12.7691" y="-2.6505"/>
<vertex x="-12.7691" y="-2.2019"/>
<vertex x="-12.3205" y="-2.2019"/>
<vertex x="-12.3205" y="-2.6505"/>
</polygon>
<polygon width="0.0042" layer="100">
<vertex x="-13.6664" y="-2.6505"/>
<vertex x="-13.6664" y="-2.2019"/>
<vertex x="-13.2177" y="-2.2019"/>
<vertex x="-13.2177" y="-2.6505"/>
</polygon>
</symbol>
<symbol name="A4_LOC3">
<frame x1="-256.54" y1="-3.81" x2="3.81" y2="175.26" columns="8" rows="5" layer="94"/>
</symbol>
<symbol name="B4_LOC">
<wire x1="-317.175" y1="-3.175" x2="-17.875" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="3.175" y1="-3.175" x2="3.175" y2="58.8" width="0.1524" layer="94"/>
<wire x1="3.175" y1="58.8" x2="3.175" y2="117.6" width="0.1524" layer="94"/>
<wire x1="3.175" y1="117.6" x2="3.175" y2="176.4" width="0.1524" layer="94"/>
<wire x1="3.175" y1="176.4" x2="3.175" y2="207.895" width="0.1524" layer="94"/>
<wire x1="-317.175" y1="207.895" x2="-317.175" y2="176.4" width="0.1524" layer="94"/>
<wire x1="-317.175" y1="176.4" x2="-317.175" y2="117.6" width="0.1524" layer="94"/>
<wire x1="-317.175" y1="117.6" x2="-317.175" y2="58.8" width="0.1524" layer="94"/>
<wire x1="-317.175" y1="58.8" x2="-317.175" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="58.8" width="0.1524" layer="94"/>
<wire x1="0" y1="58.8" x2="0" y2="117.6" width="0.1524" layer="94"/>
<wire x1="0" y1="117.6" x2="0" y2="176.4" width="0.1524" layer="94"/>
<wire x1="0" y1="176.4" x2="0" y2="204.72" width="0.1524" layer="94"/>
<wire x1="-314" y1="204.72" x2="-314" y2="176.4" width="0.1524" layer="94"/>
<wire x1="-314" y1="176.4" x2="-314" y2="117.6" width="0.1524" layer="94"/>
<wire x1="-314" y1="117.6" x2="-314" y2="58.8" width="0.1524" layer="94"/>
<wire x1="-314" y1="58.8" x2="-314" y2="0" width="0.1524" layer="94"/>
<wire x1="-317.175" y1="117.6" x2="-314" y2="117.6" width="0.1016" layer="94"/>
<wire x1="0" y1="176.4" x2="3.175" y2="176.4" width="0.1016" layer="94"/>
<wire x1="-317.175" y1="58.8" x2="-314" y2="58.8" width="0.1016" layer="94"/>
<wire x1="0" y1="117.6" x2="3.175" y2="117.6" width="0.1016" layer="94"/>
<wire x1="-317.175" y1="176.4" x2="-314" y2="176.4" width="0.1016" layer="94"/>
<wire x1="0" y1="58.8" x2="3.175" y2="58.8" width="0.1016" layer="94"/>
<wire x1="0" y1="204.72" x2="-31.4" y2="204.72" width="0.1524" layer="94"/>
<wire x1="-31.4" y1="204.72" x2="-62.8" y2="204.72" width="0.1524" layer="94"/>
<wire x1="-62.8" y1="204.72" x2="-314" y2="204.72" width="0.1524" layer="94"/>
<wire x1="3.175" y1="207.895" x2="-31.4" y2="207.895" width="0.1524" layer="94"/>
<wire x1="-31.4" y1="207.895" x2="-317.175" y2="207.895" width="0.1524" layer="94"/>
<wire x1="-314" y1="0" x2="-79.627" y2="0" width="0.1524" layer="94"/>
<wire x1="-79.246" y1="0" x2="-17.875" y2="0" width="0.1524" layer="94"/>
<wire x1="-17.748" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-17.748" y1="-3.175" x2="3.175" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="-31.4" y1="204.72" x2="-31.4" y2="207.895" width="0.1016" layer="94"/>
<wire x1="-62.8" y1="204.72" x2="-62.8" y2="207.895" width="0.1016" layer="94"/>
<wire x1="-94.2" y1="204.72" x2="-94.2" y2="207.895" width="0.1016" layer="94"/>
<wire x1="-125.6" y1="204.72" x2="-125.6" y2="207.895" width="0.1016" layer="94"/>
<wire x1="-157" y1="204.72" x2="-157" y2="207.895" width="0.1016" layer="94"/>
<wire x1="-188.4" y1="204.72" x2="-188.4" y2="207.895" width="0.1016" layer="94"/>
<wire x1="-219.8" y1="204.72" x2="-219.8" y2="207.895" width="0.1016" layer="94"/>
<wire x1="-251.2" y1="204.72" x2="-251.2" y2="207.895" width="0.1016" layer="94"/>
<wire x1="-282.6" y1="204.72" x2="-282.6" y2="207.895" width="0.1016" layer="94"/>
<wire x1="-31.4" y1="-3.175" x2="-31.4" y2="0" width="0.1016" layer="94"/>
<wire x1="-62.8" y1="-3.175" x2="-62.8" y2="0" width="0.1016" layer="94"/>
<wire x1="-94.2" y1="-3.175" x2="-94.2" y2="0" width="0.1016" layer="94"/>
<wire x1="-125.6" y1="-3.175" x2="-125.6" y2="0" width="0.1016" layer="94"/>
<wire x1="-157" y1="-3.175" x2="-157" y2="0" width="0.1016" layer="94"/>
<wire x1="-188.4" y1="-3.175" x2="-188.4" y2="0" width="0.1016" layer="94"/>
<wire x1="-219.8" y1="-3.175" x2="-219.8" y2="0" width="0.1016" layer="94"/>
<wire x1="-251.2" y1="-3.175" x2="-251.2" y2="0" width="0.1016" layer="94"/>
<wire x1="-282.6" y1="-3.175" x2="-282.6" y2="0" width="0.1016" layer="94"/>
<text x="0.889" y="29.527" size="2.54" layer="94">1</text>
<text x="0.635" y="88.327" size="2.54" layer="94">2</text>
<text x="0.635" y="147.127" size="2.54" layer="94">3</text>
<text x="-172.335" y="205.101" size="2.54" layer="94">E</text>
<text x="-205.227" y="205.101" size="2.54" layer="94">D</text>
<text x="-236.151" y="205.101" size="2.54" layer="94">C</text>
<text x="-267.297" y="205.101" size="2.54" layer="94">B</text>
<text x="-298.951" y="205.101" size="2.54" layer="94">A</text>
<text x="-316.413" y="147.127" size="2.54" layer="94">3</text>
<text x="-316.413" y="88.073" size="2.54" layer="94">2</text>
<text x="-316.159" y="27.495" size="2.54" layer="94">1</text>
<text x="-140.173" y="205.101" size="2.54" layer="94">F</text>
<text x="0.381" y="192.973" size="2.54" layer="94">4</text>
<text x="-316.667" y="192.973" size="2.54" layer="94">4</text>
<text x="-110.773" y="205.101" size="2.54" layer="94">G</text>
<text x="-79.373" y="205.101" size="2.54" layer="94">H</text>
<text x="-45.973" y="205.101" size="2.54" layer="94">I</text>
<text x="-14.573" y="205.101" size="2.54" layer="94">J</text>
<text x="-172.335" y="-2.794" size="2.54" layer="94">E</text>
<text x="-205.227" y="-2.794" size="2.54" layer="94">D</text>
<text x="-236.151" y="-2.794" size="2.54" layer="94">C</text>
<text x="-267.297" y="-2.794" size="2.54" layer="94">B</text>
<text x="-298.951" y="-2.794" size="2.54" layer="94">A</text>
<text x="-140.173" y="-2.794" size="2.54" layer="94">F</text>
<text x="-110.773" y="-2.794" size="2.54" layer="94">G</text>
<text x="-79.373" y="-2.794" size="2.54" layer="94">H</text>
<text x="-45.973" y="-2.794" size="2.54" layer="94">I</text>
<text x="-14.573" y="-2.794" size="2.54" layer="94">J</text>
</symbol>
<symbol name="C">
<wire x1="-2.54" y1="0" x2="-0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="0.508" y2="0" width="0.1524" layer="94"/>
<wire x1="0.508" y1="1.27" x2="0.508" y2="0" width="0.254" layer="94"/>
<wire x1="-0.635" y1="1.27" x2="-0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-0.635" y1="-1.27" x2="-0.508" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-0.508" y1="-1.27" x2="-0.508" y2="0" width="0.254" layer="94"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="1.27" width="0.254" layer="94"/>
<wire x1="-0.508" y1="1.27" x2="-0.635" y2="1.27" width="0.254" layer="94"/>
<wire x1="0.508" y1="-1.27" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="1.27" x2="0.635" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="1.27" x2="0.508" y2="1.27" width="0.254" layer="94"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-1.27" width="0.254" layer="94"/>
<text x="-2.794" y="-1.27" size="0.8636" layer="93">1</text>
<text x="2.286" y="-1.27" size="0.8636" layer="93">2</text>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<text x="-2.54" y="1.651" size="1.4224" layer="95" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.4224" layer="96" ratio="10">&gt;VALUE</text>
</symbol>
<symbol name="R_EU">
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.254" layer="94"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-3.175" y1="-1.27" x2="3.175" y2="-1.27" width="0.254" layer="94"/>
<wire x1="3.175" y1="-1.27" x2="3.175" y2="0" width="0.254" layer="94"/>
<wire x1="3.175" y1="0" x2="3.175" y2="1.27" width="0.254" layer="94"/>
<wire x1="3.175" y1="1.27" x2="-3.175" y2="1.27" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-3.175" y2="0" width="0.1524" layer="94"/>
<wire x1="3.175" y1="0" x2="5.08" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="1.905" size="1.4224" layer="95" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.4224" layer="96" ratio="10">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SCHRIFTF" prefix="SF" uservalue="yes">
<gates>
<gate name="FRAME_A3" symbol="A3_LOC" x="0" y="0" addlevel="request"/>
<gate name="FRAME_A1" symbol="A1_LOC" x="0" y="0" addlevel="request"/>
<gate name="FRAME_A0" symbol="A0_LOC" x="0" y="0" addlevel="request"/>
<gate name="FRAME_A2" symbol="A2_LOC" x="0" y="0" addlevel="request"/>
<gate name="G$1" symbol="A4_LOC2" x="0" y="0" addlevel="request"/>
<gate name="G$2" symbol="SCHRIFTF" x="0" y="0"/>
<gate name="FRAME_A4" symbol="A4_LOC3" x="0" y="0"/>
<gate name="FRAME_B4" symbol="B4_LOC" x="0" y="0" addlevel="request"/>
</gates>
<devices>
<device name="A4_OLD" package="SCHRIFTF">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="A5" package="SCHRI_A5">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="A6" package="SCHRI_A6">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="A4" package="SCHRIFTA">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="C_SMD" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2220" package="2220">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0306" package="0306">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R_SMD" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="R_EU" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812" package="1812">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1825" package="1825">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3425" package="3425">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ZIF-SteckerverbinderundFlexibleFlachbandkabel">
<description>&lt;BR&gt;Wurth Elektronik   FPC Connector and FFC Cable&lt;br&gt;&lt;Hr&gt;
&lt;BR&gt;&lt;BR&gt; 
&lt;TABLE BORDER=0 CELLSPACING=0 CELLPADDING=0&gt;
&lt;TR&gt;   
&lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;BR&gt;&lt;br&gt;
      &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
&lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;br&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;
      -----&lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt; &lt;FONT FACE=ARIAL SIZE=3&gt;&lt;br&gt;
      ---------------------------&lt;BR&gt;
&lt;B&gt;&lt;I&gt;&lt;span style='font-size:26pt;
  color:#FF6600;'&gt;WE &lt;/span&gt;&lt;/i&gt;&lt;/b&gt;
&lt;BR&gt;
      ---------------------------&lt;BR&gt;&lt;b&gt;Würth Elektronik&lt;/b&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;br&gt;
      ---------O---&lt;BR&gt;
      ----O--------&lt;BR&gt;
      ---------O---&lt;BR&gt;
      ----O--------&lt;BR&gt;
      ---------O---&lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
   
&lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;&lt;BR&gt;
      &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;
       &lt;BR&gt;&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;

  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  
&lt;/TABLE&gt;
&lt;B&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;More than you expect&lt;BR&gt;&lt;BR&gt;&lt;BR&gt;&lt;/B&gt;

&lt;HR&gt;&lt;BR&gt;
&lt;b&gt;Würth Elektronik eiSos GmbH &amp; Co. KG&lt;/b&gt;&lt;br&gt;
EMC &amp; Inductive Solutions&lt;br&gt;

Max-Eyth-Str.1&lt;br&gt;
D-74638 Waldenburg&lt;br&gt;
&lt;br&gt;
Tel: +49 (0)7942-945-0&lt;br&gt;
Fax:+49 (0)7942-945-405&lt;br&gt;
&lt;br&gt;
&lt;a href="http://www.we-online.com/eagle"&gt;http://www.we-online.com/eagle&lt;/a&gt;&lt;br&gt;
&lt;a href="mailto:libraries@we-online.com"&gt;libraries@we-online.com&lt;/a&gt; &lt;BR&gt;&lt;BR&gt;
&lt;br&gt;&lt;HR&gt;&lt;BR&gt;
Neither CadSoft nor WE-eiSos does warrant that this library is error-free or &lt;br&gt;
that it meets your specific requirements.&lt;br&gt;&lt;BR&gt;
Please contact us for more information.&lt;br&gt;&lt;BR&gt;&lt;br&gt;
&lt;hr&gt;
Eagle Version 6, Library Revision 2015b, 08.04.2015&lt;br&gt;
&lt;HR&gt;
Copyright: Würth Elektronik</description>
<packages>
<package name="687310124422">
<description>WR-FPC-0.50mm SMT ZIF Vertical Type A,pins 10</description>
<wire x1="-5.68" y1="1.75" x2="-5.68" y2="-1.75" width="0.127" layer="51"/>
<wire x1="-5.68" y1="-1.75" x2="5.72" y2="-1.75" width="0.127" layer="51"/>
<wire x1="5.72" y1="-1.75" x2="5.72" y2="1.75" width="0.127" layer="51"/>
<wire x1="5.72" y1="1.75" x2="-5.68" y2="1.75" width="0.127" layer="51"/>
<wire x1="-5.72" y1="2.42" x2="-5.72" y2="-2.42" width="0.127" layer="21"/>
<wire x1="5.72" y1="2.42" x2="-5.72" y2="2.42" width="0.127" layer="21"/>
<wire x1="-5.72" y1="-2.42" x2="5.72" y2="-2.42" width="0.127" layer="21"/>
<wire x1="5.72" y1="-2.42" x2="5.72" y2="2.42" width="0.127" layer="21"/>
<smd name="4" x="-0.77" y="1.4" dx="0.4" dy="1.5" layer="1" rot="R180"/>
<smd name="Z2" x="-3.27" y="-1.4" dx="0.8" dy="1.5" layer="1" rot="R180"/>
<smd name="5" x="-0.27" y="-1.4" dx="0.4" dy="1.5" layer="1" rot="R180"/>
<smd name="6" x="0.23" y="1.4" dx="0.4" dy="1.5" layer="1" rot="R180"/>
<smd name="3" x="-1.27" y="-1.4" dx="0.4" dy="1.5" layer="1" rot="R180"/>
<smd name="1" x="-2.27" y="-1.4" dx="0.4" dy="1.5" layer="1" rot="R180"/>
<smd name="2" x="-1.77" y="1.4" dx="0.4" dy="1.5" layer="1"/>
<smd name="Z3" x="3.23" y="-1.4" dx="0.8" dy="1.5" layer="1" rot="R180"/>
<smd name="Z1" x="-3.27" y="1.4" dx="0.8" dy="1.5" layer="1" rot="R180"/>
<smd name="Z4" x="3.23" y="1.4" dx="0.8" dy="1.5" layer="1" rot="R180"/>
<smd name="7" x="0.73" y="-1.4" dx="0.4" dy="1.5" layer="1" rot="R180"/>
<smd name="8" x="1.23" y="1.4" dx="0.4" dy="1.5" layer="1" rot="R180"/>
<smd name="9" x="1.73" y="-1.4" dx="0.4" dy="1.5" layer="1" rot="R180"/>
<smd name="10" x="2.23" y="1.4" dx="0.4" dy="1.5" layer="1" rot="R180"/>
<text x="-2.82" y="-1.4" size="1.27" layer="51">1</text>
<text x="-3.085" y="3.1" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.02" y="-4.05" size="1.27" layer="27">&gt;VALUE</text>
<text x="1.27" y="0" size="1.27" layer="51">10</text>
<wire x1="-5" y1="-2.5" x2="-5" y2="0" width="0.1027" layer="21"/>
<wire x1="-5" y1="0" x2="-4.5" y2="0" width="0.1027" layer="21"/>
<wire x1="-4.5" y1="0" x2="-4" y2="0" width="0.1027" layer="21"/>
<wire x1="-3.5" y1="0" x2="-3" y2="0" width="0.1027" layer="21"/>
<wire x1="-2.5" y1="0" x2="-2" y2="0" width="0.1027" layer="21"/>
<wire x1="-1.5" y1="0" x2="-1" y2="0" width="0.1027" layer="21"/>
<wire x1="-0.5" y1="0" x2="0" y2="0" width="0.1027" layer="21"/>
<wire x1="0.5" y1="0" x2="1" y2="0" width="0.1027" layer="21"/>
<wire x1="1.5" y1="0" x2="2" y2="0" width="0.1027" layer="21"/>
<wire x1="2.5" y1="0" x2="3" y2="0" width="0.1027" layer="21"/>
<wire x1="3.5" y1="0" x2="4" y2="0" width="0.1027" layer="21"/>
<wire x1="4" y1="0" x2="4.5" y2="0" width="0.1027" layer="21"/>
<wire x1="4.5" y1="0" x2="4.5" y2="-2.5" width="0.1027" layer="21"/>
<wire x1="-4.5" y1="0" x2="-4.5" y2="-0.5" width="0.1027" layer="21"/>
<wire x1="-4.5" y1="-0.5" x2="-4" y2="-0.5" width="0.1027" layer="21"/>
<wire x1="-4" y1="-0.5" x2="-4" y2="0" width="0.1027" layer="21"/>
<wire x1="-4" y1="0" x2="-3.5" y2="0" width="0.1027" layer="21"/>
<wire x1="-3.5" y1="0" x2="-3.5" y2="-0.5" width="0.1027" layer="21"/>
<wire x1="-3.5" y1="-0.5" x2="-3" y2="-0.5" width="0.1027" layer="21"/>
<wire x1="-3" y1="-0.5" x2="-3" y2="0" width="0.1027" layer="21"/>
<wire x1="-3" y1="0" x2="-2.5" y2="0" width="0.1027" layer="21"/>
<wire x1="-2.5" y1="0" x2="-2.5" y2="-0.5" width="0.1027" layer="21"/>
<wire x1="-2.5" y1="-0.5" x2="-2" y2="-0.5" width="0.1027" layer="21"/>
<wire x1="-2" y1="-0.5" x2="-2" y2="0" width="0.1027" layer="21"/>
<wire x1="-2" y1="0" x2="-1.5" y2="0" width="0.1027" layer="21"/>
<wire x1="-1.5" y1="0" x2="-1.5" y2="-0.5" width="0.1027" layer="21"/>
<wire x1="-1.5" y1="-0.5" x2="-1" y2="-0.5" width="0.1027" layer="21"/>
<wire x1="-1" y1="-0.5" x2="-1" y2="0" width="0.1027" layer="21"/>
<wire x1="-1" y1="0" x2="-0.5" y2="0" width="0.1027" layer="21"/>
<wire x1="-0.5" y1="0" x2="-0.5" y2="-0.5" width="0.1027" layer="21"/>
<wire x1="-0.5" y1="-0.5" x2="0" y2="-0.5" width="0.1027" layer="21"/>
<wire x1="0" y1="-0.5" x2="0" y2="0" width="0.1027" layer="21"/>
<wire x1="0" y1="0" x2="0.5" y2="0" width="0.1027" layer="21"/>
<wire x1="0.5" y1="0" x2="0.5" y2="-0.5" width="0.1027" layer="21"/>
<wire x1="0.5" y1="-0.5" x2="1" y2="-0.5" width="0.1027" layer="21"/>
<wire x1="1" y1="-0.5" x2="1" y2="0" width="0.1027" layer="21"/>
<wire x1="1" y1="0" x2="1.5" y2="0" width="0.1027" layer="21"/>
<wire x1="1.5" y1="0" x2="1.5" y2="-0.5" width="0.1027" layer="21"/>
<wire x1="1.5" y1="-0.5" x2="2" y2="-0.5" width="0.1027" layer="21"/>
<wire x1="2" y1="-0.5" x2="2" y2="0" width="0.1027" layer="21"/>
<wire x1="2" y1="0" x2="2.5" y2="0" width="0.1027" layer="21"/>
<wire x1="2.5" y1="0" x2="2.5" y2="-0.5" width="0.1027" layer="21"/>
<wire x1="2.5" y1="-0.5" x2="3" y2="-0.5" width="0.1027" layer="21"/>
<wire x1="3" y1="-0.5" x2="3" y2="0" width="0.1027" layer="21"/>
<wire x1="3" y1="0" x2="3.5" y2="0" width="0.1027" layer="21"/>
<wire x1="3.5" y1="0" x2="3.5" y2="-0.5" width="0.1027" layer="21"/>
<wire x1="3.5" y1="-0.5" x2="4" y2="-0.5" width="0.1027" layer="21"/>
<wire x1="4" y1="-0.5" x2="4" y2="0" width="0.1027" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="5X5">
<wire x1="-10.16" y1="5.08" x2="20.32" y2="5.08" width="0.254" layer="94"/>
<wire x1="20.32" y1="5.08" x2="20.32" y2="-5.08" width="0.254" layer="94"/>
<wire x1="20.32" y1="-5.08" x2="-10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-5.08" x2="-10.16" y2="5.08" width="0.254" layer="94"/>
<text x="-17.78" y="2.032" size="1.27" layer="95">&gt;NAME</text>
<text x="-18.288" y="-1.778" size="1.27" layer="96">&gt;VALUE</text>
<pin name="1" x="-7.62" y="-10.16" length="middle" rot="R90"/>
<pin name="2" x="-5.08" y="10.16" length="middle" rot="R270"/>
<pin name="3" x="-2.54" y="-10.16" length="middle" rot="R90"/>
<pin name="4" x="0" y="10.16" length="middle" rot="R270"/>
<pin name="5" x="2.54" y="-10.16" length="middle" rot="R90"/>
<pin name="6" x="5.08" y="10.16" length="middle" rot="R270"/>
<pin name="7" x="7.62" y="-10.16" length="middle" rot="R90"/>
<pin name="8" x="10.16" y="10.16" length="middle" rot="R270"/>
<pin name="9" x="12.7" y="-10.16" length="middle" rot="R90"/>
<pin name="10" x="15.24" y="10.16" length="middle" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="687310124422" prefix="K">
<description>&lt;b&gt;WR-FPC-0.50mm SMT ZIF Vertical Type A,pins 10 Pins&lt;/b&gt;=&gt;Code : Con_FPC_ZIF_0.50_687310124422
&lt;br&gt;&lt;a href="http://katalog.we-online.de/media/images/eican/Con_FPC_ZIF_0.50_6873xx124422_pf2.jpg" title="Enlarge picture"&gt;
&lt;img src="http://katalog.we-online.de/media/thumbs2/eican/thb_Con_FPC_ZIF_0.50_6873xx124422_pf2.jpg" width="320"&gt;&lt;/a&gt;&lt;p&gt;
Details see: &lt;a href="http://katalog.we-online.de/en/em/687_3xx_124_422"&gt;http://katalog.we-online.de/en/em/687_3xx_124_422&lt;/a&gt;&lt;p&gt;
Created 2014-07-09, Karrer Zheng&lt;br&gt;
2014 (C) Würth Elektronik</description>
<gates>
<gate name="G$1" symbol="5X5" x="-2.54" y="0"/>
</gates>
<devices>
<device name="" package="687310124422">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="LAN-Transformer">
<packages>
<package name="ALT3232M">
<wire x1="-1.6" y1="-1.6" x2="1.6" y2="-1.6" width="0.254" layer="21"/>
<wire x1="1.6" y1="-1.6" x2="1.6" y2="1.6" width="0.254" layer="21"/>
<wire x1="1.6" y1="1.6" x2="-1.6" y2="1.6" width="0.254" layer="21"/>
<wire x1="-1.6" y1="1.6" x2="-1.6" y2="-1.6" width="0.254" layer="21"/>
<smd name="5" x="1.325" y="0.7" dx="0.95" dy="0.45" layer="1"/>
<smd name="4" x="1.325" y="1.35" dx="0.95" dy="0.45" layer="1"/>
<smd name="6" x="1.325" y="-1.35" dx="0.95" dy="0.45" layer="1"/>
<smd name="2" x="-1.325" y="-0.7" dx="0.95" dy="0.45" layer="1" rot="R180"/>
<smd name="1" x="-1.325" y="-1.35" dx="0.95" dy="0.45" layer="1" rot="R180"/>
<smd name="3" x="-1.325" y="1.35" dx="0.95" dy="0.45" layer="1" rot="R180"/>
<text x="-1.8" y="2.675" size="0.8128" layer="25">&gt;Name</text>
<text x="-1.8" y="1.8" size="0.8128" layer="27">&gt;Value</text>
</package>
<package name="ACM2012-2P">
<smd name="4" x="0.93" y="0.41" dx="0.45" dy="0.75" layer="1" rot="R90"/>
<smd name="3" x="0.93" y="-0.41" dx="0.45" dy="0.75" layer="1" rot="R90"/>
<smd name="2" x="-0.93" y="-0.41" dx="0.45" dy="0.75" layer="1" rot="R270"/>
<smd name="1" x="-0.93" y="0.41" dx="0.45" dy="0.75" layer="1" rot="R270"/>
<wire x1="-1" y1="-0.6" x2="1" y2="-0.6" width="0.1524" layer="21"/>
<wire x1="1" y1="-0.6" x2="1" y2="0.6" width="0.1524" layer="21"/>
<wire x1="1" y1="0.6" x2="-1" y2="0.6" width="0.1524" layer="21"/>
<wire x1="-1" y1="0.6" x2="-1" y2="-0.6" width="0.1524" layer="21"/>
<text x="-1" y="1.675" size="0.8128" layer="25">&gt;Name</text>
<text x="-1" y="0.8" size="0.8128" layer="27">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="LAN_TRANS">
<pin name="D+" x="-10.16" y="5.08" visible="off" length="short"/>
<pin name="TX" x="10.16" y="0" visible="off" length="short" rot="R180"/>
<pin name="D-" x="-10.16" y="-5.08" visible="off" length="short"/>
<circle x="-2.455" y="4.445" radius="0.1" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-5.08" width="0.1524" layer="94" curve="180"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.1524" layer="94" curve="180"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="0" width="0.1524" layer="94" curve="180"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="2.54" width="0.1524" layer="94" curve="180"/>
<wire x1="7.62" y1="-5.08" x2="2.54" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="2.54" y1="5.08" x2="7.62" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="5.08" width="0.1524" layer="94" curve="180"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="2.54" width="0.1524" layer="94" curve="180"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="0" width="0.1524" layer="94" curve="180"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-2.54" width="0.1524" layer="94" curve="180"/>
<wire x1="3.81" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-7.62" y2="5.08" width="0.1524" layer="94"/>
<wire x1="0.3175" y1="3.4925" x2="0.3175" y2="-2.8575" width="0.1524" layer="94"/>
<wire x1="-0.3175" y1="3.4925" x2="-0.3175" y2="-2.8575" width="0.1524" layer="94"/>
<circle x="2.3375" y="4.345" radius="0.1" width="0.254" layer="94"/>
<circle x="2.6375" y="-0.6175" radius="0.1" width="0.254" layer="94"/>
<circle x="-2.525" y="-0.735" radius="0.1" width="0.254" layer="94"/>
<wire x1="3.81" y1="0" x2="7.62" y2="0" width="0.1542" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-2.54" y2="-5.08" width="0.1542" layer="94"/>
<wire x1="-2.54" y1="0" x2="-7.62" y2="0" width="0.1542" layer="94"/>
<pin name="X+" x="10.16" y="5.08" visible="off" length="short" rot="R180"/>
<pin name="X-" x="10.16" y="-5.08" visible="off" length="short" rot="R180"/>
<pin name="TD" x="-10.16" y="0" visible="off" length="short"/>
<text x="-7.366" y="5.588" size="1.27" layer="94">D+</text>
<text x="-7.366" y="0.508" size="1.27" layer="94">TD</text>
<text x="-7.366" y="-4.572" size="1.27" layer="94">D-</text>
<wire x1="-7.874" y1="7.874" x2="-7.874" y2="-6.858" width="0.254" layer="94"/>
<wire x1="-7.874" y1="-6.858" x2="7.874" y2="-6.858" width="0.254" layer="94"/>
<wire x1="7.874" y1="-6.858" x2="7.874" y2="7.874" width="0.254" layer="94"/>
<wire x1="7.874" y1="7.874" x2="-7.874" y2="7.874" width="0.254" layer="94"/>
<text x="7.366" y="5.588" size="1.27" layer="94" rot="MR0">X+</text>
<text x="7.366" y="0.508" size="1.27" layer="94" rot="MR0">TX</text>
<text x="7.366" y="-4.572" size="1.27" layer="94" rot="MR0">X-</text>
<text x="-7.62" y="10.16" size="1.27" layer="95">&gt;NAME</text>
<text x="-7.62" y="8.382" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="COM_COIL">
<pin name="1" x="-7.62" y="-5.08" visible="off" length="short"/>
<pin name="3" x="7.62" y="5.08" visible="off" length="short" rot="R180"/>
<circle x="-3.175" y="-2.455" radius="0.1" width="0.254" layer="94"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.1524" layer="94" curve="180"/>
<wire x1="-3.81" y1="2.54" x2="-1.27" y2="2.54" width="0.1524" layer="94" curve="180"/>
<wire x1="-1.27" y1="-2.54" x2="-3.81" y2="-2.54" width="0.1524" layer="94" curve="180"/>
<wire x1="1.27" y1="-2.54" x2="-1.27" y2="-2.54" width="0.1524" layer="94" curve="180"/>
<wire x1="-3.2385" y1="0.3175" x2="3.1115" y2="0.3175" width="0.1524" layer="94"/>
<wire x1="-3.2385" y1="-0.3175" x2="3.1115" y2="-0.3175" width="0.1524" layer="94"/>
<circle x="-3.075" y="2.3375" radius="0.1" width="0.254" layer="94"/>
<pin name="4" x="7.62" y="-5.08" visible="off" length="short" rot="R180"/>
<pin name="2" x="-7.62" y="5.08" visible="off" length="short"/>
<wire x1="-5.334" y1="6.35" x2="-5.334" y2="-6.35" width="0.254" layer="94"/>
<wire x1="-5.334" y1="-6.35" x2="5.334" y2="-6.35" width="0.254" layer="94"/>
<wire x1="5.334" y1="-6.35" x2="5.334" y2="6.35" width="0.254" layer="94"/>
<wire x1="5.334" y1="6.35" x2="-5.334" y2="6.35" width="0.254" layer="94"/>
<text x="-5.08" y="8.636" size="1.27" layer="95">&gt;NAME</text>
<text x="-5.08" y="6.858" size="1.27" layer="96">&gt;VALUE</text>
<wire x1="1.27" y1="2.54" x2="3.81" y2="2.54" width="0.1524" layer="94" curve="180"/>
<wire x1="3.81" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94" curve="180"/>
<wire x1="-5.08" y1="-5.08" x2="-3.81" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="-5.08" x2="-3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="3.81" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="3.81" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-3.81" y2="5.08" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="5.08" x2="-3.81" y2="2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="5.08" x2="3.81" y2="5.08" width="0.1524" layer="94"/>
<wire x1="3.81" y1="5.08" x2="3.81" y2="2.54" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ALT3232M" prefix="L">
<gates>
<gate name="G$1" symbol="LAN_TRANS" x="0" y="0"/>
</gates>
<devices>
<device name="-151-T001" package="ALT3232M">
<connects>
<connect gate="G$1" pin="D+" pad="1"/>
<connect gate="G$1" pin="D-" pad="2"/>
<connect gate="G$1" pin="TD" pad="6"/>
<connect gate="G$1" pin="TX" pad="3"/>
<connect gate="G$1" pin="X+" pad="5"/>
<connect gate="G$1" pin="X-" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOEHE" value="2,9mm"/>
<attribute name="INDUCTANCE" value="151µH"/>
<attribute name="RS" value="831-9295" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ACM2012-2P" prefix="L">
<gates>
<gate name="G$1" symbol="COM_COIL" x="2.54" y="0"/>
</gates>
<devices>
<device name="-900" package="ACM2012-2P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOEHE" value="1,3mm"/>
<attribute name="RS" value="547-4715" constant="no"/>
<attribute name="STROM" value="400mA"/>
</technology>
</technologies>
</device>
<device name="-361" package="ACM2012-2P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="HOEHE" value="1,2mm"/>
<attribute name="RS" value="605-9250"/>
<attribute name="STROM" value="220mA"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
<class number="1" name="Ethernet" width="0.1905" drill="0.125">
<clearance class="1" value="0.127"/>
</class>
<class number="4" name="SUPPLY" width="0.254" drill="0.3">
</class>
</classes>
<parts>
<part name="GND25" library="supply1" deviceset="GND" device="" value="GND"/>
<part name="X1" library="HAuto_CON_6" deviceset="RJ45-PLUG" device="" value="RJ45-PLUG"/>
<part name="SF1" library="HAutomation_6" deviceset="SCHRIFTF" device="A4"/>
<part name="X2" library="ZIF-SteckerverbinderundFlexibleFlachbandkabel" deviceset="687310124422" device=""/>
<part name="GND1" library="supply1" deviceset="GND" device="" value="GND"/>
<part name="SF2" library="HAutomation_6" deviceset="SCHRIFTF" device="A4"/>
<part name="L1" library="LAN-Transformer" deviceset="ALT3232M" device="-151-T001"/>
<part name="L2" library="LAN-Transformer" deviceset="ACM2012-2P" device="-900"/>
<part name="L3" library="LAN-Transformer" deviceset="ALT3232M" device="-151-T001"/>
<part name="L4" library="LAN-Transformer" deviceset="ACM2012-2P" device="-900"/>
<part name="L5" library="LAN-Transformer" deviceset="ALT3232M" device="-151-T001"/>
<part name="L6" library="LAN-Transformer" deviceset="ACM2012-2P" device="-900"/>
<part name="L7" library="LAN-Transformer" deviceset="ALT3232M" device="-151-T001"/>
<part name="L8" library="LAN-Transformer" deviceset="ACM2012-2P" device="-900"/>
<part name="R3" library="HAutomation_6" deviceset="R_SMD" device="0805" value="75R"/>
<part name="R4" library="HAutomation_6" deviceset="R_SMD" device="0805" value="75R"/>
<part name="R5" library="HAutomation_6" deviceset="R_SMD" device="0805" value="75R"/>
<part name="R6" library="HAutomation_6" deviceset="R_SMD" device="0805" value="75R"/>
<part name="C2" library="HAutomation_6" deviceset="C_SMD" device="1210" value="1000pF/2kV"/>
<part name="GND2" library="supply1" deviceset="GND" device="" value="GND"/>
<part name="C1" library="HAutomation_6" deviceset="C_SMD" device="0603" value="100nF"/>
<part name="GND3" library="supply1" deviceset="GND" device="" value="GND"/>
<part name="C3" library="HAutomation_6" deviceset="C_SMD" device="0603" value="100nF"/>
<part name="GND4" library="supply1" deviceset="GND" device="" value="GND"/>
<part name="C4" library="HAutomation_6" deviceset="C_SMD" device="0603" value="100nF"/>
<part name="GND5" library="supply1" deviceset="GND" device="" value="GND"/>
<part name="C5" library="HAutomation_6" deviceset="C_SMD" device="0603" value="100nF"/>
</parts>
<sheets>
<sheet>
<description>Schnittstelle Steuerplatine</description>
<plain>
<frame x1="0" y1="0" x2="299.72" y2="210.82" columns="8" rows="5" layer="94"/>
<text x="218.196" y="26.334" size="1.4224" layer="94">24.10.2019</text>
<text x="231.99" y="26.334" size="1.4224" layer="94">Seitz</text>
<text x="257.07" y="27.588" size="1.778" layer="94">RJ45 auf ZIF no PoE</text>
</plain>
<instances>
<instance part="GND25" gate="1" x="81.28" y="86.36" smashed="yes">
<attribute name="VALUE" x="78.74" y="83.82" size="1.778" layer="96"/>
</instance>
<instance part="X1" gate="G$1" x="203.2" y="119.38" smashed="yes">
<attribute name="NAME" x="208.788" y="122.682" size="1.778" layer="95"/>
<attribute name="VALUE" x="208.026" y="96.52" size="1.778" layer="96"/>
</instance>
<instance part="SF1" gate="G$2" x="229.87" y="13.97" smashed="yes">
<attribute name="LAST_DATE_TIME" x="250.35" y="5.0099" size="1.6764" layer="94"/>
<attribute name="DRAWING_NAME" x="250.35" y="8.1849" size="1.6764" layer="94"/>
<attribute name="SHEET" x="284.48" y="5.08" size="1.6764" layer="94"/>
<attribute name="LAST_DATE_TIME" x="250.35" y="5.0099" size="1.6764" layer="94"/>
<attribute name="DRAWING_NAME" x="250.35" y="8.1849" size="1.6764" layer="94"/>
<attribute name="SHEET" x="284.48" y="5.08" size="1.6764" layer="94"/>
</instance>
<instance part="X2" gate="G$1" x="93.98" y="116.84" smashed="yes" rot="R270">
<attribute name="NAME" x="96.012" y="134.62" size="1.27" layer="95" rot="R270"/>
<attribute name="VALUE" x="92.202" y="135.128" size="1.27" layer="96" rot="R270"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="LAN_IN_D_P" class="1">
<segment>
<pinref part="X2" gate="G$1" pin="3"/>
<wire x1="83.82" y1="119.38" x2="76.2" y2="119.38" width="0.1524" layer="91"/>
<label x="76.2" y="119.38" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="LAN_IN_C_P" class="1">
<segment>
<pinref part="X2" gate="G$1" pin="5"/>
<wire x1="83.82" y1="114.3" x2="76.2" y2="114.3" width="0.1524" layer="91"/>
<label x="76.2" y="114.3" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="LAN_IN_B_P" class="1">
<segment>
<pinref part="X2" gate="G$1" pin="7"/>
<wire x1="83.82" y1="109.22" x2="76.2" y2="109.22" width="0.1524" layer="91"/>
<label x="76.2" y="109.22" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="LAN_IN_A_P" class="1">
<segment>
<pinref part="X2" gate="G$1" pin="9"/>
<wire x1="83.82" y1="104.14" x2="76.2" y2="104.14" width="0.1524" layer="91"/>
<label x="76.2" y="104.14" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="LAN_IN_D_N" class="1">
<segment>
<pinref part="X2" gate="G$1" pin="2"/>
<wire x1="104.14" y1="121.92" x2="111.76" y2="121.92" width="0.1524" layer="91"/>
<label x="111.76" y="121.92" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="LAN_IN_C_N" class="1">
<segment>
<pinref part="X2" gate="G$1" pin="4"/>
<wire x1="104.14" y1="116.84" x2="111.76" y2="116.84" width="0.1524" layer="91"/>
<label x="111.76" y="116.84" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="LAN_IN_B_N" class="1">
<segment>
<pinref part="X2" gate="G$1" pin="6"/>
<wire x1="104.14" y1="111.76" x2="111.76" y2="111.76" width="0.1524" layer="91"/>
<label x="111.76" y="111.76" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="LAN_IN_A_N" class="1">
<segment>
<pinref part="X2" gate="G$1" pin="8"/>
<wire x1="104.14" y1="106.68" x2="111.76" y2="106.68" width="0.1524" layer="91"/>
<label x="111.76" y="106.68" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="GND" class="1">
<segment>
<pinref part="X2" gate="G$1" pin="10"/>
<wire x1="104.14" y1="101.6" x2="106.68" y2="101.6" width="0.1524" layer="91"/>
<pinref part="X2" gate="G$1" pin="1"/>
<wire x1="83.82" y1="124.46" x2="81.28" y2="124.46" width="0.1524" layer="91"/>
<wire x1="81.28" y1="124.46" x2="81.28" y2="91.44" width="0.1524" layer="91"/>
<wire x1="81.28" y1="91.44" x2="106.68" y2="91.44" width="0.1524" layer="91"/>
<wire x1="106.68" y1="91.44" x2="106.68" y2="101.6" width="0.1524" layer="91"/>
<pinref part="GND25" gate="1" pin="GND"/>
<wire x1="81.28" y1="88.9" x2="81.28" y2="91.44" width="0.1524" layer="91"/>
<junction x="81.28" y="91.44"/>
</segment>
</net>
<net name="LAN_OUT_A_P" class="1">
<segment>
<pinref part="X1" gate="G$1" pin="1"/>
<wire x1="203.2" y1="101.6" x2="198.12" y2="101.6" width="0.1524" layer="91"/>
<label x="198.12" y="101.6" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="LAN_OUT_A_N" class="1">
<segment>
<pinref part="X1" gate="G$1" pin="2"/>
<wire x1="203.2" y1="104.14" x2="193.04" y2="104.14" width="0.1524" layer="91"/>
<label x="193.04" y="104.14" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="LAN_OUT_B_P" class="1">
<segment>
<pinref part="X1" gate="G$1" pin="3"/>
<wire x1="203.2" y1="106.68" x2="198.12" y2="106.68" width="0.1524" layer="91"/>
<label x="198.12" y="106.68" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="LAN_OUT_C_P" class="1">
<segment>
<pinref part="X1" gate="G$1" pin="4"/>
<wire x1="203.2" y1="109.22" x2="193.04" y2="109.22" width="0.1524" layer="91"/>
<label x="193.04" y="109.22" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="LAN_OUT_C_N" class="1">
<segment>
<pinref part="X1" gate="G$1" pin="5"/>
<wire x1="203.2" y1="111.76" x2="198.12" y2="111.76" width="0.1524" layer="91"/>
<label x="198.12" y="111.76" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="LAN_OUT_B_N" class="1">
<segment>
<pinref part="X1" gate="G$1" pin="6"/>
<wire x1="203.2" y1="114.3" x2="193.04" y2="114.3" width="0.1524" layer="91"/>
<label x="193.04" y="114.3" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="LAN_OUT_D_N" class="1">
<segment>
<pinref part="X1" gate="G$1" pin="8"/>
<wire x1="203.2" y1="119.38" x2="193.04" y2="119.38" width="0.1524" layer="91"/>
<label x="193.04" y="119.38" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="LAN_OUT_D_P" class="1">
<segment>
<pinref part="X1" gate="G$1" pin="7"/>
<wire x1="203.2" y1="116.84" x2="198.12" y2="116.84" width="0.1524" layer="91"/>
<label x="198.12" y="116.84" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>Netzwerk PoE Entkopplung</description>
<plain>
<frame x1="0" y1="0" x2="299.72" y2="210.82" columns="8" rows="5" layer="94"/>
<text x="218.196" y="26.334" size="1.4224" layer="94">24.10.2019</text>
<text x="230.736" y="26.334" size="1.4224" layer="94">Seitz</text>
<text x="258.324" y="27.588" size="1.778" layer="94">RJ45 auf ZIF no PoE</text>
</plain>
<instances>
<instance part="GND1" gate="1" x="149.86" y="48.26" smashed="yes">
<attribute name="VALUE" x="147.32" y="45.72" size="1.778" layer="96"/>
</instance>
<instance part="SF2" gate="G$2" x="229.87" y="13.97" smashed="yes">
<attribute name="LAST_DATE_TIME" x="250.35" y="5.0099" size="1.6764" layer="94"/>
<attribute name="DRAWING_NAME" x="250.35" y="8.1849" size="1.6764" layer="94"/>
<attribute name="SHEET" x="284.48" y="5.08" size="1.6764" layer="94"/>
<attribute name="LAST_DATE_TIME" x="250.35" y="5.0099" size="1.6764" layer="94"/>
<attribute name="DRAWING_NAME" x="250.35" y="8.1849" size="1.6764" layer="94"/>
<attribute name="SHEET" x="284.48" y="5.08" size="1.6764" layer="94"/>
</instance>
<instance part="L1" gate="G$1" x="71.12" y="160.02" smashed="yes">
<attribute name="NAME" x="63.5" y="170.18" size="1.27" layer="95"/>
<attribute name="VALUE" x="63.5" y="168.402" size="1.27" layer="96"/>
</instance>
<instance part="L2" gate="G$1" x="93.98" y="160.02" smashed="yes">
<attribute name="NAME" x="88.9" y="168.656" size="1.27" layer="95"/>
<attribute name="VALUE" x="88.9" y="166.878" size="1.27" layer="96"/>
</instance>
<instance part="L3" gate="G$1" x="200.66" y="160.02" smashed="yes">
<attribute name="NAME" x="193.04" y="170.18" size="1.27" layer="95"/>
<attribute name="VALUE" x="193.04" y="168.402" size="1.27" layer="96"/>
</instance>
<instance part="L4" gate="G$1" x="223.52" y="160.02" smashed="yes">
<attribute name="NAME" x="218.44" y="168.656" size="1.27" layer="95"/>
<attribute name="VALUE" x="218.44" y="166.878" size="1.27" layer="96"/>
</instance>
<instance part="L5" gate="G$1" x="71.12" y="91.44" smashed="yes">
<attribute name="NAME" x="63.5" y="101.6" size="1.27" layer="95"/>
<attribute name="VALUE" x="63.5" y="99.822" size="1.27" layer="96"/>
</instance>
<instance part="L6" gate="G$1" x="93.98" y="91.44" smashed="yes">
<attribute name="NAME" x="88.9" y="100.076" size="1.27" layer="95"/>
<attribute name="VALUE" x="88.9" y="98.298" size="1.27" layer="96"/>
</instance>
<instance part="L7" gate="G$1" x="200.66" y="91.44" smashed="yes">
<attribute name="NAME" x="193.04" y="101.6" size="1.27" layer="95"/>
<attribute name="VALUE" x="193.04" y="99.822" size="1.27" layer="96"/>
</instance>
<instance part="L8" gate="G$1" x="223.52" y="91.44" smashed="yes">
<attribute name="NAME" x="218.44" y="100.076" size="1.27" layer="95"/>
<attribute name="VALUE" x="218.44" y="98.298" size="1.27" layer="96"/>
</instance>
<instance part="R3" gate="G$1" x="83.82" y="142.24" smashed="yes" rot="R90">
<attribute name="NAME" x="81.915" y="139.7" size="1.4224" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="86.995" y="139.7" size="1.4224" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R4" gate="G$1" x="213.36" y="142.24" smashed="yes" rot="R90">
<attribute name="NAME" x="211.455" y="139.7" size="1.4224" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="216.535" y="139.7" size="1.4224" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R5" gate="G$1" x="83.82" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="81.915" y="71.12" size="1.4224" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="86.995" y="71.12" size="1.4224" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="R6" gate="G$1" x="213.36" y="73.66" smashed="yes" rot="R90">
<attribute name="NAME" x="211.455" y="71.12" size="1.4224" layer="95" ratio="10" rot="R90"/>
<attribute name="VALUE" x="216.535" y="71.12" size="1.4224" layer="96" ratio="10" rot="R90"/>
</instance>
<instance part="C2" gate="G$1" x="149.86" y="55.88" smashed="yes" rot="R270">
<attribute name="NAME" x="151.511" y="58.42" size="1.4224" layer="95" ratio="10" rot="R270"/>
<attribute name="VALUE" x="146.685" y="58.42" size="1.4224" layer="96" ratio="10" rot="R270"/>
</instance>
<instance part="GND2" gate="1" x="53.34" y="68.58" smashed="yes">
<attribute name="VALUE" x="50.8" y="66.04" size="1.778" layer="96"/>
</instance>
<instance part="C1" gate="G$1" x="53.34" y="76.2" smashed="yes" rot="MR270">
<attribute name="NAME" x="51.689" y="78.74" size="1.4224" layer="95" ratio="10" rot="MR270"/>
<attribute name="VALUE" x="56.515" y="78.74" size="1.4224" layer="96" ratio="10" rot="MR270"/>
</instance>
<instance part="GND3" gate="1" x="182.88" y="68.58" smashed="yes">
<attribute name="VALUE" x="180.34" y="66.04" size="1.778" layer="96"/>
</instance>
<instance part="C3" gate="G$1" x="182.88" y="76.2" smashed="yes" rot="MR270">
<attribute name="NAME" x="181.229" y="78.74" size="1.4224" layer="95" ratio="10" rot="MR270"/>
<attribute name="VALUE" x="186.055" y="78.74" size="1.4224" layer="96" ratio="10" rot="MR270"/>
</instance>
<instance part="GND4" gate="1" x="53.34" y="137.16" smashed="yes">
<attribute name="VALUE" x="50.8" y="134.62" size="1.778" layer="96"/>
</instance>
<instance part="C4" gate="G$1" x="53.34" y="144.78" smashed="yes" rot="MR270">
<attribute name="NAME" x="51.689" y="147.32" size="1.4224" layer="95" ratio="10" rot="MR270"/>
<attribute name="VALUE" x="56.515" y="147.32" size="1.4224" layer="96" ratio="10" rot="MR270"/>
</instance>
<instance part="GND5" gate="1" x="182.88" y="137.16" smashed="yes">
<attribute name="VALUE" x="180.34" y="134.62" size="1.778" layer="96"/>
</instance>
<instance part="C5" gate="G$1" x="182.88" y="144.78" smashed="yes" rot="MR270">
<attribute name="NAME" x="181.229" y="147.32" size="1.4224" layer="95" ratio="10" rot="MR270"/>
<attribute name="VALUE" x="186.055" y="147.32" size="1.4224" layer="96" ratio="10" rot="MR270"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="RXI_N1" class="1">
<segment>
<wire x1="81.28" y1="165.1" x2="86.36" y2="165.1" width="0.1524" layer="91"/>
<pinref part="L2" gate="G$1" pin="2"/>
<pinref part="L1" gate="G$1" pin="X+"/>
</segment>
</net>
<net name="N$1" class="1">
<segment>
<wire x1="81.28" y1="160.02" x2="83.82" y2="160.02" width="0.1524" layer="91"/>
<wire x1="83.82" y1="160.02" x2="83.82" y2="147.32" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="TX"/>
<pinref part="R3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="RXI_N2" class="1">
<segment>
<wire x1="210.82" y1="165.1" x2="215.9" y2="165.1" width="0.1524" layer="91"/>
<pinref part="L4" gate="G$1" pin="2"/>
<pinref part="L3" gate="G$1" pin="X+"/>
</segment>
</net>
<net name="N$2" class="1">
<segment>
<wire x1="210.82" y1="160.02" x2="213.36" y2="160.02" width="0.1524" layer="91"/>
<wire x1="213.36" y1="160.02" x2="213.36" y2="147.32" width="0.1524" layer="91"/>
<pinref part="L3" gate="G$1" pin="TX"/>
<pinref part="R4" gate="G$1" pin="2"/>
</segment>
</net>
<net name="RXI_N3" class="1">
<segment>
<wire x1="81.28" y1="96.52" x2="86.36" y2="96.52" width="0.1524" layer="91"/>
<pinref part="L6" gate="G$1" pin="2"/>
<pinref part="L5" gate="G$1" pin="X+"/>
</segment>
</net>
<net name="N$3" class="1">
<segment>
<wire x1="81.28" y1="91.44" x2="83.82" y2="91.44" width="0.1524" layer="91"/>
<wire x1="83.82" y1="91.44" x2="83.82" y2="78.74" width="0.1524" layer="91"/>
<pinref part="L5" gate="G$1" pin="TX"/>
<pinref part="R5" gate="G$1" pin="2"/>
</segment>
</net>
<net name="RXI_N4" class="1">
<segment>
<wire x1="210.82" y1="96.52" x2="215.9" y2="96.52" width="0.1524" layer="91"/>
<pinref part="L8" gate="G$1" pin="2"/>
<pinref part="L7" gate="G$1" pin="X+"/>
</segment>
</net>
<net name="N$4" class="1">
<segment>
<wire x1="210.82" y1="91.44" x2="213.36" y2="91.44" width="0.1524" layer="91"/>
<wire x1="213.36" y1="91.44" x2="213.36" y2="78.74" width="0.1524" layer="91"/>
<pinref part="L7" gate="G$1" pin="TX"/>
<pinref part="R6" gate="G$1" pin="2"/>
</segment>
</net>
<net name="GND" class="1">
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="149.86" y1="50.8" x2="149.86" y2="53.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="53.34" y1="73.66" x2="53.34" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="182.88" y1="73.66" x2="182.88" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="53.34" y1="142.24" x2="53.34" y2="139.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="2"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="182.88" y1="142.24" x2="182.88" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="1">
<segment>
<pinref part="L1" gate="G$1" pin="X-"/>
<pinref part="L2" gate="G$1" pin="1"/>
<wire x1="81.28" y1="154.94" x2="86.36" y2="154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="L3" gate="G$1" pin="X-"/>
<pinref part="L4" gate="G$1" pin="1"/>
<wire x1="210.82" y1="154.94" x2="215.9" y2="154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="1">
<segment>
<pinref part="L8" gate="G$1" pin="1"/>
<pinref part="L7" gate="G$1" pin="X-"/>
<wire x1="215.9" y1="86.36" x2="210.82" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="1">
<segment>
<pinref part="L5" gate="G$1" pin="X-"/>
<pinref part="L6" gate="G$1" pin="1"/>
<wire x1="81.28" y1="86.36" x2="86.36" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="1">
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="83.82" y1="63.5" x2="83.82" y2="68.58" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="149.86" y1="58.42" x2="149.86" y2="63.5" width="0.1524" layer="91"/>
<wire x1="149.86" y1="63.5" x2="83.82" y2="63.5" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="213.36" y1="68.58" x2="213.36" y2="63.5" width="0.1524" layer="91"/>
<wire x1="213.36" y1="63.5" x2="149.86" y2="63.5" width="0.1524" layer="91"/>
<junction x="149.86" y="63.5"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="83.82" y1="137.16" x2="83.82" y2="132.08" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="83.82" y1="132.08" x2="149.86" y2="132.08" width="0.1524" layer="91"/>
<wire x1="149.86" y1="132.08" x2="213.36" y2="132.08" width="0.1524" layer="91"/>
<wire x1="213.36" y1="132.08" x2="213.36" y2="137.16" width="0.1524" layer="91"/>
<wire x1="149.86" y1="63.5" x2="149.86" y2="132.08" width="0.1524" layer="91"/>
<junction x="149.86" y="132.08"/>
</segment>
</net>
<net name="LAN_OUT_D_P" class="1">
<segment>
<pinref part="L8" gate="G$1" pin="3"/>
<wire x1="231.14" y1="96.52" x2="241.3" y2="96.52" width="0.1524" layer="91"/>
<label x="241.3" y="96.52" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="LAN_OUT_D_N" class="1">
<segment>
<pinref part="L8" gate="G$1" pin="4"/>
<wire x1="231.14" y1="86.36" x2="241.3" y2="86.36" width="0.1524" layer="91"/>
<label x="241.3" y="86.36" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="LAN_OUT_C_P" class="1">
<segment>
<pinref part="L6" gate="G$1" pin="3"/>
<wire x1="101.6" y1="96.52" x2="111.76" y2="96.52" width="0.1524" layer="91"/>
<label x="111.76" y="96.52" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="LAN_OUT_C_N" class="1">
<segment>
<pinref part="L6" gate="G$1" pin="4"/>
<wire x1="101.6" y1="86.36" x2="111.76" y2="86.36" width="0.1524" layer="91"/>
<label x="111.76" y="86.36" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="LAN_OUT_B_P" class="1">
<segment>
<pinref part="L4" gate="G$1" pin="3"/>
<wire x1="231.14" y1="165.1" x2="241.3" y2="165.1" width="0.1524" layer="91"/>
<label x="241.3" y="165.1" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="LAN_OUT_B_N" class="1">
<segment>
<pinref part="L4" gate="G$1" pin="4"/>
<wire x1="231.14" y1="154.94" x2="241.3" y2="154.94" width="0.1524" layer="91"/>
<label x="241.3" y="154.94" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$5" class="1">
<segment>
<pinref part="L5" gate="G$1" pin="TD"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="60.96" y1="91.44" x2="53.34" y2="91.44" width="0.1524" layer="91"/>
<wire x1="53.34" y1="91.44" x2="53.34" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="1">
<segment>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="190.5" y1="91.44" x2="182.88" y2="91.44" width="0.1524" layer="91"/>
<wire x1="182.88" y1="91.44" x2="182.88" y2="78.74" width="0.1524" layer="91"/>
<pinref part="L7" gate="G$1" pin="TD"/>
</segment>
</net>
<net name="N$7" class="1">
<segment>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="60.96" y1="160.02" x2="53.34" y2="160.02" width="0.1524" layer="91"/>
<wire x1="53.34" y1="160.02" x2="53.34" y2="147.32" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="TD"/>
</segment>
</net>
<net name="N$13" class="1">
<segment>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="190.5" y1="160.02" x2="182.88" y2="160.02" width="0.1524" layer="91"/>
<wire x1="182.88" y1="160.02" x2="182.88" y2="147.32" width="0.1524" layer="91"/>
<pinref part="L3" gate="G$1" pin="TD"/>
</segment>
</net>
<net name="LAN_IN_C_P" class="1">
<segment>
<pinref part="L5" gate="G$1" pin="D+"/>
<wire x1="60.96" y1="96.52" x2="50.8" y2="96.52" width="0.1524" layer="91"/>
<label x="50.8" y="96.52" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="LAN_IN_C_N" class="1">
<segment>
<pinref part="L5" gate="G$1" pin="D-"/>
<wire x1="60.96" y1="86.36" x2="50.8" y2="86.36" width="0.1524" layer="91"/>
<label x="50.8" y="86.36" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="LAN_IN_D_P" class="1">
<segment>
<pinref part="L7" gate="G$1" pin="D+"/>
<wire x1="190.5" y1="96.52" x2="180.34" y2="96.52" width="0.1524" layer="91"/>
<label x="180.34" y="96.52" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="LAN_IN_D_N" class="1">
<segment>
<pinref part="L7" gate="G$1" pin="D-"/>
<wire x1="190.5" y1="86.36" x2="180.34" y2="86.36" width="0.1524" layer="91"/>
<label x="180.34" y="86.36" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="LAN_IN_A_N" class="1">
<segment>
<pinref part="L1" gate="G$1" pin="D-"/>
<wire x1="60.96" y1="154.94" x2="50.8" y2="154.94" width="0.1524" layer="91"/>
<label x="50.8" y="154.94" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="LAN_IN_A_P" class="1">
<segment>
<pinref part="L1" gate="G$1" pin="D+"/>
<wire x1="60.96" y1="165.1" x2="50.8" y2="165.1" width="0.1524" layer="91"/>
<label x="50.8" y="165.1" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="LAN_IN_B_N" class="1">
<segment>
<pinref part="L3" gate="G$1" pin="D-"/>
<wire x1="190.5" y1="154.94" x2="180.34" y2="154.94" width="0.1524" layer="91"/>
<label x="180.34" y="154.94" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="LAN_IN_B_P" class="1">
<segment>
<pinref part="L3" gate="G$1" pin="D+"/>
<wire x1="190.5" y1="165.1" x2="180.34" y2="165.1" width="0.1524" layer="91"/>
<label x="180.34" y="165.1" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="LAN_OUT_A_P" class="1">
<segment>
<pinref part="L2" gate="G$1" pin="3"/>
<wire x1="101.6" y1="165.1" x2="111.76" y2="165.1" width="0.1524" layer="91"/>
<label x="111.76" y="165.1" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="LAN_OUT_A_N" class="1">
<segment>
<pinref part="L2" gate="G$1" pin="4"/>
<wire x1="101.6" y1="154.94" x2="111.76" y2="154.94" width="0.1524" layer="91"/>
<label x="111.76" y="154.94" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="113,1,235.903,17.78,SF1,,,,,"/>
<approved hash="113,2,235.903,17.78,SF2,,,,,"/>
</errors>
</schematic>
</drawing>
</eagle>
